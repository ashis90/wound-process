<?php
if (!defined('BASEPATH'))
    EXIT("No direct script access allowed");
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization');
header('Content-Type:application/json');
class Frontapi extends MY_Controller {

    function __construct() {
        parent::__construct();
				date_default_timezone_set('MST7MDT');
    }
    function index()
    {
    }
    function home_page_details()
    {
	    $conditions = " ( `site_page_id` = '1' AND `site_page_status` = 'Active')";
	    $select_fields = '*';
	    $is_multy_result = 1;
		$page = $this->BlankModel->getTableData('sitepages', $conditions, $select_fields, $is_multy_result);
		
		if($page)
		{
			echo json_encode($page);
		}
		else
		{
			echo json_encode(array('status'=>'0'));
		}
    }
    function about_page_details()
    {
	    $conditions = " ( `site_page_id` = '2' AND `site_page_status` = 'Active')";
	    $select_fields = '*';
	    $is_multy_result = 1;
		$page = $this->BlankModel->getTableData('sitepages', $conditions, $select_fields, $is_multy_result);
		
		if($page)
		{
			echo json_encode($page);
		}
		else
		{
			echo json_encode(array('status'=>'0'));
		}
    }
    function contact_page_details()
    {
	    $conditions = " ( `site_page_id` = '3' AND `site_page_status` = 'Active')";
	    $select_fields = '*';
	    $is_multy_result = 1;
		$page = $this->BlankModel->getTableData('sitepages', $conditions, $select_fields, $is_multy_result);
		
		if($page)
		{
			echo json_encode($page);
		}
		else
		{
			echo json_encode(array('status'=>'0'));
		}
    }   
    /**
	* 
	* Wound Care
	*/
    function wound_care_page_details()
    {
	    $conditions = " ( `site_page_id` = '7' AND `site_page_status` = 'Active' AND `site_page_type` = 'ABLITY')";
	    $select_fields = '*';
	    $is_multy_result = 1;
		$page = $this->BlankModel->getTableData('sitepages', $conditions, $select_fields, $is_multy_result);
		
		$site_page_desc = html_entity_decode($page['site_page_desc']);
		
		if($page)
		{
			echo json_encode( array('site_page_desc' => $site_page_desc,'site_page_title'=>$page['site_page_title']));
		}
		else
		{
			echo json_encode(array('status'=>'0'));
		}
		}

		function science_based_service()
    {
	    $conditions = " ( `site_page_id` = '9' AND `site_page_status` = 'Active' AND `site_page_type` = 'ABLITY')";
	    $select_fields = '*';
	    $is_multy_result = 1;
		$page = $this->BlankModel->getTableData('sitepages', $conditions, $select_fields, $is_multy_result);
		if($page)
		{
			echo json_encode($page);
		}
		else
		{
			echo json_encode(array('status'=>'0'));
		}
    }
    function privacy_page_details()
    {
	    $conditions = " ( `site_page_id` = '4' AND `site_page_status` = 'Active')";
	    $select_fields = '*';
	    $is_multy_result = 1;
		$page = $this->BlankModel->getTableData('sitepages', $conditions, $select_fields, $is_multy_result);
		
		if($page)
		{
			echo json_encode($page);
		}
		else
		{
			echo json_encode(array('status'=>'0'));
		}
    }
    function service_page_details()
    {
	    $conditions = " (`service_status` = 'Active' AND service_type = '')";
	    $select_fields = '*';
	    $is_multy_result = 0;
		$page = $this->BlankModel->getTableData('services', $conditions, $select_fields, $is_multy_result, 'service_id', 'ASC');
		
		if($page)
		{
			echo json_encode($page);
		}
		else
		{
			echo json_encode(array('status'=>'0'));
		}
    }
    function service_details()
    {
    	$id=json_decode(file_get_contents('php://input'), true);
    	$conditions = " ( `service_id` = '".$id."')";
    	$select_fields = '*';
	    $is_multy_result = 0;
		$page = $this->BlankModel->getTableData('services', $conditions, $select_fields, $is_multy_result);
		if($page)
		{
			echo json_encode($page);
		}
		else
		{
			echo json_encode(array('status'=>'0'));
		}

		}
	function service_page_details_for_nails()
    {
	    $conditions = " (`service_status` = 'Active' AND service_type = 'Nail')";
	    $select_fields = '*';
	    $is_multy_result = 0;
		$page = $this->BlankModel->getTableData('services', $conditions, $select_fields, $is_multy_result, 'service_id', 'ASC');
		
		if($page)
		{
			echo json_encode($page);
		}
		else
		{
			echo json_encode(array('status'=>'0'));
		}
    }
    
		
		function send_contact_mail()
		{
			$data=json_decode(file_get_contents('php://input'), true);
			$message_body ="<div class='cont_wrapperInner style='border:4px solid #34b6e6;width:670px;padding:10px;text-align:justify;'>
			<p style='text-align: center'><img src='".base_url()."assets/uploads/thumb/logo.png' alt='logo' align='middle'></p>
			<h1 style='color:#33B6E6' align='center'>Contact Us</h1>
			<table width='100%' border='0' cellpadding='2' cellspacing='2'>
			  <tr>
				<td colspan='2'>Hello Administrator,<br>
				  <br>
				  A new user has provided a request today. Please check the details below:<br>
				  <br></td>
			  </tr>
			  <tr>
				<td align='left' valign='top'>Name:</td>
				<td align='left' valign='top'>".$data['name']."</td>
			  </tr>
			  <tr>
				<td align='left' valign='top'>Email:</td>
				<td align='left' valign='top'>".$data['email']."</td>
			  </tr>
			  <tr>
				<td align='left' valign='top'>Subject:</td>
				<td align='left' valign='top'>".$data['subject']."</td>
			  </tr>
			  <tr>
				<td align='left' valign='top'>Message:</td>
				<td align='left' valign='top'>".$data['message']."</td>
			  </tr>
			</table>
			<br>
			<br>
			Thank you,<br>
			Ability Diagnostics Team. </div>";            
			$message_body.= "";
			$subject="Contact Request from ".$data['name']."";
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$headers .= "From:Ability Diagnostics <mails@abilitydiagnostics.com>\r\n";  
			$to_email = "brooks.ability@gmail.com, support@abilitydiagnostics.com";
			//$to_email = $data['email'];
			//$to_email = "riadey@sleekinfosolutions.com";
			@mail($to_email, $subject, $message_body, $headers);

			$insertData['contact_name'] = $data['name'];
			$insertData['contact_email'] = $data['email'];
			$insertData['contact_subject'] = $data['subject'];
			$insertData['contact_msg'] = $data['message'];
			$this->db->insert('wp_abd_contact',$insertData);

			echo json_encode(array('status'=>'1'));
		}

		
		function get_google_analytics(){
			$this->db->where('type','Ability');
			$analytics_data = $this->db->get('wp_abd_google_analytics')->row_array();

			if(!empty($analytics_data)){
				die(json_encode(array('status'=>'1', 'analytics_data'=>$analytics_data)));
			}else{
				die(json_encode(array('status'=>'0')));
			}
		}
    
    

}

?>