<?php
if (!defined('BASEPATH'))
    EXIT("No direct script access allowed");
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
	header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization');
	header('Content-Type:application/json');

class NewSpecimens extends MY_Controller {

    function __construct() {
        parent::__construct();
       $this->load->library('phpqrcode/qrlib');
       $this->load->library('m_pdf');
    }
  
    function index()
    {
	  
    }
    
    /**
	* /
	* 
	* @physicians_list
	*/
    
    function physicians_list()
     {
    	$physicians_data = array();
    	$physicians_det = array();
	    $physician_details = get_users_by_role_n_status($role = 'physician',$select_data = 'ID', $status = 'Active');
	 
	 if($physician_details){	 	
	
	    foreach($physician_details as $key => $physician){
	  
	   	 $physician_first_name = get_user_meta( $physician['ID'], 'first_name', '' );
		 $physician_last_name  = get_user_meta( $physician['ID'], 'last_name' , '' );
	   	 $physicians_det['physician_name'] = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
	     $physicians_det['id'] = $physician['ID'];
	   
	   	 array_push($physicians_data, $physicians_det);
	     }
	     die( json_encode(array( "physicians_data" => $physicians_data )));
		 }
		 else{
		 	 die( json_encode(array( "status" => "0", "message" => "No Physician Found." )));
		 }		
	}
	   
   /**
	* /
	* Specimens List Start here
	*/
    function specimens_list()
    {
    	$specimen_details = array();
    	$physicians_data = array();
    	$physicians_det = array();
	 
	    $physician_sql = "SELECT `id`, `physician_id` FROM `wp_abd_specimen` WHERE `status` = '0' AND `physician_accepct` = '0' AND `create_date` > '2017-03-27 23:59:59' GROUP BY `physician_id`";
	 
	    $physician_ids = $this->BlankModel->customquery($physician_sql);
	  
	    foreach($physician_ids as $key => $physician){
	  
	   	 $physician_first_name = get_user_meta( $physician['physician_id'], 'first_name', '' );
		 $physician_last_name  = get_user_meta( $physician['physician_id'], 'last_name' , '' );
	   	 $physicians_det['physician_name'] = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
	     $physicians_det['id'] = $physician['physician_id'];
	   
	   	 array_push($physicians_data, $physicians_det);
	   	}
	  
	    $conditions = "( status = '0' AND `physician_accepct` = '0' AND `create_date` > '2017-03-27 23:59:59' AND `modify_date` >= DATE(NOW()) - INTERVAL 30 DAY )";
	    $select_fields = '`id`, `assessioning_num`, `physician_id` , `p_lastname`, `p_firstname`, `collection_date`, `qc_check`';
	    $is_multy_result = 0;
			
		$specimens_details = $this->BlankModel->getTableData('specimen', $conditions, $select_fields, $is_multy_result);
		
		if($specimens_details)
		{
		
	    $num_rows_count = 0;
		foreach($specimens_details as $specimen){
		$physician_first_name = get_user_meta( $specimen['physician_id'], 'first_name' );
		$physician_last_name  = get_user_meta( $specimen['physician_id'], 'last_name');
		
		$physician_name = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
		
		$specimen_information =  array( 'id' => $specimen['id'], 'assessioning_num' => $specimen['assessioning_num'], 'physician_id' => $specimen['physician_id'], 'p_lastname' => $specimen['p_lastname'], 'p_firstname' => $specimen['p_firstname'], 'qc_check' => $specimen['qc_check'], 'collection_date' => $specimen['collection_date'], 'physician_name' => $physician_name);
		
		  $num_rows_count++;	
	      array_push($specimen_details, $specimen_information);
        }
		
		die( json_encode(array('specimen_count' => $num_rows_count, "specimens_detail" => $specimen_details , "physicians_data" => $physicians_data )));
		
		}
		else
		{
			die( json_encode(array('status'=>'0')));
		}
	}
	
	function search_specimen()
	{	
		$search = "";
		$data = json_decode(file_get_contents('php://input'), true);
		if($data['physician']!=""){
			$search=" AND `physician_id`='".$data['physician']."'";	
		 }
		 if($data['date']!=""){
			$collection_date =$data['date'];
			$search.=" AND (`collection_date` LIKE '%".$collection_date."%')";	
		 }
		 if($data['fname']!=""){
			 $search.=" AND `p_firstname` LIKE '%".$data['fname']."%'";	
		 }
		 if($data['lname']!=""){
			 $search.=" AND `p_lastname` LIKE '%".$data['lname']."%'";	
		 }
		 if($data['barcode']!=""){
			$search.=" AND (`barcode_number` LIKE '%".$data['barcode']."%' OR `assessioning_num` LIKE '%".$data['barcode']."%')";
			
	 
		 }
		 if($data['assessioning_num']!=""){
			$search.=" AND `assessioning_num` ='".$data['assessioning_num']."'";	
		 }
		 $specimen_details = array();
		 $physicians_data = array();
		 $physicians_det = array();
	  
		 $physician_sql = "SELECT `id`, `physician_id` FROM `wp_abd_specimen` WHERE `status` = '0' AND `physician_accepct` = '0' AND `create_date` > '2017-03-27 23:59:59' GROUP BY `physician_id`";
	  
		 $physician_ids = $this->BlankModel->customquery($physician_sql);
	   
		 foreach($physician_ids as $key => $physician){
	   
			$physician_first_name = get_user_meta( $physician['physician_id'], 'first_name', '' );
			$physician_last_name  = get_user_meta( $physician['physician_id'], 'last_name' , '' );
			$physicians_det['physician_name'] = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
		 	$physicians_det['id'] = $physician['physician_id'];
		
			 array_push($physicians_data, $physicians_det);
			}
	   
		 $conditions = "( status = '0' AND `physician_accepct` = '0' AND `create_date` > '2017-03-27 23:59:59'".$search." )";
		 $select_fields = '`id`, `assessioning_num`, `physician_id` , `p_lastname`, `p_firstname`, `collection_date`, `qc_check`';
		 $is_multy_result = 0;
			 
		 $specimens_details = $this->BlankModel->getTableData('specimen', $conditions, $select_fields, $is_multy_result);
		 
		 if($specimens_details)
		 {
		 
		 $num_rows_count = 0;
		 foreach($specimens_details as $specimen){
			$physician_first_name = get_user_meta( $specimen['physician_id'], 'first_name' );
			$physician_last_name  = get_user_meta( $specimen['physician_id'], 'last_name');		 
			$physician_name = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];		 
			$specimen_information =  array( 'id' => $specimen['id'], 'assessioning_num' => $specimen['assessioning_num'], 'physician_id' => $specimen['physician_id'], 'p_lastname' => $specimen['p_lastname'], 'p_firstname' => $specimen['p_firstname'], 'qc_check' => $specimen['qc_check'], 'collection_date' => $specimen['collection_date'], 'physician_name' => $physician_name);

			$num_rows_count++;	
		   array_push($specimen_details, $specimen_information);
		 }
		 
		 die( json_encode(array('specimen_count' => $num_rows_count, "specimens_detail" => $specimen_details , "physicians_data" => $physicians_data )));
		 
		 }
		 else
		 {
			 die( json_encode(array('status'=>'0')));
		 }
	}

	function load_all_specimens()
    {
    	$specimen_details = array();
    	$physicians_data = array();
    	$physicians_det = array();
	 
	    $physician_sql = "SELECT `id`, `physician_id` FROM `wp_abd_specimen` WHERE `status` = '0' AND `physician_accepct` = '0' AND `create_date` > '2017-03-27 23:59:59' GROUP BY `physician_id`";
	 
	    $physician_ids = $this->BlankModel->customquery($physician_sql);
	  
	    foreach($physician_ids as $key => $physician){
	  
	   	 $physician_first_name = get_user_meta( $physician['physician_id'], 'first_name', '' );
		 $physician_last_name  = get_user_meta( $physician['physician_id'], 'last_name' , '' );
	   	 $physicians_det['physician_name'] = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
	     $physicians_det['id'] = $physician['physician_id'];
	   
	   	 array_push($physicians_data, $physicians_det);
	   	}
	  
	    $conditions = "( status = '0' AND `physician_accepct` = '0' AND `create_date` > '2017-03-27 23:59:59' )";
	    $select_fields = '`id`, `assessioning_num`, `physician_id` , `p_lastname`, `p_firstname`, `collection_date`, `qc_check`';
	    $is_multy_result = 0;
			
		$specimens_details = $this->BlankModel->getTableData('specimen', $conditions, $select_fields, $is_multy_result);
		
		if($specimens_details)
		{
		
	    $num_rows_count = 0;
		foreach($specimens_details as $specimen){
		$physician_first_name = get_user_meta( $specimen['physician_id'], 'first_name' );
		$physician_last_name  = get_user_meta( $specimen['physician_id'], 'last_name');
		
		$physician_name = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
		
		$specimen_information =  array( 'id' => $specimen['id'], 'assessioning_num' => $specimen['assessioning_num'], 'physician_id' => $specimen['physician_id'], 'p_lastname' => $specimen['p_lastname'], 'p_firstname' => $specimen['p_firstname'], 'qc_check' => $specimen['qc_check'], 'collection_date' => $specimen['collection_date'], 'physician_name' => $physician_name);
		
		  $num_rows_count++;	
	      array_push($specimen_details, $specimen_information);
        }
		
		die( json_encode(array('specimen_count' => $num_rows_count, "specimens_detail" => $specimen_details , "physicians_data" => $physicians_data )));
		
		}
		else
		{
			die( json_encode(array('status'=>'0')));
		}
	}
	
    
    /**
	* Specimens Suspended
	*/
	function suspend_specimens_list()
	{
		$specimen_details = array();
		$physicians_data = array();
		$physicians_det = array();
		
		$conditions = " status = '1' AND (physician_accepct ='1' OR physician_accepct ='0') ORDER BY `modify_date` DESC";
		$select_fields = '`id`,`patient_address`,`physician_accepct`,`assessioning_num`,`physician_id` ,`p_lastname`,`p_firstname`, `collection_date`, `qc_check`';
	    $is_multy_result = 0;
			
		$specimens_details = $this->BlankModel->getTableData('specimen', $conditions, $select_fields, $is_multy_result);
		$count = count($specimens_details);

	    $physician_sql = "SELECT `id`, `physician_id` FROM `wp_abd_specimen` WHERE `status` = '0' AND `physician_accepct` = '0' AND `create_date` > '2017-03-27 23:59:59' GROUP BY `physician_id`";
	 
		$physician_ids = $this->BlankModel->customquery($physician_sql);
		
		foreach($physician_ids as $key => $physician){
	  
	   	 $physician_first_name = get_user_meta( $physician['physician_id'], 'first_name', '' );
		 $physician_last_name  = get_user_meta( $physician['physician_id'], 'last_name' , '' );
	   	 $physicians_det['physician_name'] = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
	     $physicians_det['id'] = $physician['physician_id'];
	   
	   	 array_push($physicians_data, $physicians_det);
		   }
		
		if($specimens_details)
		{
			foreach($specimens_details as $specimen){
				$physician_first_name = get_user_meta( $specimen['physician_id'], 'first_name' );
				$physician_last_name  = get_user_meta( $specimen['physician_id'], 'last_name');
				
				$physician_name = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
				
				$specimen_information =   array( 'id' => $specimen['id'], 'assessioning_num' => $specimen['assessioning_num'], 'physician_id' => $specimen['physician_id'], 'p_lastname' => $specimen['p_lastname'], 'p_firstname' => $specimen['p_firstname'], 'qc_check' => $specimen['qc_check'], 'collection_date' => $specimen['collection_date'], 'physician_name' => $physician_name, 'patient_address' => $specimen['patient_address'], 'physician_accepct' => $specimen['physician_accepct']);
	 
				array_push($specimen_details, $specimen_information);
			}
		}
	  
		if($specimens_details)
		{
			echo json_encode(array('status'=>'1', 'specimens_detail'=> $specimen_details, 'count'=>$count, "physicians_data" => $physicians_data));
		}
		else
		{
			echo json_encode(array('status'=>'0'));
		}
	}
	
	function search_sus()
	{	
		$search = "";
		$data = json_decode(file_get_contents('php://input'), true);
		if($data['physician']!=""){
			$search=" AND `physician_id`='".$data['physician']."'";	
		 }
		 if($data['date']!=""){
			$collection_date =$data['date'];
			$search.=" AND (`collection_date` LIKE '%".$collection_date."%')";	
		 }
		 if($data['fname']!=""){
			 $search.=" AND `p_firstname` LIKE '%".$data['fname']."%'";	
		 }
		 if($data['lname']!=""){
			 $search.=" AND `p_lastname` LIKE '%".$data['lname']."%'";	
		 }
		 if($data['barcode']!=""){
			$search.=" AND (`barcode_number` LIKE '%".$data['barcode']."%' OR `assessioning_num` LIKE '%".$data['barcode']."%')";
			
	 
		 }
		 if($data['assessioning_num']!=""){
			$search.=" AND `assessioning_num` ='".$data['assessioning_num']."'";	
		 }
		$specimen_details = array();
		$physicians_data = array();
		$physicians_det = array();
		
		$conditions = " status = '1' AND (physician_accepct ='1' OR physician_accepct ='0')".$search." ORDER BY `modify_date` DESC";
		$select_fields = '`id`,`patient_address`,`physician_accepct`,`assessioning_num`,`physician_id` ,`p_lastname`,`p_firstname`, `collection_date`, `qc_check`';
	    $is_multy_result = 0;
			
		$specimens_details = $this->BlankModel->getTableData('specimen', $conditions, $select_fields, $is_multy_result);
		$count = count($specimens_details);

	    $physician_sql = "SELECT `id`, `physician_id` FROM `wp_abd_specimen` WHERE `status` = '0' AND `physician_accepct` = '0' AND `create_date` > '2017-03-27 23:59:59' GROUP BY `physician_id`";
	 
		$physician_ids = $this->BlankModel->customquery($physician_sql);
		
		foreach($physician_ids as $key => $physician){
	  
	   	 $physician_first_name = get_user_meta( $physician['physician_id'], 'first_name', '' );
		 $physician_last_name  = get_user_meta( $physician['physician_id'], 'last_name' , '' );
	   	 $physicians_det['physician_name'] = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
	     $physicians_det['id'] = $physician['physician_id'];
	   
	   	 array_push($physicians_data, $physicians_det);
		   }
		
		if($specimens_details)
		{
			foreach($specimens_details as $specimen){
				$physician_first_name = get_user_meta( $specimen['physician_id'], 'first_name' );
				$physician_last_name  = get_user_meta( $specimen['physician_id'], 'last_name');
				
				$physician_name = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
				
				$specimen_information =   array( 'id' => $specimen['id'], 'assessioning_num' => $specimen['assessioning_num'], 'physician_id' => $specimen['physician_id'], 'p_lastname' => $specimen['p_lastname'], 'p_firstname' => $specimen['p_firstname'], 'qc_check' => $specimen['qc_check'], 'collection_date' => $specimen['collection_date'], 'physician_name' => $physician_name, 'patient_address' => $specimen['patient_address'], 'physician_accepct' => $specimen['physician_accepct']);
	 
				array_push($specimen_details, $specimen_information);
			}
		}
	  
		if($specimens_details)
		{
			echo json_encode(array('status'=>'1', 'specimens_detail'=> $specimen_details, 'count'=>$count, "physicians_data" => $physicians_data));
		}
		else
		{
			echo json_encode(array('status'=>'0'));
		}

	}

	/**
	* Specimens QC check
	*/
	function specimens_qclist()
    {
    	$specimen_details = array();
    	$physicians_data = array();
    	$physicians_det = array();
	 
	    $physician_sql = "SELECT `id`, `physician_id` FROM `wp_abd_specimen` WHERE `status` = '0' AND `physician_accepct` = '0' AND `create_date` > '2017-03-27 23:59:59' GROUP BY `physician_id`";
	 
	    $physician_ids = $this->BlankModel->customquery($physician_sql);
	  
	    foreach($physician_ids as $key => $physician){
	  
	   	 $physician_first_name = get_user_meta( $physician['physician_id'], 'first_name', '' );
		 $physician_last_name  = get_user_meta( $physician['physician_id'], 'last_name' , '' );
	   	 $physicians_det['physician_name'] = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
	     $physicians_det['id'] = $physician['physician_id'];
	   
	   	 array_push($physicians_data, $physicians_det);
	   	}
	  
	    $conditions = "( status = '0' AND physician_accepct ='0' AND qc_check = '1' AND `create_date` > '2017-03-27 23:59:59' )";
	    $select_fields = '`id`,`lab_id`, `assessioning_num`, `physician_id` , `p_lastname`, `p_firstname`, `collection_date`, `qc_check`, `patient_address`';
	    $is_multy_result = 0;
			
		$specimens_details = $this->BlankModel->getTableData('specimen', $conditions, $select_fields, $is_multy_result);
		
		if($specimens_details)
		{
		
	    $num_rows_count = 0;
		foreach($specimens_details as $specimen){
		$physician_first_name = get_user_meta( $specimen['physician_id'], 'first_name' );
		$physician_last_name  = get_user_meta( $specimen['physician_id'], 'last_name');
		
		$physician_name = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];

		$labtech_first_name = get_user_meta( $specimen['lab_id'], 'first_name' );
		$labtech_last_name  = get_user_meta( $specimen['lab_id'], 'last_name');
		
		$labtech_name = $labtech_first_name['meta_value'].' '.$labtech_last_name['meta_value'];
		
		$specimen_information =  array( 'id' => $specimen['id'], 'assessioning_num' => $specimen['assessioning_num'], 'physician_id' => $specimen['physician_id'], 'p_lastname' => $specimen['p_lastname'], 'p_firstname' => $specimen['p_firstname'], 'qc_check' => $specimen['qc_check'], 'patient_address' => $specimen['patient_address'] ,'collection_date' => $specimen['collection_date'], 'physician_name' => $physician_name, 'labtech_name' => $labtech_name);
		
		  $num_rows_count++;	
	      array_push($specimen_details, $specimen_information);
        }
		
		die( json_encode(array('specimen_count' => $num_rows_count, "specimens_detail" => $specimen_details , "physicians_data" => $physicians_data )));
		
		}
		else
		{
			die( json_encode(array('status'=>'0')));
		}
	}
	
	function search_qcspecimen()
	{	
		$search = "";
		$data = json_decode(file_get_contents('php://input'), true);
		if($data['physician']!=""){
			$search=" AND `physician_id`='".$data['physician']."'";	
		 }
		 if($data['date']!=""){
			$collection_date =$data['date'];
			// $month_day = substr($collection_date , '0','6');
			// $year = substr($collection_date , '6','4');
			//$trimed_date = end(explode('20', $year)); 
			$search.=" AND (`collection_date` LIKE '%".$collection_date."%')";	
		 }
		 if($data['fname']!=""){
			 $search.=" AND `p_firstname` LIKE '%".$data['fname']."%'";	
		 }
		 if($data['lname']!=""){
			 $search.=" AND `p_lastname` LIKE '%".$data['lname']."%'";	
		 }
		 if($data['barcode']!=""){
			$search.=" AND (`barcode_number` LIKE '%".$data['barcode']."%' OR `assessioning_num` LIKE '%".$data['barcode']."%')";
			
	 
		 }
		 if($data['assessioning_num']!=""){
			$search.=" AND `assessioning_num` ='".$data['assessioning_num']."'";	
		 }
		 $specimen_details = array();
		 $physicians_data = array();
		 $physicians_det = array();
	  
		 $physician_sql = "SELECT `id`, `physician_id` FROM `wp_abd_specimen` WHERE `status` = '0' AND `physician_accepct` = '0' AND `create_date` > '2017-03-27 23:59:59' GROUP BY `physician_id`";
	  
		 $physician_ids = $this->BlankModel->customquery($physician_sql);
	   
		 foreach($physician_ids as $key => $physician){
	   
			 $physician_first_name = get_user_meta( $physician['physician_id'], 'first_name', '' );
		  $physician_last_name  = get_user_meta( $physician['physician_id'], 'last_name' , '' );
			 $physicians_det['physician_name'] = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
		  $physicians_det['id'] = $physician['physician_id'];
		
			 array_push($physicians_data, $physicians_det);
			}
	   
		 $conditions = "( status = '0' AND physician_accepct ='0' AND qc_check ='1'".$search." )";
		 $select_fields = '`id`,`lab_id`, `assessioning_num`, `physician_id` , `p_lastname`, `p_firstname`, `collection_date`, `qc_check`,`patient_address`';
		 $is_multy_result = 0;
			 
		 $specimens_details = $this->BlankModel->getTableData('specimen', $conditions, $select_fields, $is_multy_result);
		 
		 if($specimens_details)
		 {
		 
		 $num_rows_count = 0;
		 foreach($specimens_details as $specimen){
		 $physician_first_name = get_user_meta( $specimen['physician_id'], 'first_name' );
		 $physician_last_name  = get_user_meta( $specimen['physician_id'], 'last_name');
		 
		 $physician_name = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
		 
		 $specimen_information =  array( 'id' => $specimen['id'], 'assessioning_num' => $specimen['assessioning_num'], 'physician_id' => $specimen['physician_id'], 'p_lastname' => $specimen['p_lastname'], 'p_firstname' => $specimen['p_firstname'], 'qc_check' => $specimen['qc_check'],'patient_address' => $specimen['patient_address'], 'collection_date' => $specimen['collection_date'], 'physician_name' => $physician_name);
		 
		   $num_rows_count++;	
		   array_push($specimen_details, $specimen_information);
		 }
		 
		 die( json_encode(array('specimen_count' => $num_rows_count, "specimens_detail" => $specimen_details , "physicians_data" => $physicians_data )));
		 
		 }
		 else
		 {
			 die( json_encode(array('status'=>'0')));
		 }
	}
	
	/**
	* Add Specimens 
	*/
	function add_specimen()
	 {
	 	$qrCode = array();
	 	$assessioning = array();
		$apiData = json_decode(file_get_contents('php://input'), true);
		//pr($apiData);
		$field_data = array('collection_date','date_received','physician_id','p_lastname','p_firstname','patient_address','patient_city','patient_state','patient_zip','patient_dob','patient_age','patient_sex', 'pri_insurance_name','pri_address','pri_city','pri_state','pri_zip', 'pri_member_id','pri_subscriber_dob','pri_group_contact','pri_sex');
		$specialChars = array(" ", "\r", "\n");
		$replaceChars = array("", "", "");
		$error = false;
		$dataCount = count($field_data);
		

		for($i=0;$i<$dataCount;$i++){
			if(empty($apiData[$field_data[$i]])){
				$error = true;
			}
		}
		if($error){
			$status = '1';
			$redirect_url = "/suspended-list";
		}
		elseif($apiData['physician_accepct'] == '0' && !$error){
			$status = '0';
			$redirect_url = "/specimens-list";
		}
		elseif(!$error || $apiData['physician_accepct'] == '1'){
			$status = '1';
			$redirect_url = "/suspended-list";
		}
		else{
			$status = '0';
			$redirect_url = "/specimens-list";
		} 

		$bill = ""; $pri_subscribers = ""; $sec_subscribers = "";$insurance_type = "";
		if(count(array_filter($apiData['bill'])) != 0){
			$bill = implode(',',$apiData['bill']);
		}
		
		if(count(array_filter($apiData['pri_subscriber_relation'])) != 0){
			$pri_subscribers =  implode(',',$apiData['pri_subscriber_relation']);
		}
		 
		if(count(array_filter($apiData['sec_subscriber_relation'])) != 0){
			$sec_subscribers = implode(',',$apiData['sec_subscriber_relation']);
		  }
		
		if(count(array_filter($apiData['insurance_type'])) != 0){
			$insurance_type = implode(',',$apiData['insurance_type']);
		}

		$insertData['lab_id'] = '100';
		$insertData['physician_id'] = $apiData['physician_id'];
		$insertData['patient_id'] = $apiData['patient_id'];
		$insertData['collection_date'] = $apiData['collection_date'];
		$insertData['collection_time'] = str_replace($specialChars, $replaceChars, $apiData['time'])." ".$apiData['format'];
		$insertData['date_received'] = $apiData['date_received']; 
		$insertData['bill'] = $bill;
		$insertData['p_lastname'] = stripslashes($apiData['p_lastname']);
		$insertData['p_firstname'] = $apiData['p_firstname']; 
		$insertData['patient_city'] = $apiData['patient_city']; 
		$insertData['patient_address'] = $apiData['patient_address']; 
		$insertData['apt'] = $apiData['apt'];	
		$insertData['patient_phn'] = $apiData['patient_phn'];
		$insertData['patient_state'] = $apiData['patient_state'];
		$insertData['patient_zip'] = $apiData['patient_zip']; 
		$insertData['patient_dob'] = $apiData['patient_dob'];
		$insertData['patient_age'] = $apiData['patient_age'];	
		$insertData['patient_sex'] = $apiData['patient_sex']; 
		$insertData['pri_address'] = $apiData['pri_address']; 
		$insertData['pri_city'] = $apiData['pri_city'];
		$insertData['pri_state'] = $apiData['pri_state'];
		$insertData['pri_zip'] = $apiData['pri_zip']; 
		$insertData['pri_employee_name'] = $apiData['pri_employee_name'];	
		$insertData['pri_member_id'] = $apiData['pri_member_id'];
		$insertData['pri_subscriber_dob'] = $apiData['pri_subscriber_dob'];	
		$insertData['pri_group_contact'] = $apiData['pri_group_contact']; 
		$insertData['pri_sex'] = $apiData['pri_sex'];
		$insertData['pri_medicare_id'] = $apiData['pri_medicare_id']; 
		$insertData['pri_madicaid_id'] = $apiData['pri_madicaid_id'];
		$insertData['sec_insurance_name'] = $apiData['sec_insurance_name']; 
		$insertData['sec_address'] = $apiData['sec_address'];
		$insertData['sec_city'] =$apiData['sec_city'];	
		$insertData['sec_state'] = $apiData['sec_state'];
		$insertData['sec_zip'] = $apiData['sec_zip'];
		$insertData['sec_employee_name'] = $apiData['sec_employee_name'];
		$insertData['sec_member_id'] = $apiData['sec_member_id']; 
		$insertData['sec_subscriber_dob'] = $apiData['sec_subscriber_dob']; 
		$insertData['sec_group_contact'] = $apiData['sec_group_contact']; 
		$insertData['sec_sex'] = $apiData['sec_sex'];
		$insertData['sec_medicare_id'] = $apiData['sec_medicare_id'];
		$insertData['sec_medicaid_id'] = $apiData['sec_medicaid_id'];
		$insertData['physician_accepct'] =$apiData['physician_accepct'];
		$insertData['status'] =$status;
		$insertData['modify_date'] = date('Y-m-d H:i:s');
		$insertData['create_date'] = date('Y-m-d H:i:s');
		$insertData['pri_insurance_name']= $apiData['pri_insurance_name'];
		$insertData['ins_type'] = $insurance_type; 
		$insertData['sec_subscriber'] =$sec_subscribers;
		$insertData['pri_subscriber'] = $pri_subscribers; 



		$speCount = count($apiData['specimen']);

		for($i=0;$i<$speCount;$i++){

			$digits = 4;
			$today_date = date('Y-m-d');
			$today_date = "SELECT `assessioning_num` FROM `wp_abd_specimen` where create_date between '".$today_date." 00:00:00' and '".$today_date." 23:59:59' AND `assessioning_num` <> 0 ORDER BY `id` DESC";
			$insert_day_data = $this->db->query($today_date)->row_array();
			$no = substr($insert_day_data['assessioning_num'],7,4);
			$no = abs($no);
			$day_data = $no+1;
			$assessioning_num = date('ymd').'-'.str_pad($day_data,$digits, '0', STR_PAD_LEFT).'-NF';

			$specimenData = $apiData['specimen'][$i];
			
			$insertData['site_indicator'] = $specimenData['site_indicator'];
			$insertData['assessioning_num'] = $assessioning_num;
			$this->db->insert('wp_abd_specimen',$insertData);
			$lastId = $this->db->insert_id();

			$clinicalData = array( 
				'specimen_id' => $lastId, 
				'clinical_specimen' => implode(',',$specimenData['clinical_specimen']),  
				'nail_unit'			=> implode(',',$specimenData['nail_unit']), 
				'assessioning_num'  => $assessioning_num, 
				'create_date' => date('Y-m-d H:i:s')
			);

			$this->db->insert('wp_abd_clinical_info',$clinicalData);

			$clinicLastId = $this->db->insert_id();
			//echo $clinicLastId;

			$folder = FCPATH."assets\uploads\qr_images\/";
			 $file_name = "qr_".$assessioning_num.".png";
			 $file_path = $folder.$file_name;

			 $info = $assessioning_num;
			 
			 QRcode::png($info, $file_path);
			 array_push($qrCode, $file_name);
			 array_push($assessioning, $assessioning_num);
		
		}

		 if($clinicLastId){
			 die( json_encode(array('returnUrl'=>$redirect_url,'qrCode'=>$qrCode,'base_url'=>base_url(),'assessioning_num'=>$assessioning)));
		 }
		
	  }

	  function edit_specimen()
	 {
	 	$qrCode = array();
	 	$assessioning = array();
		$apiData = json_decode(file_get_contents('php://input'), true);
		//pr($apiData);
		$field_data = array('collection_date','date_received','physician_id','p_lastname','p_firstname','patient_address','patient_city','patient_state','patient_zip','patient_dob','patient_age','patient_sex', 'pri_insurance_name','pri_address','pri_city','pri_state','pri_zip', 'pri_member_id','pri_subscriber_dob','pri_group_contact','pri_sex');
		$specialChars = array(" ", "\r", "\n");
		$replaceChars = array("", "", "");
		$error = false;
		$dataCount = count($field_data);
		

		for($i=0;$i<$dataCount;$i++){
			if(empty($apiData[$field_data[$i]])){
				$error = true;
			}
		}
		if($error){
			$status = '1';
			$redirect_url = "/suspended-list";
		}
		elseif($apiData['physician_accepct'] == '0' && !$error){
			$status = '0';
			$redirect_url = "/specimens-list";
		}
		elseif(!$error || $apiData['physician_accepct'] == '1'){
			$status = '1';
			$redirect_url = "/suspended-list";
		}
		else{
			$status = '0';
			$redirect_url = "/specimens-list";
		} 

		$bill = ""; $pri_subscribers = ""; $sec_subscribers = "";$insurance_type = "";
		if(count(array_filter($apiData['bill'])) != 0){
			$bill = implode(',',$apiData['bill']);
		}
		
		if(count(array_filter($apiData['pri_subscriber_relation'])) != 0){
			$pri_subscribers =  implode(',',$apiData['pri_subscriber_relation']);
		}
		 
		if(count(array_filter($apiData['sec_subscriber_relation'])) != 0){
			$sec_subscribers = implode(',',$apiData['sec_subscriber_relation']);
		  }
		
		if(count(array_filter($apiData['insurance_type'])) != 0){
			$insurance_type = implode(',',$apiData['insurance_type']);
		}

		$insertData['lab_id'] = '100';
		$insertData['physician_id'] = $apiData['physician_id'];
		$insertData['patient_id'] = $apiData['patient_id'];
		$insertData['collection_date'] = $apiData['collection_date'];
		$insertData['collection_time'] = str_replace($specialChars, $replaceChars, $apiData['time'])." ".$apiData['format'];
		$insertData['date_received'] = $apiData['date_received']; 
		$insertData['bill'] = $bill;
		$insertData['p_lastname'] = stripslashes($apiData['p_lastname']);
		$insertData['p_firstname'] = $apiData['p_firstname']; 
		$insertData['patient_city'] = $apiData['patient_city']; 
		$insertData['patient_address'] = $apiData['patient_address']; 
		$insertData['apt'] = $apiData['apt'];	
		$insertData['patient_phn'] = $apiData['patient_phn'];
		$insertData['patient_state'] = $apiData['patient_state'];
		$insertData['patient_zip'] = $apiData['patient_zip']; 
		$insertData['patient_dob'] = $apiData['patient_dob'];
		$insertData['patient_age'] = $apiData['patient_age'];	
		$insertData['patient_sex'] = $apiData['patient_sex']; 
		$insertData['pri_address'] = $apiData['pri_address']; 
		$insertData['pri_city'] = $apiData['pri_city'];
		$insertData['pri_state'] = $apiData['pri_state'];
		$insertData['pri_zip'] = $apiData['pri_zip']; 
		$insertData['pri_employee_name'] = $apiData['pri_employee_name'];	
		$insertData['pri_member_id'] = $apiData['pri_member_id'];
		$insertData['pri_subscriber_dob'] = $apiData['pri_subscriber_dob'];	
		$insertData['pri_group_contact'] = $apiData['pri_group_contact']; 
		$insertData['pri_sex'] = $apiData['pri_sex'];
		$insertData['pri_medicare_id'] = $apiData['pri_medicare_id']; 
		$insertData['pri_madicaid_id'] = $apiData['pri_madicaid_id'];
		$insertData['sec_insurance_name'] = $apiData['sec_insurance_name']; 
		$insertData['sec_address'] = $apiData['sec_address'];
		$insertData['sec_city'] =$apiData['sec_city'];	
		$insertData['sec_state'] = $apiData['sec_state'];
		$insertData['sec_zip'] = $apiData['sec_zip'];
		$insertData['sec_employee_name'] = $apiData['sec_employee_name'];
		$insertData['sec_member_id'] = $apiData['sec_member_id']; 
		$insertData['sec_subscriber_dob'] = $apiData['sec_subscriber_dob']; 
		$insertData['sec_group_contact'] = $apiData['sec_group_contact']; 
		$insertData['sec_sex'] = $apiData['sec_sex'];
		$insertData['sec_medicare_id'] = $apiData['sec_medicare_id'];
		$insertData['sec_medicaid_id'] = $apiData['sec_medicaid_id'];
		$insertData['physician_accepct'] =$apiData['physician_accepct'];
		$insertData['status'] =$status;
		$insertData['modify_date'] = date('Y-m-d H:i:s');
		$insertData['pri_insurance_name']= $apiData['pri_insurance_name'];
		$insertData['ins_type'] = $insurance_type; 
		$insertData['sec_subscriber'] =$sec_subscribers;
		$insertData['pri_subscriber'] = $pri_subscribers; 



		$speCount = count($apiData['specimen']);

		for($i=0;$i<$speCount;$i++){
			$specimenData = $apiData['specimen'][$i];
			if($i==0){
				
				$insertData['site_indicator'] = $specimenData['site_indicator'];
				$this->db->where('id',$apiData['user_id']);
				$this->db->update('wp_abd_specimen',$insertData);

				$clinicalData = array(  
					'clinical_specimen' => implode(',',$specimenData['clinical_specimen']),  
					'nail_unit'			=> implode(',',$specimenData['nail_unit']), 
				);
				$this->db->where('specimen_id',$apiData['user_id']);
				$this->db->update('wp_abd_clinical_info',$clinicalData);
			}else{
				$digits = 4;
				$today_date = date('Y-m-d');
				$today_date = "SELECT `assessioning_num` FROM `wp_abd_specimen` where create_date between '".$today_date." 00:00:00' and '".$today_date." 23:59:59' AND `assessioning_num` <> 0 ORDER BY `id` DESC";
				$insert_day_data = $this->db->query($today_date)->row_array();
				$no = substr($insert_day_data['assessioning_num'],7,4);
				$no = abs($no);
				$day_data = $no+1;
				$assessioning_num = date('ymd').'-'.str_pad($day_data,$digits, '0', STR_PAD_LEFT).'-NF';
				$insertData['site_indicator'] = $specimenData['site_indicator'];
				$insertData['assessioning_num'] = $assessioning_num;
				$insertData['create_date'] = date('Y-m-d H:i:s');

				$this->db->insert('wp_abd_specimen',$insertData);
				$lastId = $this->db->insert_id();

				$clinicalData = array( 
					'specimen_id' => $lastId, 
					'clinical_specimen' => implode(',',$specimenData['clinical_specimen']),  
					'nail_unit'			=> implode(',',$specimenData['nail_unit']), 
					'assessioning_num'  => $assessioning_num, 
					'create_date' => date('Y-m-d H:i:s')
				);

				$this->db->insert('wp_abd_clinical_info',$clinicalData);

				$clinicLastId = $this->db->insert_id();



			}
		}

		 if($this->db->affected_rows() > 0){
			 die( json_encode(array('returnUrl'=>$redirect_url,'qrCode'=>$qrCode,'base_url'=>base_url(),'assessioning_num'=>$assessioning)));
		 }
		
	  }


	  function get_assessioning_num(){
		$digits = 4;
		$today_date = date('Y-m-d');
		$today_date = "SELECT `assessioning_num` FROM `wp_abd_specimen` where create_date between '".$today_date." 00:00:00' and '".$today_date." 23:59:59' AND `assessioning_num` <> 0 ORDER BY `id` DESC";
		$insert_day_data = $this->db->query($today_date)->row_array();
		$no = substr($insert_day_data['assessioning_num'],7,4);
		$no = abs($no);
		$day_data = $no+1;
		$assessioning_num = date('ymd').'-'.str_pad($day_data,$digits, '0', STR_PAD_LEFT).'-NF';
		echo( json_encode(array('assessioning_num'=>$assessioning_num)));
	  }

	/**
	* Cancelled Specimens 
	*/
	  function cancelled_specimens_list()
	  {
		$search = "";  
		$specimens_details = $this->BlankModel->get_cancelled($search);
		$num_rows_count = count($specimens_details);
		$details = array();
		$physicians_det = array();
		$physicians_data = array();
		 $physician_sql = "SELECT `id`, `physician_id` FROM `wp_abd_specimen` WHERE `status` = '0' AND `physician_accepct` = '0' AND `create_date` > '2017-03-27 23:59:59' GROUP BY `physician_id`";
	  
		 $physician_ids = $this->BlankModel->customquery($physician_sql);
	   
		 foreach($physician_ids as $key => $physician){
	   
			 $physician_first_name = get_user_meta( $physician['physician_id'], 'first_name', '' );
		  $physician_last_name  = get_user_meta( $physician['physician_id'], 'last_name' , '' );
			 $physicians_det['physician_name'] = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
		  $physicians_det['id'] = $physician['physician_id'];
		
			 array_push($physicians_data, $physicians_det);
			}
	   
		if($specimens_details)
		{
			foreach($specimens_details as $specimen)
			{
				$physician_first_name = get_user_meta( $specimen->physician_id, 'first_name' );
				$physician_last_name  = get_user_meta( $specimen->physician_id, 'last_name');
				$physician_name = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
				$specimen_information =  array( 'id' => $specimen->id, 'assessioning_num' => $specimen->assessioning_num, 'physician_id' => $specimen->physician_id, 'p_lastname' => $specimen->p_lastname, 'p_firstname' => $specimen->p_firstname, 'qc_check' => $specimen->qc_check,'patient_address' => $specimen->patient_address, 'collection_date' => $specimen->collection_date, 'physician_name' => $physician_name);
				array_push($details, $specimen_information);
				
				
			}
			echo( json_encode(array('specimen_count' => $num_rows_count, "specimens_detail" => $details , "physicians_data" => $physicians_data )));
			
		}
		else
		{
			echo( json_encode(array('status'=>'0')));
		}
	  }
	  function search_cancelled_specimen()
	  {
		$search = "";
		$data = json_decode(file_get_contents('php://input'), true);
		
		if($data['physician']!=""){
			$search=" AND `physician_id`='".$data['physician']."'";	
		 }
		 if($data['date']!=""){
			$collection_date =$data['date'];
			// $month_day = substr($collection_date , '0','6');
			// $year = substr($collection_date , '6','4');
			//$trimed_date = end(explode('20', $year)); 
			$search.=" AND (`collection_date` LIKE '%".$collection_date."%')";	
		 }
		 if($data['fname']!="")
		 {
			$search.=" AND `p_firstname` LIKE '%".$data['fname']."%'";	
		 }
		 if($data['lname']!=""){
			 $search.=" AND `p_lastname` LIKE '%".$data['lname']."%'";	
		 }
		 if($data['barcode']!=""){
			$search.=" AND (`barcode_number` LIKE '%".$data['barcode']."%' OR `assessioning_num` LIKE '%".$data['barcode']."%')";
		 }
		 if($data['assessioning_num']!=""){
			$search.=" AND `assessioning_num` ='".$data['assessioning_num']."'";	
		 }

		$specimens_details = $this->BlankModel->get_cancelled($search);
		$num_rows_count = count($specimens_details);
		$details = array();
		$physicians_det = array();
		$physicians_data = array();
		 $physician_sql = "SELECT `id`, `physician_id` FROM `wp_abd_specimen` WHERE `status` = '0' AND `physician_accepct` = '0' AND `create_date` > '2017-03-27 23:59:59' GROUP BY `physician_id`";
	  
		 $physician_ids = $this->BlankModel->customquery($physician_sql);
	   
		 foreach($physician_ids as $key => $physician){
	   
			 $physician_first_name = get_user_meta( $physician['physician_id'], 'first_name', '' );
		  $physician_last_name  = get_user_meta( $physician['physician_id'], 'last_name' , '' );
			 $physicians_det['physician_name'] = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
		  $physicians_det['id'] = $physician['physician_id'];
		
			 array_push($physicians_data, $physicians_det);
			}
	   
		if($specimens_details)
		{
			foreach($specimens_details as $specimen)
			{
				$physician_first_name = get_user_meta( $specimen->physician_id, 'first_name' );
				$physician_last_name  = get_user_meta( $specimen->physician_id, 'last_name');
				$physician_name = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
				$specimen_information =  array( 'id' => $specimen->id, 'assessioning_num' => $specimen->assessioning_num, 'physician_id' => $specimen->physician_id, 'p_lastname' => $specimen->p_lastname, 'p_firstname' => $specimen->p_firstname, 'qc_check' => $specimen->qc_check,'patient_address' => $specimen->patient_address, 'collection_date' => $specimen->collection_date, 'physician_name' => $physician_name);
				array_push($details, $specimen_information);
				
				
			}
			echo( json_encode(array('specimen_count' => $num_rows_count, "specimens_detail" => $details , "physicians_data" => $physicians_data )));
			
		}
		else
		{
			echo( json_encode(array('status'=>'0')));
		}

	  }
	
	/**
	* Specimens Stage details
	*/
	function specimen_stage_list()
	{
		$details = array();
		$specimens_details = $this->BlankModel->get_specimen_stages();
		$num_rows_count = count($specimens_details);
		if($specimens_details)
		{
			foreach($specimens_details as $specimen)
			{
				$stage1_sql = "SELECT `stage_det_id` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` = '".$specimen->id."' AND `status` = 'Complete' AND `stage_id` ='1'";
				$stage1_data = $this->BlankModel->customquery($stage1_sql);
				if(!empty($stage1_data))
				{
					$first_data = $stage1_data[0]['stage_det_id'];
				}
				else
				{
					$first_data = "No";
				}
				
				 $stage2_sql = "SELECT `stage_det_id` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` = '".$specimen->id."' AND `status` = 'Complete' AND `stage_id` ='2'";
				 $stage2_data = $this->BlankModel->customquery($stage2_sql);
				 if(!empty($stage2_data))
				{
					$second_data = $stage2_data[0]['stage_det_id'];
				}
				else
				{
					$second_data = "No";
				}

				 $stage3_sql = "SELECT `stage_det_id` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` = '".$specimen->id."' AND `status` = 'Complete' AND `stage_id` ='3'";
				 $stage3_data = $this->BlankModel->customquery($stage3_sql);
				 if(!empty($stage3_data))
				{
					$third_data = $stage3_data[0]['stage_det_id'];
				}
				else
				{
					$third_data = "No";
				}

				 $stage4_sql = "SELECT `stage_det_id` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` = '".$specimen->id."' AND `status` = 'Complete' AND `stage_id` ='4'";
				 $stage4_data = $this->BlankModel->customquery($stage4_sql);

				 if(!empty($stage4_data))
				 {
					 $fourth_data = $stage4_data[0]['stage_det_id'];
				 }
				 else
				 {
					 $fourth_data = "No";
				 }

				$specimen_information =  array( 'id' => $specimen->id, 'assessioning_num' => $specimen->assessioning_num,'first_data'=>$first_data, 'second_data'=>$second_data, 'third_data'=>$third_data, 'fourth_data'=>$fourth_data);
				array_push($details, $specimen_information);
			}
			echo(json_encode(array('count' => $num_rows_count, "specimens_detail" => $details)));
		}
		else
		{
			echo(json_encode(array('status'=>'0')));
		}

	}

	public function view_specimen() 
	{
		$details = array();
		$data = json_decode(file_get_contents('php://input'), true);
		$sql = "SELECT * FROM (SELECT * FROM `wp_abd_specimen`)specimen INNER JOIN 
		(SELECT * FROM `wp_abd_clinical_info`)clinical_det ON specimen.id = clinical_det.specimen_id WHERE specimen.id ='".$data."'";
		$specimens_details = $this->BlankModel->customquery($sql);
		if($specimens_details)
		{

			$bill= $specimens_details[0]['bill'];
			$bils = explode(",",$bill);
			$pri_subscriber = $specimens_details[0]['pri_subscriber']; 
			$subscriber = explode(",",$pri_subscriber);
			$sec_subscriber = $specimens_details[0]['sec_subscriber']; 
			$subscriber_sec = explode(",",$sec_subscriber);
			$insurance_type = explode(",",$specimens_details[0]['ins_type']);
			$clinical_specimen_fst = $specimens_details[0]['clinical_specimen'];
			$clinical_specimen = explode(",",$clinical_specimen_fst);
			$nail_units = $specimens_details[0]['nail_unit'];
			$nail_unit = explode(",",$nail_units);

			$pid = $specimens_details[0]['physician_id'];
			$physicianId = "'".$pid."'";
			$sql_new = "select * from `wp_abd_users` where ID ='".$pid."'";
			$result =  $this->BlankModel->customquery($sql_new);
			
			$physician_first_name = get_user_meta( $physicianId, 'first_name' );
			$physician_last_name  = get_user_meta( $physicianId, 'last_name');
			$physician_name = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
			$phy_email = $result[0]['user_email'];
			$Physician_add= get_user_meta($physicianId, '_address'); 
			$add = $Physician_add['meta_value'];
			$Physician_mob= get_user_meta($physicianId, '_mobile');
			$mob = $Physician_mob['meta_value'];
			$phy_information =  array( 'name' => $physician_name, 'phy_email' => $phy_email, 'phy_add' => $add, 'mob' => $mob);
			array_push($details, $phy_information);
			echo(json_encode(array('spe_det'=>$specimens_details,'phy_det'=>$details,'bill'=>$bils,'subscriber'=>$subscriber, 'subscriber_sec'=>$subscriber_sec, 'clinical_specimen'=>$clinical_specimen,'nail_unit'=>$nail_unit,'insurance_type'=>$insurance_type,'status'=>'1')));
		}
		else
		{
			echo(json_encode(array('status'=>'0')));
		}

	}

	function get_physician_details(){
		$details = array();
		$pid = json_decode(file_get_contents('php://input'), true);
		$sql_new = "select * from `wp_abd_users` where ID ='".$pid."'";
		$result =  $this->BlankModel->customquery($sql_new);
		$physicianId = "'".$pid."'";
		if($result){
		$physician_first_name = get_user_meta( $physicianId, 'first_name' );
		$physician_last_name  = get_user_meta( $physicianId, 'last_name');
		$physician_name = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
		$phy_email = $result[0]['user_email'];
		$Physician_add= get_user_meta($physicianId, '_address'); 
		$add = $Physician_add['meta_value'];
		$Physician_mob= get_user_meta($physicianId, '_mobile');
		$mob = $Physician_mob['meta_value'];
		$phy_information =  array( 'name' => $physician_name, 'phy_email' => $phy_email, 'phy_add' => $add, 'mob' => $mob);
		array_push($details, $phy_information);
			echo(json_encode(array('phy_det'=>$details,'status'=>'1')));
		}else{
			echo(json_encode(array('status'=>'0')));
		}
	}

	function print_add_specimen_pdf(){
	$filecontent = $this->load->view('add_specimen','',true);
	$pdfTotalBody = $filecontent;
	$find=array();
	$find[]="{logo}";
	$find[]="{foot_pic}";
	$replace=array();
	$replace[] = base_url()."assets/frontend/images/logo.png";
	$replace[] = base_url()."assets/frontend/images/pdf-ft.jpg";
	for($data_int=0;$data_int<count($find);$data_int++){ 
		$pdfTotalBody = str_replace($find[$data_int],$replace[$data_int],$pdfTotalBody); 
	}
	$body = $pdfTotalBody;
	$message = $body;
	$this->m_pdf->pdf->WriteHTML($message);
	$ability_pdf = "ability_".rand().".pdf";
	$this->m_pdf->pdf->Output(FCPATH."assets\uploads\pdf\/".$ability_pdf,"F");

	echo(json_encode(array('base_url'=>base_url().'assets/uploads/pdf/','pdf_name'=>$ability_pdf)));
}
      
}
?>