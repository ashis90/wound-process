<?php
if (!defined('BASEPATH'))
    EXIT("No direct script access allowed");
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
	header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization');
	header('Content-Type:application/json');

class Notifications extends MY_Controller {

    function __construct() {
        parent::__construct();
       $this->load->library('m_pdf');
       $this->load->library('efax');
       date_default_timezone_set('MST7MDT');
    }
  
    function index()
    {
	  
    }

    function get_all_notifications(){		
    	$data = json_decode(file_get_contents('php://input'), true);
    	$incompleted_notes_arr = array(array());
    	$complted_notes_arr = array(array());
    	if(!empty($data)){
    		// $result = $this->get_notification_with_status('1',$data['user_role'],$data['user_id']);
    		$incompleted_notes_sql = $this->get_notification_with_status('0','data_entry_oparator',$data['user_id']);
    		if(!empty($incompleted_notes_sql)){
	    		$i=0;
	    		foreach($incompleted_notes_sql as $incompleted_notes){ 
					// $read_note_sql = $this->db->query("SELECT `note_id` FROM wp_abd_notification WHERE `note_id` = '".$incompleted_notes['note_id']."' AND `status` = '0' AND `note_read` ='Read' ORDER BY `note_id` DESC")->result_array(); 		   
					$note_pub_date = $incompleted_notes['note_publish_date'];
					$CurDate = date("Y-m-d");
					if( $CurDate >= $note_pub_date){
						$incompleted_notes_arr[$i]['note_id'] = $incompleted_notes['note_id'];
						$incompleted_notes_arr[$i]['notification_details'] = $incompleted_notes['notification_details'];
						$incompleted_notes_arr[$i]['note_read'] = $incompleted_notes['note_read'];
					}
					$i++;
				}
			}

			$complted_note_sql = $this->get_notification_with_status('1','data_entry_oparator',$data['user_id']);
    		$j=0;
    		foreach($complted_note_sql as $complted_note){ 
				// $read_note_sql = $this->db->query("SELECT `note_id` FROM `wp_abd_notification` WHERE `note_id` = '".$complted_note['note_id']."' AND `status` = '1' AND `note_read` ='Read'")->result_array(); 		   
				$note_pub_date = $complted_note['note_publish_date'];
				$CurDate = date("Y-m-d");
				if( $CurDate >= $note_pub_date){
					$fname = get_user_meta($complted_note['reader_id'], 'first_name',true);
					$lname = get_user_meta( $complted_note['reader_id'],'last_name',true );
					$name = $fname['meta_value'].' '.$lname['meta_value'];
					$complted_notes_arr[$j]['note_id'] = $complted_note['note_id'];
					$complted_notes_arr[$j]['notification_details'] = $complted_note['notification_details'];
					$complted_notes_arr[$j]['reader_id'] = $complted_note['reader_id'];
					$complted_notes_arr[$j]['modify_date'] = date('F j, Y H:i',strtotime($complted_note['modify_date']));
					$complted_notes_arr[$j]['reader_name'] = $name;
					$complted_notes_arr[$j]['note_read'] = $complted_note['note_read'];
				}
				$j++;
			}
			if(!empty($complted_notes_arr) || !empty($incompleted_notes_arr)){
    		 	die( json_encode(array('status'=>'1','incompleted_notes'=>$incompleted_notes_arr,'complted_notes'=>$complted_notes_arr)));
			}else{
				die( json_encode(array('status'=>'0')));
			}
    	}
      
     }

     function get_notification_with_status($status,$user_role,$current_user_id){
	  $sql="";
	  
	  // $sql.="SELECT * FROM wp_abd_notification WHERE status = $status AND note_for ='DataEntry' AND DATE(note_publish_date) <= CURDATE()";
	  
	  // if($user_role != 'data_entry_oparator'){
	  // $sql.= " AND users_id =$current_user_id";
	  // }
	  $sql.="SELECT * FROM wp_abd_notification WHERE status = $status AND DATE(note_publish_date) <= CURDATE() AND users_id =$current_user_id";

	  if($status = 1){
	   	 $sql.=" ORDER BY `modify_date` DESC";
	  }
	  else{
	  	$sql.=" ORDER BY `note_id` DESC";
	  }
	  $notes_sql = $this->db->query($sql)->result_array();
	  return $notes_sql;
	}

	function read_notifications(){
		$data = json_decode(file_get_contents('php://input'), true);
		if(!empty($data)){
			$this->db->where('note_id',$data['note_id']);
			$updateData = $this->db->update('wp_abd_notification', 
				array( 'note_read' =>'Read'));

			if($this->db->affected_rows() > 0){
				die( json_encode(array('status'=>'1')));
			}else{
				die( json_encode(array('status'=>'0')));
			}
		}
	}

	function dismiss_notifications(){
		$data = json_decode(file_get_contents('php://input'), true);
		if(!empty($data)){
			$this->db->where('note_id',$data['note_id']);
			$updateData = $this->db->update('wp_abd_notification', 
				array( 'status' => '1', 'modify_date' => date('Y-m-d H:i:s'),'reader_id'=>$data['reader_id']));

			if($this->db->affected_rows() > 0){
				die( json_encode(array('status'=>'1')));
			}else{
				die( json_encode(array('status'=>'0')));
			}
		}
	}

	function get_pending_notifications(){
		$data = json_decode(file_get_contents('php://input'), true);
		if(!empty($data)){
			$user_id = $data['user_id'];
			$pending_notifications = $this->db->query("SELECT * FROM wp_abd_notification WHERE users_id =$user_id  AND status = '0' AND DATE(note_publish_date) <= CURDATE() ORDER BY note_id DESC")->result_array();
			if(!empty($pending_notifications)){
				die( json_encode(array('status'=>'1','pending_notifications'=>$pending_notifications)));
			}else{
				die( json_encode(array('status'=>'0')));
			}
		}
	}

	function data_entry_notification(){
		$nail_report_specimen_det = "SELECT `physician_id`, count(specimen.physician_id)  as report_count,`specimen_id` as `specimen`,`lab_id` as data_pep FROM 
	  								(SELECT `specimen_id`,`nail_funagl_id` FROM wp_abd_nail_pathology_report) report
	  		                        INNER JOIN 
	  		                        (SELECT `id`, `physician_id`, `lab_id` FROM wp_abd_specimen) specimen
	  		                        ON
	  		                        report.specimen_id = specimen.id GROUP BY specimen.physician_id";

	  	$get_specimen_report  = $this->db->query($nail_report_specimen_det)->result_array();
	  	 
	    foreach($get_specimen_report as $spe_rep){	
	    	
	  		if($spe_rep['report_count'] == '1'){
	  	 	  
	  	 		$note_count = $this->db->query("SELECT `note_id` FROM wp_abd_notification WHERE `users_id` = '".$spe_rep['data_pep']."' AND `specimen_id` = '".$spe_rep['specimen']."' AND `physician_id` = '".$spe_rep['physician_id']."' AND `note_for` = 'DataEntry'")->row_array();  
	  	 		
	  	 	  if(empty($note_count)){
	  	 	  	 $fname = get_user_meta($spe_rep['physician_id'], 'first_name',true);
	  	 	  	 $lname = get_user_meta($spe_rep['physician_id'],'last_name',true );
	  	 	  	 $mobile = get_user_meta($spe_rep['physician_id'], '_mobile',true);
	  	 	  	 $physician_name = $fname['meta_value'].' '.$lname['meta_value'];
	  	 	  if($physician_name == ""){
	  			 $physician_sql = $this->db->query("SELECT `user_login` FROM wp_abd_users WHERE `ID` = '".$spe_rep['physician_id']."'")->row_array();  
	  			 $physician_name = $physician_sql['user_login'].'’s';
	  			}
	  		    $notification_details ="The first report for Dr. ".$physician_name." was just published. Call office at ".$mobile['meta_value']." to help with site login.";  	
	  	 		  	
	  	 		$aft_two_wk_notification_details ="It’s been 2 weeks since Dr. ".$physician_name." first report. Call ".$mobile['meta_value']." to help reorder supplies.";  
	  	 		
	  	 		$aft_thirty_days_notification_details ="It’s been 30 days since Dr. ".$physician_name." first report. Call ".$mobile['meta_value']." to check-in.";
	  	 		$after_two_week = date("Y-m-d", strtotime("+2 week")); 
	  		    $after_thirthy_dys_pub_date = date("Y-m-d", strtotime("+30 days")); 
	  			
	 		    $notification_insert_array = array(
													'users_id'    => $spe_rep['data_pep'],
													'specimen_id' => $spe_rep['specimen'],
													'physician_id'=> $spe_rep['physician_id'],
													'notification_details' =>  nl2br($notification_details),
													'note_read'	  => 'Unread',
													'notification_details_aft_date' => '0',
													'note_publish_date'=> date('Y-m-d'),
													'note_for'	  => 'DataEntry',
													'status'      => '0',
													'create_date' => date('Y-m-d H:i:s')
													);
				$insert_notification = $this->db->insert('wp_abd_notification', $notification_insert_array);		
				
				$aft_two_wk_notification_details_array = array(
													'users_id'     => $spe_rep['data_pep'],
													'specimen_id'  => $spe_rep['specimen'],
													'physician_id' => $spe_rep['physician_id'],
													'notification_details' => nl2br($aft_two_wk_notification_details),
													'note_publish_date'=> $after_two_week,
													'notification_details_aft_date' => '2',
													'note_read'		=> 'Unread',
													'note_for'		=> 'DataEntry',
													'status'      => '0',
													'create_date' => date('Y-m-d H:i:s')
													);
		        $insert_notification = $this->db->insert('wp_abd_notification', $aft_two_wk_notification_details_array);	
				
				$aft_four_wk_notification_details_array = array(
													'users_id'     => $spe_rep['data_pep'],
													'specimen_id'  => $spe_rep['specimen'],
													'physician_id' => $spe_rep['physician_id'],
													'notification_details' => nl2br($aft_thirty_days_notification_details),
													'note_publish_date'=> $after_thirthy_dys_pub_date,
													'notification_details_aft_date' => '3',
													'note_read'		=> 'Unread',
													'note_for'		=> 'DataEntry',
													'status'      => '0',
													'create_date' => date('Y-m-d H:i:s')
													);
	
				$insert_notification = $this->db->insert('wp_abd_notification', $aft_four_wk_notification_details_array);
					}
	  	 		}
	  	} 
	}

	function sales_notification(){
	$nail_report_specimen_det="SELECT `physician_id`,count(specimen.physician_id) as report_count,`specimen_id` as `specimen`,`lab_id` as `data_pep` FROM 
							   (SELECT `specimen_id`,`nail_funagl_id` FROM wp_abd_nail_pathology_report) report
	  		                   INNER JOIN 
	  		                   (SELECT `id`, `physician_id`, `lab_id` FROM wp_abd_specimen) specimen
	  		                   ON
	  		                   report.specimen_id = specimen.id GROUP BY specimen.physician_id";
	  	$get_specimen_report = $this->db->query($nail_report_specimen_det)->result_array();
	    foreach($get_specimen_report as $spe_rep){
	    	if($spe_rep['report_count'] == '1'){
	  	 	   $sales_id = get_user_meta( $spe_rep['physician_id'],'added_by',true );
	  	 	   $sales_id =$sales_id['meta_value'];
	  	 	   $note = $this->db->query("SELECT `note_id` FROM wp_abd_notification WHERE `users_id` = '".$sales_id."' AND `specimen_id` = '".$spe_rep['specimen']."' AND `physician_id` = '".$spe_rep['physician_id']."' AND `note_for` = 'Sales'")->row_array();  
			   $note_count = count($note);
	  	 	if($note_count){
	  	 	}
			else{
				$fname = get_user_meta($spe_rep['physician_id'], 'first_name',true);
				$lname = get_user_meta( $spe_rep['physician_id'],'last_name',true );
				$physician_name = $fname['meta_value'].' '.$lname['meta_value'];
		  	 	if( $physician_name == ""){
		  			$physician_sql  = $this->db->query("SELECT `user_login` FROM wp_abd_users WHERE `ID` = '".$spe_rep['physician_id']."'")->row_array();  
		  			$physician_name = $physician_sql['user_login'].'’s';
		  			}
		  		$sales_notification_details ="Dr. " .$physician_name." just got his first report.";  	
	  	 		$sales_id = get_user_meta( $spe_rep['physician_id'],'added_by',true );
	  	 		$sales_id = ['meta_value'];
	        	$notification_insert_array = array(
													'users_id'     => $sales_id,
													'specimen_id'  => $spe_rep['specimen'],
													'physician_id' => $spe_rep['physician_id'],
													'notification_details' =>  nl2br($sales_notification_details),
													'note_read'		=> 'Unread',
													'note_publish_date'=> date('Y-m-d'),
													'notification_details_aft_date' => '0',
													'status'      => '0',
													'note_for'		=> 'Sales',
													'create_date' => date('Y-m-d H:i:s')
													);
				$insert_notification = $this->db->insert('wp_abd_notification', $notification_insert_array);		
				}
	  	 	}
	  	} 
	  	
	}


	function new_physician_added_notification($sales_id, $physician_id){
		$fname = get_user_meta($physician_id, 'first_name',true);
		$lname = get_user_meta( $physician_id,'last_name',true );
		$physician_name = $fname['meta_value'].' '.$lname['meta_value'];
	  	if( $physician_name == ""){
	  		$physician_sql  = $this->db->query("SELECT `user_login` FROM wp_abd_users WHERE `ID` = '".$physician_id."'");  
	  		$physician_name = $physician_sql['user_login'];
	  	}
  	$notification_details ="New physician Dr. " .$physician_name." added ";  
  	$assigened_sales_id  = get_user_meta($sales_id,'assign_to',TRUE);
  	$assigened_sales_id  = $assigened_sales_id['meta_value'];
    
    if($assigened_sales_id){
    	
	$new_physician_added_notification_fr_abv_sales_array = array(
  										'users_id'     => $assigened_sales_id,
  										'physician_id' => $physician_id,
  										'notification_details' => nl2br($notification_details),
  										'notification_details_aft_date' => '5',
  										'note_publish_date'=> date('Y-m-d'),
  										'note_read'	  => 'Unread',
  										'note_for'	  => 'Sales',
  										'status'      => '0',
  										'create_date' => date('Y-m-d H:i:s')
  										);
    $insert_new_physician_added_notification_fr_abv_sales = $this->db->insert('wp_abd_notification',$new_physician_added_notification_fr_abv_sales_array);
	}
   	
   	$new_physician_added_notification_fr_sale_admin_array = array(
  										'users_id'     => '66',
  										'physician_id' => $physician_id,
  										'notification_details' => nl2br($notification_details),
  										'notification_details_aft_date' => '5',
  										'note_publish_date'=> date('Y-m-d'),
  										'note_read'	  => 'Unread',
  										'note_for'	  => 'Sales',
  										'status'      => '0',
  										'create_date' => date('Y-m-d H:i:s')
  										);
    $new_physician_added_notification_fr_sale_admin = $this->db->insert('wp_abd_notification', $new_physician_added_notification_fr_sale_admin_array); 
    $new_physician_added_notification_array = array(
  										'users_id'     => $sales_id,
  										'physician_id' => $physician_id,
  										'notification_details' => nl2br($notification_details),
  										'notification_details_aft_date' => '5',
  										'note_publish_date'=> date('Y-m-d'),
  										'note_read'	  => 'Unread',
  										'note_for'	  => 'Sales',
  										'status'      => '0',
  										'create_date' => date('Y-m-d H:i:s')
  										);
    $insert__new_physician_added_notification = $this->db->insert('wp_abd_notification', $new_physician_added_notification_array);			
    }

    function physician_report_after_thirty_days_notification(){

	 	$nail_report_specimen_det="SELECT `physician_id`,count(`specimen`.`physician_id`) as report_count,`specimen_id` as `specimen`,`lab_id` as `data_pep` FROM 
								   (SELECT `specimen_id`,`nail_funagl_id` FROM wp_abd_nail_pathology_report) report
		  		                   INNER JOIN 
		  		                   (SELECT `id`, `physician_id`, `lab_id` FROM wp_abd_specimen) specimen
		  		                   ON
		  		                   `report`.`specimen_id` = `specimen`.`id` GROUP BY `specimen`.`physician_id`";
	  	$get_specimen_report = $this->db->query($nail_report_specimen_det)->result_array();

	  	foreach($get_specimen_report as $spe_rep){
   			$thirty_days_no_physician_report_sql ="SELECT `specimen_id` as `specimen`, `report`.`create_date` as `last_report_date`,`physician_id` FROM (SELECT `specimen_id`,`nail_funagl_id`,`create_date` FROM `wp_abd_nail_pathology_report`) report INNER JOIN (SELECT `id`, `physician_id`, `lab_id` FROM `wp_abd_specimen` WHERE `physician_id` = '".$spe_rep['physician_id']."') specimen ON `report`.specimen_id = `specimen`.id ORDER BY `specimen` DESC LIMIT 0,1";
				
			$thirty_days_no_physician_report = $this->db->query($thirty_days_no_physician_report_sql)->row_array();
			$current_date = date('Y-m-d');
			$last_report_day = $thirty_days_no_physician_report['last_report_date'];
            $after_thirty_date= date('Y-m-d', strtotime($last_report_day.' + 30 days'));
			$fname = get_user_meta($spe_rep['physician_id'], 'first_name', true);
			$lname = get_user_meta($spe_rep['physician_id'], 'last_name', true); 
			$physician_name = $fname['meta_value'].' '.$lname['meta_value'];
			if($current_date >= $after_thirty_date){
			   $sales_id = get_user_meta($spe_rep['physician_id'],'added_by',true );
			   $sales_id = $sales_id['meta_value'];
	           $note_count = $this->db->query("SELECT `note_id` FROM wp_abd_notification WHERE `users_id` = '".$sales_id."' AND `specimen_id` = '".$spe_rep['specimen']."' AND `physician_id` = '".$spe_rep['physician_id']."' AND `note_for` = 'Sales' AND `notification_details_aft_date` = '4'");  
	          if(empty($note_count)){
	          	$mobile = get_user_meta($spe_rep->physician_id, '_mobile',true);
				$aft_thirty_days_notification_details ="No specimen has been reported in past 30 days for Dr. ".$physician_name.". You can call at ".$mobile['meta_value']." to check-in.";  
	         
			    $aft_four_wk_notification_details_array = array(
														'users_id'     => $sales_id,
														'specimen_id'  => $spe_rep['specimen'],
														'physician_id' => $spe_rep['physician_id'],
														'notification_details' => nl2br($aft_thirty_days_notification_details),
														'note_publish_date'=> date('Y-m-d'),
														'notification_details_aft_date' => '4',
														'note_read'	   => 'Unread',
														'note_for'	   => 'Sales',
														'status'       => '0',
														'create_date'  => date('Y-m-d H:i:s')
														);
			    $insert_notification = $this->db->insert('wp_abd_notification', $aft_four_wk_notification_details_array);
	        }
           }       
        }       
	}
    
 
 }
