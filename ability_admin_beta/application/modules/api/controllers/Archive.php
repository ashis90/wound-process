<?php
if (!defined('BASEPATH'))
    EXIT("No direct script access allowed");
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
	header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization');
	header('Content-Type:application/json');
class Archive extends MY_Controller {

    function __construct() {
        parent::__construct();
         $this->load->library('m_pdf');
	date_default_timezone_set('MST7MDT');
    }
  
    function index()
    {
    	
    }

    function get_pending_specimens(){

    	$specimen_det = "SELECT * FROM `wp_abd_specimen_archive` WHERE `create_date` < '2017-03-27 00:00:00' AND `physician_accepct`= 0 AND `status`= 0 AND `qc_check`=0  AND `id` NOT IN (SELECT `specimen_id` FROM `wp_abd_nail_pathology_report_archive`) ORDER BY `wp_abd_specimen_archive`.`id` ASC";
  	   $specimen_results = $this->db->query($specimen_det)->result_array();


  	   if(!empty($specimen_results)){
  	   	for($i=0;$i<count($specimen_results);$i++){
  	   		$fname = get_user_meta($specimen_results[$i]['physician_id'], 'first_name',true);
	 	  	$lname = get_user_meta($specimen_results[$i]['physician_id'],'last_name',true );
	 	  	$physician_name = $fname['meta_value'].' '.$lname['meta_value'];
	 	  	$specimen_results[$i]['physician_name'] = $physician_name;
  	   	}
			die( json_encode(array('status'=>'1','pending_specimens'=>$specimen_results,'specimen_count'=>count($specimen_results))));
  	   	
		}else{
			die( json_encode(array('status'=>'0')));
		}
    }

    function search_specimen()
	{	
		$search = "";
		$data = json_decode(file_get_contents('php://input'), true);
		if($data['physician']!=""){
			$search=" AND `physician_id`='".$data['physician']."'";	
		 }
		 if($data['p_firstname']!=""){
			 $search.=" AND `p_firstname` LIKE '%".$data['p_firstname']."%'";	
		 }
		 if($data['p_lastname']!=""){
			 $search.=" AND `p_lastname` LIKE '%".$data['p_lastname']."%'";	
		 }
		 if($data['barcode']!=""){
			$search.=" AND (`barcode_number` LIKE '%".$data['barcode']."%' OR `assessioning_num` LIKE '%".$data['barcode']."%')";
		 }
		 if($data['assessioning_num']!=""){
			$search.=" AND `assessioning_num` ='".$data['assessioning_num']."'";	
		 }

		if(!empty($data['collection_date'])){
	  	   $collection_date =$data['collection_date'];
	  	   $month_day = substr($collection_date , '0','6');
	  	   $year = substr($collection_date , '6','4');
	  	   $yy = explode('20', $year);
	  	   $trimed_date = end($yy); 

	  	   $search.=" AND (`collection_date` LIKE '%".$collection_date."%' OR `collection_date` LIKE '%".$month_day.$trimed_date."%')";	
	  	}

		$specimen_det = "SELECT * FROM `wp_abd_specimen_archive` WHERE `create_date` < '2017-03-27 00:00:00' AND `physician_accepct`= 0 AND `status`= 0 AND `qc_check`=0 ".$search." AND `id` NOT IN (SELECT `specimen_id` FROM `wp_abd_nail_pathology_report_archive`) ORDER BY `wp_abd_specimen_archive`.`id` ASC";

  	    $specimen_results = $this->db->query($specimen_det)->result_array();
	  
		
		 
		 if(!empty($specimen_results)){
  	   	for($i=0;$i<count($specimen_results);$i++){
  	   		$fname = get_user_meta($specimen_results[$i]['physician_id'], 'first_name',true);
	 	  	$lname = get_user_meta($specimen_results[$i]['physician_id'],'last_name',true );
	 	  	$physician_name = $fname['meta_value'].' '.$lname['meta_value'];
	 	  	$specimen_results[$i]['physician_name'] = $physician_name;
  	   	}
			die( json_encode(array('status'=>'1','pending_specimens'=>$specimen_results,'specimen_count'=>count($specimen_results))));
  	   	
		}else{
			die( json_encode(array('status'=>'0')));
		}
	}

	function get_nail_fungal_pathology_report(){
		$data = json_decode(file_get_contents('php://input'), true);
		date_default_timezone_set('MST7MDT');
		if(!empty($data)){
			$specimen_id = $data['id'];
			if(isset($data['nail_funagl_id'])){
				$nail_funagl_id = $data['nail_funagl_id'];
				$nail_report       		= "SELECT * FROM wp_abd_nail_pathology_report WHERE nail_funagl_id = '".$nail_funagl_id."'";
				$nail_results      		= $this->db->query($nail_report)->row_array();
				$nail_results['dor'] 	= date('m/d/Y', strtotime($nail_results['dor']));
			}else{
				$nail_results = array('dor'=>date('m/d/Y'));
			}
			
			$specimen_det      		= "SELECT * FROM wp_abd_specimen WHERE `status` = '0' AND `physician_accepct` ='0' AND `qc_check`='0' AND id ='".$specimen_id."' ORDER BY `id` DESC LIMIT 1";
			$specimen_results  		= $this->db->query($specimen_det)->row_array();
			$timestamp       		= $specimen_results['create_date'];
			$datetime        		= explode(" ",$timestamp);

			$fname 		= get_user_meta( $specimen_results['physician_id'], 'first_name',true );
			$lname 		= get_user_meta( $specimen_results['physician_id'],'last_name',true );
			$address 	= get_user_meta($specimen_results['physician_id'], '_address',true);
			$mob 		= get_user_meta($specimen_results['physician_id'], '_mobile',true);
			$fax 		= get_user_meta($specimen_results['physician_id'], 'fax',true);
			$name = $fname['meta_value'].' '.$lname['meta_value'];
			$clinical_information   = $this->clinical_function($specimen_id);

			$physician_info = array('name'=>$name,'add'=>$address['meta_value'],'mob'=>$mob['meta_value'],'fax'=>$fax['meta_value']);

			$specimen_stage_details = "SELECT `addtional_desc`,`comments`,`total_desc` FROM wp_abd_specimen_stage_details WHERE `specimen_id`= '".$specimen_id."' AND`stage_id`=2";
			$GrossDescription       = $this->db->query($specimen_stage_details)->row_array();

			if(!empty($specimen_results)){
				die( json_encode(array('status'=>'1','nail_results'=>$nail_results,'specimen_results'=>$specimen_results,'datetime'=>$datetime,'physician_info'=>$physician_info,'clinical_information'=>$clinical_information,'GrossDescription'=>$GrossDescription)));
			}else{
				die( json_encode(array('status'=>'0')));
			}

		}
		
	}

	function clinical_function($specimen_id)
	{
		$specimen_det      = "SELECT * FROM wp_abd_clinical_info WHERE specimen_id ='".$specimen_id."'";
		$specimen_results  = $this->db->query($specimen_det)->row_array();
		
		$clinical_information  = array();
		$clinical_specimen_fst = $specimen_results['clinical_specimen'];
		$clinical_specimen     = explode(",",$clinical_specimen_fst);
		// $clinical_information .= '<div style="float:left; padding:0; width:62px; margin:0;"><strong>Location: </strong></div>';
		foreach ($clinical_specimen as $specimen){
			if($specimen != '')
		 	array_push($clinical_information, $specimen);
		}
		return $clinical_information; 
	}

	function clinical_function_html($specimen_id)
	{
	$specimen_det      = "SELECT * FROM wp_abd_clinical_info WHERE specimen_id ='".$specimen_id."'";
	$specimen_results  = $this->db->query($specimen_det)->row_array();
	
	$clinical_information  = '';
	$clinical_specimen_fst = $specimen_results['clinical_specimen'];
	$clinical_specimen     = explode(",",$clinical_specimen_fst);
	$clinical_information .= '<div style="float:left; padding:0; width:62px; margin:0;"><strong>Location: </strong></div>';
	foreach ($clinical_specimen as $specimen){ $clinical_information.= '<div class="inr-spn" style="float:left; padding:0; margin:0 10px 0 0;">'.$specimen .'</div>'; }
	return $clinical_information; 
	}

	function get_nail_macro_code(){
		$data = json_decode(file_get_contents('php://input'), true);
		if(!empty($data)){
			$info = $data['sort_code'];
			$name = $data['name'];
			$result = $this->db->query("SELECT * FROM `wp_abd_nail_macro_codes` WHERE sc LIKE '" . $info . "'")->row_array();

			if(!empty($result)){
				die( json_encode(array('status'=>'1','marcro_data'=>$result,'name'=>$name)));
			}else{
				die( json_encode(array('status'=>'0')));
			}
			
		}
	}


	 function add_report_data(){
	 	if($this->input->post()){
		 	$addendum_code = array();
		 	$addendum_text = array();
		 	$name = $this->input->post('diagnostic_text');
		 	$id = $this->input->post('id');
		 	$addendum = json_decode($this->input->post('addendum'));
		 	$clinical_history = $this->input->post('clinical_history');
		 	$diagnostic_color = $this->input->post('diagnostic_color');
		 	$diagnostic_short_code = $this->input->post('diagnostic_short_code');
		 	$diagnostic_text = $this->input->post('diagnostic_text');
		 	$gross_description = $this->input->post('gross_description');
		 	$gross_micro_desc_text = $this->input->post('gross_micro_desc_text');
		 	$stains_addendum_first = $this->input->post('stains_addendum_first');
		 	$stains_addendum_sec = $this->input->post('stains_addendum_sec');
		 	$stains_addendum_third = $this->input->post('stains_addendum_third');
		 	$stains_first = $this->input->post('stains_first');
		 	$stains_sec = $this->input->post('stains_sec');
		 	$stains_third = $this->input->post('stains_third');
		 	$user_id = $this->input->post('user_id');
		 	

		 	$specimen_det      		= "SELECT * FROM wp_abd_specimen WHERE `status` = '0' AND `physician_accepct` ='0' AND `qc_check`='0' AND id ='".$id."' ORDER BY `id` DESC LIMIT 1";
			$specimen_results  		= $this->db->query($specimen_det)->row_array();
			$timestamp       		= $specimen_results['create_date'];
			$datetime        		= explode(" ",$timestamp);
			$clinical_information   = $this->clinical_function_html($id);
			$specimen_stage_details = "SELECT `addtional_desc`,`comments`,`total_desc` FROM wp_abd_specimen_stage_details WHERE `specimen_id`= '".$id."' AND`stage_id`=2";
			$GrossDescription       = $this->db->query($specimen_stage_details)->row_array();
		 	
			$nail_report = "SELECT `nail_funagl_id` FROM wp_abd_nail_pathology_report WHERE specimen_id = '".$id."'";
	  		$nail_results = $this->db->query($nail_report)->row_array();
	  
		  if(!empty($nail_results)){
		     $reports_already_exits = "Report is already generated for this Specimen whose accessioning number is ".$specimen_results['assessioning_num'];
		  	die( json_encode(array('status'=>'0','reports_already_exits'=>$reports_already_exits)));
		  }
		  else
		  {

		  	if(!empty($addendum)){
		 		foreach ($addendum as $value) {
			 		array_push($addendum_code, $value->addendum_code);
			 		array_push($addendum_text, $value->addendum_text);
			 	}
		 	}
		 	
		 	$ser_value = serialize($addendum_text);

		 	if($_FILES && $_FILES['file_data']['name']){
				$config['upload_path'] = 'assets/uploads/nail_fungal';
				$config['allowed_types'] = 'jpeg|jpg|png';
				$config['max_size'] = 5000000;
				$new_name = time().'_'.$_FILES["file_data"]['name'];
				$config['file_name'] = $new_name;

				$this->load->library('upload',$config);
				if(!$this->upload->do_upload('file_data')){
					die( json_encode(array( "status" => '0', 'message'=>$this->upload->display_errors())));
				}else{
					$uploadData = $this->upload->data();
					$fileName = $uploadData['file_name'];
				}
			}

		  if($diagnostic_color == "green"){
		  	$diagnostic_color = "#96faa2";
		  }
		  elseif($diagnostic_color == "red"){
		  	$diagnostic_color = "#e53131";
		  }
		  else{
		  	$diagnostic_color = "#f1ec2d";
		  }
		  $filename_new = $fileName;
		  
		  $array_val = count(array_filter($addendum_text));
		  if(0 < $array_val)
		  {
		  $addendum_value = unserialize($ser_value); 	
		  $addendum_pdf_content = "";
		  $addendum_pdf_content .="<div style='margin:0 0 5px 0;'><div style='width:22%; padding:0 1% 0 0;float:left; margin:0;color:#444;font:400 12px Arial,Helvetica,sans-serif'><label>Addendum:</label></div>";
		  
		  foreach($addendum_value as $value)
		  {
		  $addendum_pdf_content .= "<div style='width:70%; padding:0;float:left; color:#444;font:400 12px Arial,Helvetica,sans-serif;margin-left:170px;'>".$value."</div>";
		  }
		  $addendum_pdf_content .="<div style='clear:both;'></div></div>"; 
		  }
	  
	 
		  if($filename_new){
		  $report_img = "<div style='margin:0 0 5px 0;'><div style='width:22%; padding:0 1% 0 0;float:left; margin:0;color:#444;font:400 12px Arial,Helvetica,sans-serif'>
		  <label>&nbsp;</label></div>
		  <div style='width:77%; padding:0;float:left; margin:0;color:#444;font:400 12px Arial,Helvetica,sans-serif'>
		  <img style='max-height:140px;' src=".base_url().'assets/uploads/nail_fungal/'.$filename_new." alt='No Image'/>
		  </div>
		  <div style='clear:both;'></div></div>";
		  }

		  $Stain_shortcodes = array();
		  array_push($Stain_shortcodes, $stains_first);
		  array_push($Stain_shortcodes, $stains_sec);
		  array_push($Stain_shortcodes, $stains_third);
		  $Stains_desc = $this->get_stains_desc($stains_first); 
		  $Stains_desc_one = $Stains_desc['text'];
		  $Stains_desc = $this->get_stains_desc($stains_sec); 
		  $Stains_desc_sec = $Stains_desc['text'];
		  $Stains_desc = $this->get_stains_desc($stains_third); 
		  $Stains_desc_thr = $Stains_desc['text'];
		  $stains_comments = "";
	  
	  for($num = 0; $num < 3; $num++){
	  	$Stain_short = "SELECT `comments`, `id` FROM `wp_abd_nail_macro_codes` where sc = '".$Stain_shortcodes[$num]."'";
	  	$Stain_shortcode = $this->db->query($Stain_short)->row_array();
	  	$stains_comments.= $Stain_shortcode['comments'];
	  	$stains_comments.=  '<div style= "margin:2px;"></div>';
	  }
	  $nail_macro_sql = "SELECT `comments`, `id` FROM `wp_abd_nail_macro_codes` where sc = '" . $diagnostic_short_code . "'";
	  $nail_macro     = $this->db->query($nail_macro_sql)->row_array();
	  $comments = nl2br($nail_macro['comments']);
	  
	  $physician_id = "'".$specimen_results['physician_id']."'";
	  
	  $assigned_partner_meta =  get_user_meta($physician_id, '_assign_partner',true);
	  
	  $assigned_partner = $assigned_partner_meta['meta_value'];
	  
	  if(!empty($assigned_partner)){
	  	$pdfHeaderfile       = $this->load->view('nail_fungal_partner','',true);
		$logo_img            = get_user_meta($assigned_partner,'company_logo',true);
		$partner_address     = get_user_meta($assigned_partner, '_address',true);
		$partner_phn         = get_user_meta($assigned_partner, '_mobile', true);
		$partner_fax         = get_user_meta($assigned_partner, 'fax',     true);
		$partner_company     = get_user_meta($assigned_partner, 'first_name',true);
		$logo = $logo_img['meta_value'];
		$paddress = $partner_address['meta_value'];
		$pph = $partner_phn['meta_value'];
		$pfax = $partner_fax['meta_value'];
		$pcompany = $partner_company['meta_value'];
	  }
	  else{
	  	$pdfHeaderfile = $this->load->view('nail_fungal','',true);
		$logo = base_url()."assets/frontend/images/logo.png";
	  }
	  
	  
	  	$logoddd_img = base_url()."assets/frontend/images/davidbolick_sign.jpg";
		$lab_doc_desc = "David R. Bolick, MD, FCAP";

	  $fname = get_user_meta($physician_id, 'first_name',true);
	  $lname = get_user_meta( $physician_id,'last_name',true );
	  $name = $fname['meta_value'].' '.$lname['meta_value'];
	  $add = get_user_meta($physician_id, '_address',true);
	  $mob = get_user_meta($physician_id, '_mobile',true);
	  $fax = get_user_meta($physician_id, 'fax',true);

	  $filecontent   = $pdfHeaderfile;
	  $pdfTotalBody  = $filecontent;
	  $replace_in_pdf = array(
	  	"{logo}" 			   => $logo,
	  	"{date}" 			   => date('m/d/Y'),
	  	"{patient_name}"	   => $specimen_results['p_firstname']." ".$specimen_results['p_lastname'],
	  	"{patient_phone}"	   => $specimen_results['patient_phn'],
	  	"{patient_dob}"		   => $specimen_results['patient_dob'],
	  	"{patient_col}"		   => $specimen_results['collection_date'],
	  	"{assessioning_num}"   => $specimen_results['assessioning_num'],
	  	"{patient_recive}"	   => date('m/d/Y', strtotime($datetime['0'])),
	  	"{physician_name}"	   => $name,
	  	"{physician_addresss}" => $add['meta_value'],
	  	"{physician_mobile}"   => $mob['meta_value'],
	  	"{physician_fax}"	   => $fax['meta_value'],
	  	"{clinical_history}"   => $clinical_history,
	  	"{site_indicator}"     => $specimen_results['site_indicator'],
	  	"{clinicial_info}"	   => $clinical_information,
	  	"{diagnostic}"		   => $diagnostic_text,
	  	"{Stains_desc_fst}"	   => nl2br($Stains_desc_one),
	  	"{Stains_desc_sec}"	   => nl2br($Stains_desc_sec),
	  	"{Stains_desc_thr}"    => nl2br($Stains_desc_thr),
	  	"{addendum}"		   => $addendum_pdf_content,
	  	"{gross_desc}"		   => $gross_description,
	  	"{nail_fungal_img}"	   => $report_img,
	  	"{lab_doc_desc}"	   => $lab_doc_desc,
	  	"{sign_img}"		   => $logoddd_img,
	  	"{diagnostic_color}"   => $diagnostic_color,
	  	"{gross_micro_description}"=> $gross_micro_desc_text,
	  	"{stains_comments}"	   => nl2br($stains_comments),
	  	"{comments}"		   => nl2br($comments),
	  	"{time}"			   => date("H:i"),
	  	"{fax}"                => $partner_fax,
	  	"{address}"            => nl2br($partner_address),
	  	"{phone}"              => $partner_phn,
	  	"{partner_company}"    => $partner_company,
	  	"{partner_footer_data}"=> "",
	  	"{assigned_sign_img}"  => $logoddd_img,
	  	/*"{partner_footer_data}"=> $partner_footer_data*/
	  );
	  //die( json_encode(array('status'=>'test')));
	  foreach($replace_in_pdf as $find => $replace){
	  $pdfTotalBody = str_replace($find, $replace, $pdfTotalBody);
	  }

	  $archivePdf = new mPDF('','A4','','', 0, 0, 0, 0, 0, 0); 
	  $archivePdf->WriteHTML($pdfTotalBody,2);
	  $accessioning_num = $specimen_results['assessioning_num'];
	  
	  $p_Name = preg_replace('/\s+/', '_', $specimen_results['p_lastname']);
	  $l_name = get_user_meta($physician_id,'last_name',true );
	  $ability_pdf_string = $p_Name.'_'.$l_name['meta_value'].'_'.$accessioning_num.".pdf";
	  
	  $ability_pdf = str_replace(' ', '', $ability_pdf_string);
	  $archivePdf->Output(FCPATH."assets/uploads/histo_report_pdf/".$ability_pdf,"F");
	  $current_date = date('Y-m-d H:i:s');
	 // die( json_encode(array('status'=>$ability_pdf)));
	  $insert_arr = array(
	  		'lab_id'               =>  $user_id,
	  		'specimen_id'          =>  $specimen_results['id'],
	  		'clinical_history'     =>  $clinical_history,
	  		'diagnostic_short_code'=>  $diagnostic_short_code,
	  		'addendum'             =>  implode(",",$addendum_code),
	      'addendum_desc'        =>  $ser_value,
	  		'gross_description'    =>  $gross_description,
	      'total_description'    =>  $GrossDescription['total_desc'],
	  		'stains'               =>  join(",",$Stain_shortcodes),
	  		'images'               =>  $filename_new,
	  		'dor'                  =>  $current_date,
	  		'nail_pdf'             =>  $ability_pdf,
	  		'signature_id'         =>  $labdoc,
	  		'create_date'          =>  $current_date,
	  		'modify_date'          =>  $current_date
	  	);
	  $insert_nail_fungal = $this->db->insert('wp_abd_nail_pathology_report',$insert_arr);

	  	die( json_encode(array('status'=>'1')));
		 }
	}
	
	}

	function get_stains_desc($info){
		 $result =  $this->db->query("select `text` from `wp_abd_nail_macro_codes` where sc = '".$info."'" )->row_array();  
		 return $result;
	}

	function get_pending_report_details(){
		$data = json_decode(file_get_contents('php://input'), true);
		if(!empty($data)){
		$sid= $data['id'];
		$addendum_val = array();
		$pending_report_sql = "SELECT * FROM `wp_abd_nail_pathology_report` WHERE `nail_funagl_id` = '".$sid."'";
		$pending_report_view_details = $this->BlankModel->customquery($pending_report_sql);	
		$pending_report_view_details[0]['dor'] 	= date('m/d/Y', strtotime($pending_report_view_details[0]['dor']));
		$pending_report_view_details[0]['addendum'] 	= explode(',',$pending_report_view_details[0]['addendum']);
		if($pending_report_view_details[0]['modify_date'] < '2018-06-12 00:00:00') 
		{
			$addendum_val = $pending_report_view_details[0]['addendum'];
			$old_date = true;
			$new_date = "";
		}
		else
		{
				 
			$addendum_ser_val = unserialize($pending_report_view_details[0]['addendum_desc']);
			if(!empty($addendum_ser_val[0]))
			{
				$addendum_val = $addendum_ser_val;
				$new_date = true;
				$old_date = "";
			}
			else if(!empty($pending_report_view_details[0]['addendum']))
			{ 
				$new_date = true;
				$old_date = "";
				$nail_addendum = $pending_report_view_details[0]['addendum'];
				$addendum_codes = explode(",",$nail_addendum);
				foreach($addendum_codes as $addendum_code)
				{	
					$addendum_desc = $this->get_stains_desc($addendum_code);
					$text_info =  array( 'text' => $addendum_desc);
					array_push($addendum_val, $text_info);
				}
			}
		}
		$clinical_information_sql = "SELECT clinical_specimen FROM `wp_abd_clinical_info` WHERE specimen_id = '".$pending_report_view_details[0]['specimen_id']."'";
	 	$clinical_information = $this->BlankModel->customquery($clinical_information_sql);		
	    $clinical_specimen_fst = $clinical_information[0]['clinical_specimen'];
	     if($clinical_specimen_fst){
		  $clinical_specimen_fst = rtrim($clinical_specimen_fst, ',');	       
	      $clinical_specimen_det = str_replace(',', '  ',$clinical_specimen_fst);
	    }	   
	    
	     $specimen_sql = "SELECT * FROM `wp_abd_specimen` WHERE `status` = '0' AND `physician_accepct` ='0' AND `qc_check`='0' AND id = '".$pending_report_view_details[0]['specimen_id']."' ORDER BY `id` DESC LIMIT 1";
		 $specimen_information_result =  $this->BlankModel->customquery($specimen_sql);	
	  
	     $specimen_information['timestamp']       =  $specimen_information_result[0]['date_received'];
	     $specimen_information['physician_id']    =  $specimen_information_result[0]['physician_id'];
	     $specimen_information['accessioning_num']=  $specimen_information_result[0]['assessioning_num'];
	     $specimen_information['site_indicator']  =  $specimen_information_result[0]['site_indicator']; 
		 $specimen_information['patient_name']  =  $specimen_information_result[0]['p_firstname']." ".$specimen_information_result[0]['p_lastname']; 
		 $specimen_information['patient_phn']  =  $specimen_information_result[0]['patient_phn']; 
		 $specimen_information['patient_dob']  =  $specimen_information_result[0]['patient_dob']; 
		 $specimen_information['collection_date']  =  $specimen_information_result[0]['collection_date']; 
		 $specimen_information['date_received']  =  $specimen_information_result[0]['date_received'];
		 $specimen_information['site_indicator']  =  $specimen_information_result[0]['site_indicator']; 
		 
		$first_name = get_user_meta( $specimen_information_result[0]['physician_id'], 'first_name');				
		$last_name  = get_user_meta( $specimen_information_result[0]['physician_id'], 'last_name');
		$mobile  = get_user_meta( $specimen_information_result[0]['physician_id'], '_mobile');
		$address  = get_user_meta( $specimen_information_result[0]['physician_id'], '_address');
		$fax  = get_user_meta( $specimen_information_result[0]['physician_id'], 'fax');

		$fname = $first_name['meta_value'];
		$lname = $last_name['meta_value'];
		$mob  = $mobile['meta_value'];
		$add = $address['meta_value'];	
		$fax = $fax['meta_value'];

		$physicain_info= array('name'=> $fname." ".$lname, 'mob'=> $mob, 'add'=> $add, 'fax'=>$fax);

	     $specimen_stage_details_sql = "SELECT `addtional_desc`, `comments` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` = '".$pending_report_view_details[0]['specimen_id']."' AND`stage_id` = '2' ";
	    
	     $specimen_stage_details = $this->BlankModel->customquery($specimen_stage_details_sql);	     
	     $gross_description = $specimen_stage_details[0]['addtional_desc'];

	     $diagnostic_short_code_sql = "SELECT `color`, `diagnosis`,`text`, `sc` FROM `wp_abd_nail_macro_codes` where `sc` = '".$pending_report_view_details[0]['diagnostic_short_code']."'";
		 $diagnosis = $this->BlankModel->customquery($diagnostic_short_code_sql);	
		 
		 if(!empty($pending_report_view_details[0]['stains']))
		 {
			$stains = explode(",",$pending_report_view_details[0]['stains']);
			if(!empty($stains[0]))
			{
			  $stv1= $stains[0];
			}
			else
			{
			   $stv1= "";
			}
			if(!empty($stains[1]))
			{
			  $stv2= $stains[1];
			}
			else
			{
				$stv2= "";
			}
			if(!empty($stains[2]))
			{
			  $stv3= $stains[2];
			}
			else
			{
				$stv3= "";
			}
			if(!empty($stains[0]))
			{
			   $Stains_desc1 = $this->get_stains_desc($stains[0]);
			}
			else
			{
			   $Stains_desc1 = "";
			}
			if(!empty($stains[1]))
			{
			   $Stains_desc2 = $this->get_stains_desc($stains[1]);
			}
			else
			{
			   $Stains_desc2 = "";
			}
			if(!empty($stains[2]))
			{
			   $Stains_desc3 = $this->get_stains_desc($stains[2]);
			}
			else
			{
			   $Stains_desc3 = "" ;
			}
		 }
		
	    
		if($pending_report_view_details)
		{
			 die( json_encode(array("status" =>'1', "pending_report_view_details" => $pending_report_view_details[0], 
			 "clinician_location" => $clinical_specimen_det, "specimen_results" => $specimen_information, 
			 "gross_description" => $gross_description, "diagnostic_short_code" => $diagnosis[0],'physician_info'=>$physicain_info,
			 "st1val"=>$stv1, "st2val"=>$stv2, "st3val"=>$stv3,"st1"=>$Stains_desc1['text'],"st2"=>$Stains_desc2['text'],"st3"=>$Stains_desc3['text'],"addendum_val"=>$addendum_val,
			"old_date"=>$old_date,"new_date"=>$new_date)));	
	    }
		else
		{
			 die( json_encode(array('status' =>'0')));		 
		}
		}
	}

	function edit_report_data(){
	 	if($this->input->post()){
		 	$addendum_code = array();
		 	$addendum_text = array();
		 	$name = $this->input->post('diagnostic_text');
		 	$id = $this->input->post('id');
		 	$addendum = json_decode($this->input->post('addendum'));
		 	$clinical_history = $this->input->post('clinical_history');
		 	$diagnostic_color = $this->input->post('diagnostic_color');
		 	$diagnostic_short_code = $this->input->post('diagnostic_short_code');
		 	$diagnostic_text = $this->input->post('diagnostic_text');
		 	$gross_description = $this->input->post('gross_description');
		 	$gross_micro_desc_text = $this->input->post('gross_micro_desc_text');
		 	$stains_addendum_first = $this->input->post('stains_addendum_first');
		 	$stains_addendum_sec = $this->input->post('stains_addendum_sec');
		 	$stains_addendum_third = $this->input->post('stains_addendum_third');
		 	$stains_first = $this->input->post('stains_first');
		 	$stains_sec = $this->input->post('stains_sec');
		 	$stains_third = $this->input->post('stains_third');
		 	$user_id = $this->input->post('user_id');
		 	
		 	$nail_report = "SELECT * FROM wp_abd_nail_pathology_report WHERE nail_funagl_id = '".$id."'";
	  		$nail_results = $this->db->query($nail_report)->row_array();

		 	$specimen_det      		= "SELECT * FROM wp_abd_specimen WHERE `status` = '0' AND `physician_accepct` ='0' AND `qc_check`='0' AND id ='".$nail_results['specimen_id']."' ORDER BY `id` DESC LIMIT 1";
			$specimen_results  		= $this->db->query($specimen_det)->row_array();
			$timestamp       		= $specimen_results['create_date'];
			$datetime        		= explode(" ",$timestamp);
			$clinical_information   = $this->clinical_function_html($nail_results['specimen_id']);
			$specimen_stage_details = "SELECT `addtional_desc`,`comments`,`total_desc` FROM wp_abd_specimen_stage_details WHERE `specimen_id`= '".$nail_results['specimen_id']."' AND`stage_id`=2";
			$GrossDescription       = $this->db->query($specimen_stage_details)->row_array();

		  	if(!empty($addendum)){
		 		foreach ($addendum as $value) {
			 		array_push($addendum_code, $value->addendum_code);
			 		array_push($addendum_text, $value->addendum_text);
			 	}
		 	}
		 	
		 	$ser_value = serialize($addendum_text);

		 	if($_FILES && $_FILES['file_data']['name']){
				$config['upload_path'] = 'assets/uploads/nail_fungal';
				$config['allowed_types'] = 'jpeg|jpg|png';
				$config['max_size'] = 5000000;
				$new_name = time().'_'.$_FILES["file_data"]['name'];
				$config['file_name'] = $new_name;

				$this->load->library('upload',$config);
				if(!$this->upload->do_upload('file_data')){
					die( json_encode(array( "status" => '0', 'message'=>$this->upload->display_errors())));
				}else{
					$uploadData = $this->upload->data();
					$fileName = $uploadData['file_name'];
				}
			}

		  if($diagnostic_color == "green"){
		  	$diagnostic_color = "#96faa2";
		  }
		  elseif($diagnostic_color == "red"){
		  	$diagnostic_color = "#e53131";
		  }
		  else{
		  	$diagnostic_color = "#f1ec2d";
		  }
		  $filename_new = $fileName;
		  
		  $array_val = count(array_filter($addendum_text));
		  if(0 < $array_val)
		  {
		  $addendum_value = unserialize($ser_value); 	
		  $addendum_pdf_content = "";
		  $addendum_pdf_content .="<div style='margin:0 0 5px 0;'><div style='width:22%; padding:0 1% 0 0;float:left; margin:0;color:#444;font:400 12px Arial,Helvetica,sans-serif'><label>Addendum:</label></div>";
		  
		  foreach($addendum_value as $value)
		  {
		  $addendum_pdf_content .= "<div style='width:70%; padding:0;float:left; color:#444;font:400 12px Arial,Helvetica,sans-serif;margin-left:170px;'>".$value."</div>";
		  }
		  $addendum_pdf_content .="<div style='clear:both;'></div></div>"; 
		  }
	  
	 
		  if($filename_new){
		  $report_img = "<div style='margin:0 0 5px 0;'><div style='width:22%; padding:0 1% 0 0;float:left; margin:0;color:#444;font:400 12px Arial,Helvetica,sans-serif'>
		  <label>&nbsp;</label></div>
		  <div style='width:77%; padding:0;float:left; margin:0;color:#444;font:400 12px Arial,Helvetica,sans-serif'>
		  <img style='max-height:140px;' src=".base_url().'assets/uploads/nail_fungal/'.$filename_new." alt='No Image'/>
		  </div>
		  <div style='clear:both;'></div></div>";
		  }

		  $Stain_shortcodes = array();
		  array_push($Stain_shortcodes, $stains_first);
		  array_push($Stain_shortcodes, $stains_sec);
		  array_push($Stain_shortcodes, $stains_third);
		  $Stains_desc = $this->get_stains_desc($stains_first); 
		  $Stains_desc_one = $Stains_desc['text'];
		  $Stains_desc = $this->get_stains_desc($stains_sec); 
		  $Stains_desc_sec = $Stains_desc['text'];
		  $Stains_desc = $this->get_stains_desc($stains_third); 
		  $Stains_desc_thr = $Stains_desc['text'];
		  $stains_comments = "";
	  
	  for($num = 0; $num < 3; $num++){
	  	$Stain_short = "SELECT `comments`, `id` FROM `wp_abd_nail_macro_codes` where sc = '".$Stain_shortcodes[$num]."'";
	  	$Stain_shortcode = $this->db->query($Stain_short)->row_array();
	  	$stains_comments.= $Stain_shortcode['comments'];
	  	$stains_comments.=  '<div style= "margin:2px;"></div>';
	  }
	  $nail_macro_sql = "SELECT `comments`, `id` FROM `wp_abd_nail_macro_codes` where sc = '" . $diagnostic_short_code . "'";
	  $nail_macro     = $this->db->query($nail_macro_sql)->row_array();
	  $comments = nl2br($nail_macro['comments']);
	  
	  $physician_id = "'".$specimen_results['physician_id']."'";
	  
	  $assigned_partner_meta =  get_user_meta($physician_id, '_assign_partner',true);
	  
	  $assigned_partner = $assigned_partner_meta['meta_value'];
	  
	  if(!empty($assigned_partner)){
	  	$pdfHeaderfile       = $this->load->view('nail_fungal_partner','',true);
		$logo_img            = get_user_meta($assigned_partner,'company_logo',true);
		$partner_address     = get_user_meta($assigned_partner, '_address',true);
		$partner_phn         = get_user_meta($assigned_partner, '_mobile', true);
		$partner_fax         = get_user_meta($assigned_partner, 'fax',     true);
		$partner_company     = get_user_meta($assigned_partner, 'first_name',true);
		$logo = $logo_img['meta_value'];
		$paddress = $partner_address['meta_value'];
		$pph = $partner_phn['meta_value'];
		$pfax = $partner_fax['meta_value'];
		$pcompany = $partner_company['meta_value'];
	  }
	  else{
	  	$pdfHeaderfile = $this->load->view('nail_fungal','',true);
		$logo = base_url()."assets/frontend/images/logo.png";
	  }
	  
	  
	  	$logoddd_img = base_url()."assets/frontend/images/davidbolick_sign.jpg";
		$lab_doc_desc = "David R. Bolick, MD, FCAP";

	  $fname = get_user_meta($physician_id, 'first_name',true);
	  $lname = get_user_meta( $physician_id,'last_name',true );
	  $name = $fname['meta_value'].' '.$lname['meta_value'];
	  $add = get_user_meta($physician_id, '_address',true);
	  $mob = get_user_meta($physician_id, '_mobile',true);
	  $fax = get_user_meta($physician_id, 'fax',true);

	  $filecontent   = $pdfHeaderfile;
	  $pdfTotalBody  = $filecontent;
	  $replace_in_pdf = array(
	  	"{logo}" 			   => $logo,
	  	"{date}" 			   => date('m/d/Y'),
	  	"{patient_name}"	   => $specimen_results['p_firstname']." ".$specimen_results['p_lastname'],
	  	"{patient_phone}"	   => $specimen_results['patient_phn'],
	  	"{patient_dob}"		   => $specimen_results['patient_dob'],
	  	"{patient_col}"		   => $specimen_results['collection_date'],
	  	"{assessioning_num}"   => $specimen_results['assessioning_num'],
	  	"{patient_recive}"	   => date('m/d/Y', strtotime($datetime['0'])),
	  	"{physician_name}"	   => $name,
	  	"{physician_addresss}" => $add['meta_value'],
	  	"{physician_mobile}"   => $mob['meta_value'],
	  	"{physician_fax}"	   => $fax['meta_value'],
	  	"{clinical_history}"   => $clinical_history,
	  	"{site_indicator}"     => $specimen_results['site_indicator'],
	  	"{clinicial_info}"	   => $clinical_information,
	  	"{diagnostic}"		   => $diagnostic_text,
	  	"{Stains_desc_fst}"	   => nl2br($Stains_desc_one),
	  	"{Stains_desc_sec}"	   => nl2br($Stains_desc_sec),
	  	"{Stains_desc_thr}"    => nl2br($Stains_desc_thr),
	  	"{addendum}"		   => $addendum_pdf_content,
	  	"{gross_desc}"		   => $gross_description,
	  	"{nail_fungal_img}"	   => $report_img,
	  	"{lab_doc_desc}"	   => $lab_doc_desc,
	  	"{sign_img}"		   => $logoddd_img,
	  	"{diagnostic_color}"   => $diagnostic_color,
	  	"{gross_micro_description}"=> $gross_micro_desc_text,
	  	"{stains_comments}"	   => nl2br($stains_comments),
	  	"{comments}"		   => nl2br($comments),
	  	"{time}"			   => date("H:i"),
	  	"{fax}"                => $partner_fax,
	  	"{address}"            => nl2br($partner_address),
	  	"{phone}"              => $partner_phn,
	  	"{partner_company}"    => $partner_company,
	  	"{partner_footer_data}"=> "",
	  	"{assigned_sign_img}"  => $logoddd_img,
	  	/*"{partner_footer_data}"=> $partner_footer_data*/
	  );
	  //die( json_encode(array('status'=>'test')));
	  foreach($replace_in_pdf as $find => $replace){
	  $pdfTotalBody = str_replace($find, $replace, $pdfTotalBody);
	  }

	  $archivePdf = new mPDF('','A4','','', 0, 0, 0, 0, 0, 0); 
	  $archivePdf->WriteHTML($pdfTotalBody,2);
	  $accessioning_num = $specimen_results['assessioning_num'];
	  
	  $p_Name = preg_replace('/\s+/', '_', $specimen_results['p_lastname']);
	  $l_name = get_user_meta($physician_id,'last_name',true );
	  $ability_pdf_string = $p_Name.'_'.$l_name['meta_value'].'_'.$accessioning_num.".pdf";
	  $ability_pdf = str_replace(' ', '', $ability_pdf_string);
	  $archivePdf->Output(FCPATH."assets/uploads/histo_report_pdf/".$ability_pdf,"F");
	  $current_date = date('Y-m-d H:i:s');
	 // die( json_encode(array('status'=>$ability_pdf)));
	 
	  		$insert_arr['lab_id'] =  $user_id;
	  		$insert_arr['clinical_history'] =  $clinical_history;
	  		$insert_arr['diagnostic_short_code']= $diagnostic_short_code;
	  		$insert_arr['addendum'] =  implode(",",$addendum_code);
	      	$insert_arr['addendum_desc']  =  $ser_value;
	  		$insert_arr['gross_description'] =  $gross_description;
	      	$insert_arr['total_description'] =  $GrossDescription['total_desc'];
	  		$insert_arr['stains'] =  join(",",$Stain_shortcodes);
	  		if(!empty($filename_new)){
	  			$insert_arr['images'] =  $filename_new;
	  		}
	  		$insert_arr['dor'] =  $current_date;
	  		$insert_arr['nail_pdf']  =  $ability_pdf;
	  		$insert_arr['signature_id'] =  $labdoc;
	  		$insert_arr['modify_date'] =  $current_date;

	  $this->db->where('nail_funagl_id',$id);
	  $insert_nail_fungal = $this->db->update('wp_abd_nail_pathology_report',$insert_arr);

	  	die( json_encode(array('status'=>$nail_results)));
		 }
	
	}


	
 }
