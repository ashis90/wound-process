<?php
if (!defined('BASEPATH'))
    EXIT("No direct script access allowed");
class PdfGenerate extends MY_Controller {

    function __construct() {
        parent::__construct();
	   $this->load->library('m_pdf');
	   date_default_timezone_set('MST7MDT');
    }
  
    function index()
    {
	  
    }
  
   /**
   * /
   * 
   * @List Delivery-to-extraction
   */   

	function view_pcr_report($accessioning_num=''){
		if(!empty($accessioning_num)){
  					/// Report Generate ///
			date_default_timezone_set('MST7MDT');	
			$accessioning_num_type = substr(strrchr($accessioning_num, '-'), 1);
			$specimen_data = "SELECT * FROM `wp_abd_specimen` WHERE `assessioning_num` LIKE '".$accessioning_num."'";
			$specimen_det = $this->db->query($specimen_data)->row_array();

			if($accessioning_num_type !='W'){
if(!empty($specimen_det['partners_company'])){
					$prtnr_id = $specimen_det['partners_company'];
					$spe_id = $specimen_det['id'];
					$partners_company_sql = 'SELECT *  FROM `wp_abd_partners_company` WHERE `partner_id` = '.$prtnr_id.'';
					$partners_company_data = $this->db->query($partners_company_sql)->row_array();

					$partners_specimen_sql = 'SELECT *  FROM `wp_abd_partners_specimen` WHERE `specimen_id` = '.$spe_id.'';
					$partners_specimen_data = $this->db->query($partners_specimen_sql)->row_array();

					$pdfLogo = base_url().'assets/uploads/partners/'.$partners_company_data["partner_logo"].'';
					$filecontent = $this->load->view('sign_pcr_report_nextgen','',true);
				}else{
					$pdfLogo = base_url()."assets/frontend/images/logo.png";
					
					$filecontent = $this->load->view('sign_pcr_report','',true);
				}
			}else{
				$filecontent = $this->load->view('sign_pcr_report_wounds','',true);
			}
			$pdfTotalBody = $filecontent;
			$specimen_data = "SELECT * FROM `wp_abd_specimen` WHERE `assessioning_num` LIKE '".$accessioning_num."'";
			$specimen_det = $this->db->query($specimen_data)->row_array();

			$clinical_data = "SELECT * FROM `wp_abd_clinical_info` WHERE `assessioning_num` LIKE '".$accessioning_num."'";
			$clinical_det = $this->db->query($clinical_data)->row_array();

			$additional_desc_sql= "SELECT * FROM `wp_abd_pcr_stage_details` WHERE `specimen_id` LIKE '".$specimen_det['id']."' AND `stage_id`='1'";
			$additional_desc = $this->db->query($additional_desc_sql)->row_array();
			
			$pat_name= $specimen_det['p_firstname']." ".$specimen_det['p_lastname'];
			$physician_fname = get_user_meta($specimen_det['physician_id'], 'first_name', true);
			$physician_lname = get_user_meta($specimen_det['physician_id'], 'last_name', true);
			$physician_name = $physician_fname['meta_value']." ".$physician_lname['meta_value'];
			$physician_phone = get_user_meta($specimen_det['physician_id'], '_mobile', true);
			$physician_address = get_user_meta($specimen_det['physician_id'], '_address', true);
			$physician_fax = get_user_meta($specimen_det['physician_id'], 'fax', true);
			
			$import_data = "SELECT DISTINCT `target_name`,`positive_negtaive` FROM `wp_abd_import_data` WHERE `accessioning_num` LIKE '".$accessioning_num."'  AND `target_name` NOT IN ('Xeno_Ac00010014_a1','Ac00010014_a1') order by `positive_negtaive` desc";
			$report_det = $this->db->query($import_data)->result_array();
			$table_data = "";
			
			$table_data.= "<table style='width:70%; border: 1px solid black; border-collapse: collapse;padding: 4px;font-size: 11px;'>";
			
			$albicans_yest = "";
			$parapsilosis_yest = "";
			foreach($report_det as $report_taxon)
			{
				
				$name = explode("_",$report_taxon['target_name']);	
				if(count($name)==1) 
				{
					$target_name = $name[0]; 
				}
				else if(count($name)==2)	
				{
					$target_name = $name[1];
				}
				else if(count($name)==3)
				{
					$target_name = trim($name[1])."_".trim($name[2]);
				}
				
				if($report_taxon['target_name'] == "Pa04230908_s1" || $report_taxon['target_name'] == "MRSA_Pa04230908_s1" )
				{

				   $target_name = "Pa04230908_s1";
				}
			
				if($report_taxon['target_name'] == "AI6RPZ6" || $report_taxon['target_name'] == "Alternaria_AI6RPZ6" || $report_taxon['target_name'] == "Alternaria_APAACDX"){

				   $target_name = "APAACDX";
				}
				
				if($report_taxon['target_name'] == 'C. albicans_Fn04646233_s1' && $report_taxon['positive_negtaive'] == 'negative') {
				   $target_name = "";	
				   $albicans_yest ="";				
				}			
					

				if($report_taxon['target_name'] == 'C. albicans_Fn04646233_s1' && $report_taxon['positive_negtaive'] == 'positive') {
				   $target_name = 'Fn04646233_s1';	
				   $albicans_yest="yes";				
				}			
				
				if($report_taxon['target_name'] == 'C. parapsilosis_Fn04646221_s1' && $report_taxon['positive_negtaive'] == 'negative')   {
				  $target_name = ""; 
				  $parapsilosis_yest ="";						
				}
				
				
				if($report_taxon['target_name'] == 'C. parapsilosis_Fn04646221_s1' && $report_taxon['positive_negtaive'] == 'positive')   {
				  $target_name = 'Fn04646221_s1'; 
				  $parapsilosis_yest ="yes";						
				}
				
				
			    $taxon_data = "SELECT `taxon` FROM `wp_abd_asses_genes` WHERE `assay_name` LIKE '".$target_name."'";
				$taxon_det = $this->db->query($taxon_data)->row_array();
				
				if($taxon_det){
			
				if($report_taxon['positive_negtaive']=="positive")
				{
				$sty = 'font-family:Arial, Helvetica, sans-serif;font-size: 11px; border: 1px solid black; border-collapse: collapse; padding: 4px;font-size: 11px; color: red;';
				}
				else
				{
				$sty = 'font-family:Arial, Helvetica, sans-serif;font-size: 11px; border: 1px solid black; border-collapse: collapse; padding: 4px;font-size: 11px;';
				}
				$table_data.= "<tr>
				<td style='font-family:Arial, Helvetica, sans-serif;font-size: 11px; border: 1px solid black; border-collapse: collapse; padding: 4px;font-size: 11px;'>".$taxon_det['taxon']."</td>
			 	<td style='".$sty."'>".$report_taxon['positive_negtaive']."</td>
			    </tr>";
			    }
			}
			
			$yest_text = "";
			
			if($parapsilosis_yest == "yes" || $albicans_yest == "yes"){
			   $yest_text = "<div><h2 style='margin-bottom:1px;font-family:Arial, Helvetica, sans-serif;font-size: 11px;'><span style='border-bottom: 2px solid #000;
margin-top: 0;'>Molecular Interpretation of Yeast:</span></h2></div> 
			<p style='margin:0;margin-top:5px;font-size:11px;'>A positive result for Candida albicans and/or Candida parapsilosis has a high specificity for the detection of the target organism. A negative result for Candida albicans and/or Candida parapsilosis does not have a sufficient negative predictive value to justify reporting negative results.</p>";	
			}
					
			$val = explode(",",$clinical_det['clinical_specimen']);
			$new_val = array();
			foreach($val as $k=>$v)
			{ 
				if(!empty($v))	
				{	
					array_push($new_val,$v);
				}
			}
			$tot_val = implode(",",$new_val);
			$table_data.= "</table>";
			$find=array();
			$find[]="{logo}";
			$find[]="{signature}";
			$find[]="{date}";
			$find[]="{acc_num}";
			$find[]="{p_name}";
			$find[]="{p_phone}";
			$find[]="{p_dob}";
			$find[]="{p_col_date}";
			$find[]="{p_rec_date}";
			$find[]="{phy_name}";
			$find[]="{phy_phone}";
			$find[]="{phy_address}";
			$find[]="{phy_fax}";
			$find[]="{table_data}";
			$find[]="{addi_desc}";
			$find[]="{site_indicator}";
			$find[]="{location}";
			$find[]="{date_generate}";
			$find[]="{time_report}";
			if(!empty($specimen_det['partners_company'])){
				$find[]="{partner_name}";
				$find[]="{address_line1}";
				$find[]="{address_line2}";
				$find[]="{partner_phone}";
				$find[]="{partner_fax}";
				$find[]="{partner_cli}";
				$find[]="{partner_lab_doc}";
				$find[]="{patient_id}";
			}
			$find[]="{yeast}";
			
			$replace=array();
			$replace[] = $pdfLogo;
			$replace[] = base_url()."assets/frontend/images/pdf-ft.jpg";
			$replace[] = date('m-d-Y');
			$replace[] = $accessioning_num;
			$replace[] = $pat_name;
			$replace[] = $specimen_det['patient_phn'];
			$replace[] = $specimen_det['patient_dob'];
			$replace[] = $specimen_det['collection_date'];
			$replace[] = $specimen_det['date_received'];
			if(!empty($specimen_det['partners_company'])){
				$replace[] = $partners_specimen_data['physician_name'];
				$replace[] = $partners_specimen_data['physician_phone'];
				$replace[] = $partners_specimen_data['physician_address'];
				$replace[] = '';
			}else{
				$replace[] = $physician_name;
				$replace[] = $physician_phone['meta_value'];
				$replace[] = $physician_address['meta_value'];
				$replace[] = $physician_fax['meta_value'];
			}
			$replace[] = $table_data;
			$replace[] = $additional_desc['addtional_desc'];
			$replace[] = $specimen_det['site_indicator'];
			$replace[] = $tot_val;
			$replace[] = date('m/d/Y');
			$replace[] = date("H:i");
			if(!empty($specimen_det['partners_company'])){
				$replace[] = $partners_company_data['partner_name'];
				$replace[] = $partners_company_data['partner_address1'];
				$replace[] = $partners_company_data['partner_address2'];
				$replace[] = $partners_company_data['phone_no'];
				$replace[] = $partners_company_data['fax_no'];
				$replace[] = $partners_company_data['clia_no'];
				$replace[] = str_ireplace('<p>','',$partners_company_data['lab_director']);
				$replace[] = $partners_specimen_data['external_id'];
			}
			$replace[] = $yest_text;
			for($data_int=0;$data_int<count($find);$data_int++){ 
				$pdfTotalBody = str_replace($find[$data_int],$replace[$data_int],$pdfTotalBody); 
			}
			$body = $pdfTotalBody;
			$message = $body;
			$this->m_pdf->pdf->WriteHTML($message);
			$ability_pdf = "pcr_report_".$accessioning_num.".pdf";
			//$this->m_pdf->pdf->Output(FCPATH."assets\uploads\pcr_report_pdf\/".$ability_pdf,"I");
			$this->m_pdf->pdf->Output(FCPATH."assets/uploads/pcr_report_pdf/".$ability_pdf,"I");
					

			 }	
		
	}

	function genarate_add_specimen_pdf(){
	$filecontent = $this->load->view('add_specimen','',true);
	$pdfTotalBody = $filecontent;
	$find=array();
	$find[]="{logo}";
	$find[]="{foot_pic}";
	$replace=array();
	$replace[] = base_url()."assets/frontend/images/logo.png";
	$replace[] = base_url()."assets/frontend/images/pdf-ft.jpg";
	for($data_int=0;$data_int<count($find);$data_int++){ 
		$pdfTotalBody = str_replace($find[$data_int],$replace[$data_int],$pdfTotalBody); 
	}
	$body = $pdfTotalBody;
	$message = $body;
	$this->m_pdf->pdf->WriteHTML($message);
	$ability_pdf = "ability_specimen_pdf.pdf";
	$this->m_pdf->pdf->Output(FCPATH."assets\uploads\add_specimen_pdf\/".$ability_pdf,"I");

}
 
 }
