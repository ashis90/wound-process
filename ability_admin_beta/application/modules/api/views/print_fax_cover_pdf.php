<div style="width:90%; margin:0 auto; padding-top:10%;">
  <div style="list-style:none; margin-bottom:3%; width:100%; text-align: center;"><img src="{logo}"></div>
    <div style="width:45%; float:left; padding-top:-17px; ">
      <h2>FAX</h2>
    </div>
      <div style="width:45%; float:right; ">
        <div style="text-align:right; list-style:none; font-size:13px;">858 S Auto Mall Drive, Suite 102</div>
        <div style="text-align:right; list-style:none; font-size:13px;">American Fork, UT 84003</div>
        <div style="text-align:right; list-style:none; font-size:13px;"> Phone 801-899-3828</div>
        <div style="text-align:right; list-style:none; font-size:13px;"> Fax 801-855-7548</div>
      </div>
  
    
    <div class="clear"></div>
    
  <div style="width:100%; float:left; margin-top:3%;"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody>
          <tr>
            <td style="border: #000 solid 1px; width: 50px; height: 15px; padding: 10px 45px 10px 10px; font-size: 13px;">To:</td>  
            <td style="border: #000 solid 1px;  height: 15px; padding: 10px 80px 10px 10px; font-size: 13px;">{physician_name}</td> 
            <td style="border: #000 solid 1px; width: 50px; height: 15px; padding: 10px 45px 10px 10px; font-size: 13px;">From:</td>
            <td style="border: #000 solid 1px;  height: 15px; padding: 10px 80px 10px 10px; font-size: 13px;">Ability Diagnostics, LLC</td>
          </tr>     
        </tbody>
          <tbody>
          <tr>
            <td style="border: #000 solid 1px; width: 50px; height: 15px; padding: 10px 45px 10px 10px; font-size: 13px;">Fax:</td> 
            <td style="border: #000 solid 1px;  height: 15px; padding: 10px 80px 10px 10px; font-size: 13px;">{physician_fax}</td>  
            <td style="border: #000 solid 1px; width: 50px; height: 15px; padding: 10px 45px 10px 10px; font-size: 13px;">Fax:</td>
            <td style="border: #000 solid 1px;  height: 15px; padding: 10px 80px 10px 10px; font-size: 13px;">801-855-7548</td>
          </tr>     
        </tbody>
        
        <tbody>
          <tr>
            <td style="border: #000 solid 1px; width: 50px; height: 15px; padding: 10px 45px 10px 10px; font-size: 13px;">Phone:</td> 
            <td style="border: #000 solid 1px;  height: 15px; padding: 10px 80px 10px 10px; font-size: 13px;">{physician_mobile}</td> 
            <td style="border: #000 solid 1px; width: 50px; height: 15px; padding: 10px 45px 10px 10px; font-size: 13px;">Phone:</td>
            <td style="border: #000 solid 1px;  height: 15px; padding: 10px 80px 10px 10px; font-size: 13px;">801-642-0716</td>
          </tr>     
        </tbody>
        
        <tbody>
          <tr>
            <td style="border: #000 solid 1px; width: 50px; height: 15px; padding: 10px 45px 10px 10px; font-size: 13px;">Subject:</td> 
            <td style="border: #000 solid 1px;  height: 15px; padding: 10px 80px 10px 10px; font-size: 13px;">Pathology Report</td> 
            <td style="border: #000 solid 1px; width: 50px; height: 15px; padding: 10px 45px 10px 10px; font-size: 13px;">Date:</td>
            <td style="border: #000 solid 1px;  height: 15px; padding: 10px 80px 10px 10px; font-size: 13px;">{date}</td>
          </tr>     
        </tbody>
        
        <tbody>
          <tr>
            <td style="border: #000 solid 1px; width: 50px; height: 15px; padding: 10px 45px 10px 10px; font-size: 13px;">No. Pages:</td> 
            <td style="border: #000 solid 1px;  height: 15px; padding: 10px 80px 10px 10px; font-size: 13px;">1</td>  
            <td style="border: #000 solid 1px; width: 50px; height: 15px; padding: 10px 45px 10px 10px; font-size: 13px;"></td>
            <td style="border: #000 solid 1px;  height: 15px; padding: 10px 80px 10px 10px; font-size: 13px;"></td>
          </tr>     
        </tbody>
  <div class="clear"></div>
</table>

</div>
<div class="clear"></div>
          
<p style="font-size: 13px;">
    Comments:<br />
    A link to the color report will be sent to the email indicated in the onboarding document. To access a color copy of the report any time, visit the client portal at antaresphysicianservices.com and login using physician credentials by clicking “Portal Login” in the upper right hand corner.
  </p>
<p style="font-size: 13px; margin-bottom: 10px;">
We encourage you to visit the portal for color reports. The reports are color-coded to allow physicians to see the diagnoses at-a-glance.  Red means a positive diagnosis, yellow means caution and green or black means a negative diagnosis.
</p>
<p style="font-size: 13px; margin-bottom: 10px;">
Here are instructions for logging on:
</p>
<p style="font-size: 13px; margin-bottom: 10px;">
Portal Access:
</p>
<p style="font-size: 13px; margin-bottom: 10px;">
AntaresPhysicianServices.com – click ‘Portal Login’ in the upper right hand corner
</p>
<p style="font-size: 13px; margin-bottom: 10px;">
Username and Password – call 801-642-0716 with login questions
</p>
<p style="font-size: 13px; ">
Reports are accessible by new reports and archived reports<br />  
</p>
<p style="font-size: 13px; margin-top:30px;">
<strong>Note:</strong> Please send any faxes to 801-855-7548. The number this fax came from is outbound only.

</p>
  
 
<p style="font-size: 13px; margin-top:20px;">
<strong>Statement of Confidentiality</strong><br />
The information contained within this facsimile message is client privileged, and confidential, and is intended only for the use of the individual or entity named above. If the reader of this message IS NOT the intended recipient, you are hereby notified that any dissemination, distribution, or copying of this communication is strictly prohibited. If you have received this communication in error, please immediately notify us by telephone, and return the original message to us at the above address via the US Postal Service, or notify us that the documents have been destroyed. Thank you.
</p>
 </div>