<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Google Analytics
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin/dashboard');?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Google Analytics List</li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h4 class="title">Update Google Analytics</h4>
          </div>
          <form action="<?php echo base_url().'admin/google_analytics/googleAnalyticsEditing/';?>" method="POST" enctype="multipart/form-data" name="analytics_frm" id="analytics_frm">
            <input type="hidden" name="id" value="<?php echo $analyticsEditData['id'];  ?>">
            <div class="form-group">
              <label for="exampleTextarea"> Google Analytics Description </label>
              <textarea name="description" rows="3" cols="80" placeholder="Description" class="form-control"><?php echo $analyticsEditData['description']; ?></textarea>
              <?php echo form_error('description','<div class="text-danger">','</div>');  ?>

            </div>

            
            <div class="form-group">
              <label for="exampleInputEmail1">Type</label>
              <select name="type" class="form-control">
                <option value="Ability" <?php echo ($analyticsEditData['type'] =='Ability' ? 'selected' : '');?>>Ability</option>
                <option value="APS" <?php echo ($analyticsEditData['type'] =='APS' ? 'selected' : '');?>>APS</option>
              </select>
            </div>


            <div class="form-group">
              <label for="exampleInputEmail1">Status</label>
              <select name="is_active" class="form-control">
                <option value="Active" <?php echo ($analyticsEditData['is_active'] =='Active' ? 'selected' : '');?>>Active</option>
                <option value="Inactive" <?php echo ($analyticsEditData['is_active'] =='Inactive' ? 'selected' : '');?>>Inactive</option>
              </select>
            </div>
            
            <button type="submit" class="btn btn-primary ban-sbmt">Update Analytics</button>
          </form>
        </div>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
    <!-- /.row -->
  </section>
</div>

