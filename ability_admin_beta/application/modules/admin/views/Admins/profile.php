<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Update Admin Profile
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin/dashboard');?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Profile</li>
    </ol>
  </section>
  <?php
    if ($this->session->flashdata('profile')) {
  ?>
  <div class="alert alert-success alert-dismissable" role="alert">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <?php
      $message = $this->session->flashdata('profile');
      echo ($message);
      ?>
  </div>
  <?php
    }
  ?>

  <?php
    if ($this->session->flashdata('ntprofile')) {
  ?>
  <div class="alert alert-success alert-dismissable" role="alert">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <?php
      $message = $this->session->flashdata('ntprofile');
      echo ($message);
      ?>
  </div>
  <?php
    }
  ?>
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h4 class="title">Update Profile</h4>
          </div>
          <form action="<?php echo base_url().'admin/admins/updateProfile/';?>" method="POST" enctype="multipart/form-data" name="role_frm" id="role_frm">

            <input type="hidden" name="sysadm_id" class="form-control" value="<?php echo $result_admin[0]['sysadm_id']; ?>">


            <div class="form-group">
              <label for="exampleTextarea">First Name </label>
              <input type="text" name="sysadm_fname" class="form-control" value="<?php echo $result_admin[0]['sysadm_fname']; ?>">

              <?php echo form_error('sysadm_fname','<div class="text-danger">','</div>'); ?>

            </div>


            <div class="form-group">
              <label for="exampleTextarea">Last Name </label>
              <input type="text" name="sysadm_lname" class="form-control" value="<?php echo $result_admin[0]['sysadm_lname']; ?>">
              <?php echo form_error('sysadm_lname','<div class="text-danger">','</div>'); ?>

            </div>

            <div class="form-group">
              <label for="exampleTextarea">User Name </label>
              <input type="text" name="sysadm_login" class="form-control" value="<?php echo $result_admin[0]['sysadm_login']; ?>">
              <?php echo form_error('sysadm_login','<div class="text-danger">','</div>'); ?>

            </div>

            <div class="form-group">
              <label for="exampleTextarea">Email Id </label>
              <input type="email" name="sysadm_email" class="form-control" value="<?php echo $result_admin[0]['sysadm_email']; ?>">
              <?php echo form_error('sysadm_email','<div class="text-danger">','</div>'); ?>

            </div>
            

            <div class="form-group">
              <label for="exampleTextarea">New Password </label>
              <input type="password" id="new_password" class="form-control" name="new_password" placeholder="Enter New Password">
              
              <?php echo form_error('new_password','<div class="text-danger">','</div>'); ?>
            </div>


            <div class="form-group">
              <label for="exampleTextarea">Confirm Password </label>
              <input type="password" id="confirm_password" class="form-control" name="confirm_password" placeholder="Enter Confirm Password">
              <?php echo form_error('confirm_password','<div class="text-danger">','</div>'); ?>
            </div>
            
            <!-- <button type="submit" class="btn btn-info pull-right">Update</button> -->
            
            <button type="submit" class="btn btn-primary ban-sbmt">Update Profile</button>
          </form>
        </div>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
    <!-- /.row -->
  </section>
</div>

