<?php
  // foreach($histroList as $histro)
  //   {
  //     echo "['".$histro['create_date']."', ".$histro['histo_count'].", ".$histro['pcr_count']."],";
  //   }
  // exit();
?>
<html lang="en">
<head>

  <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"
         rel = "stylesheet">
      <script src = "https://code.jquery.com/jquery-1.10.2.js"></script>
      <script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

      <!-- Javascript -->
      <script>
         $(function() {
          <?php if(isset($_REQUEST['start_date'])){ ?>
            $( "#datepicker-1" ).datepicker();
          <?php }else{ ?>
            $( "#datepicker-1" ).datepicker().datepicker('setDate', 'today');
          <?php } ?>
          <?php if(isset($_REQUEST['end_date'])){ ?>
            $( "#datepicker-2" ).datepicker();
          <?php }else{ ?>
            $( "#datepicker-2" ).datepicker().datepicker('setDate', 'today');
          <?php } ?>
         });
      </script>
   

<?php if(!defined('BASEPATH')) EXIT("No direct script access allowed"); ?>
<!-- Content Wrapper. Contains page content -->
<?php
$args  = array('DPM'      => 'DPM',
      'PCP'               => 'PCP',
      'Ortho'             => 'Ortho',
      'Nurse Practitioner'=> 'Nurse Practitioner',
      'DO'           => 'DO',
      'Dermatologist'=> 'Dermatologist',
      'Geriatric'    => 'Geriatric',
      'Bariatric'    => 'Bariatric',
      'PA'           => 'PA',
      'OBGYN'        => 'OBGYN');
?>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);
google.charts.setOnLoadCallback(drawCharts);
google.charts.setOnLoadCallback(drawChartPcr);


function drawChart() {

    var data = google.visualization.arrayToDataTable([
                  ['Language', 'Rating'],
                   <?php  
                    foreach($args as $key => $specialties)
                    {
                      $physician_count = get_role_specialist('physician', $key);
                      $total_physician_count = count($physician_count);                    
                      echo "['".$key."', ".$total_physician_count."],";
                    }
                    ?>
   ]);
    
    var options = {
        title: 'Physicians Specializations Chart',
        height: 500,
        is3D: true,
        
    };
    
    var chart = new google.visualization.PieChart(document.getElementById('piechart'));
    
    chart.draw(data, options);
}

function drawCharts() {
    var data = google.visualization.arrayToDataTable([
                  ['Language', 'Rating'],
                  <?php  

                    foreach($result as $specialties)
                    {
                      echo "['".$specialties['test_type']."', ".$specialties['COUNT_TYPE']."],";
                    }
                  ?>
   ]);
    
    var options = {
        title: 'Specimen Chart',
        height: 500,
        is3D: true,
    };
    
    var chart = new google.visualization.PieChart(document.getElementById('piecharts'));
    
    chart.draw(data, options);
}

function drawChartPcr() {
    var data = google.visualization.arrayToDataTable([
      ['YEAR', 'HISTO','PCR'],
      // ['YEAR', 'HISTO'],
      
      <?php  
        foreach($histroList as $histro)
        {

           echo "['".$histro['create_date']."', ".$histro['histo_count'].", ".$histro['pcr_count']."],";
          // echo "['".$histro['create_date']."', ".$histro['histo_count']."],";

        }
      ?>

    ]);

    var options = {
      title: 'Report Chart',
      curveType: 'function',
      // legend: { position: 'bottom'},
      hAxis: {
          title: 'Time'
        },

        // vAxis:{viewWindow: {min: 0},
        //         title: 'No Of Specimen'},
        vAxis: {
          title: 'No Of Reports'
        },
        backgroundColor: '#fff',
        height: 500,

         axes: {
          x: {
            0: {side: 'top'}
          }
        }
    };

    var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

    chart.draw(data, options);
}



</script>
</head>
<style type="text/css">
 #piechart svg{width: 650px !important;
   } 

    div#piechart{
        background: #fff;width: 100%; 
    }


    #piecharts svg{width: 650px !important;
   } 

    div#piecharts{
        background: #fff;width: 100%; 
    }
</style>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Dashboard
      <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Dashboard</li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-aqua">
          <div class="inner">
            <h3> </h3>
            <p>Users List</p>
          </div>
          <div class="icon">
            <!-- <i class="ion ion-bag"></i> -->
            <i class="ion ion-person-add"></i>
          </div>
          <a href="<?php echo base_url('admin/user')?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <!-- ./col -->
      <div class="col-lg-3 col-xs-6">
        <!-- small box -->        
          <div class="small-box bg-green">
          <div class="inner">
            <h3><sup style="font-size: 20px"></sup></h3>
            <p>Services</p>
          </div>
          <div class="icon">
            <i class="ion ion-stats-bars"></i>
          </div>
          <a href="<?php echo base_url('admin/service')?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <!-- ./col -->
      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-yellow">
          <div class="inner">
            <h3></h3>
            <p>Pages</p>
          </div>
          <div class="icon">
            <i class="ion ion-bag"></i>
          </div>
          <a href="<?php echo base_url('admin/cms')?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <!-- ./col -->

      <!-- ./col -->
      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-blue">
          <div class="inner">
            <h3></h3>
            <p>Report Shortcodes</p>
          </div>
          <div class="icon">
            <i class="ion ion-stats-bars"></i>
          </div>
          <a href="<?php echo base_url('admin/report')?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <!-- ./col -->
      <div class="clearfix"></div>
      <div>
        <div class="row  no-margin-bottom">
           <div class="col-sm-6 col-xs-12">
              <div class="card card-success">
                    <div class="card-header">
                        <div class="card-title">
                            <div class="title"><i class="fa fa-user"></i> Last 5 Registered Users</div>
                        </div>
                        <div class="clear-both"></div>
                    </div>
                    <div class="card-body no-padding">
                    <?php 
      //print_r($latestuserList);
    ?>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <!-- <th>#</th> -->
                                    <th>Name</th>
                                    <th>Email Id</th>
                                    <th>Last Registration</th>
                                </tr>
                            </thead>
                            <tbody>
                                 <?php 
                                    // foreach($latestuserList as $userdata)
                                    foreach($userList as $userdata)
                                    {
                                      $last_registration_date = date('Y-m-d',strtotime($userdata['user_registered']));
                                   ?>
                                 <tr>
                                    <!-- <th scope="row"> -->
                                      <?php 
                                      // echo $userdata['ID'];
                                      ?>
                                    <!-- </th> -->
                                    <td><?php echo $userdata['firstname']." ".$userdata['lastname'];?> </td>
                                    <td><?php echo $userdata['user_email'];?></td>
                                    <td><?php echo $last_registration_date;  ?></td>
                                    <!-- <td><?php echo $userdata['user_registered'];?></td> -->
                                </tr>
                                <?php }?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-xs-12">
              <div class="card card-success">
                    <div class="card-header">
                        <div class="card-title">
                            <div class="title"><i class="fa fa-user"></i> Last 5 Frequently Logged-in Users</div>
                        </div>
                        <div class="clear-both"></div>
                    </div>
                    <div class="card-body no-padding">
                   
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <!-- <th>#</th> -->
                                    <th>First Name</th>
                                    <th>Email Id</th>
                                    <th>Count</th>
                                    <th>Last Login</th>
                                   
                                </tr>
                            </thead>
                            <tbody>
                              <?php
                                foreach($latestLogin as $login_user)
						        {
						           date_default_timezone_set('MST7MDT');
                                   $last_login = date('Y-m-d H:i:s',$login_user['lastlogin']);
                                ?>
                                 <tr>
                                    <!-- <th scope="row"><?php echo $login_user['ID'];?></th> -->
                                    <td><?php echo $login_user['firstname'];?> </td>
                                    <td><?php echo $login_user['user_email'];?></td>
                                    <td><?php echo $login_user['lastlogincount'];?></td>
                                    <td><?php echo $last_login;?>
                                  </td>
                                </tr>
                                <?php }?>                               
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
        
        <!--==================PIE Chart====================-->
        <div class="col-md-6">
          <div class="clearfix"></div>
          <div id="piechart"></div>
        </div>

        <div class="col-md-6">
          <div class="clearfix"></div>
          <div id="piecharts"></div>
        </div>



       



        <div class="col-md-12">
        <!------------------------ Histro PCR Graph ------------------------------------->
        <!-- <div class="col-md-6"> -->
        <!-- <div class="clearfix"></div> -->
        <div class="graph">
        <form method="post" action="<?php echo base_url().'admin/admins/';?>" class="form-inline">
          <div class="form-group">
              <label for="exampleTextarea">From Date</label>
              <input type="text" name="start_date" id = "datepicker-1" class="form-control" autocomplete="off" value="<?php echo(isset($_REQUEST['start_date'])? $_REQUEST['start_date']:''); ?>">
              <?php echo form_error('start_date','<div class="text-danger">', '</div>'); ?>
          </div>
          <div class="form-group">
              <label for="exampleTextarea">To Date</label>
              <input type="text" name="end_date" id = "datepicker-2" class="form-control" autocomplete="off" value="<?php echo(isset($_REQUEST['end_date'])?$_REQUEST['end_date']:''); ?>">
              <?php echo form_error('end_date','<div class="text-danger">','</div>'); ?>
          </div>
          <input type="submit" name="submit" class="btn btn-primary ban-sbmt" value="Submit">
          
        </form> 
        </div> 
        </div>
        <!------------------------End Histro PCR Graph ------------------------------------->





      <!-- </div> -->
      
      </div>
    </div>
        <div id="curve_chart" style=""></div>
    <!--------tab------>
  </section>
</div>
<script type="text/javascript">
$('#basic_DataTable').dataTable({
   "paging":   false,    
});
</script>