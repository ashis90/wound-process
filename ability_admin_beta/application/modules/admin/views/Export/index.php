<?php 
  if(!defined('BASEPATH')) EXIT("No direct script access allowed");
  ?>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Export Management
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin/dashboard');?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Export Data</li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header"></div>
          <!-------------------------------- Show Roles ------------------------------------>

          <h3>Select Role To Export</h3>
          <form action="<?php echo base_url().'admin/export/value/';?>" method="POST" enctype="multipart/form-data" name="program_frm" id="program_frm">
          <div class="lidot">
            <ul>
              <?php foreach($roleList as $rolval) { ?>
              <li>
                <label><input type="checkbox" name="role[]" value="<?php print_r($rolval['role_shortname']);  ?>"/>

                <?php echo $rolval['role_name']; ?>  </label>
                  <p></p>
              </li>
              <?php } ?>
              
            </ul>
          </div></n>
          <!-------------------------------- Show Roles ------------------------------------>


          <!-------------------------------- Show User value ------------------------------------>
          <div class="box-header"></div>
          <h3>Select Meta Key To Export</h3>
          <div class="lidot">
            <ul>
              <?php foreach($resUser as $userval) { ?>
                <?php 
                  if (($userval['COLUMN_NAME'] == 'ID') || 
                    ($userval['COLUMN_NAME'] == 'user_email') || 
                    ($userval['COLUMN_NAME'] == 'user_registered')) {
                ?>
              <li>
                <label><input type="checkbox" name="user[]" value="<?php print_r($userval['COLUMN_NAME']); ?>" />&nbsp;
                <?php 
                    print_r($userval['COLUMN_NAME']); ?> </label>
              </li>
                <?php    echo "<br><br>";
                }
                ?>
                  <p></p>
              <?php } ?> </n>

              <?php
                // foreach ($val as $key => $metaval) {
                //   print_r($key.$metaval);
                // }
                //   exit();


               foreach($val as $key=> $metaval) { ?>
              <li>
               
                <label><input type="checkbox" name="meta[]" value="<?php print_r($key); ?>" /> 
                <?php echo $metaval; ?> </label>
                  <p></p>
              </li>
              <?php } ?>

            </ul>
            <div class="row" style="padding-left: 7px;">
              <div class="col-md-6">
                <div class="row">
              <div class="form-group col-md-6">
                  <label for="exampleTextarea">Created From Date</label>
                  <input type="text" name="created_start_date" id = "datepicker-1" class="form-control" autocomplete="off" value="">
              </div>
              <div class="form-group col-md-6">
                  <label for="exampleTextarea">Created To Date</label>
                  <input type="text" name="created_end_date" id = "datepicker-2" class="form-control" autocomplete="off" value="">
              </div>
            </div>
          </div>
          </div>
          </div>
            <button type="submit" class="btn btn-primary ban-sbmt">Export</button>

          <!-------------------------------- Show Meta Key ------------------------------------>
        </form>

          
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
</div>
<style>
  .lidot ul {
  display: flex;
  list-style: none;
  flex-flow: row wrap;
  padding: 0;
  }
  .lidot ul li {
  margin: 7px;
  border: 1px solid #282828;
  padding: 5px;
  display: flex;
  align-items: center;
  }
  .lidot ul li a {
  display:flex;
  align-items:center;
  color:#000;
  }
  .lidot ul li p{
  margin-bottom:0;
  margin-left:6px;
  }
</style>
      <script>
         $(function() {
          <?php if(isset($_REQUEST['start_date'])){ ?>
            $( "#datepicker-1" ).datepicker();
          <?php }else{ ?>
            $( "#datepicker-1" ).datepicker().datepicker('setDate', 'today');
          <?php } ?>
          <?php if(isset($_REQUEST['end_date'])){ ?>
            $( "#datepicker-2" ).datepicker();
          <?php }else{ ?>
            $( "#datepicker-2" ).datepicker().datepicker('setDate', 'today');
          <?php } ?>
         });
      </script>
