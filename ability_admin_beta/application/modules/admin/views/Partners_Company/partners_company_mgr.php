<?php 
  if(!defined('BASEPATH')) EXIT("No direct script access allowed");
  ?>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Partners Company
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin/dashboard');?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Partners Company List</li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h4 class="title">Partners Company List </h4>
          </div>
          <a href="javascript:void(0);" class="btn btn-warning" id="img_upload" ><i class="fa fa-plus" aria-hidden="true"></i> Add Partners Company</a>
          <div id="img_upload_section" style="display: none">
            <form action="<?php echo base_url().'admin/Partners_company/PartnerAdd';?>"  method="POST" enctype="multipart/form-data" name="partners_company_frm" id="partners_company_frm">
              <div class="row">
             
                  <div class="col-md-12">
                    
                    <div class="form-group">
                      <label for="exampleTextarea">Partners Company Name</label>
                      <input type="text" name="partner_name" id="partner_name" value="" autocomplete="off">
                      <div class="text-danger" id="errorName"></div>
 
                      <!-- <?php //echo form_error('partner_name','<div class="text-danger">','</div>');  ?> -->
                    </div>


                    <div class="form-group">
                      <label for="exampleTextarea">Partners Email Id</label>
                      <input type="text" name="email" id="email" value="" autocomplete="off">
                      <div class="text-danger" id="errorEmail"></div>
 
                      <?php //echo form_error('email','<div class="text-danger">','</div>');  ?>
                    </div>


                    <div class="form-group">
                      <label for="exampleTextarea">Partners Company's Phone No</label>
                      <input type="text" name="phone_no" id="phone_no" value="" autocomplete="off">
                      <div class="text-danger" id="errorPh"></div>
 
                      <?php //echo form_error('phone_no','<div class="text-danger">','</div>');  ?>
                    </div>


                    <div class="form-group">
                      <label for="exampleTextarea">Partners Company's Fax No</label>
                      <input type="text" name="fax_no" id="fax_no" value="" autocomplete="off">
                      <?php //echo form_error('fax_no','<div class="text-danger">','</div>');  ?>
                    </div>


                    <div class="form-group">
                      <label for="exampleTextarea">Partners Company's CLIA No</label>
                      <input type="text" name="clia_no" id="clia_no" value="" autocomplete="off">
                      <?php //echo form_error('clia_no','<div class="text-danger">','</div>');  ?>
                    </div>


                    <div class="form-group">
                      <label for="exampleTextarea">Partners Company's Address1</label>
                      <input type="text" name="partner_address1" id="partner_address1" value="" autocomplete="off">
                      <div class="text-danger" id="errorAddress1"></div>
 
                      <?php //echo form_error('partner_address1','<div class="text-danger">','</div>');  ?>
                    </div>


                    <div class="form-group">
                      <label for="exampleTextarea">Partners Company's Address2</label>
                      <input type="text" name="partner_address2" id="partner_address2" value="" autocomplete="off">
                      <?php //echo form_error('partner_address2','<div class="text-danger">','</div>');  ?>
                    </div>


                    <div class="form-group">
                      <label for="exampleTextarea">Laboratory Director Details </label>
                      <textarea class="tnytextarea" id="lab_director" name="lab_director" rows="10" cols="80" placeholder="Enter Laboratory Director Name" autocomplete="off"></textarea>
                      <div class="text-danger" id="errorDirector"></div>
                      <?php //echo form_error('lab_director','<div class="text-danger">','</div>');  ?>
                    </div>


                    <div class="form-group">
                      <label for="exampleTextarea">Company Logo</label>
                      <input type="file" name="partner_logo" class="btn btn-default btn-file">
                      <?php //echo form_error('partner_logo','<div class="text-danger">','</div>');  ?>
                    </div>



                    <div class="form-group">
                      <label>Additional ways to get reports:</label>
                      <input type="checkbox" name="way_recive_fax" value="Fax" id="way_recive_fax">Fax
                      <input type="checkbox" name="way_recive_email" value="Email" id="way_recive_email">Email
                      <input type="checkbox" name="way_recive_none" value="None" id="way_recive_none">None
                    </div>     




                    <div class="form-group">
                      <label for="exampleInputEmail1">Status</label>
                      <select name="status" class="form-control">
                        <option value="1">Active</option>
                        <option value="0"s>Inactive</option>
                      </select>
                    </div>
                    
                    <input type="submit" value="Submit" name="Submit" class="btn btn-warning"/>
                    <input type="submit" value="Close" name="close" id="close"  class="btn btn-warning ban-sbmt"/>
                  </div>
               
              </div>
            </form>
          </div>
          
          <?php
            if ($this->session->flashdata('succ')) {
            $message = $this->session->flashdata('succ');
            ?>                
          <div class="alert alert-success alert-dismissable" role="alert" id="alert-success">
           <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <?php print_r($message);?></div>
          <?php
            }
            ?>
          <?php
            if ($this->session->flashdata('Err')) {
            ?>
          <div class="alert alert-danger alert-dismissable" role="alert">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <?php
              $message = $this->session->flashdata('Err');
              echo ($message);
              ?>
          </div>
          <?php
            }
            ?>

            <?php
            if ($this->session->flashdata('Error')) {
            ?>
          <div class="alert alert-danger alert-dismissable" role="alert">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <?php
              $message = $this->session->flashdata('Error');
              echo ($message);
              ?>
          </div>
          <?php
            }
            ?>


            <?php
            if ($this->session->flashdata('delete')) {
            ?>
          <div class="alert alert-success alert-dismissable" role="alert">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <?php
              $message = $this->session->flashdata('delete');
              echo ($message);
              ?>
          </div>
          <?php
            }
          ?>


          <div class="box-body"> 
           <div class="svt">          
            <div class="table-responsive">
              <table class="table table-bordered table-hover" id="basicDataTable">
                <thead>
                  <tr>
                    <th style="text-align: center;" width="8%">Sr No</th>
                    <th style="text-align: center;" width="8%">Partners Company Name</th>
                    <th style="text-align: center;" width="8%">Phone No</th>
                    <th style="text-align: center;" width="8%">CLIA No</th>
                    <th style="text-align: center;" width="10%">Address1</th>
                    <th style="text-align: center;" width="30%">Lab Director Name</th>
                    <th style="text-align: center;" width="30%">Logo</th>
                    <th style="text-align: center;" width="30%">Status</th>
                    <th style="text-align: center;" width="30%">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if($partnersCompanyList): ?>
                  <?php
                    $count= 1;
                    foreach($partnersCompanyList as $partnersCompany):	
                    ?>
                  <tr class="odd gradeX" id="data-<?php echo $partnersCompany['partner_id'];?>">
                    <td><?php echo $count;?> </td>

                    <td><?php echo $partnersCompany['partner_name'];?></td>

                    <td><?php echo $partnersCompany['phone_no'];?></td>

                    <td><?php echo $partnersCompany['clia_no'];?></td>

                    <td><?php echo short_description($partnersCompany['partner_address1'],100);?></td>

                    <td><?php echo short_description($partnersCompany['lab_director'], 100);?></td>

                    <?php
                    if (($partnersCompany['partner_logo']) != '') { ?>
                    <td>
                      <a href="<?php echo base_url().'assets/uploads/partners/'.$partnersCompany['partner_logo'];?>" class="fancy imag"><img src = "<?php echo base_url().'assets/uploads/partners/'.$partnersCompany['partner_logo'];?>" alt="<?php echo $partnersCompany['partner_logo'];?>" width="50px" height="500px;" id="fancyLaunch" class="imagess"></a>
                    </td>
                  <?php  }else{ ?>
                    <td></td>
                  <?php }  ?>
                    
                    <td>
                      <div class="alert alert-<?php echo ($partnersCompany['status']== '1' ? 'success' : 'danger');?>"><?php echo ($partnersCompany['status']== '0' ? 'Inactive' : 'Active');?></div>
                    </td>


                    <td>
                      <a href="<?php echo base_url('partners-update/'.$partnersCompany['partner_id']); ?>" class="btn btn-warning edit-btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>

                      <a href="<?php echo base_url('partners-delete/'.$partnersCompany['partner_id']);  ?>" class="btn btn-warning" onclick="return confirm('Are you sure to delete?')"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a>
                    </td>

                  </tr>
                  <?php 
                    $count++;
                    endforeach ;
                    endif;?>
                </tbody>
              </table>
            </div>
          </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
</div>
