<?php
if(!defined('BASEPATH')) EXIT("No script direct access allowed");
$conntellername=(isset($active_controller))?$active_controller:'';
$logged_in_id = $this->session->userdata('admin_id');
$logged_in_user_details = get_user_details($logged_in_id);
?>
 <!-- Left side column. contains the logo and sidebar -->
   <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar" >
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
            <img src="<?php echo base_url().'assets/uploads/Admin/admin.png'?>" class="img-circle" alt="User Image">
          
        </div>
        <div class="pull-left info">
          <p><?php echo $logged_in_user_details['sysadm_fname'].' '.$logged_in_user_details['sysadm_lname'];?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        
        <li class="treeview <?php if(empty($conntellername) || $conntellername=='dashboard'){ echo 'active';}?>">
          <a href="<?php echo base_url('admin/dashboard');?>">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>

        <li class="treeview">
          <a href="<?php echo base_url('admin/user');?>">
           <i class="fa fa-users"></i> <span>Users Management</span>
          </a>

            <ul>
              <li class="treeview <?php if($conntellername=='export'){ echo 'active';}?>">
                <a href="<?php echo base_url('admin/export');?>">
                  <i class="fa fa-file-excel-o"></i> <span>Export to Exel</span>
                </a>
              </li>
            </ul>
        </li>

        <li class="treeview <?php if($conntellername=='role'){ echo 'active';} ?>">
          <a href="<?php echo base_url('admin/role');?>">
           <i class="glyphicon glyphicon-king"></i> <span>Role Management</span>
          </a>
        </li>

        <li class="treeview <?php if($conntellername=='report'){ echo 'active';} ?>">
          <a href="<?php echo base_url('admin/report');?>">
           <!-- <i class="glyphicon glyphicon-king"></i> <span>Report Shortcodes</span> -->
           <i class="fa fa-flag"></i> <span>Report Shortcodes</span>
          </a>
        </li>

        
        <!-- <li class="treeview">
          <a href="<?php echo base_url('admin/role');?>">
           <i class="glyphicon glyphicon-king"></i> <span>Role Management</span>
          </a>
        </li>

        <li class="treeview">
          <a href="<?php echo base_url('admin/report');?>">
           <i class="glyphicon glyphicon-king"></i> <span>Report Shortcodes</span>
          </a>
        </li> -->

		    <!-- <li class="treeview <?php if($conntellername=='charity'){ echo 'active';}?>">
          <a href="<?php echo base_url('admin/service');?>">
           <i class="glyphicon glyphicon-king"></i> <span>Service Management</span>
          </a>
        </li> -->

        <li class="treeview <?php if($conntellername=='service'){ echo 'active';}?>">
          <a href="<?php echo base_url('admin/service');?>">
           <i class="fa fa-flask"></i> <span>Our Science-Based Services</span>
          </a>
        </li>


        <li class="treeview <?php if($conntellername=='nail_fungus'){ echo 'active';}?>">
          <a href="<?php echo base_url('admin/nail_fungus');?>">
           <i class="fa fa-hand-o-up"></i> <span>Nail Fungus</span>
          </a>
        </li>


        <li class="treeview <?php if($conntellername=='partners_company'){ echo 'active';}?>">
          <a href="<?php echo base_url('admin/partners_company');?>">
           <i class="fa fa-building"></i> <span>Partners Company</span>
          </a>
        </li>
        

        <li class="treeview <?php if($conntellername=='cms'){ echo 'active';}?>">
          <a href="<?php echo base_url('admin/cms');?>">
            <i class="fa fa-bars"></i> <span>Page Management</span>
          </a>
        </li>


        <!-- <li class="treeview <?php //if($conntellername=='google_analytics'){ echo 'active';}?>">
          <a href="<?php //echo base_url('admin/google_analytics');?>">
            <i class="fa fa-line-chart"></i> <span>Google Analytics</span>
          </a>
        </li> --> 


        <li class="treeview <?php if($conntellername=='holiday'){ echo 'active';}?>">
          <a href="<?php echo base_url('admin/holiday');?>">
            <i class="fa fa-calendar"></i> <span>Holiday Management</span>
          </a>
        </li>

        <li class="treeview <?php if($conntellername=='contact'){ echo 'active';}?>">
          <a href="<?php echo base_url('admin/contact');?>">
            <i class="fa fa-envelope"></i> <span>Contact Management</span>
          </a>
        </li> 

        <li class="treeview <?php if($conntellername=='export'){ echo 'active';}?>">
          <a href="<?php echo base_url('admin/Stateexport');?>">
            <i class="fa fa-envelope"></i> <span>State Export</span>
          </a>
        </li> 
        <li class="treeview">
          <a href="<?php echo base_url('admin/StateExport/StateExportList');?>">
            <i class="fa fa-envelope"></i> <span>State Export List</span>
          </a>
        </li>
        <!-- <li class="treeview <?php if($conntellername=='contact_list'){ echo 'active';}?>">
          <a href="<?php echo base_url('admin/contact');?>">
            <i class="fa fa-users"></i> <span>Contact Management</span>
          </a>
        </li> -->

        <!--<li class="treeview">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>Settings</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i> Site Content</a></li>
          </ul>
        </li>    -->   
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>