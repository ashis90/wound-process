<html lang="en">
<head>
  <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"
         rel = "stylesheet">
      <script src = "https://code.jquery.com/jquery-1.10.2.js"></script>
      <script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

      <!-- Javascript -->
      <script>
         $(function() {
            $( "#datepicker-1" ).datepicker();
            $( "#datepicker-2" ).datepicker();
         });
      </script>
 
</head>
<?php
  $start_date = date('m/d/Y',strtotime($holidayData['start_date']));
  $end_date = date('m/d/Y',strtotime($holidayData['end_date']));
?>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Holiday Management
    </h1>

    <?php
      if ($this->session->flashdata('sdate')) {
      ?>
    <div class="alert alert-danger alert-dismissable" role="alert">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <?php
        $message = $this->session->flashdata('sdate');
        echo ($message);
        ?>
    </div>
    <?php
      }
      ?>
      
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin/dashboard');?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Holiday List</li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h4 class="title">Update Holiday</h4>
          </div>
          <form action="<?php echo base_url().'admin/holiday/holidayEditing/';?>" method="POST" enctype="multipart/form-data" name="holiday_frm" id="holiday_frm">
            <input type="hidden" name="id" value="<?php echo $holidayData['id'];  ?>">
            <div class="form-group">
              <label for="exampleTextarea">Holiday Title</label>
              <input type="text" id="title" name="title"  placeholder="Enter Holiday Title" value="<?php echo $holidayData['title']; ?>">
              <?php echo form_error('title','<div class="text-danger">','</div>');  ?>
            </div>
            
            <div class="form-group">
              <label for="exampleTextarea">Start Date</label>
              <input type="text" name="start_date" id = "datepicker-1" value="<?php echo $start_date;  ?>">
              <?php echo form_error('start_date','<div class="text-danger">','</div>');  ?>
            </div>

            <div class="form-group">
              <label for="exampleTextarea">End Date</label>
              <input type="text" name="end_date" id = "datepicker-2" value="<?php echo $end_date; ?>">
              <?php echo form_error('end_date','<div class="text-danger">','</div>');  ?>
            </div>

            <div class="form-group">
              <label for="exampleTextarea">End Date</label>
              <select name="is_active" id="is_active">
                <option value="SelectStatus">Select Status</option>
                <?php 

                  if (($holidayData['is_active']) == '1') { ?>
                    <option value="1" selected>Active</option>
                    <option value="0">Inactive</option>
                  <?php } ?>
                  <?php  
                  if (($holidayData['is_active']) == '0') { ?>
                    <option value="1">Active</option>
                    <option value="0" selected>Inactive</option>
                  <?php  } ?>
              </select>
            </div>

            
            <button type="submit" class="btn btn-primary ban-sbmt">Update Holiday</button>
          </form>
        </div>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
    <!-- /.row -->
  </section>
</div>
</html>
