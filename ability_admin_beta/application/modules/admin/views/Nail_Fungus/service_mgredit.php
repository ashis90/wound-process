<?php 
  if(!defined('BASEPATH')) EXIT("No direct script access allowed");
  if($service['service_status']=="Active"){
      $redirect_url=base_url().'admin/service/';
  }else{
      $redirect_url=base_url().'admin/service/service_list/'.$_REQUEST['service_id'];
  }  
?>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Service Management
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin');?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Service List</li>
    </ol>
  </section>
     <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h4 class="title">Service List</h4>
			<div id="loaderImg" style="display:none;"><img src="<?php echo base_url('assets/frontend/image/loder.gif');?>" alt="loder" width="50px" height="50px"></div>
          </div>
        <form action="<?php echo base_url().'admin/service/serviceEditing/';?>" method="POST" enctype="multipart/form-data" name="service_frm" id="service_frm">
         
          <input type="hidden" value="<?php echo $_REQUEST['service_id'];?>" name="service_id"
                 id="service_id"/>
           


          <div class="form-group">
      			<label for="exampleInputFile">Image</label>
      			<input type="file" class="btn btn-default btn-file" id="service_image" aria-describedby="fileHelp" name="service_image" style="margin-bottom: 10px">


              <div class="image_cross">
      			     <a href="<?php echo base_url().'assets/uploads/Service/'.$service['service_image'];?>" class="fancy"><img src = "<?php echo base_url().'assets/uploads/thumb/'.$service['service_image'];?>" alt="<?php echo $service['service_image'];?>"  id="fancyLaunch" class="images"></a>

                 <?php
                  if (($service['service_image']) != '') { ?>
                 <a class="delete_image">
                   <i class="fa fa-close"></i>
                 </a>
                <?php  } ?>
                  <?php echo form_error('service_image','<div class="text-danger">','</div>');  ?>
              </div>
			    </div>

           



            <span class="note-txt">[Note: Please upload image of size 300 X 180]</span>
		


          <div class="form-group">
            <label for="exampleTextarea"> Service Title </label>
            <input type="text" id="service_title" name="service_title" placeholder="Enter Service Description" value="<?php echo $service['service_title'];?>">
            <?php echo form_error('service_title','<div class="text-danger">','</div>');  ?>
          </div>
        	


          <div class="form-group">
            <label for="exampleTextarea"> Service Description </label>
            <textarea class="tnytextarea" id="service_desc" name="service_desc" rows="10" cols="80" placeholder="Enter Service Description"><?php echo $service['service_desc'];?></textarea>
            <?php echo form_error('service_desc','<div class="text-danger">','</div>');  ?>
          </div>
          


          <div class="form-group">
            <label for="exampleLink">Service Link</label>
            <input type="text" name="service_link" id="service_link"
                    placeholder="Enter Service Link"
                    value="<?php echo $service['service_link']; ?>">
          </div>



          <div class="form-group">
            <label for="exampleInputEmail1">Status</label>
            <select name="service_status" class="form-control">
              <option value="1" <?php echo ($service['service_status'] =='Active' ? 'selected' : '');?>>Active</option>
              <option value="2" <?php echo ($service['service_status'] =='Inactive' ? 'selected' : '');?>>Inactive</option>
            </select>
          </div>
          <button type="submit" class="btn btn-primary ban-sbmt">Submit</button>
           <a href="<?php echo $redirect_url;?>" class="btn btn-warning"><?php echo '<< Back';?></a>
        </form>
        </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
  
    <!-- /.row -->
  </section>
</div>
