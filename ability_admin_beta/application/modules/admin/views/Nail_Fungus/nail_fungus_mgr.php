<?php 
  if(!defined('BASEPATH')) EXIT("No direct script access allowed");
  ?>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Nail Fungus Management
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('admin/dashboard');?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Nail Fungus List</li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h4 class="title">Nail Fungus List </h4>
          </div>
          <a href="javascript:void(0);" class="btn btn-warning" id="img_upload" ><i class="fa fa-plus" aria-hidden="true"></i> Add Nail Fungus</a>
          <div id="img_upload_section" style="display: none">
            <form action="<?php echo base_url().'admin/nail_fungus/NailAdd';?>"  method="POST" enctype="multipart/form-data" name="nailfungus_frm" id="nailfungus_frm">
              <div class="row">
             
                  <div class="col-md-12">
                    <div class="form-group">
                      <input type="file" name="service_image" class="btn btn-default btn-file">
                      <?php echo form_error('service_image','<div class="text-danger">','</div>');  ?>
                    </div>
                     <div class="form-group">
                      <label for="exampleTextarea"> Nail Fungus Title </label>
                      <input type="text" name="service_title" id="service_title" value="">
                      <?php echo form_error('service_title','<div class="text-danger">','</div>');  ?>
                    </div>
                    <div class="form-group">
                      <label for="exampleTextarea"> Nail Fungus Description </label>
                      <textarea class="tnytextarea" id="service_desc" name="service_desc" rows="10" cols="80" placeholder="Enter service Description"></textarea>
                      <?php echo form_error('service_desc','<div class="text-danger">','</div>');  ?>
                    </div>
                    <!-- <div class="form-group">
                      <label for="exampleLink">Nail Fungus Link</label>
                      <input type="text" name="service_link" id="service_link">
                    </div> -->
                    <input type="submit" value="Submit" name="Submit" class="btn btn-warning"/>
                    <input type="submit" value="Close" name="close" id="close"  class="btn btn-warning ban-sbmt"/>
                  </div>
               
              </div>
            </form>
          </div>
          
          <?php
            if ($this->session->flashdata('succ')) {
            $message = $this->session->flashdata('succ');
            ?>                
          <div class="alert alert-success alert-dismissable" role="alert" id="alert-success">
           <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <?php print_r($message);?></div>
          <?php
            }
            ?>
          <?php
            if ($this->session->flashdata('Err')) {
            ?>
          <div class="alert alert-danger alert-dismissable" role="alert">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <?php
              $message = $this->session->flashdata('Err');
              echo ($message);
              ?>
          </div>
          <?php
            }
            ?>

            <?php
            if ($this->session->flashdata('Error')) {
            ?>
          <div class="alert alert-danger alert-dismissable" role="alert">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <?php
              $message = $this->session->flashdata('Error');
              echo ($message);
              ?>
          </div>
          <?php
            }
            ?>


            <?php
            if ($this->session->flashdata('delete')) {
            ?>
          <div class="alert alert-success alert-dismissable" role="alert">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <?php
              $message = $this->session->flashdata('delete');
              echo ($message);
              ?>
          </div>
          <?php
            }
            ?>


          <div class="box-body"> 
           <div class="svt">          
            <div class="table-responsive">
              <table class="table table-bordered table-hover" id="basicDataTable">
                <thead>
                  <tr>
                    <th style="text-align: center;" width="8%">Sr No</th>
                    <th style="text-align: center;" width="15%">Service Title</th>
                    <th style="text-align: center;" width="8%">Image</th>
                    <th style="text-align: center;" width="15%">Text</th>
                    <th style="text-align: center;" width="8%">Status</th>
                    <th style="text-align: center;" width="30%">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if($serviceList): ?>
                  <?php
                    $count= 1;
                    foreach($serviceList as $service):	
                    ?>
                  <tr class="odd gradeX" id="data-<?php echo $service['service_id'];?>">
                    <td><?php echo $count;?> </td>

                    <td><?php echo $service['service_title'];?></td>

                    <?php
                    if (($service['service_image']) != '') { ?>
                    <td>
                      <a href="<?php echo base_url().'assets/uploads/Nail/'.$service['service_image'];?>" class="fancy imag"><img src = "<?php echo base_url().'assets/uploads/Nail/'.$service['service_image'];?>" alt="<?php echo $service['service_image'];?>" width="50px" height="500px;" id="fancyLaunch" class="images"></a>
                    </td>
                  <?php  }else{ ?>
                    <td></td>
                  <?php }  ?>
                    
                    <td><?php echo short_description($service['service_desc'],100);?></td>
                    <td>
                      <div class="alert alert-<?php echo ($service['service_status']== 'Active' ? 'success' : 'danger');?>"><?php echo ($service['service_status']== 'Active' ? 'Active' : 'Inactive');?></div>
                    </td>
                    <td>
                      <a href="<?php echo base_url().'admin/nail_fungus/nailFungusEdit?service_id='.$service['service_id'];?>" class="btn btn-warning edit-btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>


                      <a href="<?php echo base_url('nail-fungus-delete/'.$service['service_id']);  ?>" class="btn btn-warning" onclick="return confirm('Are you sure to delete?')"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a>


                     <!--  <a href="javascript:void(0);" class="btn btn-warning" onclick="delete_data('<?php echo $service['service_id'];?>','services','service_id');"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a> -->
                    </td>
                  </tr>
                  <?php 
                    $count++;
                    endforeach ;
                    endif;?>
                </tbody>
              </table>
            </div>
</div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
</div>
