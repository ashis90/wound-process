<?php
if (!defined('BASEPATH'))
    EXIT("No direct script access allowed");
class  Google_analytics extends MY_Controller {

    function __construct() {
        parent::__construct();
          $this->session_checked($is_active_session = 1);
          $this->load->library('form_validation');
    }

	public function index() {
	 	$this->session_checked($is_active_session = 1);
    	$conditions = " ( `is_active` <>'Deleted') ";
	  	$select_fields = '*';
	    $is_multy_result = 0;
	    $analyticsList = $this->BlankModel->getTableData('wp_abd_google_analytics', $conditions, $select_fields, $is_multy_result);
	    common_viewloader('Google_analytics/google_analytics_mgr', array('analyticsList' => $analyticsList)); 
	}

	


	function googleAnalyticsEdit($eid=''){
		$this->session_checked($is_active_session = 1);
		$conditions = " ( `id` = '".$eid."') ";
		$select_fields = '*';
		$is_multy_result = 1;
		$analyticsEditData = $this->BlankModel->getTableData('wp_abd_google_analytics', $conditions, $select_fields, $is_multy_result);
		common_viewloader('Google_analytics/google_analytics_mgredit', array('analyticsEditData' => $analyticsEditData));
	}

	function googleAnalyticsEditing(){
		if ($this->input->post()) {
			$data = $this->input->post();
			$id = $data['id'];
			$this->form_validation->set_rules('description','Description','required');
			if ($this->form_validation->run() == FALSE) {
				
				$this->googleAnalyticsEdit($id);
			} else{

				$data = $this->input->post();
				$id = $data['id'];

				$conditions = "(`id` = '".$id."')";
				$update_analytics = $this->BlankModel->editTableData('wp_abd_google_analytics', $data, $conditions);
				if ($update_analytics) {
					$this->session->set_flashdata('update','Successfully Updated');
					header('location:'.base_url().'admin/google_analytics/');
				}
			}
		} else{
			$this->session->set_flashdata('Err','Submission Failed');
			header('location:'.base_url().'admin/google_analytics');
		}
	}
 
	

   
}
?>