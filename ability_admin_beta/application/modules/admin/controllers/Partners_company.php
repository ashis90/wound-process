<?php
if (!defined('BASEPATH'))
    EXIT("No direct script access allowed");
class  Partners_company extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->session_checked($is_active_session = 1);
    }

 public function index() {
 	$this->session_checked($is_active_session = 1);
    // $conditions = " ( `service_status` <>'Deleted') AND ( `service_type` = 'Nail')";
    $conditions = " ( `status` <>'Deleted') ";
  	$select_fields = '*';
    $is_multy_result = 0;
    $partnersCompanyList = $this->BlankModel->getTableData('wp_abd_partners_company', $conditions, $select_fields, $is_multy_result);
    common_viewloader('Partners_Company/partners_company_mgr', array('partnersCompanyList' => $partnersCompanyList)); 
 }
 
 function partnersEdit($eid= ''){
	$conditions = " ( `partner_id` = '".$eid."')";		
    $select_fields = '*';
    $is_multy_result = 1;
	
	$partnersCompany = $this->BlankModel->getTableData('wp_abd_partners_company', $conditions, $select_fields, $is_multy_result);
   	common_viewloader('Partners_Company/partners_company_mgredit', array('partnersCompany' => $partnersCompany)); 	
}
 
function partnersCompanyEditing()
{
    if ($this->input->post()) {
		$data = $this->input->post();
    	$partner_id = $data['partner_id'];
    	$this->form_validation->set_rules('partner_name','Partners Company Name','required');
    	$this->form_validation->set_rules('phone_no','Phone No','required');
    	$this->form_validation->set_rules('partner_address1','Partners Address1','required');
    	$this->form_validation->set_rules('lab_director','Lab Directors','required');
		if ($this->form_validation->run() == FALSE) {
			
			// $title = '';
			
			// if (form_error('partner_name')) {
			// 	$title .= "<p>Please Enter Partners Company Name</p>";
			// }
			// if (form_error('phone_no')) {
			// 	$title .= "<p>Please Enter Phone No</p>";
			// }
			// if (form_error('partner_address1')) {
			// 	$title .= "<p>Please Enter Address</p>";
			// }
			// if (form_error('lab_director')) {
			// 	$title .= "<p>Please Enter Lab Directors</p>";
			// }
			// $this->session->set_flashdata('Error',$title);
			$this->partnersEdit($partner_id);
		} else{
			$data = array();
			$data = $this->input->post();

			$select_fields = '*';
			$is_multy_result = 1;
			$get_img_conditions = " ( `partner_id` = '".$data['partner_id']."' )";
			
			$img = $this->BlankModel->getTableData('wp_abd_partners_company', $get_img_conditions, $select_fields, $is_multy_result);


			$redirect_url = base_url().'admin/Partners_company/';
			
			if($_FILES['partner_logo']['name']==''){
			   $image_name = $img['partner_logo'];
			}
			else{
				$image = $img['partner_logo'];
				$path = FCPATH.'assets/uploads/partners/';
				$value = $path.$image;
				if (!empty($image)) {
					unlink($value);
				}
				// unlink($value);

				$logo_uploads  = image_uploads('partners',$thumb_Size_width = '500', $thumb_Size_hight = '300', 'partner_logo');
				
				if(is_array($logo_uploads))
				{
					$image_name = $logo_uploads['file_name'];
				}
				
				if(!is_array($logo_uploads))
				{
					$this->session->set_flashdata('Err', $logo_uploads);
					header('location:'.base_url().'admin/Partners_company/');	
					// exit;    
				}
			}
			 
			 $conditions = " ( `partner_id` = '".$data['partner_id']."' )";



			 $receivereport='';
				 
			if(isset($data['way_recive_fax']))
			{
			 	$receivereport .=$data['way_recive_fax'].",";
			}
			if(isset($data['way_recive_email']))
			{
			 	$receivereport .=$data['way_recive_email'].",";
			}
			if(isset($data['way_recive_none']))
			{
			 	$receivereport .=$data['way_recive_none'].",";
			}





			 $partners_company_data = array('partner_name' => $data['partner_name'],
			        				'partner_address1' => $data['partner_address1'],
			        				'partner_address2' => $data['partner_address2'],
			        				'email' => $data['email'],
			        				'status' => $data['status'],
			        				'phone_no' => $data['phone_no'],
			        				'fax_no' => $data['fax_no'],
			        				'clia_no' => $data['clia_no'],
			        				'lab_director' => $data['lab_director'],
			        				'report_recive_way' => $receivereport,
			        				'partner_logo' => $image_name);
			 
			if($_FILES['partner_logo']['name']==''){
			   $partners_company_data =array('partner_name' => $data['partner_name'],
			        				'partner_address1' => $data['partner_address1'],
			        				'partner_address2' => $data['partner_address2'],
			        				'email' => $data['email'],
			        				'status' => $data['status'],
			        				'phone_no' => $data['phone_no'],
			        				'fax_no' => $data['fax_no'],
			        				'clia_no' => $data['clia_no'],
			        				'lab_director' => $data['lab_director'],
			        				'report_recive_way' => $receivereport
			        			);
			}
			
			 $programsedit = $this->BlankModel->editTableData('wp_abd_partners_company', $partners_company_data, $conditions);
			 $this->session->set_flashdata('succ', 'Partners Company Updated successfully');
			 header('location:'.$redirect_url);
			 // exit;
		}
	} else{
		$this->session->set_flashdata('Err','Submission Failed');
		header('location:'.base_url().'admin/Partners_company');
	}
		   
}

    public function PartnerAdd() {
    
		if($post_data = $this->input->post()){
			$this->form_validation->set_rules('partner_name','Partners Name', 'required');
			$this->form_validation->set_rules('phone_no','Phone No','required');
		    $this->form_validation->set_rules('partner_address1','Partners Address1','required');
		    // $this->form_validation->set_rules('lab_director','Lab Directors Details','required');
			if ($this->form_validation->run() == FALSE) {
				$error['partner_name'] = form_error('partner_name');
				$error['phone_no'] = form_error('phone_no');
				$error['partner_address1'] = form_error('partner_address1');
				// $error['lab_director'] = form_error('lab_director');
				echo json_encode( $error);
			}
			else{
		        $redirect_url = base_url().'admin/Partners_company/';
		        $data = array();
			    $data = $this->input->post();
		      	
		    	$logo_uploads  = image_uploads('partners',$thumb_Size_width = '500', $thumb_Size_hight = '300', 'partner_logo');
		       
		        if (is_array($logo_uploads)) {
		        	
		              //save the file info in the database
		            $image_name = $logo_uploads['file_name'];
		        } else{
		        	$image_name = '';
		        }  



		        $receivereport='';
				 
				if(isset($data['way_recive_fax']))
				{
				 	$receivereport .=$data['way_recive_fax'].",";
				}
				if(isset($data['way_recive_email']))
				{
				 	$receivereport .=$data['way_recive_email'].",";
				}
				if(isset($data['way_recive_none']))
				{
				 	$receivereport .=$data['way_recive_none'].",";
				}


		        $partners_company_data = array('partner_name' => $data['partner_name'],
		        				'partner_address1' => $data['partner_address1'],
		        				'partner_address2' => $data['partner_address2'],
			        			'email' => $data['email'],
		        				'status' => $data['status'],
		        				'phone_no' => $data['phone_no'],
		        				'fax_no' => $data['fax_no'] ,
		        				'clia_no' => $data['clia_no'],
		        				'lab_director' => $data['lab_director_new'],
		        				'report_recive_way' => $receivereport,
		        				'partner_logo' => $image_name
		        				);
		     		
				$partners_company_data = $this->BlankModel->addTableData('wp_abd_partners_company',$partners_company_data);
				$this->session->set_flashdata('succ', 'New Partners Company Added successfull.');
				header('location:'.$redirect_url);	
				// exit;    
			}

		}
			// exit;
		// } else{
		// 	$this->session->set_flashdata('Err','Submission Failed');
		// 	header('location:'.base_url().'admin/Partners_company');
		// }
	}

	public function partnersDelete($did = ''){
		$select_fields = '*';
		$is_multy_result = 1;
		$get_img_conditions = " ( `partner_id` = '".$did."')";
		
		$img = $this->BlankModel->getTableData('wp_abd_partners_company', $get_img_conditions, $select_fields, $is_multy_result);
		$image = $img['partner_logo'];

		$path = FCPATH.'assets/uploads/partners/';
		$value = $path.$image;
		// print_r($image);
		// exit();

		if (!empty($image)) {
			unlink($value);
		}
		// echo "<pre>";
		// print_r($img);
		// echo "<br>";
		// print_r($value);
		// exit();



		$delete_nailfungus = $this->BlankModel->delete_id('wp_abd_partners_company','partner_id',$did);
		if ($delete_nailfungus) {
			$this->session->set_flashdata('delete','Successfully Deleted');
			header('location:'.base_url().'admin/Partners_company');
		}
	}
}?>