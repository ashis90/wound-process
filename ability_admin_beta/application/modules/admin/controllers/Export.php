<?php if (!defined('BASEPATH')) EXIT("No direct script access allowed");
class Export extends MY_Controller{
		public function __construct(){
			parent::__construct();
 			$this->session_checked($is_active_session = 1);			
		}
		public function index(){

			/////////////////////////////////////For User Value//////////////////////////////////
			$sqlUser= "SELECT `COLUMN_NAME` 
						FROM `INFORMATION_SCHEMA`.`COLUMNS` 
						WHERE `TABLE_SCHEMA`='abilitydiagnoswpdb' 
						AND `TABLE_NAME`='wp_abd_users'";
			$executeUser = $this->db->query($sqlUser);
			$resUser = $executeUser->result_array();

			/////////////////////////////////////For Meta Value//////////////////////////////////
			$val = array('first_name' => 'First Name',
						'last_name' => 'Last Name',
						'_mobile' => 'Mobile',
						'_address' => 'Address',
						'_status' => 'Status',
						'combined_by' => 'Combined By',
						'clinic_addrs' => 'Clinic Address',
						'fax' => 'Fax',
						'anthr_fax_number' => 'Another Fax Number',
						'cell_phone' => 'Cell Phone',
						'manager_contact_name' => 'Manager Contact Name',
						'manager_cell_number' => 'Manager Cell Number',
						'npi' => 'NPI',
						'added_by' => 'Added By',
						'_assign_clinic' => 'Assign Clinic',
						'_assign_partner' => 'Assign Partner',
						'assign_specialty' => 'Assign Speciality',
						'_clinic_name' => 'Clinic Name',
						'state' => 'State',
						'report_recive_way' => 'Report Receive Way',
						'assign_to' => 'Assign To',
						'_nbaccount' => 'NB Account',
						'_ein_no' => 'EIN No');

			$sqlMetakey = "SELECT meta_key 
							FROM wp_abd_usermeta GROUP BY meta_key HAVING COUNT(meta_key) > 0";
			$executeMetakey = $this->db->query($sqlMetakey);
			$resMetakey = $executeMetakey->result_array();

			$sqlRole = "SELECT * FROM `wp_abd_role_management`";
			$query = $this->db->query($sqlRole);
			$resRole = $query->result_array();
			// common_viewloader('Export/indexx',array('val'=> $val));
			common_viewloader('Export/index', array('roleList' => $resRole,'resUser' => $resUser, 
				'val'=> $val));
		}

		public function value(){
			header('Content-Type: text/csv; charset=utf-8');  
			header('Content-Disposition: attachment; filename=userexport.csv');  
			$output = fopen("php://output", "w");  

			if ($this->input->post()) {
			  	$data = $this->input->post();
			  	$role = $data['role'];
			  	$created_start_date = $data['created_start_date']?date('Y-m-d',strtotime($data['created_start_date'])):'';
			  	$created_end_date = $data['created_end_date']?date('Y-m-d',strtotime($data['created_end_date'])):'';
			  	$roleValue = '';
			  	$roleValue .= " AND (";
			  	$addition_sql='';
  				foreach ($role as $value) {
	  				$roleValue .=" m3.meta_value LIKE '%".$value."%'";
  					if (end($role) == $value) {
  				    	$roleValue .="";
  			        } else{
  				      	$roleValue .=" OR ";
  			        }
	  			}
		  			
			  	$roleValue .= " ) ";

			  	if((!empty($created_start_date) && !empty($created_end_date) && ($created_end_date >= $created_start_date))){
			  		$addition_sql = "AND (u1.user_registered BETWEEN '".$created_start_date." 00:00:00' AND '".$created_end_date." 23:59:59')";
			  	}
				$sqlUser = "SELECT u1.ID,u1.user_login AS nickname, m1.meta_value AS firstname, m2.meta_value AS lastname, u1.user_email,m3.meta_value AS role, u1.user_registered as 'user_registered'
				FROM wp_abd_users u1
				JOIN wp_abd_usermeta m1 ON (m1.user_id = u1.ID AND m1.meta_key = 'first_name')
				JOIN wp_abd_usermeta m2 ON (m2.user_id = u1.ID AND m2.meta_key = 'last_name')
				JOIN wp_abd_usermeta m3 ON (m3.user_id = u1.ID AND m3.meta_key = 'wp_abd_capabilities') 
				$roleValue $addition_sql GROUP BY u1.ID ORDER BY `u1`.`user_registered`";
				$exequery = $this->db->query($sqlUser);
				$resArr = $exequery->result_array(); 

				if(!empty($resArr)){
				$sqlRole = "SELECT * FROM `wp_abd_role_management`";
				$executeRole = $this->db->query($sqlRole);
				$resRole = $executeRole->result_array();

/////////////////////////////////////////////Exel Heading////////////////////////////////////////////
			  	
			  	$headerValue = array();
			  	$yourArray = array();
			  	$temp = array();
			  	$id = 'ID';
			  	$email = 'USER EMAIL';
			  	$user_login = 'USER LOGIN';
			  	$registered_user = 'USER REGISTERED';
			  	$role = 'ROLE';
				foreach ($resArr as $value) {
					$user_id = $value['ID'];
			  		$meta = ($data['meta']);
			  		// $metakeys = array();

			  		foreach ($meta as $key => $keyValue) {
			  			array_push($headerValue, $id, $email, $user_login, $role, $keyValue);
			  		}

				}

				array_push($headerValue,$registered_user);


				$headerValue = array_unique($headerValue);

				$headerValue = array_filter($headerValue,'strlen');
				
				$yourArray = array_map('strtoupper', $headerValue);
				$temp = preg_replace("/[^a-zA-Z 0-9]+/", " ", $yourArray );
				fputcsv($output, $temp);
//////////////////////////////////End  Exel Heading/////////////////////////////////////////////

/////////////////////////////////////////////Exel Value////////////////////////////////////////////
				foreach ($resArr as $value) {
					$user_id = $value['ID'];
					$user_email = $value['user_email'];
					$nickname = $value['nickname'];					
					$user_registered = date('Y-m-d',strtotime($value['user_registered']));	
			  		$meta = $data['meta'];
			  		$metakeys = array();	

			  		foreach($resRole as $rolval) {  
                    	if (($rolval['role_shortname']) == (get_role($value['role']))){ 
                    		$role = $rolval['role_name'];
			  				array_push($metakeys, $user_id, $user_email, $nickname, $role);
                    	} 

                    }

			  		foreach ($meta as $key => $keyValue) {
				  		
				  		if ($keyValue == 'added_by') {
				  			$added_by = get_user_meta_value($user_id, $keyValue, TRUE);
				  			if ($added_by) {
				  				$uname = get_user_meta_value($added_by, 'first_name', TRUE).' '.get_user_meta_value($added_by, 'last_name', TRUE);
				  			}else{
				  				$uname = '';
				  			}
	         				

				  			array_push($metakeys, $uname);	
				  			
				  		}else if ($keyValue == 'assign_to') {
				  			$assign_to = get_user_meta_value($user_id, $keyValue, TRUE);
				  			if ($assign_to) {
				  				$uname = get_user_meta_value($assign_to, 'first_name', TRUE).' '.get_user_meta_value($assign_to, 'last_name', TRUE);
				  			}else{
				  				$uname = '';
				  			}
	         				

				  			array_push($metakeys, $uname);	
				  		}else if ($keyValue == '_assign_clinic') {
							$_assign_clinic = get_user_meta_value($user_id, $keyValue, TRUE);
							if ($_assign_clinic) {
								$uname = get_user_meta_value($_assign_clinic, 'first_name', TRUE).' '.get_user_meta_value($_assign_clinic, 'last_name', TRUE);
							}else{
								$uname = '';
							}
						   

							array_push($metakeys, str_replace('&amp;', '&', $uname));	
						}else if ($keyValue == '_assign_partner') {
							$_assign_partner = get_user_meta_value($user_id, $keyValue, TRUE);
							if ($_assign_partner) {
								$uname = get_user_meta_value($_assign_partner, 'first_name', TRUE).' '.get_user_meta_value($_assign_partner, 'last_name', TRUE);
							}else{
								$uname = '';
							}
						   

							array_push($metakeys, $uname);	
						} 
				  		else{
	         				$metaValue = get_user_meta_value($user_id, $keyValue, TRUE);
	         				// $added_by = get_user_meta_value($user_id, $keyValue, TRUE);

	         				// $uname = get_user_meta_value($added_by, 'first_name', TRUE).' '.get_user_meta_value($added_by, 'last_name', TRUE);


				  			array_push($metakeys, $metaValue);	
				  		}	


			  		}

				  	array_push($metakeys,$user_registered);

			  		
			  		fputcsv($output, $metakeys); 
				}
				 		
			}else{
				echo "No data found.";
			} 
			fclose($output); 		
			}				
		}						

}

