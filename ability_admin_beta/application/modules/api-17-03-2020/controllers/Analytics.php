<?php
if (!defined('BASEPATH'))
EXIT("No direct script access allowed");
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization');
header('Content-Type:application/json');

class Analytics extends MY_Controller {

    function __construct() {
        parent::__construct();
       date_default_timezone_set('MST7MDT');
    }
    function index()
    {}
     
  /**
	* /
	*  @ get_physician_active_list
	*
	*/
 function get_physician_active_list(){
 	    $physicians_data = array();
        $physicians_det = array();
        $physician_details = get_users_by_role_n_status($role = 'physician',$select_data = 'ID', $status = 'Active');
       
        foreach($physician_details as $key => $physician){        
			$physician_first_name = get_user_meta( $physician['ID'], 'first_name', '' );
			$physician_last_name  = get_user_meta( $physician['ID'], 'last_name' , '' );
			$physicians_det['physician_name'] = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
			$physicians_det['id'] = $physician['ID'];        
	        array_push($physicians_data, $physicians_det);
        }         
        echo json_encode(array('status'=>'1',  'physicians_data' => $physicians_data ));
   }
   
    
    function get_lab_anlytics()
    {
        $details = array();
      //Current month
        $currentmonth              = mktime(0, 0, 0, date("m"), 1, date("Y"));
        $currentmonthday           = mktime(0, 0, 0, date("m"),date("d"),date("Y"));
        $Current_Month             = date('M j',  $currentmonth);
        $Current_Month_Day         = date('M j',  $currentmonthday);
        $current_Month_LastDay     = date('t', strtotime(date('Y-m-d',$currentmonth)) );
        $Current_Month_present_Day = date('j',    $currentmonthday);
        $query_current_month       = date('Y-m-d',$currentmonth);
        $query_current_month_day   = date('Y-m-d',$currentmonthday);
        // last month
        $lastmonth                 = mktime(0, 0, 0, date("m")-1, 1, date("Y"));
        $Last_Month_Name           = date('M', $lastmonth); 
        $Last_Month                = date('M j', $lastmonth) ; 
        $Last_Month_Day            = date('t', strtotime(date('Y-m-d',$lastmonth)) );
        $query_last_month          = date('Y-m-d', $lastmonth);
        $query_last_month_days     = mktime(0, 0, 0, date("m")-1,$Last_Month_Day,date("Y"));
        $query_last_month_day      = date('Y-m-d', $query_last_month_days);
        //Current Month and date Details of Specimen
        $current_date_specimen_count_sql = "SELECT count(`id`) as number_of_id FROM `wp_abd_specimen` WHERE `create_date` BETWEEN '".$query_current_month." 00:00:00' and '".$query_current_month_day." 23:59:59' AND `status` = 0 AND `physician_accepct` = 0";
        $current_date_specimen_added_count = $this->BlankModel->customquery($current_date_specimen_count_sql);
        //Previous Month and date Details of Specimen
        $previous_date_specimen_count_sql = "SELECT count(`id`) as number_of_id FROM `wp_abd_specimen` WHERE `create_date` BETWEEN '".$query_last_month." 00:00:00' and '".$query_last_month_day." 23:59:59' AND `status` = 0 AND `physician_accepct` = 0";
        $previous_date_specimen_added_count = $this->BlankModel->customquery($previous_date_specimen_count_sql);
        //Current Month and date Details of Qc Check List
        $current_date_QcCheck_List_count_sql = "SELECT count(`id`) as number_of_id FROM `wp_abd_specimen` WHERE `qc_check` = 1 AND `status` = 0 AND `physician_accepct` = 0";
        $current_date_QcCheck_List_count = $this->BlankModel->customquery($current_date_QcCheck_List_count_sql);

        //Current Month Physician Count
        $get_users_sql = "SELECT `ID` 
        FROM wp_abd_users AS u
        LEFT JOIN wp_abd_usermeta AS um1 ON u.ID = um1.user_id
        LEFT JOIN wp_abd_usermeta AS um2 ON u.ID = um2.user_id
        WHERE  um1.meta_key = '_status' AND um1.meta_value = 'Active'
        AND um2.meta_key = 'wp_abd_capabilities' AND um2.meta_value LIKE '%physician%' AND u.user_registered BETWEEN '".$query_current_month." 00:00:00' AND '".$query_current_month_day." 23:59:59'";
        $users = $this->BlankModel->customquery($get_users_sql);
        $current_month_physician_count = count($users);
        
        //Previous Month Physician Count
        $get_users_sql_last = "SELECT `ID` 
        FROM wp_abd_users AS u
        LEFT JOIN wp_abd_usermeta AS um1 ON u.ID = um1.user_id
        LEFT JOIN wp_abd_usermeta AS um2 ON u.ID = um2.user_id
        WHERE  um1.meta_key = '_status' AND um1.meta_value = 'Active'
        AND um2.meta_key = 'wp_abd_capabilities' AND um2.meta_value LIKE '%physician%' AND u.user_registered BETWEEN '".$query_last_month." 00:00:00' AND '".$query_last_month_day." 23:59:59'";
        $users_last = $this->BlankModel->customquery($get_users_sql_last);
        $previous_month_physician_count = count($users_last);

        $physician_details = get_users_by_role_n_status($role = 'physician',$select_data = 'ID', $status = 'Active');
        $total_physician_count = count($physician_details);

        //Current Month and date Details of Nail Reports
        $current_date_pending_report_count_sql = "SELECT count(`nail_funagl_id`) as pending_report_count FROM `wp_abd_nail_pathology_report` WHERE `status` = 'Inactive'";
        $current_date_pending_report_added_count = $this->BlankModel->customquery($current_date_pending_report_count_sql);

        //Previous Month and date Details of Report
        $previous_date_pending_report_count_sql = "SELECT count(`nail_funagl_id`) as pending_report_count FROM `wp_abd_nail_pathology_report` WHERE `create_date` BETWEEN '".$query_last_month." 00:00:00' and '".$query_last_month_day." 23:59:59' AND `status` = 'Inactive'";
        $previous_date_pending_report_added_count = $this->BlankModel->customquery($previous_date_pending_report_count_sql);

        //Current Month and date Details of Nail Reports
        $current_date_issued_report_count_sql = "SELECT count(`nail_funagl_id`) as issued_report_count FROM `wp_abd_nail_pathology_report` WHERE `create_date` BETWEEN '".$query_current_month." 00:00:00' and '".$query_current_month_day." 23:59:59' AND `status` = 'Active'";
        $current_date_issued_report_added_count = $this->BlankModel->customquery($current_date_issued_report_count_sql);

        //Previous Month and date Details of Report
        $previous_date_issued_report_count_sql = "SELECT count(`nail_funagl_id`) as issued_report_count FROM `wp_abd_nail_pathology_report` WHERE `create_date` BETWEEN '".$query_last_month." 00:00:00' and '".$query_last_month_day." 23:59:59' AND `status` = 'Active'";
        $previous_date_issued_report_added_count = $this->BlankModel->customquery($previous_date_issued_report_count_sql);

        $active_physician_sql = "SELECT DISTINCT `physician_id` FROM `wp_abd_specimen`";
        $active_phy_count = $this->BlankModel->customquery($active_physician_sql);
        $count_active = count($active_phy_count);

        if(!empty($users))
        {
            $physicians_sending_specimens_count = $this->BlankModel->physicians_sending_specimens_count($users);
        }
        else
        {
            $physicians_sending_specimens_count=0;  
        }
        if(!empty($users_last))
        {
            $pre_physicians_sending_specimens_count = $this->BlankModel->physicians_sending_specimens_count($users_last);
        }
        else
        {
            $pre_physicians_sending_specimens_count=0;
        }
       
        $stage1_sql = "SELECT count(`id`) as number_of_incompleted_stages FROM `wp_abd_specimen` WHERE `status` = 0 AND `physician_accepct` = 0 AND `qc_check`=0 AND `id` NOT IN (SELECT `specimen_id` FROM `wp_abd_specimen_stage_details` WHERE `status` = 'Complete' AND `stage_id` = '1')";
        $stages1 = $this->BlankModel->customquery($stage1_sql);
        $stage2_sql = "SELECT count(`id`) as number_of_incompleted_stages FROM `wp_abd_specimen` WHERE `status` = 0 AND `physician_accepct` = 0 AND `qc_check`=0 AND `id` NOT IN (SELECT `specimen_id` FROM `wp_abd_specimen_stage_details` WHERE `status` = 'Complete' AND `stage_id` = '2')";
        $stages2 = $this->BlankModel->customquery($stage2_sql);
        $stage3_sql = "SELECT count(`id`) as number_of_incompleted_stages FROM `wp_abd_specimen` WHERE `status` = 0 AND `physician_accepct` = 0 AND `qc_check`=0 AND `id` NOT IN (SELECT `specimen_id` FROM `wp_abd_specimen_stage_details` WHERE `status` = 'Complete' AND `stage_id` = '3')";
        $stages3 = $this->BlankModel->customquery($stage3_sql);
        $stage4_sql = "SELECT count(`id`) as number_of_incompleted_stages FROM `wp_abd_specimen` WHERE `status` = 0 AND `physician_accepct` = 0 AND `qc_check`=0 AND `id` NOT IN (SELECT `specimen_id` FROM `wp_abd_specimen_stage_details` WHERE `status` = 'Complete' AND `stage_id` = '4')";
        $stages4 = $this->BlankModel->customquery($stage4_sql);
       
        die( json_encode(array( "status" => '1', 'current_Month' =>$Current_Month, 'Current_Month_Day'=>$Current_Month_Day, 'last_Month'=>$Last_Month, 
        'last_Month_Name'=>$Last_Month_Name, 'last_Month_Day'=>$Last_Month_Day,'current_specimen_count'=>$current_date_specimen_added_count[0]['number_of_id'],
        'previous_specimen_count'=>$previous_date_specimen_added_count[0]['number_of_id'],'current_phy_count'=>$current_month_physician_count, 'pre_phy_count'=>$previous_month_physician_count,
        'specimen_per_phy_current'=>round($current_date_specimen_added_count[0]['number_of_id']/$total_physician_count,1),
        'specimen_per_phy_prev'=>round($previous_date_specimen_added_count[0]['number_of_id']/$total_physician_count,1),
        'specimen_per_active_current'=>round($current_date_specimen_added_count[0]['number_of_id']/$count_active,1),
        'specimen_per_active_prev'=>round($previous_date_specimen_added_count[0]['number_of_id']/$count_active,1),
        'physicians_sending_specimens_count'=>$physicians_sending_specimens_count,
        'pre_physicians_sending_specimens_count'=>$pre_physicians_sending_specimens_count,
        'report_issued_curr'=>$current_date_issued_report_added_count[0]['issued_report_count'],
        'report_issued_prev'=>$previous_date_issued_report_added_count[0]['issued_report_count'],
        'total_phy_count'=>$total_physician_count,'qc_check_count'=>$current_date_QcCheck_List_count[0]['number_of_id'],
        'report_added'=>$current_date_pending_report_added_count[0]['pending_report_count'],
        'stage1'=>$stages1[0]['number_of_incompleted_stages'],'stage2'=>$stages2[0]['number_of_incompleted_stages'],
        'stage3'=>$stages3[0]['number_of_incompleted_stages'],'stage4'=>$stages4[0]['number_of_incompleted_stages']
        )));

    }

    function search_get_lab_anlytics()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        $from_date = $data['from_date'];
        $to_date = $data['to_date'];
        $spe_opt = $data['histo_pcr'];
        if(empty($spe_opt))
            {
            $search_date_specimen_count_sql = "SELECT count(`id`) as number_of_id FROM `wp_abd_specimen` WHERE `create_date` BETWEEN '".$from_date." 00:00:00' and '".$to_date." 23:59:59' AND `status` = 0 AND `physician_accepct` = 0";
            $search_date_specimen_count = $this->BlankModel->customquery($search_date_specimen_count_sql);
            }
            else if($spe_opt == 'Histo')
            {
            $search_date_specimen_count_sql = "SELECT count(`id`) as number_of_id
                                                FROM `wp_abd_specimen`
                                                INNER JOIN `wp_abd_clinical_info`
                                                ON `wp_abd_specimen`.`id` = `wp_abd_clinical_info`.`specimen_id` 
                                                 WHERE (`wp_abd_clinical_info`.`nail_unit`  LIKE '%1%' OR `wp_abd_clinical_info`.`nail_unit`  LIKE '%2%' OR `wp_abd_clinical_info`.`nail_unit`  LIKE '%3%')
                                                AND `wp_abd_specimen`.`status` = '0'
                                                AND `wp_abd_specimen`.`qc_check` = '0'
                                                AND `wp_abd_specimen`.`physician_accepct` = '0' 
                                                AND `wp_abd_specimen`.`create_date` BETWEEN '".$from_date." 00:00:00' and '".$to_date." 23:59:59'"; 
            $search_date_specimen_count = $this->BlankModel->customquery($search_date_specimen_count_sql);
            }
            else if($spe_opt == 'PCR')
            {
                $PCR_search_date_specimen_count_sql = "SELECT count(`id`) as number_of_id
                                                FROM `wp_abd_specimen`
                                                INNER JOIN `wp_abd_clinical_info`
                                                ON `wp_abd_specimen`.`id` = `wp_abd_clinical_info`.`specimen_id` 
                                                 WHERE (`wp_abd_clinical_info`.`nail_unit`  LIKE '%4%' OR `wp_abd_clinical_info`.`nail_unit`  LIKE '%5%' OR `wp_abd_clinical_info`.`nail_unit`  LIKE '%6%' OR `wp_abd_clinical_info`.`nail_unit`  LIKE '%7%')
                                                AND `wp_abd_specimen`.`status` = '0'
                                                AND `wp_abd_specimen`.`qc_check` = '0'
                                                AND `wp_abd_specimen`.`physician_accepct` = '0' 
                                                AND `wp_abd_specimen`.`create_date` BETWEEN '".$from_date." 00:00:00' and '".$to_date." 23:59:59'";
                $search_date_specimen_count = $this->BlankModel->customquery($PCR_search_date_specimen_count_sql);								

            }
            $Current_Month     = date('M j Y', strtotime($from_date));
            $Current_Month_Day = date('M j Y',  strtotime($to_date));
            $get_users_sql = "SELECT `ID` 
            FROM wp_abd_users AS u
            LEFT JOIN wp_abd_usermeta AS um1 ON u.ID = um1.user_id
            LEFT JOIN wp_abd_usermeta AS um2 ON u.ID = um2.user_id
            WHERE  um1.meta_key = '_status' AND um1.meta_value = 'Active'
            AND um2.meta_key = 'wp_abd_capabilities' AND um2.meta_value LIKE '%physician%' AND u.user_registered BETWEEN '".$from_date." 00:00:00' AND '".$to_date." 23:59:59'";
            $users = $this->BlankModel->customquery($get_users_sql);
            $search_physician_count = count($users);
            
            $query_from_month = date('Y-m-d',strtotime($from_date));
            $query_to_month = date('Y-m-d',strtotime($to_date));
            
            $get_users_sql_new = "SELECT `ID` 
            FROM wp_abd_users AS u
            LEFT JOIN wp_abd_usermeta AS um1 ON u.ID = um1.user_id
            LEFT JOIN wp_abd_usermeta AS um2 ON u.ID = um2.user_id
            WHERE  um1.meta_key = '_status' AND um1.meta_value = 'Active'
            AND um2.meta_key = 'wp_abd_capabilities' AND um2.meta_value LIKE '%physician%' AND u.user_registered BETWEEN '".$from_date." 00:00:00' AND '".$to_date." 23:59:59'";
            $physician_arg = $this->BlankModel->customquery($get_users_sql_new);
            $physicians_specimen_count = count($physician_arg);
            
            $physician_details = get_users_by_role_n_status($role = 'physician',$select_data = 'ID', $status = 'Active');
            $total_physician_count = count($physician_details);

            $search_date_QcCheck_List_count_sql ="SELECT count(`id`) as number_of_id FROM `wp_abd_specimen` WHERE `qc_check` = 1 AND `status` = 0 AND `physician_accepct` = 0";
            $search_date_QcCheck_List_count = $this->BlankModel->customquery($search_date_QcCheck_List_count_sql);
            $search_date_pending_report_count_sql ="SELECT count(`nail_funagl_id`) as pending_report_count FROM `wp_abd_nail_pathology_report` WHERE `status` = 'Inactive'";
            $search_date_pending_report_added_count =$this->BlankModel->customquery($search_date_pending_report_count_sql);
            
            if($spe_opt == 'PCR')
            {
            $search_date_issued_report_count_sql ="SELECT count(`id`) as issued_report_count FROM `wp_abd_auto_mail` WHERE `create_date` BETWEEN '".$from_date." 00:00:00' and '".$to_date." 23:59:59' AND `report_id` = '0'";
            $search_date_issued_report_added_count = $this->BlankModel->customquery($search_date_issued_report_count_sql);
            }
            else
            {
            $search_date_issued_report_count_sql ="SELECT count(`nail_funagl_id`) as issued_report_count FROM `wp_abd_nail_pathology_report` WHERE `create_date` BETWEEN '".$from_date." 00:00:00' and '".$to_date." 23:59:59' AND `status` = 'Active'";
            $search_date_issued_report_added_count = $this->BlankModel->customquery($search_date_issued_report_count_sql);
            }

            $active_physician_sql = "SELECT DISTINCT `physician_id` FROM `wp_abd_specimen`";
            $active_phy_count = $this->BlankModel->customquery($active_physician_sql);
            $count_active = count($active_phy_count);

            if(!empty($users))
            {
                $physicians_sending_specimens_count = $this->BlankModel->physicians_sending_specimens_count($users);
            }
            else
            {
                $physicians_sending_specimens_count=0;  
            }
            $stage1_sql = "SELECT count(`id`) as number_of_incompleted_stages FROM `wp_abd_specimen` WHERE `status` = 0 AND `physician_accepct` = 0 AND `qc_check`=0 AND `id` NOT IN (SELECT `specimen_id` FROM `wp_abd_specimen_stage_details` WHERE `status` = 'Complete' AND `stage_id` = '1')";
            $stages1 = $this->BlankModel->customquery($stage1_sql);
            $stage2_sql = "SELECT count(`id`) as number_of_incompleted_stages FROM `wp_abd_specimen` WHERE `status` = 0 AND `physician_accepct` = 0 AND `qc_check`=0 AND `id` NOT IN (SELECT `specimen_id` FROM `wp_abd_specimen_stage_details` WHERE `status` = 'Complete' AND `stage_id` = '2')";
            $stages2 = $this->BlankModel->customquery($stage2_sql);
            $stage3_sql = "SELECT count(`id`) as number_of_incompleted_stages FROM `wp_abd_specimen` WHERE `status` = 0 AND `physician_accepct` = 0 AND `qc_check`=0 AND `id` NOT IN (SELECT `specimen_id` FROM `wp_abd_specimen_stage_details` WHERE `status` = 'Complete' AND `stage_id` = '3')";
            $stages3 = $this->BlankModel->customquery($stage3_sql);
            $stage4_sql = "SELECT count(`id`) as number_of_incompleted_stages FROM `wp_abd_specimen` WHERE `status` = 0 AND `physician_accepct` = 0 AND `qc_check`=0 AND `id` NOT IN (SELECT `specimen_id` FROM `wp_abd_specimen_stage_details` WHERE `status` = 'Complete' AND `stage_id` = '4')";
            $stages4 = $this->BlankModel->customquery($stage4_sql);

        die( json_encode(array( "status" => '1', 'current_Month' =>$Current_Month, 'Current_Month_Day'=>$Current_Month_Day,
        'current_specimen_count'=>$search_date_specimen_count[0]['number_of_id'],
        'current_phy_count'=>$search_physician_count,
        'specimen_per_phy_current'=>round($search_date_specimen_count[0]['number_of_id']/$total_physician_count,1),
        'specimen_per_active_current'=>round($search_date_specimen_count[0]['number_of_id']/$count_active,1),
        'physicians_sending_specimens_count'=>$physicians_sending_specimens_count,
        'report_issued_curr'=>$search_date_issued_report_added_count[0]['issued_report_count'],
        'total_phy_count'=>$total_physician_count,'qc_check_count'=>$search_date_QcCheck_List_count[0]['number_of_id'],
        'report_added'=>$search_date_pending_report_added_count[0]['pending_report_count'],
        'stage1'=>$stages1[0]['number_of_incompleted_stages'],'stage2'=>$stages2[0]['number_of_incompleted_stages'],
        'stage3'=>$stages3[0]['number_of_incompleted_stages'],'stage4'=>$stages4[0]['number_of_incompleted_stages']
        )));

    }

    function get_histo_pcr_anlytics()
    {
        $histo_pcr_analytice = array();
        $specimen_sql ="SELECT `specimen`.`id`,`specimen`.`assessioning_num`, `specimen`.`patient_age`, `specimen`.`p_firstname`, `specimen`.`p_lastname`, `specimen`.`patient_sex`, `specimen`.`patient_state`, `specimen`.`patient_state`, `specimen`.`physician_id`, `report`.`diagnostic_short_code`, `report`.`nail_funagl_id`,`report`.`stains`, `report`.`create_date` as `report_create_date`  FROM 
        (SELECT `id`,`assessioning_num`,`patient_age`,`p_firstname`,`p_lastname`, `patient_sex`, `patient_state`, `patient_city`,`physician_id` FROM `wp_abd_specimen` WHERE `create_date` > '2017-03-27 23:59:59' AND `physician_accepct`= '0' AND `status`= '0' AND `qc_check`= '0')specimen
        INNER JOIN  
        (SELECT `nail_funagl_id`, `specimen_id`, `diagnostic_short_code`, `stains`, `create_date` FROM `wp_abd_nail_pathology_report`)report
        ON
        `specimen`.`id` = `report`.`specimen_id` ORDER BY `report`.`nail_funagl_id` DESC LIMIT 0,10";
            $specimen_data = $this->BlankModel->customquery($specimen_sql);
            $spe_count = count($specimen_data);
    
        if($specimen_data) {
	
        foreach($specimen_data as $histo_pcr_ana)
        {
					
        $specimen_id = $histo_pcr_ana['id'];
        $accessioning_num = $histo_pcr_ana['assessioning_num']; 
        $diagnostic_short_code =  $histo_pcr_ana['diagnostic_short_code'];
        $report_create_date = date('Y-m-d', strtotime($histo_pcr_ana['report_create_date']));

        $specimen_det = "SELECT * FROM `wp_abd_import_data` WHERE accessioning_num = '".$accessioning_num."' AND positive_negtaive ='positive'";	
        $specimen_resultss = $this->BlankModel->customquery($specimen_det);
        $count = count($specimen_resultss);

        $negative_sql =  "SELECT `positive_negtaive` FROM `wp_abd_import_data` WHERE accessioning_num = '".$accessioning_num."' AND positive_negtaive ='negative'";
        $negative_results = $this->BlankModel->customquery($negative_sql);
        $count_negative= count($negative_results);

        $run_sql =  "SELECT * FROM `wp_abd_import_data` WHERE accessioning_num = '".$accessioning_num."'";
        $run_sql_results = $this->BlankModel->customquery($run_sql);

        if(!empty($count))
        {
            $count_result_pos= $count." Pos ";
            if(!empty($count_negative)) { $count_result_neg= $count_negative." Neg";}
            $count_result = $count_result_pos.$count_result_neg;
        }
        if(empty($run_sql_results)) 
        {
            $count_result = "Not Run";
        }
        if (($count<1) && (!empty($run_sql_results)))
        {
            $count_result = "All Negative";
        }

							
	    $histo_pcr_analytics_info =  array( 'id' => $specimen_id,'count_result'=>$count_result, 'assessioning_num' => $accessioning_num, 'report_create_date' => $report_create_date, 'diagnostic_short_code' => $diagnostic_short_code);
				
		 array_push($histo_pcr_analytice,$histo_pcr_analytics_info);
		 
		 }
			 echo json_encode(array('status'=>'1','count' => $spe_count,"details" => $histo_pcr_analytice));	 
		}
        else
        {
			 echo json_encode(array('status'=>'0'));
		}

    }


    function get_more_histo_pcr_anlytics()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        if(!empty($data)){
            $start = $data['load'];
            $histo_pcr_analytice = array();
        $specimen_sql ="SELECT `specimen`.`id`,`specimen`.`assessioning_num`, `specimen`.`patient_age`, `specimen`.`p_firstname`, `specimen`.`p_lastname`, `specimen`.`patient_sex`, `specimen`.`patient_state`, `specimen`.`patient_state`, `specimen`.`physician_id`, `report`.`diagnostic_short_code`, `report`.`nail_funagl_id`,`report`.`stains`, `report`.`create_date` as `report_create_date`  FROM 
        (SELECT `id`,`assessioning_num`,`patient_age`,`p_firstname`,`p_lastname`, `patient_sex`, `patient_state`, `patient_city`,`physician_id` FROM `wp_abd_specimen` WHERE `create_date` > '2017-03-27 23:59:59' AND `physician_accepct`= '0' AND `status`= '0' AND `qc_check`= '0')specimen
        INNER JOIN  
        (SELECT `nail_funagl_id`, `specimen_id`, `diagnostic_short_code`, `stains`, `create_date` FROM `wp_abd_nail_pathology_report`)report
        ON
        `specimen`.`id` = `report`.`specimen_id` ORDER BY `report`.`nail_funagl_id` DESC LIMIT $start,10";
            $specimen_data = $this->BlankModel->customquery($specimen_sql);
            $spe_count = count($specimen_data);
    
        if($specimen_data) {
    
        foreach($specimen_data as $histo_pcr_ana)
        {
                    
        $specimen_id = $histo_pcr_ana['id'];
        $accessioning_num = $histo_pcr_ana['assessioning_num']; 
        $diagnostic_short_code =  $histo_pcr_ana['diagnostic_short_code'];
        $report_create_date = date('Y-m-d', strtotime($histo_pcr_ana['report_create_date']));

        $specimen_det = "SELECT * FROM `wp_abd_import_data` WHERE accessioning_num = '".$accessioning_num."' AND positive_negtaive ='positive'";    
        $specimen_resultss = $this->BlankModel->customquery($specimen_det);
        $count = count($specimen_resultss);

        $negative_sql =  "SELECT `positive_negtaive` FROM `wp_abd_import_data` WHERE accessioning_num = '".$accessioning_num."' AND positive_negtaive ='negative'";
        $negative_results = $this->BlankModel->customquery($negative_sql);
        $count_negative= count($negative_results);

        $run_sql =  "SELECT * FROM `wp_abd_import_data` WHERE accessioning_num = '".$accessioning_num."'";
        $run_sql_results = $this->BlankModel->customquery($run_sql);

        if(!empty($count))
        {
            $count_result_pos= $count." Pos ";
            if(!empty($count_negative)) { $count_result_neg= $count_negative." Neg";}
            $count_result = $count_result_pos.$count_result_neg;
        }
        if(empty($run_sql_results)) 
        {
            $count_result = "Not Run";
        }
        if (($count<1) && (!empty($run_sql_results)))
        {
            $count_result = "All Negative";
        }

                            
        $histo_pcr_analytics_info =  array( 'id' => $specimen_id,'count_result'=>$count_result, 'assessioning_num' => $accessioning_num, 'report_create_date' => $report_create_date, 'diagnostic_short_code' => $diagnostic_short_code);
                
         array_push($histo_pcr_analytice,$histo_pcr_analytics_info);
         
         }
             echo json_encode(array('status'=>'1','count' => $spe_count,"details" => $histo_pcr_analytice));     
        }
        else
        {
             echo json_encode(array('status'=>'0'));
        }

        }
    }

    function get_all_histo_pcr_anlytics()
    {
        $histo_pcr_analytice = array();
        $specimen_sql ="SELECT `specimen`.`id`,`specimen`.`assessioning_num`, `specimen`.`patient_age`, `specimen`.`p_firstname`, `specimen`.`p_lastname`, `specimen`.`patient_sex`, `specimen`.`patient_state`, `specimen`.`patient_state`, `specimen`.`physician_id`, `report`.`diagnostic_short_code`, `report`.`nail_funagl_id`,`report`.`stains`, `report`.`create_date` as `report_create_date`  FROM 
        (SELECT `id`,`assessioning_num`,`patient_age`,`p_firstname`,`p_lastname`, `patient_sex`, `patient_state`, `patient_city`,`physician_id` FROM `wp_abd_specimen` WHERE `create_date` > '2017-03-27 23:59:59' AND `physician_accepct`= '0' AND `status`= '0' AND `qc_check`= '0')specimen
        INNER JOIN  
        (SELECT `nail_funagl_id`, `specimen_id`, `diagnostic_short_code`, `stains`, `create_date` FROM `wp_abd_nail_pathology_report`)report
        ON
        `specimen`.`id` = `report`.`specimen_id` ORDER BY `report`.`nail_funagl_id` DESC";
            $specimen_data = $this->BlankModel->customquery($specimen_sql);
            $spe_count = count($specimen_data);
    
        if($specimen_data) {
	
        foreach($specimen_data as $histo_pcr_ana)
        {
					
        $specimen_id = $histo_pcr_ana['id'];
        $accessioning_num = $histo_pcr_ana['assessioning_num']; 
        $diagnostic_short_code =  $histo_pcr_ana['diagnostic_short_code'];
        $report_create_date = date('Y-m-d', strtotime($histo_pcr_ana['report_create_date']));
        $count =0;
        $count_negative=0;
        $count_result = '';

        $run_sql =  "SELECT * FROM `wp_abd_import_data` WHERE accessioning_num = '".$accessioning_num."'";
        $run_sql_results = $this->BlankModel->customquery($run_sql);
        if(!empty($run_sql_results)){

            for($i=0;$i<count($run_sql_results);$i++){

                if($run_sql_results[$i]['positive_negtaive'] == 'positive'){
                   $count++; 
                }
                if($run_sql_results[$i]['positive_negtaive'] == 'negative'){
                   $count_negative++; 
                }
            }
            
        }
        
        if(!empty($count))
        {
            $count_result_pos= $count." Pos ";
            if(!empty($count_negative)) { $count_result_neg= $count_negative." Neg";}
            $count_result = $count_result_pos.$count_result_neg;
        }
        if(empty($run_sql_results)) 
        {
            $count_result = "Not Run";
        }
        if (($count<1) && (!empty($run_sql_results)))
        {
            $count_result = "All Negative";
        }

							
	    $histo_pcr_analytics_info =  array( 'id' => $specimen_id,'count_result'=>$count_result, 'assessioning_num' => $accessioning_num, 'report_create_date' => $report_create_date, 'diagnostic_short_code' => $diagnostic_short_code);
				
		 array_push($histo_pcr_analytice,$histo_pcr_analytics_info);
		 
		 }
			 echo json_encode(array('status'=>'1','count' => $spe_count,"details" => $histo_pcr_analytice));	 
		}
        else
        {
			 echo json_encode(array('status'=>'0'));
		}
    }

    function search_all_histo_pcr_anlytics()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        $search = "";
        if($data['acc_no'])
        {
            $search.= " AND `specimen`.`assessioning_num` = '".$data['acc_no']."'";
        }
        if($data['from_date'] != "" &&  $data['to_date'] != ""){
            $search.= " AND `report`.`create_date` BETWEEN '".$data['from_date']." 00:00:00' and '".$data['to_date']." 23:59:59'";
        }

        $histo_pcr_analytice = array();
        $specimen_sql ="SELECT `specimen`.`id`,`specimen`.`assessioning_num`, `specimen`.`patient_age`, `specimen`.`p_firstname`, `specimen`.`p_lastname`, `specimen`.`patient_sex`, `specimen`.`patient_state`, `specimen`.`patient_state`, `specimen`.`physician_id`, `report`.`diagnostic_short_code`, `report`.`nail_funagl_id`,`report`.`stains`, `report`.`create_date` as `report_create_date`  FROM 
        (SELECT `id`,`assessioning_num`,`patient_age`,`p_firstname`,`p_lastname`, `patient_sex`, `patient_state`, `patient_city`,`physician_id` FROM `wp_abd_specimen` WHERE `create_date` > '2017-03-27 23:59:59' AND `physician_accepct`= '0' AND `status`= '0' AND `qc_check`= '0')specimen
        INNER JOIN  
        (SELECT `nail_funagl_id`, `specimen_id`, `diagnostic_short_code`, `stains`, `create_date` FROM `wp_abd_nail_pathology_report`)report
        ON
        `specimen`.`id` = `report`.`specimen_id`".$search." ORDER BY `report`.`nail_funagl_id` DESC";
        $histo_pcr_ana = $this->BlankModel->customquery($specimen_sql);
        $spe_count = count($histo_pcr_ana);
        if($histo_pcr_ana) {
	
        for($j=0;$j<$spe_count;$j++){
					
        $specimen_id = $histo_pcr_ana[$j]['id'];
        $accessioning_num = $histo_pcr_ana[$j]['assessioning_num']; 
        $diagnostic_short_code =  $histo_pcr_ana[$j]['diagnostic_short_code'];
        $report_create_date = date('Y-m-d', strtotime($histo_pcr_ana[$j]['report_create_date']));
        $count =0;
        $count_negative=0;
        $count_result = '';

        $run_sql =  "SELECT * FROM `wp_abd_import_data` WHERE accessioning_num = '".$accessioning_num."'";
        $run_sql_results = $this->BlankModel->customquery($run_sql);

        if(!empty($run_sql_results)){

            for($i=0;$i<count($run_sql_results);$i++){

                if($run_sql_results[$i]['positive_negtaive'] == 'positive'){
                   $count++; 
                }
                if($run_sql_results[$i]['positive_negtaive'] == 'negative'){
                   $count_negative++; 
                }
            }
            
        }

        if(!empty($count))
        {
            $count_result_pos= $count." Pos ";
            if(!empty($count_negative)) { $count_result_neg= $count_negative." Neg";}
            $count_result = $count_result_pos.$count_result_neg;
        }
        if(empty($run_sql_results)) 
        {
            $count_result = "Not Run";
        }
        if (($count<1) && (!empty($run_sql_results)))
        {
            $count_result = "All Negative";
        }

							
	    $histo_pcr_analytics_info =  array( 'id' => $specimen_id,'count_result'=>$count_result, 'assessioning_num' => $accessioning_num, 'report_create_date' => $report_create_date, 'diagnostic_short_code' => $diagnostic_short_code);
				
		 array_push($histo_pcr_analytice,$histo_pcr_analytics_info);
		 
		 }
			 echo json_encode(array('status'=>'1','count' => $spe_count,"details" => $histo_pcr_analytice));	 
        }
        else
        {
			 echo json_encode(array('status'=>'0'));
		}

    }
  //   function get_lab_data_anlytics()
  //   {   
  //       $data = json_decode(file_get_contents('php://input'), true);
  //       $search = "";
  //       $date= date('Y-m-d');
  //       $specimen_sql ="SELECT `assessioning_num`,`id`,`p_firstname`,`p_lastname`,`patient_age`,`create_date` FROM `wp_abd_specimen` WHERE `create_date` BETWEEN '2018-03-01 00:00:00' AND '".$date." 23:59:59' AND `physician_accepct`= 0 AND `status`= 0 AND `qc_check`=0";
  //       $run_sql_results = $this->BlankModel->customquery($specimen_sql);
  //       $count = count($run_sql_results);
  //       $data_display_date = "Data display from March 01, 2018 to the current date.";

  //       if($run_sql_results)
  //       {
  //           echo json_encode(array('status'=>'1','count' => $count,"details" => $run_sql_results,'data_display_date' =>$data_display_date));	 
  //       }
  //       else
  //       {
		// 	 echo json_encode(array('status'=>'0'));
		// }

  //   }

    function get_lab_data_anlytics()
    {   
        $data = json_decode(file_get_contents('php://input'), true);
        $search = "";
        $date= date('Y-m-d');

        $new_array = array();

        $specimen_sql ="SELECT `assessioning_num`,`id`,`p_firstname`,`p_lastname`,`patient_age`,`create_date` FROM `wp_abd_specimen` WHERE `create_date` BETWEEN '2018-03-01 00:00:00' AND '".$date." 23:59:59' AND `physician_accepct`= 0 AND `status`= 0 AND `qc_check`=0";
        $run_sql_results = $this->BlankModel->customquery($specimen_sql);
        $count = count($run_sql_results);
        $data_display_date = "Data display from March 01, 2018 to the current date.";
        foreach($run_sql_results as $val)
        {
        $new_array_data = array(
			'assessioning_num' =>  $val['assessioning_num'],
			'p_firstname'=>  trim($val['p_firstname']),
			'p_lastname'        =>  $val['p_lastname'],
			'create_date'       =>  $val['create_date'],
			'patient_age'        => (int)$val['patient_age'],
			'id'        => (int) $val['id'],
		);
		array_push($new_array,$new_array_data);
        }
        if($run_sql_results)
        {
            echo json_encode(array('status'=>'1','count' => $count,"details" => $new_array,'data_display_date' =>$data_display_date));	 
        }
        else
        {
			 echo json_encode(array('status'=>'0'));
		}

    }
    
    function search_lab_data_anlytics()
    {   
        $data = json_decode(file_get_contents('php://input'), true);
        $search = "";
        $date= date('Y-m-d');
        $specimen_sql ="SELECT `assessioning_num`,`id`,`p_firstname`,`p_lastname`,`patient_age`,`create_date` FROM `wp_abd_specimen` WHERE `create_date` BETWEEN '".$data['from_date']." 00:00:00' AND '".$data['to_date']." 23:59:59' AND `physician_accepct`= 0 AND `status`= 0 AND `qc_check`=0";
        $run_sql_results = $this->BlankModel->customquery($specimen_sql);
        $count = count($run_sql_results);
        $data_display_date = "Data display from ".$data['from_date']." to ".$data['to_date'];

        if($run_sql_results)
        {
            echo json_encode(array('status'=>'1','count' => $count,"details" => $run_sql_results,'data_display_date' =>$data_display_date));	 
        }
        else
        {
			 echo json_encode(array('status'=>'0'));
		}


    }

    function get_short_code_anlytics()
    {
        $details =  array();
        $specimen_sql ="SELECT `specimen`.`id`,`specimen`.`assessioning_num` AS `accessioning_number`, `specimen`.`patient_age`, CONCAT(`specimen`.`p_firstname`,' ',`specimen`.`p_lastname`) AS patient_name, `specimen`.`patient_sex`, `specimen`.`patient_city`, `specimen`.`patient_state`, `specimen`.`physician_id`, `report`.`diagnostic_short_code`, `report`.`nail_funagl_id`, `report`.`addendum_desc`, `report`.`addendum`, `report`.`stains`,`report`.`gross_description`, DATE_FORMAT(`report`.`create_date`, '%Y-%m-%d') AS `report_create_date` FROM 
        (SELECT `id`,`assessioning_num`, `patient_age`,`p_firstname`,`p_lastname`, `patient_sex`, `patient_state`, `patient_city`,`physician_id`, `create_date` FROM `wp_abd_specimen` WHERE `create_date` > '2017-03-27 23:59:59' AND `physician_accepct`= '0' AND `status`= '0' AND `qc_check`= '0')specimen
        INNER JOIN (SELECT * FROM `wp_abd_nail_pathology_report`)report
        ON
        `specimen`.`id` = `report`.`specimen_id` ORDER BY `report`.`nail_funagl_id` DESC";
        $run_sql_results = $this->BlankModel->customquery($specimen_sql);
        
        $count = count($run_sql_results);
        if($run_sql_results)
        {
            foreach($run_sql_results as $specimen)
            {
                $physician_first_name = get_user_meta( $specimen['physician_id'], 'first_name', '' );
                $physician_last_name  = get_user_meta( $specimen['physician_id'], 'last_name' , '' );
                $phy_name =  $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
                $specimen_information =  array('id' => $specimen['id'], 'assessioning_num' => $specimen['accessioning_number'], 'physician_name' => $phy_name, 'patient_name' => $specimen['patient_name'], 'patient_age' => $specimen['patient_age'], 'patient_state' => $specimen['patient_state'],'patient_city' => $specimen['patient_city'],'report_create_date' => $specimen['report_create_date'],'gross_description' => $specimen['gross_description'],'short_code'=>$specimen['diagnostic_short_code']);
                array_push($details, $specimen_information);
		
            }
            echo json_encode(array('status'=>'1','count' => $count,"details" => $details));	 
        }
        else
        {
			 echo json_encode(array('status'=>'0'));
		}
    }

    /**
	* 
	* Search Short Code
	*/

     function search_short_code_anlytics()
     {
        $data = json_decode(file_get_contents('php://input'), true);
        $search = "";
		  
            if($data['from_date']!="" && $data['to_date']!=""){
            $search.= " AND `report`.`create_date` BETWEEN '".$data['from_date']." 00:00:00' and '".$data['to_date']." 23:59:59'";	
            }
            if($data['report_code']!=""){
            $search.= " AND `report`.`diagnostic_short_code` LIKE '%".$data['report_code']."%'";
            }
            if($data['patient_age_from']!="" && $data['patient_age_to']!=""){
            $search.= " AND `specimen`.`patient_age` BETWEEN '".$data['patient_age_from']."' AND '".$data['patient_age_to']."'";
            }
            if($data['sex']!=""){
            $search.= " AND `specimen`.`patient_sex`='".$data['sex']."'";
            }
            if($data['patient_name']!=""){
            $search.= " AND (CONCAT( `specimen`.`p_firstname`, ' ', `specimen`.`p_lastname`) LIKE '%".$data['patient_name']."%')";
            } 
            if($data['state']!=""){
            $search.= " AND `specimen`.`patient_state` LIKE '%".$data['state']."%'";
            }
            if($data['city']!=""){
            $search.= " AND `specimen`.`patient_city` LIKE '%".$data['city']."%'";
            }
            if($data['stain_code']!=""){
            $search.= " AND `report`.`stains` LIKE '%".$data['stain_code']."%'";
            } 
         
            if($data['acc_no']!=""){
            $search.= " AND `specimen`.`assessioning_num` LIKE '%".$data['acc_no']."%'";
            }
            if($data['physician_id']!=""){
            $search.= " AND `specimen`.`physician_id` = '".$data['physician_id']."'";
            }
          
            $details =  array();
            $specimen_sql = "SELECT `specimen`.`id`,`specimen`.`assessioning_num` AS `accessioning_number`, `specimen`.`patient_age`, CONCAT(`specimen`.`p_firstname`,' ',`specimen`.`p_lastname`) AS patient_name, `specimen`.`patient_sex`, `specimen`.`patient_city`, `specimen`.`patient_state`, `specimen`.`physician_id`, `report`.`diagnostic_short_code`, `report`.`nail_funagl_id`, `report`.`gross_description`, `report`.`addendum_desc`, `report`.`addendum`, DATE_FORMAT(`report`.`create_date`, '%Y-%m-%d') AS `report_create_date`, `report`.`stains`  
            FROM 
            (SELECT `id`,`assessioning_num`, `patient_age`,`p_firstname`,`p_lastname`, `patient_sex`, `patient_state`, `patient_city`,`physician_id`, `create_date` FROM `wp_abd_specimen` WHERE `create_date` > '2017-03-27 23:59:59' AND `physician_accepct`= '0' AND `status`= '0' AND `qc_check`= '0')specimen
          
            INNER JOIN (SELECT `diagnostic_short_code`, `specimen_id`, `nail_funagl_id`, `gross_description`, `addendum_desc`, `addendum`, `create_date`, `stains`  FROM `wp_abd_nail_pathology_report`)report
            ON
            `specimen`.`id` = `report`.`specimen_id` ".$search." ORDER BY `specimen`.`assessioning_num` DESC";
           
            $run_sql_results = $this->BlankModel->customquery($specimen_sql);
            
            $count = count($run_sql_results);
            if($run_sql_results){   
                foreach($run_sql_results as $specimen){
                
                    $pos_count      =  0;
                    $count_result   = "";
                    $physician_first_name = get_user_meta_value( $specimen['physician_id'], 'first_name', '' );
                    $physician_last_name  = get_user_meta_value( $specimen['physician_id'], 'last_name' , '' );
                    $phy_name             = $physician_first_name.' '.$physician_last_name;
           
                    $specimen_id = $specimen['id'];
                    $accessioning_num = $specimen['accessioning_number']; 

                    $import_data_sql = "SELECT COUNT( `positive_negtaive` ) AS `count_data`, `positive_negtaive` FROM `wp_abd_import_data` WHERE `accessioning_num` = '".$accessioning_num."' GROUP BY `positive_negtaive` ";
                    
                    $import_data_results = $this->BlankModel->customquery($import_data_sql);


                    if(!empty($import_data_results)){

                            foreach($import_data_results as $pos_nev_count){
						    if($pos_nev_count['positive_negtaive'] == 'positive'){                      	
                               $count_result_pos = $pos_nev_count['count_data']." Pos ";                        
                               $pos_count = $pos_nev_count['count_data'];                                                           
                            }
                            
                            if($pos_nev_count['positive_negtaive'] == 'negative'){
                               $count_result_neg = $pos_nev_count['count_data']." Neg";                      
                            }
                          	}                     
                            $count_result = $count_result_pos.$count_result_neg;
                    }
                    else{
						    $count_result = "Not Run";
					}                    
                    if( ($pos_count < 1) && (!empty($import_data_results)) ){
						    $count_result = "All Negative";
					} 
                     
                
                    $specimen_information = array('id' => $specimen_id, 'assessioning_num' => $accessioning_num, 'physician_name' => $phy_name, 'patient_name' => $specimen['patient_name'], 'patient_age' => $specimen['patient_age'], 'patient_state' => $specimen['patient_state'],'patient_city' => $specimen['patient_city'],'report_create_date' => $specimen['report_create_date'], 'gross_description' => $specimen['gross_description'], 'short_code'=> $specimen['diagnostic_short_code'], 'count_result' => $count_result );
                  
                    array_push($details, $specimen_information);
                     
                 }
                echo json_encode(array('status' => '1', 'count' => $count, 'details' => $details ));	 
            }
            else
            {
                 echo json_encode(array('status'=>'0','msg' => 'No data found'));
            } 
                 
            
    }

	 /**
	 * 
	 * Export Short Code
	 */
    function export_short_code_anlytics()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        $search = "";

            if($data['from_date']!="" && $data['to_date']!=""){
            $search.= " AND `report`.`create_date` BETWEEN '".$data['from_date']." 00:00:00' and '".$data['to_date']." 23:59:59'";	
            }
            if($data['report_code']!=""){
            $search.= " AND `report`.`diagnostic_short_code` LIKE '%".$data['report_code']."%'";
            }
            if($data['patient_age_from']!="" && $data['patient_age_to']!=""){
            $search.= " AND `specimen`.`patient_age` BETWEEN '".$data['patient_age_from']."' AND '".$data['patient_age_to']."'";
            }
            if($data['sex']!=""){
            $search.= " AND `specimen`.`patient_sex`='".$data['sex']."'";
            }
            if($data['patient_name']!=""){
            $search.= " AND (CONCAT( `specimen`.`p_firstname`, ' ', `specimen`.`p_lastname`) LIKE '%".$data['patient_name']."%')";
            } 
            if($data['state']!=""){
            $search.= " AND `specimen`.`patient_state` LIKE '%".$data['state']."%'";
            }
            if($data['city']!=""){
            $search.= " AND `specimen`.`patient_city` LIKE '%".$data['city']."%'";
            }
            if($data['stain_code']!=""){
            $search.= " AND `report`.`stains` LIKE '%".$data['stain_code']."%'";
            }                       
            if($data['acc_no']!=""){
            $search.= " AND `specimen`.`assessioning_num` LIKE '%".$data['acc_no']."%'";
            }
            if($data['physician_id']!=""){
            $search.= " AND `specimen`.`physician_id` = '".$data['physician_id']."'";
            }                 

            $details =  array();            
            $specimen_sql ="SELECT `specimen`.`id`,`specimen`.`assessioning_num` AS `accessioning_number`, `specimen`.`patient_age`, CONCAT(`specimen`.`p_firstname`,' ',`specimen`.`p_lastname`) AS patient_name, `specimen`.`patient_sex`, `specimen`.`patient_city`, `specimen`.`patient_state`, `specimen`.`physician_id`, `report`.`diagnostic_short_code`, `report`.`nail_funagl_id`, `report`.`gross_description`, `report`.`addendum_desc`, `report`.`addendum`, DATE_FORMAT(`report`.`create_date`, '%Y-%m-%d') AS `report_create_date`, `report`.`stains` 
            FROM 
            (SELECT `id`,`assessioning_num`, `patient_age`,`p_firstname`,`p_lastname`, `patient_sex`, `patient_state`, `patient_city`,`physician_id`, `create_date` FROM `wp_abd_specimen` WHERE `create_date` > '2017-03-27 23:59:59' AND `physician_accepct`= '0' AND `status`= '0' AND `qc_check`= '0')specimen
            
            INNER JOIN (SELECT `diagnostic_short_code`, `specimen_id`, `nail_funagl_id`, `gross_description`, `addendum_desc`, `addendum`, `create_date`, `stains`  FROM `wp_abd_nail_pathology_report`)report
            ON
            `specimen`.`id` = `report`.`specimen_id` ".$search." ORDER BY `specimen`.`assessioning_num` DESC";
           
   
            $run_sql_results = $this->BlankModel->customquery($specimen_sql);           
            $count = count($run_sql_results);
            $field_arr = array();
            $api_data = array();
            $target_arr = array();
            $neg_arr = array();
            $access_arr = array();
            $selected_fields['physician_id'] = $data['physicianId'] ;       
            $selected_fields['accessioning_number'] = $data['accessioning_number'] ;      
            $selected_fields['patient_sex'] = $data['gender'] ;        
            $selected_fields['patient_name'] = $data['patientName'];       
            $selected_fields['patient_age'] = $data['patient_age'];          
            $selected_fields['patient_state'] = $data['patient_state'];    
            $selected_fields['patient_city'] = $data['patient_city'];        
            $selected_fields['report_create_date'] = $data['report_genarate_date'];       
            $selected_fields['diagnostic_short_code'] = $data['diagnostic_short_code'];         
            $selected_fields['stains'] = $data['stains'];         
            $selected_fields['gross_description'] = $data['gross_dimension'];           
            $selected_fields['pcr_test'] = $data['pcr_test'];       
            $selected_fields['addendum_desc'] = $data['addendum'];          
            
            $filter_array = array_filter($selected_fields);
            $fields_Name = array_keys($filter_array);
           
        
           if($run_sql_results){
               foreach($run_sql_results as $specimen_result){
                    $schema_insert = "";
                   
                    foreach ($fields_Name as $value){
                       
                        if($value == "physician_id"){
                        $field_value = get_user_meta_value($specimen_result[$value], 'first_name', true).' '.get_user_meta_value($specimen_result[$value], 'last_name', true);
                        }                      
                       else if($value == "pcr_test"){                        	
			            $field_value = "";
                        $spe_acc_no  = $specimen_result['accessioning_number'];                           
			            $import_data_sql = "SELECT `target_name`,`accessioning_num`, `positive_negtaive` FROM `wp_abd_import_data` WHERE `accessioning_num` = '".$spe_acc_no."'";
			            $import_data_results = $this->BlankModel->customquery($import_data_sql);
		                     
                        $target_name = "";
                        $neg_arr = array();
                        if(!empty($import_data_results)){
                        
                            foreach ($import_data_results as $key) {
                               $count_negative = 0;
                              
                            if( $key['accessioning_num'] == $spe_acc_no && $key['positive_negtaive'] == 'positive'){
                              
                                $nw_arr = array( 'target_name' => $key['target_name'] );
                                array_push($target_arr, $nw_arr);


                                if($key['target_name'] != "Xeno_Ac00010014_a1"){
                                
                                    $assay_name = explode("_", $key['target_name']);
                                    $excel_data = $assay_name[0]." >> ";
                                    $target_name.= $excel_data;	
                                    }
                            }
                            
                            if($key['accessioning_num'] == $spe_acc_no && $key['positive_negtaive'] == 'negative'){
                                array_push($neg_arr, $key['positive_negtaive']);
                            }
                            
                            if($key['accessioning_num'] == $spe_acc_no){
                                $nw_arr = array('accessioning_num'=>$key['accessioning_num']);
                                array_push($access_arr, $nw_arr);
                            }
                        }
                        
                        $target_count   = count($target_arr);
                        $count_negative = count($neg_arr);
                 
                        if(!empty($target_arr)){
                            if(!empty($count_negative)){ 
                                $negative_count = $count_negative." Neg";
                            }             
                            $field_value = $target_name.' '.$negative_count ;
                        }

                        if (($target_count < 1) && (!empty($access_arr['accessioning_num']))){
                            $field_value = "All Negative";
                        }
                       
                        }
                        else{
						    $field_value = "Not Run";
					        }    
                        }
                      
                        else if($value == "addendum_desc"){
                            $field_value = "";
                            $addendum_val = unserialize($specimen_result['addendum_desc']);
                            if(!empty($addendum_val[0]))
                            {
                           	 $rep_field_value='';
                            foreach($addendum_val as $rep_value)
                            {
                           	 $rep_field_value.=  $rep_value.',-';
                            } 
                            
                           	 $field_value = $rep_field_value;
                            }

                            else if($specimen_result['addendum'] != "")
                            {
	                            $nail_addendum = $specimen_result['addendum'];
	                            $addendum_codes  = explode(",",$nail_addendum);
	                            
	                            foreach($addendum_codes as $addendum_code){
		                            if($addendum_code){
		                                $addendum_desc = get_new_stains_desc($addendum_code); 
		                                $field_value = $addendum_desc;
		                            }
	                            }
                            }
                        }
                        
                        else{
                        $field_value = "";
                        $field_value = $specimen_result[$value];
                        }
                                          
                        if($value == 'addendum_desc'){                            
                            $field_arr['addendum_desc'] = $field_value;                           
                        }
                        if($value == 'accessioning_number'){
                            $field_arr['accessioning_number'] = $field_value;
                        }
                        if($value == 'patient_age'){
                            $field_arr['patient_age'] = $field_value;
                        }
                        if($value == 'patient_name'){
                            $field_arr['patient_name'] = $field_value;
                        }
                        if($value == 'patient_sex'){
                            $field_arr['patient_sex'] = $field_value;
                        }
                        if($value == 'patient_city'){
                            $field_arr['patient_city'] = $field_value;
                        }
                        if($value == 'patient_state'){
                            $field_arr['patient_state'] = $field_value;
                        }
                        if($value == 'diagnostic_short_code'){
                            $field_arr['diagnostic_short_code'] = $field_value;
                        }
                        if($value == 'nail_funagl_id'){
                            $field_arr['nail_funagl_id'] = $field_value;
                        }
                        if($value == 'stains'){
                            $field_arr['stains'] = $field_value;
                        }
                        if($value == 'gross_description'){
                            $field_arr['gross_description'] = $field_value;
                        }
                        if($value == 'physician_id'){
                            $field_arr['physician_name'] = $field_value;
                        }
                        if($value == 'report_create_date'){
                            $field_arr['report_create_date'] = $field_value;
                        }
                        if($value == 'pcr_test'){
                            $field_arr['pcr_test'] = $field_value;
                        }                       
                    }
               
                    array_push($api_data, $field_arr);                               
                }
    
                die(json_encode(array('status'=>'1', 'details'=> $api_data, 'count' => $count)));
            }
            else{
                 die(json_encode(array('status'=>'0','msg' =>'No data found')));
            }    
            
    }
        
    function get_diagnosis_list()
    {
        $information = array();
        $information2 = array();
        $information3 = array();
        $total_specimen_green_code=0;
        $total_specimen_yellow_code=0;
        $total_specimen_red_code=0;
        $physicians_data = array();
        $physicians_det = array();
        $physician_details = get_users_by_role_n_status($role = 'physician',$select_data = 'ID', $status = 'Active');
        foreach($physician_details as $key => $physician){        
			$physician_first_name = get_user_meta( $physician['ID'], 'first_name', '' );
			$physician_last_name  = get_user_meta( $physician['ID'], 'last_name' , '' );
			$physicians_det['physician_name'] = $physician_first_name['meta_value'].' '.$physician_last_name['meta_value'];
			$physicians_det['id'] = $physician['ID'];        
            array_push($physicians_data, $physicians_det);
        } 
          
        $green_report_codes = $this->BlankModel->customquery("select * from `wp_abd_nail_macro_codes` where color = 'green'");
        foreach($green_report_codes as $dis){
            $total_green_code =0;
            $specimen_count = specimen_code_count($dis['sc']);
            $total_green_code += $specimen_count;    
            $total_specimen_green_code += $total_green_code;
            
            $specimen_count_sql = $this->BlankModel->customquery("SELECT count(`nail_funagl_id`) as number_of_specimen FROM `wp_abd_nail_pathology_report` WHERE `diagnostic_short_code` = '".$dis['sc']."'");
            $green_specimen_count = $specimen_count_sql[0]['number_of_specimen'];
            $info['count'] = $green_specimen_count;
            array_push($information,$info);
        }
   										
        $yellow_report_codes = $this->BlankModel->customquery("select * from `wp_abd_nail_macro_codes` where color = 'yellow'");
        foreach($yellow_report_codes as $dis)
        {	
            $total_yellow_code =0;	
  	        $specimen_count = specimen_code_count($dis['sc']);
  		    $total_yellow_code += $specimen_count;
            $total_specimen_yellow_code += $total_yellow_code;
            
            $specimen_count_sql = $this->BlankModel->customquery("SELECT count(`nail_funagl_id`) as number_of_specimen FROM `wp_abd_nail_pathology_report` WHERE `diagnostic_short_code` = '".$dis['sc']."'");
            $yellow_specimen_count = $specimen_count_sql[0]['number_of_specimen'];
            $info_yellow['count'] = $yellow_specimen_count;
            array_push($information2,$info_yellow);
  			
        }
  
        $red_report_codes = $this->BlankModel->customquery("select * from `wp_abd_nail_macro_codes` where color = 'red'");
        foreach($red_report_codes as $dis) 
        {	
            $total_red_code =0;	
            $specimen_count = specimen_code_count($dis['sc']);
            $total_red_code += $specimen_count;
            $total_specimen_red_code += $total_red_code;
            
            $specimen_count_sql = $this->BlankModel->customquery("SELECT count(`nail_funagl_id`) as number_of_specimen FROM `wp_abd_nail_pathology_report` WHERE `diagnostic_short_code` = '".$dis['sc']."'");
            $red_specimen_count = $specimen_count_sql[0]['number_of_specimen'];
            $info_red['count'] = $red_specimen_count;
            array_push($information3,$info_red);
        }
        $total_specimen_codes_count = $total_specimen_red_code+	$total_specimen_yellow_code +$total_specimen_green_code;	
        $green_per = round(($total_specimen_green_code / $total_specimen_codes_count) *100);
        $yellow_per = round(($total_specimen_yellow_code / $total_specimen_codes_count) *100);
        $red_per = round(($total_specimen_red_code / $total_specimen_codes_count) *100);
        
        echo json_encode(array('status'=>'1','physicians_data' => $physicians_data,
        'green'=>$total_specimen_green_code,'yellow'=>$total_specimen_yellow_code,'red'=>$total_specimen_red_code,
        'green_per'=>$green_per, 'yellow_per'=>$yellow_per,'red_per'=>$red_per,'short_code1'=>$green_report_codes,
        'short_code2'=>$yellow_report_codes,'short_code3'=>$red_report_codes,'green_info'=>$information,'yellow_info'=>$information2,'red_info'=>$information3));
    }

	 /**
	 * Search_diagnosis
	 */

    function search_diagnosis()
    {   
        $green_information = array();
        $yellow_information = array();
        $red_information = array();
        $total_green_report = 0;
        $total_yellow_report =0;
        $total_red_report = 0;
        $total_specimen_red_code = 0;
        $total_green_code = 0;
        $specimen_count = 0;
        $search = "";
        $number_of_specimen = 0;
        $total_specimen_codes_count = 0;
        $data = json_decode(file_get_contents('php://input'), true);
        
		if(isset( $data['physician'])){
            $specimen_det = $this->BlankModel->customquery("SELECT `id` FROM `wp_abd_specimen` WHERE status = '0' AND `physician_id`='".$data['physician']."' AND `physician_accepct` = '0' AND qc_check = '0' AND `create_date` > '2017-03-27 23:59:59'");	
         }
         if($data['from_date']!="" && $data['to_date']!="")
         {  
            $specimen_det = $this->BlankModel->customquery("SELECT `id` FROM `wp_abd_specimen` WHERE status = '0' AND `create_date` BETWEEN '".$data['from_date']." 00:00:00' and '".$data['to_date']." 23:59:59' AND `physician_accepct` = '0' AND qc_check = '0' AND `create_date` > '2017-03-27 23:59:59'");
         }
         
		  /**
		   * 
		   * @var / Green Report specimen count
		   * 
		   */	        
         $green_report_codes = $this->BlankModel->customquery("select `sc` from `wp_abd_nail_macro_codes` where color = 'green'");
         if($green_report_codes){			
	      foreach($green_report_codes as $green_report_d_s_code) 
          {
            
           $number_of_specimen = 0;
           $info = array();
           foreach($specimen_det as $specimen)
           {                        
            $green_specimen_count = specimen_color_code_count($green_report_d_s_code['sc'] ,$specimen['id']);
	        if($green_specimen_count){		 
				$number_of_specimen += $green_specimen_count;
		        $info['count'] = $number_of_specimen;
	          }              
            } 
            array_push($green_information, $info);          
            $total_green_report += $number_of_specimen;		 
		  }         
          $total_specimen_green_code = $total_green_report;      	 
      	 }  
   
		   /**
		   * 
		   * @var / Yellow Report specimen count
		   * 
		   */	  
	      $yellow_report_codes = $this->BlankModel->customquery("select `sc` from `wp_abd_nail_macro_codes` where color = 'yellow'");
        if($yellow_report_codes){			
	      foreach($yellow_report_codes as $yellow_report_d_s_code) 
          {            
           $number_of_specimen = 0;
           $info_yellow = array();
           foreach($specimen_det as $specimen)
           {                        
             $yellow_report_count = specimen_color_code_count($yellow_report_d_s_code['sc'] ,$specimen['id']);
	        if($yellow_report_count){		 
				$number_of_specimen += $yellow_report_count;
		        $info_yellow['count'] = $number_of_specimen;
	          }              
            } 
            array_push($yellow_information, $info_yellow);          
            $total_yellow_report += $number_of_specimen;		 
		  }         
          $total_specimen_yellow_code = $total_yellow_report;      	 
      	 }      
      	 
      	 /**
		   * 
		   * @var / Red Report specimen count
		   * 
		   */
          $red_report_codes = $this->BlankModel->customquery("select `sc` from `wp_abd_nail_macro_codes` where color = 'red'");
        if($red_report_codes){			
	      foreach($red_report_codes as $red_report_d_s_code) 
          {            
           $number_of_specimen = 0;
           $info_red = array();
           foreach($specimen_det as $specimen)
           {                        
             $red_report_count = specimen_color_code_count($red_report_d_s_code['sc'] ,$specimen['id']);
	        if($red_report_count){		 
				$number_of_specimen += $red_report_count;
		        $info_red['count'] = $number_of_specimen;
	          }              
            } 
            array_push($red_information, $info_red);          
            $total_red_report += $number_of_specimen;		 
		  }         
          $total_specimen_red_code = $total_red_report;      	 
      	 }  

         $total_specimen_codes_count = ($total_specimen_red_code+$total_specimen_yellow_code +$total_specimen_green_code);	
         if($total_specimen_codes_count!=0)
         {
            $yellow_per = round(($total_specimen_yellow_code / $total_specimen_codes_count) *100);
            $red_per = round(($total_specimen_red_code / $total_specimen_codes_count) *100);
            $green_per = round(($total_specimen_green_code / $total_specimen_codes_count) *100);
       
         }
         else
         {
            $yellow_per=0;
            $red_per=0;
            $green_per=0;
         }
         
         echo json_encode(array('status' => '1',
         'green' => $total_specimen_green_code,
         'yellow' => $total_specimen_yellow_code,
         'red' => $total_specimen_red_code,
         'short_code1' => $green_report_codes,
         'short_code2' => $yellow_report_codes,
         'short_code3' => $red_report_codes,
         'green_info' => $green_information,
         'yellow_info' => $yellow_information,
         'red_info' => $red_information,
         'green_per' => $green_per, 
         'yellow_per' => $yellow_per,
         'red_per' => $red_per        
         ));      
        
    }

    function get_avg_time()
    {
        $turn_around_time_specimen_sql = "SELECT DATEDIFF(`nail_report`.`create_date`, `specimen`.`create_date`) as difference, `specimen`.`assessioning_num` FROM (SELECT * FROM `wp_abd_specimen` WHERE `create_date` > '2017-03-27 23:59:59' AND `qc_check` = '0' AND `physician_accepct` = '0' AND `status` = '0')specimen INNER JOIN (SELECT * FROM `wp_abd_nail_pathology_report`) nail_report ON `nail_report`.`specimen_id` = `specimen`. `id`"; 
        $turn_around_time_specimen = $this->BlankModel->customquery($turn_around_time_specimen_sql);
        $count = count($turn_around_time_specimen);
        $turn_around_time = array();
        if($turn_around_time_specimen)
        {
         foreach($turn_around_time_specimen as $data)
         {
         	$result_data['difference']	     = (int)$data['difference'];
			$result_data['assessioning_num'] = $data['assessioning_num']; 
			array_push($turn_around_time, $result_data);   
	     } 
		}
        if(!empty( $turn_around_time ))
        {          	
         echo json_encode(array('status' => '1', 'details' => $turn_around_time, 'count' => $count));   
        }
        else
        {
         echo json_encode(array('status'=>'0'));
        }
    }

    function search_avg_time()
    {
		$data = json_decode(file_get_contents('php://input'), true);      
		$total_working_day =0;
		$search_date_turn_around_time_specimen_sql = "SELECT `specimen`.`assessioning_num`,`specimen`.`id`,`specimen`.`create_date` as specimen_create_date, `nail_report`.`create_date` as nail_create_date
		FROM 
		(SELECT * FROM `wp_abd_specimen` WHERE `qc_check` = '0' AND `physician_accepct` = '0' AND `status` = '0' AND `create_date` BETWEEN '".$data['from_date']." 00:00:00' and '".$data['to_date']." 23:59:59')specimen 
		INNER JOIN 
		(SELECT * FROM `wp_abd_nail_pathology_report`) nail_report 
		ON 
		`nail_report`.`specimen_id` = `specimen`.`id` ";
		$search_date_turn_around_time_specimen = $this->BlankModel->customquery($search_date_turn_around_time_specimen_sql);

		$search_specimen_count_sql = "SELECT count(`specimen`.`id`) as `specimen_count` 
		FROM 
		(SELECT * FROM `wp_abd_specimen` WHERE `qc_check` = '0' AND `physician_accepct` = '0' AND `status` = '0' AND `create_date` BETWEEN '".$data['from_date']." 00:00:00' and '".$data['to_date']." 23:59:59')specimen 
		INNER JOIN 
		(SELECT * FROM `wp_abd_nail_pathology_report`) nail_report 
		ON 
		`nail_report`.`specimen_id` = `specimen`.`id`";

		$specimen_count_by_date_range = $this->BlankModel->customquery($search_specimen_count_sql);
  
        $get_working_days = "";
        $holiday_sql = "SELECT * FROM `wp_abd_holidays` WHERE `is_active` ='1' ";
        $holiday_range = $this->BlankModel->customquery($holiday_sql);  

        foreach($holiday_range as $holidays ){
        $holiday_list[] = $this->getDatesFromRange($holidays['start_date'], $holidays['end_date']);
        }
        $holidays_list = $this->array_flatten($holiday_list);
        $i=0;
        foreach($search_date_turn_around_time_specimen as $specimens){

            $specimen_date = date('Y-m-d', strtotime($specimens['specimen_create_date']));
            $nail_date = date('Y-m-d', strtotime($specimens['nail_create_date']));
            $get_working_days = $this->getWorkingDays($specimen_date, $nail_date, $holidays_list)-1;
            $total_working_day += $get_working_days;
            $search_date_turn_around_time_specimen[$i]['working_days'] = $get_working_days;
            $i++;
        }
    
        if($search_date_turn_around_time_specimen)
        {
            echo json_encode(array('status'=>'1','details' => $search_date_turn_around_time_specimen,'count'=>$specimen_count_by_date_range,'total_working_day'=>$total_working_day));   
        }
        else
        {
            echo json_encode(array('status'=>'0'));
        }
    }

    //The function returns the no. of business days between two dates and it skips the holidays
function getWorkingDays($startDate,$endDate,$holidays){
    // do strtotime calculations just once
    $endDate = strtotime($endDate);
    $startDate = strtotime($startDate);


    //The total number of days between the two dates. We compute the no. of seconds and divide it to 60*60*24
    //We add one to inlude both dates in the interval.
    $days = ($endDate - $startDate) / 86400 + 1;

    $no_full_weeks = floor($days / 7);
    $no_remaining_days = fmod($days, 7);

    //It will return 1 if it's Monday,.. ,7 for Sunday
    $the_first_day_of_week = date("N", $startDate);
    $the_last_day_of_week = date("N", $endDate);

    //---->The two can be equal in leap years when february has 29 days, the equal sign is added here
    //In the first case the whole interval is within a week, in the second case the interval falls in two weeks.
    if ($the_first_day_of_week <= $the_last_day_of_week) {
        if ($the_first_day_of_week <= 6 && 6 <= $the_last_day_of_week) $no_remaining_days--;
        if ($the_first_day_of_week <= 7 && 7 <= $the_last_day_of_week) $no_remaining_days--;
    }
    else {
        // (edit by Tokes to fix an edge case where the start day was a Sunday
        // and the end day was NOT a Saturday)

        // the day of the week for start is later than the day of the week for end
        if ($the_first_day_of_week == 7) {
            // if the start date is a Sunday, then we definitely subtract 1 day
            $no_remaining_days--;

            if ($the_last_day_of_week == 6) {
                // if the end date is a Saturday, then we subtract another day
                $no_remaining_days--;
            }
        }
        else {
            // the start date was a Saturday (or earlier), and the end date was (Mon..Fri)
            // so we skip an entire weekend and subtract 2 days
            $no_remaining_days -= 2;
        }
    }

    //The no. of business days is: (number of weeks between the two dates) * (5 working days) + the remainder
//---->february in none leap years gave a remainder of 0 but still calculated weekends between first and last day, this is one way to fix it
   $workingDays = $no_full_weeks * 5;
    if ($no_remaining_days > 0 )
    {
      $workingDays += $no_remaining_days;
    }

    //We subtract the holidays
    foreach($holidays as $holiday){
            $time_stamp=strtotime($holiday);
            //If the holiday doesn't fall in weekend
            if ($startDate <= $time_stamp && $time_stamp <= $endDate && date("N",$time_stamp) != 6 && date("N",$time_stamp) != 7)
                $workingDays--;
        }

        return $workingDays;
    }

    function getDatesFromRange($start, $end, $format = 'Y-m-d') {
        $array = array();
        $interval = new DateInterval('P1D');

        $realEnd = new DateTime($end);
        $realEnd->add($interval);

        $period = new DatePeriod(new DateTime($start), $interval, $realEnd);

        foreach($period as $date) { 
            $array[] = $date->format($format); 
        }

        return $array;
    }

     function array_flatten($array_list) { 
      if (!is_array($array_list)) { 
        return FALSE; 
      } 
      $result = array(); 
      foreach ($array_list as $key => $value) { 
        if (is_array($value)) { 
          $result = array_merge($result, $this->array_flatten($value)); 
        } 
        else { 
          $result[$key] = $value; 
        } 
      } 
      return $result; 
    } 

    function get_slide_analytics()
    {
        $FM_clinical_data_sql = "SELECT COUNT(`nail_unit`) AS `fm_number_report_count` FROM `wp_abd_clinical_info` WHERE `nail_unit` LIKE '%1%' ";    
        $fm_clinical_datas = $this->BlankModel->customquery($FM_clinical_data_sql);	 
        $tota_pas_gms_fm = $fm_clinical_datas[0]['fm_number_report_count'] * 4 ;
        
        $gms_clinical_data_sql = "SELECT COUNT(`nail_unit`) AS `gms_number_report_count` FROM `wp_abd_clinical_info` WHERE `nail_unit` LIKE '%2%'";   
        $gms_clinical_data = $this->BlankModel->customquery($gms_clinical_data_sql);	
        
        $tota_pas_gms = $gms_clinical_data[0]['gms_number_report_count'] * 3;

        $clinical_data_PAS_sql = "SELECT COUNT(`nail_unit`) AS `pas_number_report_count` FROM `wp_abd_clinical_info` WHERE `nail_unit` LIKE '%3%'";   
        $clinical_PAS_datas = $this->BlankModel->customquery($clinical_data_PAS_sql);	
        
        $tota_pas = $clinical_PAS_datas[0]['pas_number_report_count'] *2;
        $total = $tota_pas_gms_fm + $tota_pas_gms + $tota_pas;

        echo json_encode(array('status'=>'1','first_val' => $tota_pas_gms_fm,'second_val'=>$tota_pas_gms,'third_val'=>$tota_pas,'total'=>$total));   

    }

    function search_slide_analytics()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        $search =" AND `create_date` BETWEEN '".$data['from_date']." 00:00:00' and '".$data['to_date']." 23:59:59'";	
        if(!empty($data['from_date']) && !empty($data['to_date']))
        {
        $FM_clinical_data_sql = "SELECT COUNT(`nail_unit`) AS `fm_number_report_count` FROM `wp_abd_clinical_info` WHERE `nail_unit` LIKE '%1%' ".$search;    
        $fm_clinical_datas = $this->BlankModel->customquery($FM_clinical_data_sql);	 
        $tota_pas_gms_fm = $fm_clinical_datas[0]['fm_number_report_count'] * 4 ;
        
        $gms_clinical_data_sql = "SELECT COUNT(`nail_unit`) AS `gms_number_report_count` FROM `wp_abd_clinical_info` WHERE `nail_unit` LIKE '%2%'".$search;   
        $gms_clinical_data = $this->BlankModel->customquery($gms_clinical_data_sql);	
        
        $tota_pas_gms = $gms_clinical_data[0]['gms_number_report_count'] * 3;

        $clinical_data_PAS_sql = "SELECT COUNT(`nail_unit`) AS `pas_number_report_count` FROM `wp_abd_clinical_info` WHERE `nail_unit` LIKE '%3%'".$search;   
        $clinical_PAS_datas = $this->BlankModel->customquery($clinical_data_PAS_sql);	
        
        $tota_pas = $clinical_PAS_datas[0]['pas_number_report_count'] *2;
        $total = $tota_pas_gms_fm + $tota_pas_gms + $tota_pas;

        echo json_encode(array('status'=>'1','first_val' => $tota_pas_gms_fm,'second_val'=>$tota_pas_gms,'third_val'=>$tota_pas,'total'=>$total));   
        }
        else
        {
            echo json_encode(array('status'=>'0'));
        }
    }
    function get_histo_avg_time()
    {   
        $tot_minutes=0;
        $tot_minutes_stage2=0;
        $tot_minutes_stage3=0;
        $tot_minutes_stage4=0;
        $tot_minutes_pending=0;
        $tot_minutes_stage=0;
        $count =0;
        $details = array();
        $specimen_results =$this->BlankModel->customquery("SELECT `specimen`.`id`,`specimen`.`assessioning_num`,`specimen`.`create_date`,`qc_check` FROM 
        (SELECT `id`,`assessioning_num`,`qc_check`,`create_date` FROM `wp_abd_specimen` WHERE `status` = '0' AND `physician_accepct` = '0' AND `qc_check` = '0' AND `create_date` > '2017-03-27 23:59:59' AND `id` NOT IN 
        (SELECT `specimen_id` FROM `wp_abd_specimen_stage_details` WHERE `pass_fail` = '0' group by `specimen_id`))specimen  INNER JOIN 
        (SELECT `nail_unit`,`specimen_id` FROM `wp_abd_clinical_info` WHERE `nail_unit` IN (1,2,3,5,6))clinic_info ON `specimen`.`id` = `clinic_info`.`specimen_id` limit 0,10");
        if($specimen_results)
        {
            $count= count($specimen_results);
            foreach ($specimen_results as $specimen_data)
            {
                $specimen_stage_results_stage1 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE `stage_id` ='1' AND `specimen_id` ='".$specimen_data['id']."'"); 
                if(!empty($specimen_stage_results_stage1[0]['specimen_timestamp']))
                {
                $timestamp = strtotime($specimen_stage_results_stage1[0]['specimen_timestamp']);
                $formatted_time= date('Y-m-d H:i:s',$timestamp);
                $original_time_stage1= date('Y-m-d H:i:s',strtotime($specimen_data['create_date']));              
                
                $minutes = floor(abs($timestamp-(strtotime($specimen_data['create_date'])))/ 60);
                $tot_minutes+= $minutes;
                $avg_minutes = floor($tot_minutes/$count);
                $hour = round(($avg_minutes/60),1);
                $day = floor($hour/24);

                $chk_hour_stage1 = floor(abs(strtotime($specimen_stage_results_stage1[0]['specimen_timestamp'])- strtotime($specimen_data['create_date'])));
                if($chk_hour_stage1>86400)
                {
                  $color1 = 'Red';
                  $time = dateDiff($formatted_time,$original_time_stage1);
                }
                else
                {
                  $color1 = '';
                  $time =  dateDiff($formatted_time,$original_time_stage1);
                }
                }
                else
                {
                  $color1 = '';
                  $time = "Not Processed";
                }
                
                $specimen_stage_results_stage2 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE `stage_id` ='2' AND `specimen_id` ='".$specimen_data['id']."'"); 
                  if(!empty($specimen_stage_results_stage2[0]['specimen_timestamp']))
                  {
                  $timestamp_stage2 = strtotime($specimen_stage_results_stage2[0]['specimen_timestamp']);
                  $formatted_time_stage2= date('Y-m-d h:i:sa',$timestamp_stage2);
                  $original_time_stage2= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));

                  $minutes_stage2 = floor(abs($timestamp_stage2-(strtotime($specimen_data['create_date'])))/ 60);
                  $tot_minutes_stage2+= $minutes_stage2;
                  $avg_minutes_stage2 = floor($tot_minutes_stage2/$count);
                  $hour_stage2 = round(($avg_minutes_stage2/60),1);
                  $day_stage2 = floor($hour_stage2/24); 

                  $chk_hour_stage2 = floor(abs(strtotime($specimen_stage_results_stage2[0]['specimen_timestamp'])-strtotime($specimen_data['create_date'])));
                  if($chk_hour_stage2>21600) 
                  {
                   $color2 = 'Red';
                   $time2 = dateDiff($formatted_time_stage2,$original_time_stage2);
                  }
                  else
                  {
                    $color2 = '';
                    $time2 = dateDiff($formatted_time_stage2,$original_time_stage2);
                  }
                  }
                  else
                  {
                    $color2 = '';
                    $time2 = "Not Processed";
                  }

                  $specimen_stage_results_stage3 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE `stage_id` ='3' AND `specimen_id` ='".$specimen_data['id']."'"); 
                 if(!empty($specimen_stage_results_stage3[0]['specimen_timestamp']))
                 {
                  $timestamp_stage3 = strtotime($specimen_stage_results_stage3[0]['specimen_timestamp']);
                  $formatted_time_stage3= date('Y-m-d h:i:sa',$timestamp_stage3);
                  $original_time_stage3= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));
                  
                  $minutes_stage3 = floor(abs($timestamp_stage3-(strtotime($specimen_data['create_date'])))/ 60);
                  $tot_minutes_stage3+= $minutes_stage3;
                  $avg_minutes_stage3 = floor($tot_minutes_stage3/$count);
                  $hour_stage3 = round(($avg_minutes_stage3/60),1);
                  $day_stage3 = floor($hour_stage3/24);

                  $chk_hour_stage3 = floor(abs(strtotime($specimen_stage_results_stage3[0]['specimen_timestamp'])-strtotime($specimen_data['create_date'])));
                  if($chk_hour_stage3>172800)
                  {
                    $color3 = 'Red';
                    $time3 = dateDiff($formatted_time_stage3,$original_time_stage3);
                  }
                  else 
                  {
                    $color3 = '';
                    $time3 = dateDiff($formatted_time_stage3,$original_time_stage3);
                  } 
                  
                  }  
                  else
                  {
                    $color3 = '';
                    $time3 = "Not Processed";
                  }

                  $specimen_stage_results_stage4 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE `stage_id` ='4' AND `specimen_id` ='".$specimen_data['id']."'"); 
                  if(!empty($specimen_stage_results_stage4[0]['specimen_timestamp']))
                  {
                  $timestamp_stage4 = strtotime($specimen_stage_results_stage4[0]['specimen_timestamp']);
                  $formatted_time_stage4= date('Y-m-d h:i:sa',$timestamp_stage4);
                  $original_time_stage4= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));

                  $minutes_stage4 = floor(abs($timestamp_stage4-(strtotime($specimen_data['create_date'])))/ 60);
                  $tot_minutes_stage4+= $minutes_stage4;
                  $avg_minutes_stage4 = floor($tot_minutes_stage4/$count);
                  $hour_stage4 = round(($avg_minutes_stage4/60),1);
                  $day_stage4 = floor($hour_stage4/24); 

                  $chk_hour_stage4 = floor(abs(strtotime($specimen_stage_results_stage4[0]['specimen_timestamp'])- strtotime($specimen_data['create_date'])));
                  if($chk_hour_stage4>18000)
                  {
                    $color4 = 'Red';
                    $time4 = dateDiff($formatted_time_stage4,$original_time_stage4); 
                  }
                  else
                  {
                    $color4 = '';
                    $time4 =  dateDiff($formatted_time_stage4,$original_time_stage4); 
                  }
                  }
                  else
                  {
                    $color4 = '';
                    $time4 = "Not Processed";
                  }

                  $pending_sql = $this->BlankModel->customquery("SELECT max(`specimen_timestamp`) AS `max_date` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` ='".$specimen_data['id']."'");
                  
                    if(!empty($pending_sql[0]['max_date']))
                    {
                      $timestamp_pending = strtotime($pending_sql[0]['max_date']);
                      $formatted_time_pending= date('Y-m-d h:i:sa',$timestamp_pending);
                      $original_time_pending= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));

                      $minutes_pending = floor(abs($timestamp_pending-(strtotime($specimen_data['create_date'])))/ 60);
                      $tot_minutes_pending+= $minutes_pending;
                      $avg_minutes_pending = floor($tot_minutes_pending/$count);
                      $hour_stage_pending = round(($avg_minutes_pending/60),1);
                      $day_stage_pending = floor($hour_stage_pending/24); 

                      $chk_hour_stage5 = floor(abs(strtotime($pending_sql[0]['max_date'])-strtotime($specimen_data['create_date'])));
                      if($chk_hour_stage5>18000)
                      {
                         $color5 = 'Red';
                         $time5 = dateDiff($formatted_time_pending,$original_time_pending); 
                      }
                      else
                      {
                        $color5 = '';
                         $time5 =  dateDiff($formatted_time_pending,$original_time_pending); 
                      }
                        
                    }
                    else
                    {
                      $color5 = '';
                      $time5 = "Not Processed";
                    }

                     $report_sql = $this->BlankModel->customquery("SELECT `create_date` FROM `wp_abd_nail_pathology_report` WHERE `specimen_id` ='".$specimen_data['id']."'");
                    if(!empty($report_sql[0]['create_date']))
                    {
                      $timestamp_report = strtotime($report_sql[0]['create_date']);
                      $formatted_time_report= date('Y-m-d h:i:sa',$timestamp_report);
                      $original_time= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));
                      
                      $minutes_stage = floor(abs($timestamp_report-(strtotime($specimen_data['create_date'])))/ 60);
                      $tot_minutes_stage+= $minutes_stage;
                      $avg_minutes_stage = floor($tot_minutes_stage/$count);
                      $hour_stage = round(($avg_minutes_stage/60),1);
                      $day_stage = floor($hour_stage/24); 

                      $chk_hour_stage6 = floor(abs(strtotime($report_sql[0]['create_date'])-strtotime($specimen_data['create_date'])));
                      if($chk_hour_stage6>86400)
                      {
                         $color6 = 'Red';
                         $time6 = dateDiff($formatted_time_report, $original_time); 
                      }
                      else
                      {
                         $color6 = '';
                         $time6 = dateDiff($formatted_time_report, $original_time); 
                      }
                    }
                    else
                    {
                        $color6 = '';
                        $time6 = "Not Processed";
                    }

                $specimen_information =  array('assessioning_num' => $specimen_data['assessioning_num'],'stage1'=>$time,'stage2'=> $time2,
                'stage3'=>$time3,'stage4'=>$time4,'pending'=>$time5,'report'=>$time6,'color1'=>$color1,'color2'=>$color2,'color3'=>$color3,
               'color4'=>$color4,'color5'=>$color5,'color6'=>$color6);
                array_push($details, $specimen_information); 
            }

            if(!empty($day))
            {
                $day1 = $day." Days";
            }
            else{
                $day1= "" ;
            }
            if(!empty($hour))
            {
                $hour1 = $hour." Hours";
            }
            else{
                $hour1= "" ;
            }

            if(!empty($day_stage2))
          {
              $day2 = $day_stage2." Days";
          }
          else{
              $day2= "" ;
          }
          if(!empty($hour_stage2))
          {
              $hour2 = $hour_stage2." Hours";
          }
          else{
              $hour2= "" ;
          }

          if(!empty($day_stage3))
          {
              $day3 = $day_stage3." Days";
          }
          else{
              $day3= "" ;
          }
          if(!empty($hour_stage3))
          {
              $hour3 = $hour_stage3." Hours";
          }
          else{
              $hour3= "" ;
          }
            if(!empty($day_stage4))
            {
                $day4 = $day_stage4." Days";
            }
            else{
                $day4= "" ;
            }
            if(!empty($hour_stage4))
            {
                $hour4 = $hour_stage4." Hours";
            }
            else{
                $hour4= "" ;
            }

              if(!empty($day_stage_pending))
              {
                  $day5 = $day_stage_pending." Days";
              }
              else{
                  $day5= "" ;
              }
              if(!empty($hour_stage_pending))
              {
                  $hour5 = $hour_stage_pending." Hours";
              }
              else{
                  $hour5= "" ;
              }
            if(!empty($day_stage))
              {
                  $day6 = $day_stage." Days";
              }
              else{
                  $day6= "" ;
              }
              if(!empty($hour_stage))
              {
                  $hour6 = $hour_stage." Hours";
              }
              else{
                  $hour6= "" ;
              }
            echo json_encode(array('status'=>'1','details'=> $details,'count'=>$count,'day1'=>$day1,'hour1'=>$hour1,'day2'=>$day2,'hour2'=>$hour2,
            'day3'=>$day3,'hour3'=>$hour3,'day4'=>$day4,'hour4'=>$hour4,'day5'=>$day5,'hour5'=>$hour5,'day6'=>$day6,'hour6'=>$hour6));
        }
    }

    function get_more_histo_avg_time()
    {   
        $data = json_decode(file_get_contents('php://input'), true);
        if(!empty($data))
        {
            $start = $data['load'];
            $tot_minutes=0;
            $tot_minutes_stage2=0;
            $tot_minutes_stage3=0;
            $tot_minutes_stage4=0;
            $tot_minutes_pending=0;
            $tot_minutes_stage=0;
            $count =0;
            $details = array();
            $specimen_results =$this->BlankModel->customquery("SELECT `specimen`.`id`,`specimen`.`assessioning_num`,`specimen`.`create_date`,`qc_check` FROM 
            (SELECT `id`,`assessioning_num`,`qc_check`,`create_date` FROM `wp_abd_specimen` WHERE `status` = '0' AND `physician_accepct` = '0' AND `qc_check` = '0' AND `create_date` > '2017-03-27 23:59:59' AND `id` NOT IN 
            (SELECT `specimen_id` FROM `wp_abd_specimen_stage_details` WHERE `pass_fail` = '0' group by `specimen_id`))specimen  INNER JOIN 
            (SELECT `nail_unit`,`specimen_id` FROM `wp_abd_clinical_info` WHERE `nail_unit` IN (1,2,3,5,6))clinic_info ON `specimen`.`id` = `clinic_info`.`specimen_id` limit $start,10");
            if($specimen_results)
            {
                $count= $start+10;
                foreach ($specimen_results as $specimen_data)
                {
                    $specimen_stage_results_stage1 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE `stage_id` ='1' AND `specimen_id` ='".$specimen_data['id']."'"); 
                    if(!empty($specimen_stage_results_stage1[0]['specimen_timestamp']))
                    {
                    $timestamp = strtotime($specimen_stage_results_stage1[0]['specimen_timestamp']);
                    $formatted_time= date('Y-m-d H:i:s',$timestamp);
                    $original_time_stage1= date('Y-m-d H:i:s',strtotime($specimen_data['create_date']));              
                    
                    $minutes = floor(abs($timestamp-(strtotime($specimen_data['create_date'])))/ 60);
                    $tot_minutes+= $minutes;
                    $avg_minutes = floor($tot_minutes/$count);
                    $hour = round(($avg_minutes/60),1);
                    $day = floor($hour/24);
    
                    $chk_hour_stage1 = floor(abs(strtotime($specimen_stage_results_stage1[0]['specimen_timestamp'])- strtotime($specimen_data['create_date'])));
                    if($chk_hour_stage1>86400)
                    {
                      $color1 = 'Red';
                      $time = dateDiff($formatted_time,$original_time_stage1);
                    }
                    else
                    {
                      $color1 = '';
                      $time =  dateDiff($formatted_time,$original_time_stage1);
                    }
                    }
                    else
                    {
                      $color1 = '';
                      $time = "Not Processed";
                    }
                    
                    $specimen_stage_results_stage2 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE `stage_id` ='2' AND `specimen_id` ='".$specimen_data['id']."'"); 
                      if(!empty($specimen_stage_results_stage2[0]['specimen_timestamp']))
                      {
                      $timestamp_stage2 = strtotime($specimen_stage_results_stage2[0]['specimen_timestamp']);
                      $formatted_time_stage2= date('Y-m-d h:i:sa',$timestamp_stage2);
                      $original_time_stage2= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));
    
                      $minutes_stage2 = floor(abs($timestamp_stage2-(strtotime($specimen_data['create_date'])))/ 60);
                      $tot_minutes_stage2+= $minutes_stage2;
                      $avg_minutes_stage2 = floor($tot_minutes_stage2/$count);
                      $hour_stage2 = round(($avg_minutes_stage2/60),1);
                      $day_stage2 = floor($hour_stage2/24); 
    
                      $chk_hour_stage2 = floor(abs(strtotime($specimen_stage_results_stage2[0]['specimen_timestamp'])-strtotime($specimen_data['create_date'])));
                      if($chk_hour_stage2>21600) 
                      {
                       $color2 = 'Red';
                       $time2 = dateDiff($formatted_time_stage2,$original_time_stage2);
                      }
                      else
                      {
                        $color2 = '';
                        $time2 = dateDiff($formatted_time_stage2,$original_time_stage2);
                      }
                      }
                      else
                      {
                        $color2 = '';
                        $time2 = "Not Processed";
                      }
    
                      $specimen_stage_results_stage3 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE `stage_id` ='3' AND `specimen_id` ='".$specimen_data['id']."'"); 
                     if(!empty($specimen_stage_results_stage3[0]['specimen_timestamp']))
                     {
                      $timestamp_stage3 = strtotime($specimen_stage_results_stage3[0]['specimen_timestamp']);
                      $formatted_time_stage3= date('Y-m-d h:i:sa',$timestamp_stage3);
                      $original_time_stage3= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));
                      
                      $minutes_stage3 = floor(abs($timestamp_stage3-(strtotime($specimen_data['create_date'])))/ 60);
                      $tot_minutes_stage3+= $minutes_stage3;
                      $avg_minutes_stage3 = floor($tot_minutes_stage3/$count);
                      $hour_stage3 = round(($avg_minutes_stage3/60),1);
                      $day_stage3 = floor($hour_stage3/24);
    
                      $chk_hour_stage3 = floor(abs(strtotime($specimen_stage_results_stage3[0]['specimen_timestamp'])-strtotime($specimen_data['create_date'])));
                      if($chk_hour_stage3>172800)
                      {
                        $color3 = 'Red';
                        $time3 = dateDiff($formatted_time_stage3,$original_time_stage3);
                      }
                      else 
                      {
                        $color3 = '';
                        $time3 = dateDiff($formatted_time_stage3,$original_time_stage3);
                      } 
                      
                      }  
                      else
                      {
                        $color3 = '';
                        $time3 = "Not Processed";
                      }
    
                      $specimen_stage_results_stage4 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE `stage_id` ='4' AND `specimen_id` ='".$specimen_data['id']."'"); 
                      if(!empty($specimen_stage_results_stage4[0]['specimen_timestamp']))
                      {
                      $timestamp_stage4 = strtotime($specimen_stage_results_stage4[0]['specimen_timestamp']);
                      $formatted_time_stage4= date('Y-m-d h:i:sa',$timestamp_stage4);
                      $original_time_stage4= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));
    
                      $minutes_stage4 = floor(abs($timestamp_stage4-(strtotime($specimen_data['create_date'])))/ 60);
                      $tot_minutes_stage4+= $minutes_stage4;
                      $avg_minutes_stage4 = floor($tot_minutes_stage4/$count);
                      $hour_stage4 = round(($avg_minutes_stage4/60),1);
                      $day_stage4 = floor($hour_stage4/24); 
    
                      $chk_hour_stage4 = floor(abs(strtotime($specimen_stage_results_stage4[0]['specimen_timestamp'])- strtotime($specimen_data['create_date'])));
                      if($chk_hour_stage4>18000)
                      {
                        $color4 = 'Red';
                        $time4 = dateDiff($formatted_time_stage4,$original_time_stage4); 
                      }
                      else
                      {
                        $color4 = '';
                        $time4 =  dateDiff($formatted_time_stage4,$original_time_stage4); 
                      }
                      }
                      else
                      {
                        $color4 = '';
                        $time4 = "Not Processed";
                      }
    
                      $pending_sql = $this->BlankModel->customquery("SELECT max(`specimen_timestamp`) AS `max_date` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` ='".$specimen_data['id']."'");
                      
                        if(!empty($pending_sql[0]['max_date']))
                        {
                          $timestamp_pending = strtotime($pending_sql[0]['max_date']);
                          $formatted_time_pending= date('Y-m-d h:i:sa',$timestamp_pending);
                          $original_time_pending= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));
    
                          $minutes_pending = floor(abs($timestamp_pending-(strtotime($specimen_data['create_date'])))/ 60);
                          $tot_minutes_pending+= $minutes_pending;
                          $avg_minutes_pending = floor($tot_minutes_pending/$count);
                          $hour_stage_pending = round(($avg_minutes_pending/60),1);
                          $day_stage_pending = floor($hour_stage_pending/24); 
    
                          $chk_hour_stage5 = floor(abs(strtotime($pending_sql[0]['max_date'])-strtotime($specimen_data['create_date'])));
                          if($chk_hour_stage5>18000)
                          {
                             $color5 = 'Red';
                             $time5 = dateDiff($formatted_time_pending,$original_time_pending); 
                          }
                          else
                          {
                            $color5 = '';
                             $time5 =  dateDiff($formatted_time_pending,$original_time_pending); 
                          }
                            
                        }
                        else
                        {
                          $color5 = '';
                          $time5 = "Not Processed";
                        }
    
                         $report_sql = $this->BlankModel->customquery("SELECT `create_date` FROM `wp_abd_nail_pathology_report` WHERE `specimen_id` ='".$specimen_data['id']."'");
                        if(!empty($report_sql[0]['create_date']))
                        {
                          $timestamp_report = strtotime($report_sql[0]['create_date']);
                          $formatted_time_report= date('Y-m-d h:i:sa',$timestamp_report);
                          $original_time= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));
                          
                          $minutes_stage = floor(abs($timestamp_report-(strtotime($specimen_data['create_date'])))/ 60);
                          $tot_minutes_stage+= $minutes_stage;
                          $avg_minutes_stage = floor($tot_minutes_stage/$count);
                          $hour_stage = round(($avg_minutes_stage/60),1);
                          $day_stage = floor($hour_stage/24); 
    
                          $chk_hour_stage6 = floor(abs(strtotime($report_sql[0]['create_date'])-strtotime($specimen_data['create_date'])));
                          if($chk_hour_stage6>86400)
                          {
                             $color6 = 'Red';
                             $time6 = dateDiff($formatted_time_report, $original_time); 
                          }
                          else
                          {
                             $color6 = '';
                             $time6 = dateDiff($formatted_time_report, $original_time); 
                          }
                        }
                        else
                        {
                            $color6 = '';
                            $time6 = "Not Processed";
                        }
    
                    $specimen_information =  array('assessioning_num' => $specimen_data['assessioning_num'],'stage1'=>$time,'stage2'=> $time2,
                    'stage3'=>$time3,'stage4'=>$time4,'pending'=>$time5,'report'=>$time6,'color1'=>$color1,'color2'=>$color2,'color3'=>$color3,
                   'color4'=>$color4,'color5'=>$color5,'color6'=>$color6);
                    array_push($details, $specimen_information); 
                }
    
                if(!empty($day))
                {
                    $day1 = $day." Days";
                }
                else{
                    $day1= "" ;
                }
                if(!empty($hour))
                {
                    $hour1 = $hour." Hours";
                }
                else{
                    $hour1= "" ;
                }
    
                if(!empty($day_stage2))
              {
                  $day2 = $day_stage2." Days";
              }
              else{
                  $day2= "" ;
              }
              if(!empty($hour_stage2))
              {
                  $hour2 = $hour_stage2." Hours";
              }
              else{
                  $hour2= "" ;
              }
    
              if(!empty($day_stage3))
              {
                  $day3 = $day_stage3." Days";
              }
              else{
                  $day3= "" ;
              }
              if(!empty($hour_stage3))
              {
                  $hour3 = $hour_stage3." Hours";
              }
              else{
                  $hour3= "" ;
              }
                if(!empty($day_stage4))
                {
                    $day4 = $day_stage4." Days";
                }
                else{
                    $day4= "" ;
                }
                if(!empty($hour_stage4))
                {
                    $hour4 = $hour_stage4." Hours";
                }
                else{
                    $hour4= "" ;
                }
    
                  if(!empty($day_stage_pending))
                  {
                      $day5 = $day_stage_pending." Days";
                  }
                  else{
                      $day5= "" ;
                  }
                  if(!empty($hour_stage_pending))
                  {
                      $hour5 = $hour_stage_pending." Hours";
                  }
                  else{
                      $hour5= "" ;
                  }
                if(!empty($day_stage))
                  {
                      $day6 = $day_stage." Days";
                  }
                  else{
                      $day6= "" ;
                  }
                  if(!empty($hour_stage))
                  {
                      $hour6 = $hour_stage." Hours";
                  }
                  else{
                      $hour6= "" ;
                  }
                echo json_encode(array('status'=>'1','details'=> $details,'count'=>$count,'day1'=>$day1,'hour1'=>$hour1,'day2'=>$day2,'hour2'=>$hour2,
                'day3'=>$day3,'hour3'=>$hour3,'day4'=>$day4,'hour4'=>$hour4,'day5'=>$day5,'hour5'=>$hour5,'day6'=>$day6,'hour6'=>$hour6));
            }
        }

    }


    function search_histo_avg_time()
    {   
        $data = json_decode(file_get_contents('php://input'), true);
        if(!empty($data)){
        $tot_minutes=0;
        $tot_minutes_stage2=0;
        $tot_minutes_stage3=0;
        $tot_minutes_stage4=0;
        $tot_minutes_pending=0;
        $tot_minutes_stage=0;
        $count =0;
        $details = array();
        
        if($data['from_date']!="" && $data['to_date']!="" && ($data['from_date'] > '2017-03-27 23:59:59') && ($data['to_date'] > '2017-03-27 23:59:59'))
        {
            $specimen_results = $this->BlankModel->customquery("SELECT `specimen`.`id`,`specimen`.`assessioning_num`,`specimen`.`create_date`,`qc_check` FROM 
            (SELECT `id`,`assessioning_num`,`qc_check`,`create_date` FROM `wp_abd_specimen` WHERE `status` = '0' AND `physician_accepct` = '0' AND `qc_check` = '0' AND `create_date` BETWEEN '".$data['from_date']." 00:00:00' and '".$data['to_date']." 23:59:59' AND `id` NOT IN (SELECT `specimen_id` FROM `wp_abd_specimen_stage_details` WHERE `pass_fail` = '0' group by `specimen_id`))specimen  INNER JOIN (SELECT `nail_unit`,`specimen_id` FROM `wp_abd_clinical_info` WHERE `nail_unit` IN (1,2,3,5,6))clinic_info ON `specimen`.`id` = `clinic_info`.`specimen_id`");
        }
        
        if($data['acc']!="")
        {
            $specimen_results = $this->BlankModel->customquery("SELECT `specimen`.`id`,`specimen`.`assessioning_num`,`specimen`.`create_date`,`qc_check` FROM 
            (SELECT `id`,`assessioning_num`,`qc_check`,`create_date` FROM `wp_abd_specimen` WHERE `status` = '0' AND `physician_accepct` = '0' AND `qc_check` = '0' AND `assessioning_num`='".$data['acc']."' AND `id` NOT IN (SELECT `specimen_id` FROM `wp_abd_specimen_stage_details` WHERE `pass_fail` = '0' group by `specimen_id`))specimen  INNER JOIN (SELECT `nail_unit`,`specimen_id` FROM `wp_abd_clinical_info` WHERE `nail_unit` IN (1,2,3,5,6))clinic_info ON `specimen`.`id` = `clinic_info`.`specimen_id`");
        }
        if($specimen_results)
        {
            $count= count($specimen_results);
            foreach ($specimen_results as $specimen_data)
            {
                $specimen_stage_results_stage1 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE `stage_id` ='1' AND `specimen_id` ='".$specimen_data['id']."'"); 
                if(!empty($specimen_stage_results_stage1[0]['specimen_timestamp']))
                {
                $timestamp = strtotime($specimen_stage_results_stage1[0]['specimen_timestamp']);
                $formatted_time= date('Y-m-d H:i:s',$timestamp);
                $original_time_stage1= date('Y-m-d H:i:s',strtotime($specimen_data['create_date']));              
                
                $minutes = floor(abs($timestamp-(strtotime($specimen_data['create_date'])))/ 60);
                $tot_minutes+= $minutes;
                $avg_minutes = floor($tot_minutes/$count);
                $hour = round(($avg_minutes/60),1);
                $day = floor($hour/24);

                $chk_hour_stage1 = floor(abs(strtotime($specimen_stage_results_stage1[0]['specimen_timestamp'])- strtotime($specimen_data['create_date'])));
                if($chk_hour_stage1>86400)
                {
                  $color1 = 'Red';
                  $time = dateDiff($formatted_time,$original_time_stage1);
                }
                else
                {
                  $color1 = '';
                  $time =  dateDiff($formatted_time,$original_time_stage1);
                }
                }
                else
                {
                  $color1 = '';
                  $time = "Not Processed";
                }
                
                $specimen_stage_results_stage2 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE `stage_id` ='2' AND `specimen_id` ='".$specimen_data['id']."'"); 
                  if(!empty($specimen_stage_results_stage2[0]['specimen_timestamp']))
                  {
                  $timestamp_stage2 = strtotime($specimen_stage_results_stage2[0]['specimen_timestamp']);
                  $formatted_time_stage2= date('Y-m-d h:i:sa',$timestamp_stage2);
                  $original_time_stage2= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));

                  $minutes_stage2 = floor(abs($timestamp_stage2-(strtotime($specimen_data['create_date'])))/ 60);
                  $tot_minutes_stage2+= $minutes_stage2;
                  $avg_minutes_stage2 = floor($tot_minutes_stage2/$count);
                  $hour_stage2 = round(($avg_minutes_stage2/60),1);
                  $day_stage2 = floor($hour_stage2/24); 

                  $chk_hour_stage2 = floor(abs(strtotime($specimen_stage_results_stage2[0]['specimen_timestamp'])-strtotime($specimen_data['create_date'])));
                  if($chk_hour_stage2>21600) 
                  {
                   $color2 = 'Red';
                   $time2 = dateDiff($formatted_time_stage2,$original_time_stage2);
                  }
                  else
                  {
                    $color2 = '';
                    $time2 = dateDiff($formatted_time_stage2,$original_time_stage2);
                  }
                  }
                  else
                  {
                    $color2 = '';
                    $time2 = "Not Processed";
                  }

                  $specimen_stage_results_stage3 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE `stage_id` ='3' AND `specimen_id` ='".$specimen_data['id']."'"); 
                 if(!empty($specimen_stage_results_stage3[0]['specimen_timestamp']))
                 {
                  $timestamp_stage3 = strtotime($specimen_stage_results_stage3[0]['specimen_timestamp']);
                  $formatted_time_stage3= date('Y-m-d h:i:sa',$timestamp_stage3);
                  $original_time_stage3= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));
                  
                  $minutes_stage3 = floor(abs($timestamp_stage3-(strtotime($specimen_data['create_date'])))/ 60);
                  $tot_minutes_stage3+= $minutes_stage3;
                  $avg_minutes_stage3 = floor($tot_minutes_stage3/$count);
                  $hour_stage3 = round(($avg_minutes_stage3/60),1);
                  $day_stage3 = floor($hour_stage3/24);

                  $chk_hour_stage3 = floor(abs(strtotime($specimen_stage_results_stage3[0]['specimen_timestamp'])-strtotime($specimen_data['create_date'])));
                  if($chk_hour_stage3>172800)
                  {
                    $color3 = 'Red';
                    $time3 = dateDiff($formatted_time_stage3,$original_time_stage3);
                  }
                  else 
                  {
                    $color3 = '';
                    $time3 = dateDiff($formatted_time_stage3,$original_time_stage3);
                  } 
                  
                  }  
                  else
                  {
                    $color3 = '';
                    $time3 = "Not Processed";
                  }

                  $specimen_stage_results_stage4 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE `stage_id` ='4' AND `specimen_id` ='".$specimen_data['id']."'"); 
                  if(!empty($specimen_stage_results_stage4[0]['specimen_timestamp']))
                  {
                  $timestamp_stage4 = strtotime($specimen_stage_results_stage4[0]['specimen_timestamp']);
                  $formatted_time_stage4= date('Y-m-d h:i:sa',$timestamp_stage4);
                  $original_time_stage4= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));

                  $minutes_stage4 = floor(abs($timestamp_stage4-(strtotime($specimen_data['create_date'])))/ 60);
                  $tot_minutes_stage4+= $minutes_stage4;
                  $avg_minutes_stage4 = floor($tot_minutes_stage4/$count);
                  $hour_stage4 = round(($avg_minutes_stage4/60),1);
                  $day_stage4 = floor($hour_stage4/24); 

                  $chk_hour_stage4 = floor(abs(strtotime($specimen_stage_results_stage4[0]['specimen_timestamp'])- strtotime($specimen_data['create_date'])));
                  if($chk_hour_stage4>18000)
                  {
                    $color4 = 'Red';
                    $time4 = dateDiff($formatted_time_stage4,$original_time_stage4); 
                  }
                  else
                  {
                    $color4 = '';
                    $time4 =  dateDiff($formatted_time_stage4,$original_time_stage4); 
                  }
                  }
                  else
                  {
                    $color4 = '';
                    $time4 = "Not Processed";
                  }

                  $pending_sql = $this->BlankModel->customquery("SELECT max(`specimen_timestamp`) AS `max_date` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` ='".$specimen_data['id']."'");
                  
                    if(!empty($pending_sql[0]['max_date']))
                    {
                      $timestamp_pending = strtotime($pending_sql[0]['max_date']);
                      $formatted_time_pending= date('Y-m-d h:i:sa',$timestamp_pending);
                      $original_time_pending= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));

                      $minutes_pending = floor(abs($timestamp_pending-(strtotime($specimen_data['create_date'])))/ 60);
                      $tot_minutes_pending+= $minutes_pending;
                      $avg_minutes_pending = floor($tot_minutes_pending/$count);
                      $hour_stage_pending = round(($avg_minutes_pending/60),1);
                      $day_stage_pending = floor($hour_stage_pending/24); 

                      $chk_hour_stage5 = floor(abs(strtotime($pending_sql[0]['max_date'])-strtotime($specimen_data['create_date'])));
                      if($chk_hour_stage5>18000)
                      {
                         $color5 = 'Red';
                         $time5 = dateDiff($formatted_time_pending,$original_time_pending); 
                      }
                      else
                      {
                        $color5 = '';
                         $time5 =  dateDiff($formatted_time_pending,$original_time_pending); 
                      }
                        
                    }
                    else
                    {
                      $color5 = '';
                      $time5 = "Not Processed";
                    }

                     $report_sql = $this->BlankModel->customquery("SELECT `create_date` FROM `wp_abd_nail_pathology_report` WHERE `specimen_id` ='".$specimen_data['id']."'");
                    if(!empty($report_sql[0]['create_date']))
                    {
                      $timestamp_report = strtotime($report_sql[0]['create_date']);
                      $formatted_time_report= date('Y-m-d h:i:sa',$timestamp_report);
                      $original_time= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));
                      
                      $minutes_stage = floor(abs($timestamp_report-(strtotime($specimen_data['create_date'])))/ 60);
                      $tot_minutes_stage+= $minutes_stage;
                      $avg_minutes_stage = floor($tot_minutes_stage/$count);
                      $hour_stage = round(($avg_minutes_stage/60),1);
                      $day_stage = floor($hour_stage/24); 

                      $chk_hour_stage6 = floor(abs(strtotime($report_sql[0]['create_date'])-strtotime($specimen_data['create_date'])));
                      if($chk_hour_stage6>86400)
                      {
                         $color6 = 'Red';
                         $time6 = dateDiff($formatted_time_report, $original_time); 
                      }
                      else
                      {
                         $color6 = '';
                         $time6 = dateDiff($formatted_time_report, $original_time); 
                      }
                    }
                    else
                    {
                        $color6 = '';
                        $time6 = "Not Processed";
                    }

                $specimen_information =  array('assessioning_num' => $specimen_data['assessioning_num'],'stage1'=>$time,'stage2'=> $time2,
                'stage3'=>$time3,'stage4'=>$time4,'pending'=>$time5,'report'=>$time6,'color1'=>$color1,'color2'=>$color2,'color3'=>$color3,
            'color4'=>$color4,'color5'=>$color5,'color6'=>$color6);
                array_push($details, $specimen_information); 
            }

            if(!empty($day))
            {
                $day1 = $day." Days";
            }
            else{
                $day1= "" ;
            }
            if(!empty($hour))
            {
                $hour1 = $hour." Hours";
            }
            else{
                $hour1= "" ;
            }

            if(!empty($day_stage2))
          {
              $day2 = $day_stage2." Days";
          }
          else{
              $day2= "" ;
          }
          if(!empty($hour_stage2))
          {
              $hour2 = $hour_stage2." Hours";
          }
          else{
              $hour2= "" ;
          }

          if(!empty($day_stage3))
          {
              $day3 = $day_stage3." Days";
          }
          else{
              $day3= "" ;
          }
          if(!empty($hour_stage3))
          {
              $hour3 = $hour_stage3." Hours";
          }
          else{
              $hour3= "" ;
          }
            if(!empty($day_stage4))
            {
                $day4 = $day_stage4." Days";
            }
            else{
                $day4= "" ;
            }
            if(!empty($hour_stage4))
            {
                $hour4 = $hour_stage4." Hours";
            }
            else{
                $hour4= "" ;
            }

              if(!empty($day_stage_pending))
              {
                  $day5 = $day_stage_pending." Days";
              }
              else{
                  $day5= "" ;
              }
              if(!empty($hour_stage_pending))
              {
                  $hour5 = $hour_stage_pending." Hours";
              }
              else{
                  $hour5= "" ;
              }
            if(!empty($day_stage))
              {
                  $day6 = $day_stage." Days";
              }
              else{
                  $day6= "" ;
              }
              if(!empty($hour_stage))
              {
                  $hour6 = $hour_stage." Hours";
              }
              else{
                  $hour6= "" ;
              }

            die(json_encode(array('status'=>'1','details'=> $details,'count'=>$count,'day1'=>$day1,'hour1'=>$hour1,'day2'=>$day2,'hour2'=>$hour2,
            'day3'=>$day3,'hour3'=>$hour3,'day4'=>$day4,'hour4'=>$hour4,'day5'=>$day5,'hour5'=>$hour5,'day6'=>$day6,'hour6'=>$hour6)));
        }else{
            die(json_encode(array('status'=>'0')));
        }
    }
    }
    function last_one_month()
    {
        $tot_minutes=0;
        $tot_minutes_stage2=0;
        $tot_minutes_stage3=0;
        $tot_minutes_stage4=0;
        $tot_minutes_pending=0;
        $tot_minutes_stage=0;
        $count =0;
        $details = array();
        $specimen_results =$this->BlankModel->customquery("SELECT `specimen`.`id`,`specimen`.`assessioning_num`,`specimen`.`create_date`,`qc_check` FROM 
        (SELECT `id`,`assessioning_num`,`qc_check`,`create_date` FROM `wp_abd_specimen` WHERE `status` = '0' AND `physician_accepct` = '0' AND `qc_check` = '0' AND `create_date` >= DATE(NOW()) - INTERVAL 1 MONTH AND `id` NOT IN (SELECT `specimen_id` FROM `wp_abd_specimen_stage_details` WHERE `pass_fail` = '0' group by `specimen_id`))specimen  INNER JOIN (SELECT `nail_unit`,`specimen_id` FROM `wp_abd_clinical_info` WHERE `nail_unit` IN (1,2,3,5,6))clinic_info ON `specimen`.`id` = `clinic_info`.`specimen_id`");
        if($specimen_results)
        {
            $count= count($specimen_results);
            foreach ($specimen_results as $specimen_data)
            {
                $specimen_stage_results_stage1 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE `stage_id` ='1' AND `specimen_id` ='".$specimen_data['id']."'"); 
                if(!empty($specimen_stage_results_stage1[0]['specimen_timestamp']))
                {
                $timestamp = strtotime($specimen_stage_results_stage1[0]['specimen_timestamp']);
                $formatted_time= date('Y-m-d H:i:s',$timestamp);
                $original_time_stage1= date('Y-m-d H:i:s',strtotime($specimen_data['create_date']));              
                
                $minutes = floor(abs($timestamp-(strtotime($specimen_data['create_date'])))/ 60);
                $tot_minutes+= $minutes;
                $avg_minutes = floor($tot_minutes/$count);
                $hour = round(($avg_minutes/60),1);
                $day = floor($hour/24);

                $chk_hour_stage1 = floor(abs(strtotime($specimen_stage_results_stage1[0]['specimen_timestamp'])- strtotime($specimen_data['create_date'])));
                if($chk_hour_stage1>86400)
                {
                  $color1 = 'Red';
                  $time = dateDiff($formatted_time,$original_time_stage1);
                }
                else
                {
                  $color1 = '';
                  $time =  dateDiff($formatted_time,$original_time_stage1);
                }
                }
                else
                {
                  $color1 = '';
                  $time = "Not Processed";
                }
                
                $specimen_stage_results_stage2 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE `stage_id` ='2' AND `specimen_id` ='".$specimen_data['id']."'"); 
                  if(!empty($specimen_stage_results_stage2[0]['specimen_timestamp']))
                  {
                  $timestamp_stage2 = strtotime($specimen_stage_results_stage2[0]['specimen_timestamp']);
                  $formatted_time_stage2= date('Y-m-d h:i:sa',$timestamp_stage2);
                  $original_time_stage2= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));

                  $minutes_stage2 = floor(abs($timestamp_stage2-(strtotime($specimen_data['create_date'])))/ 60);
                  $tot_minutes_stage2+= $minutes_stage2;
                  $avg_minutes_stage2 = floor($tot_minutes_stage2/$count);
                  $hour_stage2 = round(($avg_minutes_stage2/60),1);
                  $day_stage2 = floor($hour_stage2/24); 

                  $chk_hour_stage2 = floor(abs(strtotime($specimen_stage_results_stage2[0]['specimen_timestamp'])-strtotime($specimen_data['create_date'])));
                  if($chk_hour_stage2>21600) 
                  {
                   $color2 = 'Red';
                   $time2 = dateDiff($formatted_time_stage2,$original_time_stage2);
                  }
                  else
                  {
                    $color2 = '';
                    $time2 = dateDiff($formatted_time_stage2,$original_time_stage2);
                  }
                  }
                  else
                  {
                    $color2 = '';
                    $time2 = "Not Processed";
                  }

                  $specimen_stage_results_stage3 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE `stage_id` ='3' AND `specimen_id` ='".$specimen_data['id']."'"); 
                 if(!empty($specimen_stage_results_stage3[0]['specimen_timestamp']))
                 {
                  $timestamp_stage3 = strtotime($specimen_stage_results_stage3[0]['specimen_timestamp']);
                  $formatted_time_stage3= date('Y-m-d h:i:sa',$timestamp_stage3);
                  $original_time_stage3= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));
                  
                  $minutes_stage3 = floor(abs($timestamp_stage3-(strtotime($specimen_data['create_date'])))/ 60);
                  $tot_minutes_stage3+= $minutes_stage3;
                  $avg_minutes_stage3 = floor($tot_minutes_stage3/$count);
                  $hour_stage3 = round(($avg_minutes_stage3/60),1);
                  $day_stage3 = floor($hour_stage3/24);

                  $chk_hour_stage3 = floor(abs(strtotime($specimen_stage_results_stage3[0]['specimen_timestamp'])-strtotime($specimen_data['create_date'])));
                  if($chk_hour_stage3>172800)
                  {
                    $color3 = 'Red';
                    $time3 = dateDiff($formatted_time_stage3,$original_time_stage3);
                  }
                  else 
                  {
                    $color3 = '';
                    $time3 = dateDiff($formatted_time_stage3,$original_time_stage3);
                  } 
                  
                  }  
                  else
                  {
                    $color3 = '';
                    $time3 = "Not Processed";
                  }

                  $specimen_stage_results_stage4 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE `stage_id` ='4' AND `specimen_id` ='".$specimen_data['id']."'"); 
                  if(!empty($specimen_stage_results_stage4[0]['specimen_timestamp']))
                  {
                  $timestamp_stage4 = strtotime($specimen_stage_results_stage4[0]['specimen_timestamp']);
                  $formatted_time_stage4= date('Y-m-d h:i:sa',$timestamp_stage4);
                  $original_time_stage4= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));

                  $minutes_stage4 = floor(abs($timestamp_stage4-(strtotime($specimen_data['create_date'])))/ 60);
                  $tot_minutes_stage4+= $minutes_stage4;
                  $avg_minutes_stage4 = floor($tot_minutes_stage4/$count);
                  $hour_stage4 = round(($avg_minutes_stage4/60),1);
                  $day_stage4 = floor($hour_stage4/24); 

                  $chk_hour_stage4 = floor(abs(strtotime($specimen_stage_results_stage4[0]['specimen_timestamp'])- strtotime($specimen_data['create_date'])));
                  if($chk_hour_stage4>18000)
                  {
                    $color4 = 'Red';
                    $time4 = dateDiff($formatted_time_stage4,$original_time_stage4); 
                  }
                  else
                  {
                    $color4 = '';
                    $time4 =  dateDiff($formatted_time_stage4,$original_time_stage4); 
                  }
                  }
                  else
                  {
                    $color4 = '';
                    $time4 = "Not Processed";
                  }

                  $pending_sql = $this->BlankModel->customquery("SELECT max(`specimen_timestamp`) AS `max_date` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` ='".$specimen_data['id']."'");
                  
                    if(!empty($pending_sql[0]['max_date']))
                    {
                      $timestamp_pending = strtotime($pending_sql[0]['max_date']);
                      $formatted_time_pending= date('Y-m-d h:i:sa',$timestamp_pending);
                      $original_time_pending= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));

                      $minutes_pending = floor(abs($timestamp_pending-(strtotime($specimen_data['create_date'])))/ 60);
                      $tot_minutes_pending+= $minutes_pending;
                      $avg_minutes_pending = floor($tot_minutes_pending/$count);
                      $hour_stage_pending = round(($avg_minutes_pending/60),1);
                      $day_stage_pending = floor($hour_stage_pending/24); 

                      $chk_hour_stage5 = floor(abs(strtotime($pending_sql[0]['max_date'])-strtotime($specimen_data['create_date'])));
                      if($chk_hour_stage5>18000)
                      {
                         $color5 = 'Red';
                         $time5 = dateDiff($formatted_time_pending,$original_time_pending); 
                      }
                      else
                      {
                        $color5 = '';
                         $time5 =  dateDiff($formatted_time_pending,$original_time_pending); 
                      }
                        
                    }
                    else
                    {
                      $color5 = '';
                      $time5 = "Not Processed";
                    }

                     $report_sql = $this->BlankModel->customquery("SELECT `create_date` FROM `wp_abd_nail_pathology_report` WHERE `specimen_id` ='".$specimen_data['id']."'");
                    if(!empty($report_sql[0]['create_date']))
                    {
                      $timestamp_report = strtotime($report_sql[0]['create_date']);
                      $formatted_time_report= date('Y-m-d h:i:sa',$timestamp_report);
                      $original_time= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));
                      
                      $minutes_stage = floor(abs($timestamp_report-(strtotime($specimen_data['create_date'])))/ 60);
                      $tot_minutes_stage+= $minutes_stage;
                      $avg_minutes_stage = floor($tot_minutes_stage/$count);
                      $hour_stage = round(($avg_minutes_stage/60),1);
                      $day_stage = floor($hour_stage/24); 

                      $chk_hour_stage6 = floor(abs(strtotime($report_sql[0]['create_date'])-strtotime($specimen_data['create_date'])));
                      if($chk_hour_stage6>86400)
                      {
                         $color6 = 'Red';
                         $time6 = dateDiff($formatted_time_report, $original_time); 
                      }
                      else
                      {
                         $color6 = '';
                         $time6 = dateDiff($formatted_time_report, $original_time); 
                      }
                    }
                    else
                    {
                        $color6 = '';
                        $time6 = "Not Processed";
                    }

                $specimen_information =  array('assessioning_num' => $specimen_data['assessioning_num'],'stage1'=>$time,'stage2'=> $time2,
                'stage3'=>$time3,'stage4'=>$time4,'pending'=>$time5,'report'=>$time6,'color1'=>$color1,'color2'=>$color2,'color3'=>$color3,
            'color4'=>$color4,'color5'=>$color5,'color6'=>$color6);
                array_push($details, $specimen_information); 
            }

            if(!empty($day))
            {
                $day1 = $day." Days";
            }
            else{
                $day1= "" ;
            }
            if(!empty($hour))
            {
                $hour1 = $hour." Hours";
            }
            else{
                $hour1= "" ;
            }

            if(!empty($day_stage2))
          {
              $day2 = $day_stage2." Days";
          }
          else{
              $day2= "" ;
          }
          if(!empty($hour_stage2))
          {
              $hour2 = $hour_stage2." Hours";
          }
          else{
              $hour2= "" ;
          }

          if(!empty($day_stage3))
          {
              $day3 = $day_stage3." Days";
          }
          else{
              $day3= "" ;
          }
          if(!empty($hour_stage3))
          {
              $hour3 = $hour_stage3." Hours";
          }
          else{
              $hour3= "" ;
          }
            if(!empty($day_stage4))
            {
                $day4 = $day_stage4." Days";
            }
            else{
                $day4= "" ;
            }
            if(!empty($hour_stage4))
            {
                $hour4 = $hour_stage4." Hours";
            }
            else{
                $hour4= "" ;
            }

              if(!empty($day_stage_pending))
              {
                  $day5 = $day_stage_pending." Days";
              }
              else{
                  $day5= "" ;
              }
              if(!empty($hour_stage_pending))
              {
                  $hour5 = $hour_stage_pending." Hours";
              }
              else{
                  $hour5= "" ;
              }
            if(!empty($day_stage))
              {
                  $day6 = $day_stage." Days";
              }
              else{
                  $day6= "" ;
              }
              if(!empty($hour_stage))
              {
                  $hour6 = $hour_stage." Hours";
              }
              else{
                  $hour6= "" ;
              }
            echo json_encode(array('status'=>'1','details'=> $details,'count'=>$count,'day1'=>$day1,'hour1'=>$hour1,'day2'=>$day2,'hour2'=>$hour2,
            'day3'=>$day3,'hour3'=>$hour3,'day4'=>$day4,'hour4'=>$hour4,'day5'=>$day5,'hour5'=>$hour5,'day6'=>$day6,'hour6'=>$hour6));
        }
        else{
            echo json_encode(array('status'=>'0'));
        }
    }

    function get_pcr_avg_time()
    {   
        $tot_minutes=0;
        $tot_minutes_stage2=0;
        $tot_minutes_stage3=0;
        $tot_minutes_stage4=0;
        $tot_minutes_pending=0;
        $tot_minutes_stage=0;
        $count =0;
        $details = array();
        $specimen_results =$this->BlankModel->customquery("SELECT `wp_abd_specimen`.`assessioning_num`, `wp_abd_specimen`.`id`, `wp_abd_specimen`.`create_date`, `wp_abd_specimen`.`qc_check`
        FROM `wp_abd_specimen` INNER JOIN `wp_abd_clinical_info` ON wp_abd_specimen.id = wp_abd_clinical_info.specimen_id  WHERE (`wp_abd_clinical_info`.`nail_unit`  LIKE '%4%' OR `wp_abd_clinical_info`.`nail_unit`  LIKE '%5%' OR `wp_abd_clinical_info`.`nail_unit`  LIKE '%6%' OR `wp_abd_clinical_info`.`nail_unit`  LIKE '%7%')
        AND wp_abd_specimen.status = '0' AND  wp_abd_specimen.qc_check = '0' AND wp_abd_specimen.physician_accepct = '0'
        AND wp_abd_specimen.create_date > '2017-03-27 23:59:59' ORDER BY `id` DESC limit 0,10");
        if($specimen_results)
        {
            $count= count($specimen_results);
            foreach ($specimen_results as $specimen_data)
            {
                $specimen_stage_results_stage1 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE `stage_id` ='1' AND `specimen_id` ='".$specimen_data['id']."'"); 
                if(!empty($specimen_stage_results_stage1[0]['specimen_timestamp']))
                {
                $timestamp = strtotime($specimen_stage_results_stage1[0]['specimen_timestamp']);
                $formatted_time= date('Y-m-d H:i:s',$timestamp);
                $original_time_stage1= date('Y-m-d H:i:s',strtotime($specimen_data['create_date']));              
                
                $minutes = floor(abs($timestamp-(strtotime($specimen_data['create_date'])))/ 60);
                $tot_minutes+= $minutes;
                $avg_minutes = floor($tot_minutes/$count);
                $hour = round(($avg_minutes/60),1);
                $day = floor($hour/24);

                $chk_hour_stage1 = floor(abs(strtotime($specimen_stage_results_stage1[0]['specimen_timestamp'])- strtotime($specimen_data['create_date'])));
                if($chk_hour_stage1>86400)
                {
                  $color1 = 'Red';
                  $time = dateDiff($formatted_time,$original_time_stage1);
                }
                else
                {
                  $color1 = '';
                  $time =  dateDiff($formatted_time,$original_time_stage1);
                }
                }
                else
                {
                  $color1 = '';
                  $time = "Not Processed";
                }
                
                $specimen_stage_results_stage2 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE `stage_id` ='2' AND `specimen_id` ='".$specimen_data['id']."'"); 
                  if(!empty($specimen_stage_results_stage2[0]['specimen_timestamp']))
                  {
                  $timestamp_stage2 = strtotime($specimen_stage_results_stage2[0]['specimen_timestamp']);
                  $formatted_time_stage2= date('Y-m-d h:i:sa',$timestamp_stage2);
                  $original_time_stage2= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));

                  $minutes_stage2 = floor(abs($timestamp_stage2-(strtotime($specimen_data['create_date'])))/ 60);
                  $tot_minutes_stage2+= $minutes_stage2;
                  $avg_minutes_stage2 = floor($tot_minutes_stage2/$count);
                  $hour_stage2 = round(($avg_minutes_stage2/60),1);
                  $day_stage2 = floor($hour_stage2/24); 

                  $chk_hour_stage2 = floor(abs(strtotime($specimen_stage_results_stage2[0]['specimen_timestamp'])-strtotime($specimen_data['create_date'])));
                  if($chk_hour_stage2>21600) 
                  {
                   $color2 = 'Red';
                   $time2 = dateDiff($formatted_time_stage2,$original_time_stage2);
                  }
                  else
                  {
                    $color2 = '';
                    $time2 = dateDiff($formatted_time_stage2,$original_time_stage2);
                  }
                  }
                  else
                  {
                    $color2 = '';
                    $time2 = "Not Processed";
                  }

                  $specimen_stage_results_stage3 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE `stage_id` ='3' AND `specimen_id` ='".$specimen_data['id']."'"); 
                 if(!empty($specimen_stage_results_stage3[0]['specimen_timestamp']))
                 {
                  $timestamp_stage3 = strtotime($specimen_stage_results_stage3[0]['specimen_timestamp']);
                  $formatted_time_stage3= date('Y-m-d h:i:sa',$timestamp_stage3);
                  $original_time_stage3= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));
                  
                  $minutes_stage3 = floor(abs($timestamp_stage3-(strtotime($specimen_data['create_date'])))/ 60);
                  $tot_minutes_stage3+= $minutes_stage3;
                  $avg_minutes_stage3 = floor($tot_minutes_stage3/$count);
                  $hour_stage3 = round(($avg_minutes_stage3/60),1);
                  $day_stage3 = floor($hour_stage3/24);

                  $chk_hour_stage3 = floor(abs(strtotime($specimen_stage_results_stage3[0]['specimen_timestamp'])-strtotime($specimen_data['create_date'])));
                  if($chk_hour_stage3>172800)
                  {
                    $color3 = 'Red';
                    $time3 = dateDiff($formatted_time_stage3,$original_time_stage3);
                  }
                  else 
                  {
                    $color3 = '';
                    $time3 = dateDiff($formatted_time_stage3,$original_time_stage3);
                  } 
                  
                  }  
                  else
                  {
                    $color3 = '';
                    $time3 = "Not Processed";
                  }

                  $specimen_stage_results_stage4 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE `stage_id` ='4' AND `specimen_id` ='".$specimen_data['id']."'"); 
                  if(!empty($specimen_stage_results_stage4[0]['specimen_timestamp']))
                  {
                  $timestamp_stage4 = strtotime($specimen_stage_results_stage4[0]['specimen_timestamp']);
                  $formatted_time_stage4= date('Y-m-d h:i:sa',$timestamp_stage4);
                  $original_time_stage4= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));

                  $minutes_stage4 = floor(abs($timestamp_stage4-(strtotime($specimen_data['create_date'])))/ 60);
                  $tot_minutes_stage4+= $minutes_stage4;
                  $avg_minutes_stage4 = floor($tot_minutes_stage4/$count);
                  $hour_stage4 = round(($avg_minutes_stage4/60),1);
                  $day_stage4 = floor($hour_stage4/24); 

                  $chk_hour_stage4 = floor(abs(strtotime($specimen_stage_results_stage4[0]['specimen_timestamp'])- strtotime($specimen_data['create_date'])));
                  if($chk_hour_stage4>18000)
                  {
                    $color4 = 'Red';
                    $time4 = dateDiff($formatted_time_stage4,$original_time_stage4); 
                  }
                  else
                  {
                    $color4 = '';
                    $time4 =  dateDiff($formatted_time_stage4,$original_time_stage4); 
                  }
                  }
                  else
                  {
                    $color4 = '';
                    $time4 = "Not Processed";
                  }

                  $pending_sql = $this->BlankModel->customquery("SELECT max(`specimen_timestamp`) AS `max_date` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` ='".$specimen_data['id']."'");
                  
                    if(!empty($pending_sql[0]['max_date']))
                    {
                      $timestamp_pending = strtotime($pending_sql[0]['max_date']);
                      $formatted_time_pending= date('Y-m-d h:i:sa',$timestamp_pending);
                      $original_time_pending= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));

                      $minutes_pending = floor(abs($timestamp_pending-(strtotime($specimen_data['create_date'])))/ 60);
                      $tot_minutes_pending+= $minutes_pending;
                      $avg_minutes_pending = floor($tot_minutes_pending/$count);
                      $hour_stage_pending = round(($avg_minutes_pending/60),1);
                      $day_stage_pending = floor($hour_stage_pending/24); 

                      $chk_hour_stage5 = floor(abs(strtotime($pending_sql[0]['max_date'])-strtotime($specimen_data['create_date'])));
                      if($chk_hour_stage5>18000)
                      {
                         $color5 = 'Red';
                         $time5 = dateDiff($formatted_time_pending,$original_time_pending); 
                      }
                      else
                      {
                        $color5 = '';
                         $time5 =  dateDiff($formatted_time_pending,$original_time_pending); 
                      }
                        
                    }
                    else
                    {
                      $color5 = '';
                      $time5 = "Not Processed";
                    }

                     $report_sql = $this->BlankModel->customquery("SELECT `create_date` FROM `wp_abd_nail_pathology_report` WHERE `specimen_id` ='".$specimen_data['id']."'");
                    if(!empty($report_sql[0]['create_date']))
                    {
                      $timestamp_report = strtotime($report_sql[0]['create_date']);
                      $formatted_time_report= date('Y-m-d h:i:sa',$timestamp_report);
                      $original_time= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));
                      
                      $minutes_stage = floor(abs($timestamp_report-(strtotime($specimen_data['create_date'])))/ 60);
                      $tot_minutes_stage+= $minutes_stage;
                      $avg_minutes_stage = floor($tot_minutes_stage/$count);
                      $hour_stage = round(($avg_minutes_stage/60),1);
                      $day_stage = floor($hour_stage/24); 

                      $chk_hour_stage6 = floor(abs(strtotime($report_sql[0]['create_date'])-strtotime($specimen_data['create_date'])));
                      if($chk_hour_stage6>86400)
                      {
                         $color6 = 'Red';
                         $time6 = dateDiff($formatted_time_report, $original_time); 
                      }
                      else
                      {
                         $color6 = '';
                         $time6 = dateDiff($formatted_time_report, $original_time); 
                      }
                    }
                    else
                    {
                        $color6 = '';
                        $time6 = "Not Processed";
                    }

                $specimen_information =  array('assessioning_num' => $specimen_data['assessioning_num'],'stage1'=>$time,'stage2'=> $time2,
                'stage3'=>$time3,'stage4'=>$time4,'pending'=>$time5,'report'=>$time6,'color1'=>$color1,'color2'=>$color2,'color3'=>$color3,
               'color4'=>$color4,'color5'=>$color5,'color6'=>$color6);
                array_push($details, $specimen_information); 
            }

            if(!empty($day))
            {
                $day1 = $day." Days";
            }
            else{
                $day1= "" ;
            }
            if(!empty($hour))
            {
                $hour1 = $hour." Hours";
            }
            else{
                $hour1= "" ;
            }

            if(!empty($day_stage2))
          {
              $day2 = $day_stage2." Days";
          }
          else{
              $day2= "" ;
          }
          if(!empty($hour_stage2))
          {
              $hour2 = $hour_stage2." Hours";
          }
          else{
              $hour2= "" ;
          }

          if(!empty($day_stage3))
          {
              $day3 = $day_stage3." Days";
          }
          else{
              $day3= "" ;
          }
          if(!empty($hour_stage3))
          {
              $hour3 = $hour_stage3." Hours";
          }
          else{
              $hour3= "" ;
          }
            if(!empty($day_stage4))
            {
                $day4 = $day_stage4." Days";
            }
            else{
                $day4= "" ;
            }
            if(!empty($hour_stage4))
            {
                $hour4 = $hour_stage4." Hours";
            }
            else{
                $hour4= "" ;
            }

              if(!empty($day_stage_pending))
              {
                  $day5 = $day_stage_pending." Days";
              }
              else{
                  $day5= "" ;
              }
              if(!empty($hour_stage_pending))
              {
                  $hour5 = $hour_stage_pending." Hours";
              }
              else{
                  $hour5= "" ;
              }
            if(!empty($day_stage))
              {
                  $day6 = $day_stage." Days";
              }
              else{
                  $day6= "" ;
              }
              if(!empty($hour_stage))
              {
                  $hour6 = $hour_stage." Hours";
              }
              else{
                  $hour6= "" ;
              }

            echo json_encode(array('status'=>'1','details'=> $details,'count'=>$count,'day1'=>$day1,'hour1'=>$hour1,'day2'=>$day2,'hour2'=>$hour2,
            'day3'=>$day3,'hour3'=>$hour3,'day4'=>$day4,'hour4'=>$hour4,'day5'=>$day5,'hour5'=>$hour5,'day6'=>$day6,'hour6'=>$hour6));
        }
    }
    
    function get_more_pcr_avg_time()
    {   
        $data = json_decode(file_get_contents('php://input'), true);
        $start = $data['load'];
        if(!empty($data))
        {
            $tot_minutes=0;
            $tot_minutes_stage2=0;
            $tot_minutes_stage3=0;
            $tot_minutes_stage4=0;
            $tot_minutes_pending=0;
            $tot_minutes_stage=0;
            $count =0;
            $details = array();
            $specimen_results =$this->BlankModel->customquery("SELECT `wp_abd_specimen`.`assessioning_num`, `wp_abd_specimen`.`id`, `wp_abd_specimen`.`create_date`, `wp_abd_specimen`.`qc_check`
            FROM `wp_abd_specimen` INNER JOIN `wp_abd_clinical_info` ON wp_abd_specimen.id = wp_abd_clinical_info.specimen_id  WHERE (`wp_abd_clinical_info`.`nail_unit`  LIKE '%4%' OR `wp_abd_clinical_info`.`nail_unit`  LIKE '%5%' OR `wp_abd_clinical_info`.`nail_unit`  LIKE '%6%' OR `wp_abd_clinical_info`.`nail_unit`  LIKE '%7%')
            AND wp_abd_specimen.status = '0' AND  wp_abd_specimen.qc_check = '0' AND wp_abd_specimen.physician_accepct = '0'
            AND wp_abd_specimen.create_date > '2017-03-27 23:59:59' ORDER BY `id` DESC limit $start,10");
            if($specimen_results)
            {
                $count= $start+10;
                foreach ($specimen_results as $specimen_data)
                {
                    $specimen_stage_results_stage1 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE `stage_id` ='1' AND `specimen_id` ='".$specimen_data['id']."'"); 
                    if(!empty($specimen_stage_results_stage1[0]['specimen_timestamp']))
                    {
                    $timestamp = strtotime($specimen_stage_results_stage1[0]['specimen_timestamp']);
                    $formatted_time= date('Y-m-d H:i:s',$timestamp);
                    $original_time_stage1= date('Y-m-d H:i:s',strtotime($specimen_data['create_date']));              
                    
                    $minutes = floor(abs($timestamp-(strtotime($specimen_data['create_date'])))/ 60);
                    $tot_minutes+= $minutes;
                    $avg_minutes = floor($tot_minutes/$count);
                    $hour = round(($avg_minutes/60),1);
                    $day = floor($hour/24);
    
                    $chk_hour_stage1 = floor(abs(strtotime($specimen_stage_results_stage1[0]['specimen_timestamp'])- strtotime($specimen_data['create_date'])));
                    if($chk_hour_stage1>86400)
                    {
                      $color1 = 'Red';
                      $time = dateDiff($formatted_time,$original_time_stage1);
                    }
                    else
                    {
                      $color1 = '';
                      $time =  dateDiff($formatted_time,$original_time_stage1);
                    }
                    }
                    else
                    {
                      $color1 = '';
                      $time = "Not Processed";
                    }
                    
                    $specimen_stage_results_stage2 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE `stage_id` ='2' AND `specimen_id` ='".$specimen_data['id']."'"); 
                      if(!empty($specimen_stage_results_stage2[0]['specimen_timestamp']))
                      {
                      $timestamp_stage2 = strtotime($specimen_stage_results_stage2[0]['specimen_timestamp']);
                      $formatted_time_stage2= date('Y-m-d h:i:sa',$timestamp_stage2);
                      $original_time_stage2= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));
    
                      $minutes_stage2 = floor(abs($timestamp_stage2-(strtotime($specimen_data['create_date'])))/ 60);
                      $tot_minutes_stage2+= $minutes_stage2;
                      $avg_minutes_stage2 = floor($tot_minutes_stage2/$count);
                      $hour_stage2 = round(($avg_minutes_stage2/60),1);
                      $day_stage2 = floor($hour_stage2/24); 
    
                      $chk_hour_stage2 = floor(abs(strtotime($specimen_stage_results_stage2[0]['specimen_timestamp'])-strtotime($specimen_data['create_date'])));
                      if($chk_hour_stage2>21600) 
                      {
                       $color2 = 'Red';
                       $time2 = dateDiff($formatted_time_stage2,$original_time_stage2);
                      }
                      else
                      {
                        $color2 = '';
                        $time2 = dateDiff($formatted_time_stage2,$original_time_stage2);
                      }
                      }
                      else
                      {
                        $color2 = '';
                        $time2 = "Not Processed";
                      }
    
                      $specimen_stage_results_stage3 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE `stage_id` ='3' AND `specimen_id` ='".$specimen_data['id']."'"); 
                     if(!empty($specimen_stage_results_stage3[0]['specimen_timestamp']))
                     {
                      $timestamp_stage3 = strtotime($specimen_stage_results_stage3[0]['specimen_timestamp']);
                      $formatted_time_stage3= date('Y-m-d h:i:sa',$timestamp_stage3);
                      $original_time_stage3= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));
                      
                      $minutes_stage3 = floor(abs($timestamp_stage3-(strtotime($specimen_data['create_date'])))/ 60);
                      $tot_minutes_stage3+= $minutes_stage3;
                      $avg_minutes_stage3 = floor($tot_minutes_stage3/$count);
                      $hour_stage3 = round(($avg_minutes_stage3/60),1);
                      $day_stage3 = floor($hour_stage3/24);
    
                      $chk_hour_stage3 = floor(abs(strtotime($specimen_stage_results_stage3[0]['specimen_timestamp'])-strtotime($specimen_data['create_date'])));
                      if($chk_hour_stage3>172800)
                      {
                        $color3 = 'Red';
                        $time3 = dateDiff($formatted_time_stage3,$original_time_stage3);
                      }
                      else 
                      {
                        $color3 = '';
                        $time3 = dateDiff($formatted_time_stage3,$original_time_stage3);
                      } 
                      
                      }  
                      else
                      {
                        $color3 = '';
                        $time3 = "Not Processed";
                      }
    
                      $specimen_stage_results_stage4 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE `stage_id` ='4' AND `specimen_id` ='".$specimen_data['id']."'"); 
                      if(!empty($specimen_stage_results_stage4[0]['specimen_timestamp']))
                      {
                      $timestamp_stage4 = strtotime($specimen_stage_results_stage4[0]['specimen_timestamp']);
                      $formatted_time_stage4= date('Y-m-d h:i:sa',$timestamp_stage4);
                      $original_time_stage4= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));
    
                      $minutes_stage4 = floor(abs($timestamp_stage4-(strtotime($specimen_data['create_date'])))/ 60);
                      $tot_minutes_stage4+= $minutes_stage4;
                      $avg_minutes_stage4 = floor($tot_minutes_stage4/$count);
                      $hour_stage4 = round(($avg_minutes_stage4/60),1);
                      $day_stage4 = floor($hour_stage4/24); 
    
                      $chk_hour_stage4 = floor(abs(strtotime($specimen_stage_results_stage4[0]['specimen_timestamp'])- strtotime($specimen_data['create_date'])));
                      if($chk_hour_stage4>18000)
                      {
                        $color4 = 'Red';
                        $time4 = dateDiff($formatted_time_stage4,$original_time_stage4); 
                      }
                      else
                      {
                        $color4 = '';
                        $time4 =  dateDiff($formatted_time_stage4,$original_time_stage4); 
                      }
                      }
                      else
                      {
                        $color4 = '';
                        $time4 = "Not Processed";
                      }
    
                      $pending_sql = $this->BlankModel->customquery("SELECT max(`specimen_timestamp`) AS `max_date` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` ='".$specimen_data['id']."'");
                      
                        if(!empty($pending_sql[0]['max_date']))
                        {
                          $timestamp_pending = strtotime($pending_sql[0]['max_date']);
                          $formatted_time_pending= date('Y-m-d h:i:sa',$timestamp_pending);
                          $original_time_pending= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));
    
                          $minutes_pending = floor(abs($timestamp_pending-(strtotime($specimen_data['create_date'])))/ 60);
                          $tot_minutes_pending+= $minutes_pending;
                          $avg_minutes_pending = floor($tot_minutes_pending/$count);
                          $hour_stage_pending = round(($avg_minutes_pending/60),1);
                          $day_stage_pending = floor($hour_stage_pending/24); 
    
                          $chk_hour_stage5 = floor(abs(strtotime($pending_sql[0]['max_date'])-strtotime($specimen_data['create_date'])));
                          if($chk_hour_stage5>18000)
                          {
                             $color5 = 'Red';
                             $time5 = dateDiff($formatted_time_pending,$original_time_pending); 
                          }
                          else
                          {
                            $color5 = '';
                             $time5 =  dateDiff($formatted_time_pending,$original_time_pending); 
                          }
                            
                        }
                        else
                        {
                          $color5 = '';
                          $time5 = "Not Processed";
                        }
    
                         $report_sql = $this->BlankModel->customquery("SELECT `create_date` FROM `wp_abd_nail_pathology_report` WHERE `specimen_id` ='".$specimen_data['id']."'");
                        if(!empty($report_sql[0]['create_date']))
                        {
                          $timestamp_report = strtotime($report_sql[0]['create_date']);
                          $formatted_time_report= date('Y-m-d h:i:sa',$timestamp_report);
                          $original_time= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));
                          
                          $minutes_stage = floor(abs($timestamp_report-(strtotime($specimen_data['create_date'])))/ 60);
                          $tot_minutes_stage+= $minutes_stage;
                          $avg_minutes_stage = floor($tot_minutes_stage/$count);
                          $hour_stage = round(($avg_minutes_stage/60),1);
                          $day_stage = floor($hour_stage/24); 
    
                          $chk_hour_stage6 = floor(abs(strtotime($report_sql[0]['create_date'])-strtotime($specimen_data['create_date'])));
                          if($chk_hour_stage6>86400)
                          {
                             $color6 = 'Red';
                             $time6 = dateDiff($formatted_time_report, $original_time); 
                          }
                          else
                          {
                             $color6 = '';
                             $time6 = dateDiff($formatted_time_report, $original_time); 
                          }
                        }
                        else
                        {
                            $color6 = '';
                            $time6 = "Not Processed";
                        }
    
                    $specimen_information =  array('assessioning_num' => $specimen_data['assessioning_num'],'stage1'=>$time,'stage2'=> $time2,
                    'stage3'=>$time3,'stage4'=>$time4,'pending'=>$time5,'report'=>$time6,'color1'=>$color1,'color2'=>$color2,'color3'=>$color3,
                   'color4'=>$color4,'color5'=>$color5,'color6'=>$color6);
                    array_push($details, $specimen_information); 
                }
    
                if(!empty($day))
                {
                    $day1 = $day." Days";
                }
                else{
                    $day1= "" ;
                }
                if(!empty($hour))
                {
                    $hour1 = $hour." Hours";
                }
                else{
                    $hour1= "" ;
                }
    
                if(!empty($day_stage2))
              {
                  $day2 = $day_stage2." Days";
              }
              else{
                  $day2= "" ;
              }
              if(!empty($hour_stage2))
              {
                  $hour2 = $hour_stage2." Hours";
              }
              else{
                  $hour2= "" ;
              }
    
              if(!empty($day_stage3))
              {
                  $day3 = $day_stage3." Days";
              }
              else{
                  $day3= "" ;
              }
              if(!empty($hour_stage3))
              {
                  $hour3 = $hour_stage3." Hours";
              }
              else{
                  $hour3= "" ;
              }
                if(!empty($day_stage4))
                {
                    $day4 = $day_stage4." Days";
                }
                else{
                    $day4= "" ;
                }
                if(!empty($hour_stage4))
                {
                    $hour4 = $hour_stage4." Hours";
                }
                else{
                    $hour4= "" ;
                }
    
                  if(!empty($day_stage_pending))
                  {
                      $day5 = $day_stage_pending." Days";
                  }
                  else{
                      $day5= "" ;
                  }
                  if(!empty($hour_stage_pending))
                  {
                      $hour5 = $hour_stage_pending." Hours";
                  }
                  else{
                      $hour5= "" ;
                  }
                if(!empty($day_stage))
                  {
                      $day6 = $day_stage." Days";
                  }
                  else{
                      $day6= "" ;
                  }
                  if(!empty($hour_stage))
                  {
                      $hour6 = $hour_stage." Hours";
                  }
                  else{
                      $hour6= "" ;
                  }
    
                echo json_encode(array('status'=>'1','details'=> $details,'count'=>$count,'day1'=>$day1,'hour1'=>$hour1,'day2'=>$day2,'hour2'=>$hour2,
                'day3'=>$day3,'hour3'=>$hour3,'day4'=>$day4,'hour4'=>$hour4,'day5'=>$day5,'hour5'=>$hour5,'day6'=>$day6,'hour6'=>$hour6));
            }
        }
       
    }


    function search_pcr_avg_time()
    {   
        $data = json_decode(file_get_contents('php://input'), true);
        $tot_minutes=0;
        $tot_minutes_stage2=0;
        $tot_minutes_stage3=0;
        $tot_minutes_stage4=0;
        $tot_minutes_pending=0;
        $tot_minutes_stage=0;
        $count =0;
        $details = array();
        $day=0;
        
        if($data['from_date']!="" && $data['to_date']!="" && ($data['from_date'] > '2017-03-27 23:59:59') && ($data['to_date'] > '2017-03-27 23:59:59'))
        {
            $specimen_results = $this->BlankModel->customquery("SELECT `wp_abd_specimen`.`assessioning_num`, `wp_abd_specimen`.`id`, `wp_abd_specimen`.`create_date`, `wp_abd_specimen`.`qc_check`
            FROM wp_abd_specimen
            INNER JOIN wp_abd_clinical_info
            ON wp_abd_specimen.id = wp_abd_clinical_info.specimen_id 
            WHERE (`wp_abd_clinical_info`.`nail_unit`  LIKE '%4%' OR `wp_abd_clinical_info`.`nail_unit`  LIKE '%5%' OR `wp_abd_clinical_info`.`nail_unit`  LIKE '%6%' OR `wp_abd_clinical_info`.`nail_unit`  LIKE '%7%')
            AND wp_abd_specimen.status = '0'
            AND wp_abd_specimen.qc_check = '0' 
            AND wp_abd_specimen.physician_accepct = '0'
            AND wp_abd_specimen.create_date BETWEEN '".$data['from_date']." 00:00:00' and '".$data['to_date']." 23:59:59'");
        }
        if($data['acc']!="")
        {
            $specimen_results = $this->BlankModel->customquery("SELECT `wp_abd_specimen`.`assessioning_num`, `wp_abd_specimen`.`id`, `wp_abd_specimen`.`create_date`, `wp_abd_specimen`.`qc_check`
            FROM wp_abd_specimen
            INNER JOIN wp_abd_clinical_info
            ON wp_abd_specimen.id = wp_abd_clinical_info.specimen_id 
            WHERE (`wp_abd_clinical_info`.`nail_unit`  LIKE '%4%' OR `wp_abd_clinical_info`.`nail_unit`  LIKE '%5%' OR `wp_abd_clinical_info`.`nail_unit`  LIKE '%6%' OR `wp_abd_clinical_info`.`nail_unit`  LIKE '%7%')
            AND wp_abd_specimen.status = '0'
            AND wp_abd_specimen.qc_check = '0' 
            AND wp_abd_specimen.physician_accepct = '0'
            AND wp_abd_specimen.assessioning_num ='".$data['acc']."'");
        }
        
        if($specimen_results)
        {
            $count= count($specimen_results);
            foreach ($specimen_results as $specimen_data)
            {
                $specimen_stage_results_stage1 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE `stage_id` ='1' AND `specimen_id` ='".$specimen_data['id']."'"); 
                if(!empty($specimen_stage_results_stage1[0]['specimen_timestamp']))
                {
                $timestamp = strtotime($specimen_stage_results_stage1[0]['specimen_timestamp']);
                $formatted_time= date('Y-m-d H:i:s',$timestamp);
                $original_time_stage1= date('Y-m-d H:i:s',strtotime($specimen_data['create_date']));              
                
                $minutes = floor(abs($timestamp-(strtotime($specimen_data['create_date'])))/ 60);
                $tot_minutes+= $minutes;
                $avg_minutes = floor($tot_minutes/$count);
                $hour = round(($avg_minutes/60),1);
                $day = floor($hour/24);

                $chk_hour_stage1 = floor(abs(strtotime($specimen_stage_results_stage1[0]['specimen_timestamp'])- strtotime($specimen_data['create_date'])));
                if($chk_hour_stage1>86400)
                {
                  $color1 = 'Red';
                  $time = dateDiff($formatted_time,$original_time_stage1);
                }
                else
                {
                  $color1 = '';
                  $time =  dateDiff($formatted_time,$original_time_stage1);
                }
                }
                else
                {
                  $color1 = '';
                  $time = "Not Processed";
                }
                
                $specimen_stage_results_stage2 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE `stage_id` ='2' AND `specimen_id` ='".$specimen_data['id']."'"); 
                  if(!empty($specimen_stage_results_stage2[0]['specimen_timestamp']))
                  {
                  $timestamp_stage2 = strtotime($specimen_stage_results_stage2[0]['specimen_timestamp']);
                  $formatted_time_stage2= date('Y-m-d h:i:sa',$timestamp_stage2);
                  $original_time_stage2= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));

                  $minutes_stage2 = floor(abs($timestamp_stage2-(strtotime($specimen_data['create_date'])))/ 60);
                  $tot_minutes_stage2+= $minutes_stage2;
                  $avg_minutes_stage2 = floor($tot_minutes_stage2/$count);
                  $hour_stage2 = round(($avg_minutes_stage2/60),1);
                  $day_stage2 = floor($hour_stage2/24); 

                  $chk_hour_stage2 = floor(abs(strtotime($specimen_stage_results_stage2[0]['specimen_timestamp'])-strtotime($specimen_data['create_date'])));
                  if($chk_hour_stage2>21600) 
                  {
                   $color2 = 'Red';
                   $time2 = dateDiff($formatted_time_stage2,$original_time_stage2);
                  }
                  else
                  {
                    $color2 = '';
                    $time2 = dateDiff($formatted_time_stage2,$original_time_stage2);
                  }
                  }
                  else
                  {
                    $color2 = '';
                    $time2 = "Not Processed";
                  }

                  $specimen_stage_results_stage3 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE `stage_id` ='3' AND `specimen_id` ='".$specimen_data['id']."'"); 
                 if(!empty($specimen_stage_results_stage3[0]['specimen_timestamp']))
                 {
                  $timestamp_stage3 = strtotime($specimen_stage_results_stage3[0]['specimen_timestamp']);
                  $formatted_time_stage3= date('Y-m-d h:i:sa',$timestamp_stage3);
                  $original_time_stage3= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));
                  
                  $minutes_stage3 = floor(abs($timestamp_stage3-(strtotime($specimen_data['create_date'])))/ 60);
                  $tot_minutes_stage3+= $minutes_stage3;
                  $avg_minutes_stage3 = floor($tot_minutes_stage3/$count);
                  $hour_stage3 = round(($avg_minutes_stage3/60),1);
                  $day_stage3 = floor($hour_stage3/24);

                  $chk_hour_stage3 = floor(abs(strtotime($specimen_stage_results_stage3[0]['specimen_timestamp'])-strtotime($specimen_data['create_date'])));
                  if($chk_hour_stage3>172800)
                  {
                    $color3 = 'Red';
                    $time3 = dateDiff($formatted_time_stage3,$original_time_stage3);
                  }
                  else 
                  {
                    $color3 = '';
                    $time3 = dateDiff($formatted_time_stage3,$original_time_stage3);
                  } 
                  
                  }  
                  else
                  {
                    $color3 = '';
                    $time3 = "Not Processed";
                  }

                  $specimen_stage_results_stage4 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE `stage_id` ='4' AND `specimen_id` ='".$specimen_data['id']."'"); 
                  if(!empty($specimen_stage_results_stage4[0]['specimen_timestamp']))
                  {
                  $timestamp_stage4 = strtotime($specimen_stage_results_stage4[0]['specimen_timestamp']);
                  $formatted_time_stage4= date('Y-m-d h:i:sa',$timestamp_stage4);
                  $original_time_stage4= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));

                  $minutes_stage4 = floor(abs($timestamp_stage4-(strtotime($specimen_data['create_date'])))/ 60);
                  $tot_minutes_stage4+= $minutes_stage4;
                  $avg_minutes_stage4 = floor($tot_minutes_stage4/$count);
                  $hour_stage4 = round(($avg_minutes_stage4/60),1);
                  $day_stage4 = floor($hour_stage4/24); 

                  $chk_hour_stage4 = floor(abs(strtotime($specimen_stage_results_stage4[0]['specimen_timestamp'])- strtotime($specimen_data['create_date'])));
                  if($chk_hour_stage4>18000)
                  {
                    $color4 = 'Red';
                    $time4 = dateDiff($formatted_time_stage4,$original_time_stage4); 
                  }
                  else
                  {
                    $color4 = '';
                    $time4 =  dateDiff($formatted_time_stage4,$original_time_stage4); 
                  }
                  }
                  else
                  {
                    $color4 = '';
                    $time4 = "Not Processed";
                  }

                  $pending_sql = $this->BlankModel->customquery("SELECT max(`specimen_timestamp`) AS `max_date` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` ='".$specimen_data['id']."'");
                  
                    if(!empty($pending_sql[0]['max_date']))
                    {
                      $timestamp_pending = strtotime($pending_sql[0]['max_date']);
                      $formatted_time_pending= date('Y-m-d h:i:sa',$timestamp_pending);
                      $original_time_pending= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));

                      $minutes_pending = floor(abs($timestamp_pending-(strtotime($specimen_data['create_date'])))/ 60);
                      $tot_minutes_pending+= $minutes_pending;
                      $avg_minutes_pending = floor($tot_minutes_pending/$count);
                      $hour_stage_pending = round(($avg_minutes_pending/60),1);
                      $day_stage_pending = floor($hour_stage_pending/24); 

                      $chk_hour_stage5 = floor(abs(strtotime($pending_sql[0]['max_date'])-strtotime($specimen_data['create_date'])));
                      if($chk_hour_stage5>18000)
                      {
                         $color5 = 'Red';
                         $time5 = dateDiff($formatted_time_pending,$original_time_pending); 
                      }
                      else
                      {
                        $color5 = '';
                         $time5 =  dateDiff($formatted_time_pending,$original_time_pending); 
                      }
                        
                    }
                    else
                    {
                      $color5 = '';
                      $time5 = "Not Processed";
                    }

                     $report_sql = $this->BlankModel->customquery("SELECT `create_date` FROM `wp_abd_nail_pathology_report` WHERE `specimen_id` ='".$specimen_data['id']."'");
                    if(!empty($report_sql[0]['create_date']))
                    {
                      $timestamp_report = strtotime($report_sql[0]['create_date']);
                      $formatted_time_report= date('Y-m-d h:i:sa',$timestamp_report);
                      $original_time= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));
                      
                      $minutes_stage = floor(abs($timestamp_report-(strtotime($specimen_data['create_date'])))/ 60);
                      $tot_minutes_stage+= $minutes_stage;
                      $avg_minutes_stage = floor($tot_minutes_stage/$count);
                      $hour_stage = round(($avg_minutes_stage/60),1);
                      $day_stage = floor($hour_stage/24); 

                      $chk_hour_stage6 = floor(abs(strtotime($report_sql[0]['create_date'])-strtotime($specimen_data['create_date'])));
                      if($chk_hour_stage6>86400)
                      {
                         $color6 = 'Red';
                         $time6 = dateDiff($formatted_time_report, $original_time); 
                      }
                      else
                      {
                         $color6 = '';
                         $time6 = dateDiff($formatted_time_report, $original_time); 
                      }
                    }
                    else
                    {
                        $color6 = '';
                        $time6 = "Not Processed";
                    }

                $specimen_information =  array('assessioning_num' => $specimen_data['assessioning_num'],'stage1'=>$time,'stage2'=> $time2,
                'stage3'=>$time3,'stage4'=>$time4,'pending'=>$time5,'report'=>$time6,'color1'=>$color1,'color2'=>$color2,'color3'=>$color3,
            'color4'=>$color4,'color5'=>$color5,'color6'=>$color6);
                array_push($details, $specimen_information); 
            }

            if(!empty($day))
            {
                $day1 = $day." Days";
            }
            else{
                $day1= "" ;
            }
            if(!empty($hour))
            {
                $hour1 = $hour." Hours";
            }
            else{
                $hour1= "" ;
            }

            if(!empty($day_stage2))
          {
              $day2 = $day_stage2." Days";
          }
          else{
              $day2= "" ;
          }
          if(!empty($hour_stage2))
          {
              $hour2 = $hour_stage2." Hours";
          }
          else{
              $hour2= "" ;
          }

          if(!empty($day_stage3))
          {
              $day3 = $day_stage3." Days";
          }
          else{
              $day3= "" ;
          }
          if(!empty($hour_stage3))
          {
              $hour3 = $hour_stage3." Hours";
          }
          else{
              $hour3= "" ;
          }
            if(!empty($day_stage4))
            {
                $day4 = $day_stage4." Days";
            }
            else{
                $day4= "" ;
            }
            if(!empty($hour_stage4))
            {
                $hour4 = $hour_stage4." Hours";
            }
            else{
                $hour4= "" ;
            }

              if(!empty($day_stage_pending))
              {
                  $day5 = $day_stage_pending." Days";
              }
              else{
                  $day5= "" ;
              }
              if(!empty($hour_stage_pending))
              {
                  $hour5 = $hour_stage_pending." Hours";
              }
              else{
                  $hour5= "" ;
              }
            if(!empty($day_stage))
              {
                  $day6 = $day_stage." Days";
              }
              else{
                  $day6= "" ;
              }
              if(!empty($hour_stage))
              {
                  $hour6 = $hour_stage." Hours";
              }
              else{
                  $hour6= "" ;
              }

            echo json_encode(array('status'=>'1','details'=> $details,'count'=>$count,'day1'=>$day1,'hour1'=>$hour1,'day2'=>$day2,'hour2'=>$hour2,
            'day3'=>$day3,'hour3'=>$hour3,'day4'=>$day4,'hour4'=>$hour4,'day5'=>$day5,'hour5'=>$hour5,'day6'=>$day6,'hour6'=>$hour6));
        }
    }
    function pcr_last_one_month()
    {
        $tot_minutes=0;
        $tot_minutes_stage2=0;
        $tot_minutes_stage3=0;
        $tot_minutes_stage4=0;
        $tot_minutes_pending=0;
        $tot_minutes_stage=0;
        $count =0;
        $details = array();
        $specimen_results =$this->BlankModel->customquery("SELECT `wp_abd_specimen`.`assessioning_num`, `wp_abd_specimen`.`id`, `wp_abd_specimen`.`create_date`, `wp_abd_specimen`.`qc_check`
        FROM `wp_abd_specimen` INNER JOIN `wp_abd_clinical_info` ON wp_abd_specimen.id = wp_abd_clinical_info.specimen_id   WHERE (`wp_abd_clinical_info`.`nail_unit`  LIKE '%4%' OR `wp_abd_clinical_info`.`nail_unit`  LIKE '%5%' OR `wp_abd_clinical_info`.`nail_unit`  LIKE '%6%' OR `wp_abd_clinical_info`.`nail_unit`  LIKE '%7%')
        AND wp_abd_specimen.status = '0' AND  wp_abd_specimen.qc_check = '0' AND wp_abd_specimen.physician_accepct = '0'
        AND wp_abd_specimen.create_date >= DATE(NOW()) - INTERVAL 1 MONTH ORDER BY `id` DESC");
        if($specimen_results)
        {
            $count= count($specimen_results);
            foreach ($specimen_results as $specimen_data)
            {
                $specimen_stage_results_stage1 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE `stage_id` ='1' AND `specimen_id` ='".$specimen_data['id']."'"); 
                if(!empty($specimen_stage_results_stage1[0]['specimen_timestamp']))
                {
                $timestamp = strtotime($specimen_stage_results_stage1[0]['specimen_timestamp']);
                $formatted_time= date('Y-m-d H:i:s',$timestamp);
                $original_time_stage1= date('Y-m-d H:i:s',strtotime($specimen_data['create_date']));              
                
                $minutes = floor(abs($timestamp-(strtotime($specimen_data['create_date'])))/ 60);
                $tot_minutes+= $minutes;
                $avg_minutes = floor($tot_minutes/$count);
                $hour = round(($avg_minutes/60),1);
                $day = floor($hour/24);

                $chk_hour_stage1 = floor(abs(strtotime($specimen_stage_results_stage1[0]['specimen_timestamp'])- strtotime($specimen_data['create_date'])));
                if($chk_hour_stage1>86400)
                {
                  $color1 = 'Red';
                  $time = dateDiff($formatted_time,$original_time_stage1);
                }
                else
                {
                  $color1 = '';
                  $time =  dateDiff($formatted_time,$original_time_stage1);
                }
                }
                else
                {
                  $color1 = '';
                  $time = "Not Processed";
                }
                
                $specimen_stage_results_stage2 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE `stage_id` ='2' AND `specimen_id` ='".$specimen_data['id']."'"); 
                  if(!empty($specimen_stage_results_stage2[0]['specimen_timestamp']))
                  {
                  $timestamp_stage2 = strtotime($specimen_stage_results_stage2[0]['specimen_timestamp']);
                  $formatted_time_stage2= date('Y-m-d h:i:sa',$timestamp_stage2);
                  $original_time_stage2= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));

                  $minutes_stage2 = floor(abs($timestamp_stage2-(strtotime($specimen_data['create_date'])))/ 60);
                  $tot_minutes_stage2+= $minutes_stage2;
                  $avg_minutes_stage2 = floor($tot_minutes_stage2/$count);
                  $hour_stage2 = round(($avg_minutes_stage2/60),1);
                  $day_stage2 = floor($hour_stage2/24); 

                  $chk_hour_stage2 = floor(abs(strtotime($specimen_stage_results_stage2[0]['specimen_timestamp'])-strtotime($specimen_data['create_date'])));
                  if($chk_hour_stage2>21600) 
                  {
                   $color2 = 'Red';
                   $time2 = dateDiff($formatted_time_stage2,$original_time_stage2);
                  }
                  else
                  {
                    $color2 = '';
                    $time2 = dateDiff($formatted_time_stage2,$original_time_stage2);
                  }
                  }
                  else
                  {
                    $color2 = '';
                    $time2 = "Not Processed";
                  }

                  $specimen_stage_results_stage3 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE `stage_id` ='3' AND `specimen_id` ='".$specimen_data['id']."'"); 
                 if(!empty($specimen_stage_results_stage3[0]['specimen_timestamp']))
                 {
                  $timestamp_stage3 = strtotime($specimen_stage_results_stage3[0]['specimen_timestamp']);
                  $formatted_time_stage3= date('Y-m-d h:i:sa',$timestamp_stage3);
                  $original_time_stage3= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));
                  
                  $minutes_stage3 = floor(abs($timestamp_stage3-(strtotime($specimen_data['create_date'])))/ 60);
                  $tot_minutes_stage3+= $minutes_stage3;
                  $avg_minutes_stage3 = floor($tot_minutes_stage3/$count);
                  $hour_stage3 = round(($avg_minutes_stage3/60),1);
                  $day_stage3 = floor($hour_stage3/24);

                  $chk_hour_stage3 = floor(abs(strtotime($specimen_stage_results_stage3[0]['specimen_timestamp'])-strtotime($specimen_data['create_date'])));
                  if($chk_hour_stage3>172800)
                  {
                    $color3 = 'Red';
                    $time3 = dateDiff($formatted_time_stage3,$original_time_stage3);
                  }
                  else 
                  {
                    $color3 = '';
                    $time3 = dateDiff($formatted_time_stage3,$original_time_stage3);
                  } 
                  
                  }  
                  else
                  {
                    $color3 = '';
                    $time3 = "Not Processed";
                  }

                  $specimen_stage_results_stage4 = $this->BlankModel->customquery("SELECT `specimen_timestamp` FROM `wp_abd_specimen_stage_details` WHERE `stage_id` ='4' AND `specimen_id` ='".$specimen_data['id']."'"); 
                  if(!empty($specimen_stage_results_stage4[0]['specimen_timestamp']))
                  {
                  $timestamp_stage4 = strtotime($specimen_stage_results_stage4[0]['specimen_timestamp']);
                  $formatted_time_stage4= date('Y-m-d h:i:sa',$timestamp_stage4);
                  $original_time_stage4= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));

                  $minutes_stage4 = floor(abs($timestamp_stage4-(strtotime($specimen_data['create_date'])))/ 60);
                  $tot_minutes_stage4+= $minutes_stage4;
                  $avg_minutes_stage4 = floor($tot_minutes_stage4/$count);
                  $hour_stage4 = round(($avg_minutes_stage4/60),1);
                  $day_stage4 = floor($hour_stage4/24); 

                  $chk_hour_stage4 = floor(abs(strtotime($specimen_stage_results_stage4[0]['specimen_timestamp'])- strtotime($specimen_data['create_date'])));
                  if($chk_hour_stage4>18000)
                  {
                    $color4 = 'Red';
                    $time4 = dateDiff($formatted_time_stage4,$original_time_stage4); 
                  }
                  else
                  {
                    $color4 = '';
                    $time4 =  dateDiff($formatted_time_stage4,$original_time_stage4); 
                  }
                  }
                  else
                  {
                    $color4 = '';
                    $time4 = "Not Processed";
                  }

                  $pending_sql = $this->BlankModel->customquery("SELECT max(`specimen_timestamp`) AS `max_date` FROM `wp_abd_specimen_stage_details` WHERE `specimen_id` ='".$specimen_data['id']."'");
                  
                    if(!empty($pending_sql[0]['max_date']))
                    {
                      $timestamp_pending = strtotime($pending_sql[0]['max_date']);
                      $formatted_time_pending= date('Y-m-d h:i:sa',$timestamp_pending);
                      $original_time_pending= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));

                      $minutes_pending = floor(abs($timestamp_pending-(strtotime($specimen_data['create_date'])))/ 60);
                      $tot_minutes_pending+= $minutes_pending;
                      $avg_minutes_pending = floor($tot_minutes_pending/$count);
                      $hour_stage_pending = round(($avg_minutes_pending/60),1);
                      $day_stage_pending = floor($hour_stage_pending/24); 

                      $chk_hour_stage5 = floor(abs(strtotime($pending_sql[0]['max_date'])-strtotime($specimen_data['create_date'])));
                      if($chk_hour_stage5>18000)
                      {
                         $color5 = 'Red';
                         $time5 = dateDiff($formatted_time_pending,$original_time_pending); 
                      }
                      else
                      {
                        $color5 = '';
                         $time5 =  dateDiff($formatted_time_pending,$original_time_pending); 
                      }
                        
                    }
                    else
                    {
                      $color5 = '';
                      $time5 = "Not Processed";
                    }

                     $report_sql = $this->BlankModel->customquery("SELECT `create_date` FROM `wp_abd_nail_pathology_report` WHERE `specimen_id` ='".$specimen_data['id']."'");
                    if(!empty($report_sql[0]['create_date']))
                    {
                      $timestamp_report = strtotime($report_sql[0]['create_date']);
                      $formatted_time_report= date('Y-m-d h:i:sa',$timestamp_report);
                      $original_time= date('Y-m-d h:i:sa',strtotime($specimen_data['create_date']));
                      
                      $minutes_stage = floor(abs($timestamp_report-(strtotime($specimen_data['create_date'])))/ 60);
                      $tot_minutes_stage+= $minutes_stage;
                      $avg_minutes_stage = floor($tot_minutes_stage/$count);
                      $hour_stage = round(($avg_minutes_stage/60),1);
                      $day_stage = floor($hour_stage/24); 

                      $chk_hour_stage6 = floor(abs(strtotime($report_sql[0]['create_date'])-strtotime($specimen_data['create_date'])));
                      if($chk_hour_stage6>86400)
                      {
                         $color6 = 'Red';
                         $time6 = dateDiff($formatted_time_report, $original_time); 
                      }
                      else
                      {
                         $color6 = '';
                         $time6 = dateDiff($formatted_time_report, $original_time); 
                      }
                    }
                    else
                    {
                        $color6 = '';
                        $time6 = "Not Processed";
                    }

                $specimen_information =  array('assessioning_num' => $specimen_data['assessioning_num'],'stage1'=>$time,'stage2'=> $time2,
                'stage3'=>$time3,'stage4'=>$time4,'pending'=>$time5,'report'=>$time6,'color1'=>$color1,'color2'=>$color2,'color3'=>$color3,
            'color4'=>$color4,'color5'=>$color5,'color6'=>$color6);
                array_push($details, $specimen_information); 
            }

            if(!empty($day))
            {
                $day1 = $day." Days";
            }
            else{
                $day1= "" ;
            }
            if(!empty($hour))
            {
                $hour1 = $hour." Hours";
            }
            else{
                $hour1= "" ;
            }

            if(!empty($day_stage2))
          {
              $day2 = $day_stage2." Days";
          }
          else{
              $day2= "" ;
          }
          if(!empty($hour_stage2))
          {
              $hour2 = $hour_stage2." Hours";
          }
          else{
              $hour2= "" ;
          }

          if(!empty($day_stage3))
          {
              $day3 = $day_stage3." Days";
          }
          else{
              $day3= "" ;
          }
          if(!empty($hour_stage3))
          {
              $hour3 = $hour_stage3." Hours";
          }
          else{
              $hour3= "" ;
          }
            if(!empty($day_stage4))
            {
                $day4 = $day_stage4." Days";
            }
            else{
                $day4= "" ;
            }
            if(!empty($hour_stage4))
            {
                $hour4 = $hour_stage4." Hours";
            }
            else{
                $hour4= "" ;
            }

              if(!empty($day_stage_pending))
              {
                  $day5 = $day_stage_pending." Days";
              }
              else{
                  $day5= "" ;
              }
              if(!empty($hour_stage_pending))
              {
                  $hour5 = $hour_stage_pending." Hours";
              }
              else{
                  $hour5= "" ;
              }
            if(!empty($day_stage))
              {
                  $day6 = $day_stage." Days";
              }
              else{
                  $day6= "" ;
              }
              if(!empty($hour_stage))
              {
                  $hour6 = $hour_stage." Hours";
              }
              else{
                  $hour6= "" ;
              }
            echo json_encode(array('status'=>'1','details'=> $details,'count'=>$count,'day1'=>$day1,'hour1'=>$hour1,'day2'=>$day2,'hour2'=>$hour2,
            'day3'=>$day3,'hour3'=>$hour3,'day4'=>$day4,'hour4'=>$hour4,'day5'=>$day5,'hour5'=>$hour5,'day6'=>$day6,'hour6'=>$hour6));
        }
        else{
            echo json_encode(array('status'=>'0'));
        }
    }
    

}       


?>