<?php
if(!defined('BASEPATH')) EXIT("No direct script access allowed");

if(!function_exists('common_viewloader')){
  // function common_viewloader($viewfilepath='',$param=array()){
	function common_viewloader($viewfilepath='',$param=array()){
		$CI = &get_instance();
		$CI->load->view('header');
		
		if($viewfilepath!=''){
			$CI->load->view($viewfilepath,$param);
		}
		$CI->load->view('footer');
	}
}

// display function 
if(!function_exists('pr')){
	function pr($display_data=array()){
		if(!empty($display_data)){
			echo "<pre>";
			print_r($display_data);
			echo "</pre>";
		}
	}
}

function short_description($string,$count_value){
		$string = strip_tags($string);
		if (strlen($string) > $count_value) {
		$stringCut = substr($string, 0, $count_value);
		$string = substr($stringCut, 0, strrpos($stringCut, ' ')).' ...'; 
		}
		return $string;
	}

if(!function_exists('get_user_details')){
	function get_user_details($logged_in_id){
		$CI = & get_instance();	
		if(!empty($logged_in_id)){
		$conditions = " ( `sysadm_id` = '".$logged_in_id."' AND `sysadm_status` = 'Active')";
		$select_fields = '*';
		$is_multy_result = 1;
		$admindata = $CI->BlankModel->getTableData('sysadmin', $conditions, $select_fields, $is_multy_result);
		return $admindata;
		}
	}	
}
  
if(!function_exists('image_uploads')){
	
	function image_uploads($folder_name, $thumb_Size_width= '', $thumb_Size_hight= '', $file_name){		
		    $CI = & get_instance();	
		    //file upload destination
            $config['upload_path'] = './assets/uploads/'.$folder_name;
            $config['allowed_types'] = 'jpg|JPG|png|PNG|JPEG|jpeg|mp4';
            $config['max_size']   = '1000000';
  		    $config['max_width']  = '1024000';
  		    $config['max_height'] = '768000';
            $config['max_filename'] = '255';
            $config['encrypt_name'] = TRUE;
            //thumbnail path
            $thumb_path = './assets/uploads/thumb/';
            //  $thumb_path = './assets/uploads/thumb_new/';
            //store file info once uploaded
            $CI->load->library('upload', $config);
            $CI->load->library('image_lib');
            
            $file_data = array();
            //check for errors
            $is_file_error = FALSE;
            //check if file was selected for upload
           if (!$_FILES) {
              $is_file_error = TRUE;
				      $display_error  =  handle_error('Select at least one file.');
            }
          
            //if file was selected then proceed to upload
            if (!$is_file_error) {
                //load the preferences
                //$CI->load->library('upload', $config);
                //check file successfully uploaded. 'file_name' is the name of the input
               
                if (!$CI->upload->do_upload($file_name)) {
                    //if file upload failed then catch the errors
                    $display_error  = handle_error($CI->upload->display_errors());
                    $is_file_error = TRUE;
                 } else {
                	  
                    //store the file info
                    $file_data = $CI->upload->data();
                    if (!is_file($thumb_path . $file_data['file_name'])) {
                        $thumb_config = array(
                            'source_image' => $file_data['full_path'], //get original image
                            'new_image' => $thumb_path,
                            'maintain_ratio' => true,
                            'width' => $thumb_Size_width,
                            'height' => $thumb_Size_hight
                        );
                       // $CI->load->library('image_lib', $config); //load library
                       
                         $CI->image_lib->initialize($thumb_config);
                        $CI->image_lib->resize(); //do whatever specified in config
                    }
                }
            }
            // There were errors, we have to delete the uploaded files
            if ($is_file_error) {
                if ($file_data) {
                    $file = './assets/uploads/'.$folder_name.$file_data['file_name'];
                    if (file_exists($file)) {
                        unlink($file);
                    }
                    $thumb = $thumb_path . $file_data['file_name'];
                    if ($thumb) {
                        unlink($thumb);
                    }
                }
                $message =  $display_error;
            }
		   if (!$is_file_error) {
                $message = $file_data; 
               	} 
		return $message;
	}
	
}

 function file_upload($path='',$fieldname='')
  {
            $CI = &get_instance();
              $config['upload_path']          = $path;
              $config['allowed_types']        = 'gif|jpg|png|jpeg';
              //$config['file_name']            =$filename;
        $config['encrypt_name']         = true;
              $CI->load->library('upload', $config);              
              $CI->upload->do_upload($fieldname);
        $filename = $CI->upload->data();
              return $filename=$filename['file_name'];
  }

  function handle_error($err) {
     	$CI = & get_instance();	
        $error = $err . "\r\n";
        return $error;
    }
    
  if(!function_exists('get_loggedin_details')){
	function get_loggedin_details($logged_in_id){
		$CI = & get_instance();	
		if(!empty($logged_in_id)){
		$conditions = " ( `id` = '".$logged_in_id."' AND `is_deleted` <> '1' AND `is_active` = '1' )";
		$select_fields = '*';
		$is_multy_result = 1;
		$admindata = $CI->BlankModel->getTableData('fans', $conditions, $select_fields, $is_multy_result);
		return $admindata;
		}
	}
  }
 
  if(!function_exists('get_data_anothertable')){
	function get_data_anothertable($id,$tablename,$field_name){
		$CI = & get_instance();	
		if(!empty($id)){
		$conditions = " ( $field_name = '".$id."')";
		$select_fields = '*';
		$is_multy_result = 0;
		$admindata = $CI->BlankModel->getTableData($tablename, $conditions, $select_fields, $is_multy_result);
		return $admindata;
		}
	}
  }   
 
	if(!function_exists('get_nos_rows')){
	function get_nos_rows($tablename,$condition){
		$CI = & get_instance();	
		if(!empty($tablename)){		
		$admindata = $CI->BlankModel->get_row($tablename,$condition);
		return $admindata;
		}
	}
	}
	
	if(!function_exists('get_table_data')){
		function get_table_data($tablename, $conditions){		
			$CI = & get_instance();	
			if(!empty($tablename)){		
			$select_fields = '*';
			$is_multy_result = 1;
			$admindata = $CI->BlankModel->getTableData($tablename, $conditions, $select_fields, $is_multy_result);
			return $admindata;
			}
		}	
	}

  function get_user_role($user_id){
     $CI = & get_instance();	
     $conditions = "`user_id` = '".$user_id."' AND `meta_key` ='wp_abd_capabilities'";
     $sql_get_user_meta = $CI->BlankModel->getTableData('wp_abd_usermeta', $conditions, '*', 1);
     $serialized_data = $sql_get_user_meta['meta_value'];
     $role_array = unserialize($serialized_data);
     $role='';
     foreach($role_array as $key=>$value){
      $role.= $key;
     }
      return $role;
  }	
  
   function get_users_by_role($role, $select_data){
      $CI = & get_instance();	
     
      $get_users_det = $CI->BlankModel->customquery( "SELECT ".$select_data." FROM wp_abd_users INNER JOIN wp_abd_usermeta ON wp_abd_users.ID = wp_abd_usermeta.user_id WHERE wp_abd_usermeta.meta_key = 'wp_abd_capabilities' AND wp_abd_usermeta.meta_value LIKE '%".$role."%' ORDER BY wp_abd_users.user_nicename");
     
      return $get_users_det;
  }	  
     
     
   function get_users_by_role_n_status($role, $select_data, $status){
      $CI = & get_instance();     
      $get_users_det = $CI->BlankModel->customquery("SELECT ".$select_data." 
						FROM wp_abd_users AS u
						LEFT JOIN wp_abd_usermeta AS um1 ON u.ID = um1.user_id
						LEFT JOIN wp_abd_usermeta AS um2 ON u.ID = um2.user_id
						WHERE  um1.meta_key = '_status' AND um1.meta_value = '".$status."'
						AND um2.meta_key = 'wp_abd_capabilities' AND um2.meta_value LIKE '%".$role."%' ORDER BY u.user_nicename ASC");
    
      return $get_users_det;
  }	  
   
   function get_user_meta($user_id, $key, $single = ""){
      $CI = & get_instance();    
      
      if(!empty($user_id)){
      $conditions = "`user_id` = ".$user_id." AND `meta_key` = '".$key."'";
      $select_fields = 'meta_value';
    $is_multy_result = 1;
    $get_users_det = $CI->BlankModel->getTableData('wp_abd_usermeta', $conditions, $select_fields, $is_multy_result);
      return $get_users_det;
  }else {
    return false;
  }
  }

  function get_user_meta_value($user_id, $key, $single = ""){
      $CI = & get_instance();
      if(!empty($user_id)){     
        $conditions = "`user_id` = ".$user_id." AND `meta_key` = '".$key."'";
        $select_fields = 'meta_value';
      $is_multy_result = 1;
      $get_users_det = $CI->BlankModel->getTableData('wp_abd_usermeta', $conditions, $select_fields, $is_multy_result);
        return $get_users_det['meta_value'];
      }else{
        return false;
      }
  }
  
   function get_role($sdata)
   {
     $role_array = unserialize($sdata);
     $role='';
     foreach($role_array as $key=>$value){
     	$role.= $key;
	 }
	 return $role;
   }

   function update_user_meta($user_id, $key, $value){
      $CI = & get_instance();	     
      $update_conditions = "( `user_id` = ".$user_id." AND `meta_key` = '".$key."' )";    
	  $update_data = array('meta_value' => $value);	
	  $update_user_meta = $CI->BlankModel->editTableData('wp_abd_usermeta', $update_data, $update_conditions);     
      return $update_user_meta;
  }

  function add_user_meta($user_id,$key,$value){
      $CI = & get_instance(); 
      $insert_data = array(
        'user_id' => $user_id,
        'meta_key' => $key,
        'meta_value' => $value);  
      $add_user_meta = $CI->BlankModel->addTableData('wp_abd_usermeta', $insert_data);     
      return $add_user_meta;
  }


///////////////////////////////////////////added by me on 21.8.19//////////////
  function get_total_user(){
      $CI = & get_instance(); 
      $get_users_det = $CI->BlankModel->customquery( "SELECT count(*) as totuser FROM wp_abd_users WHERE user_status=0");
    return $get_users_det[0]['totuser'];
  }
  
  function get_latest_user(){
      $CI = & get_instance(); 
      $get_users_det = $CI->BlankModel->customquery( "SELECT * FROM wp_abd_users WHERE user_status=0 ORDER BY user_registered desc LIMIT 5");
    return $get_users_det;
  }  
 
  function get_latest_login(){
      $CI = & get_instance(); 
      $get_users_det = $CI->BlankModel->customquery( "SELECT u1.ID,u1.user_email,m1.meta_value AS firstname,m2.meta_value AS lastlogincount,m3.meta_value AS lastlogin FROM wp_abd_users u1 JOIN wp_abd_usermeta m1 ON (m1.user_id = u1.ID AND m1.meta_key = 'first_name') JOIN wp_abd_usermeta m2 ON (m2.user_id = u1.ID AND m2.meta_key = 'when_last_login_count') JOIN wp_abd_usermeta m3 ON (m3.user_id = u1.ID AND m3.meta_key = 'when_last_login') ORDER BY m3.meta_value DESC LIMIT 5");
    return $get_users_det;
  }
  
  function get_role_specialist($role, $speciality){
  $CI = & get_instance();
   
  $get_users_det = $CI->BlankModel->customquery("SELECT u1.ID,u1.user_email,m1.meta_value AS firstname,m2.meta_value AS speciality,m3.meta_value AS role FROM wp_abd_users u1
  JOIN wp_abd_usermeta m1 ON (m1.user_id = u1.ID AND m1.meta_key = 'first_name')
  JOIN wp_abd_usermeta m2 ON (m2.user_id = u1.ID AND m2.meta_key = 'assign_specialty')
  JOIN wp_abd_usermeta m3 ON (m3.user_id = u1.ID AND m3.meta_key = 'wp_abd_capabilities') 
  WHERE m2.meta_value='".$speciality."' 
  AND m3.meta_value like '%".$role."%' 
  GROUP BY u1.ID");
  
  return $get_users_det;
  }
///////////////////////////////////////////added by me on 21.8.19//////////////

  // function test($type, $)



  function specimen_code_count($code_color){
    $CI = & get_instance();	  
    $specimen_sql =  $CI->BlankModel->customquery("SELECT count(`nail_funagl_id`) as number_of_specimen FROM `wp_abd_nail_pathology_report` WHERE `diagnostic_short_code` = '".$code_color."'");
    $specimen_count = $specimen_sql[0]['number_of_specimen'];
	return $specimen_count;
    }
    
    function specimen_color_code_count($code_color, $specimen_id, $dataType){
      $CI = & get_instance();
      $tbl1=NAIL_PATHOLOGY_REPORT_MAIN;
      if($dataType == 'general'){
        $tbl1=NAIL_PATHOLOGY_REPORT_MAIN;
      }else if($dataType == 'archive'){
        $tbl1=NAIL_PATHOLOGY_REPORT_ARCHIVE;
      }
      $conditions = "`diagnostic_short_code` = '".$code_color."' AND `specimen_id` = '".$specimen_id."'";
      $select_fields = 'COUNT(`nail_funagl_id`) as number_of_specimen';
	  $is_multy_result = 1;
	  $specimen_count = $CI->BlankModel->getTableData($tbl1, $conditions, $select_fields, $is_multy_result);      
      $specimnen_count = $specimen_count['number_of_specimen'];
    
      return $specimnen_count;
     }

    /**
	* Diagnosis Color Code Count
	* 
	* 	
	*/

     function diagnosis_color_code_count($code_color, $data_type, $physician_id, $from_date, $to_date){
      $CI = & get_instance();
      $tbl1=SPECIMEN_MAIN;
      $tbl2=NAIL_PATHOLOGY_REPORT_MAIN;   
      if($data_type == 'general'){
         $tbl1=SPECIMEN_MAIN;
         $tbl2=NAIL_PATHOLOGY_REPORT_MAIN;       
      }
      else if($data_type == 'archive'){
         $tbl1=SPECIMEN_ARCHIVE;
         $tbl2=NAIL_PATHOLOGY_REPORT_ARCHIVE;
         $tbl3=GENERATE_PCR_REPORT_ARCHIVE;
      }
      else{
	  	 $tbl1=SPECIMEN_MAIN;
         $tbl2=NAIL_PATHOLOGY_REPORT_MAIN;   
	  }	
    
    if($physician_id != '') {
	   $physician_sql = " AND `physician_id` = '".$physician_id."'";
	}
	else{
	   $physician_sql = "";
	} 
     
     $histo_report_diagnoses_sql = "SELECT `short_code`.`color`, `specimen`.`id`,`specimen`.`create_date` as specimen_create_date, `nail_report`.`create_date` as nail_create_date
    FROM 
    (SELECT `id`, `create_date` FROM $tbl1 WHERE `qc_check` = '0' ".$physician_sql." AND `physician_accepct` = '0' AND `status` = '0' AND `create_date` BETWEEN '".$from_date." 00:00:00' AND '".$to_date." 23:59:59')specimen 
    INNER JOIN 
    (SELECT `specimen_id`, `create_date`, `diagnostic_short_code` FROM $tbl2)nail_report 
    ON 
    `nail_report`.`specimen_id` = `specimen`.`id`    
   
    INNER JOIN 
    (SELECT `sc`, `color` FROM ".NAIL_MACRO_CODES." WHERE `color` = '$code_color')short_code 
    ON 
    `nail_report`.`diagnostic_short_code` = `short_code`.`sc`";
         
     $histo_report_diagnoses_query = $CI->db->query($histo_report_diagnoses_sql);     
     $diagnoses_count              = $histo_report_diagnoses_query->num_rows();
 	
     return $diagnoses_count;
     }

    /**
	 * 
	 * Postive HISTO PCR Process Check 
	 * @return
	 * 
	 */

    function pos_histo_pcr_process_check($specimen_id, $accessioning_num, $tbl2, $tbl4){
      $CI = & get_instance();	  
     /**
	 * 
	 * @var HISTO Positive Check
	 * 
	 */           
      
      $histo_report_diagnoses_sql = "SELECT `short_code`.`color`, `nail_report`.`specimen_id`
	    FROM     
	    (SELECT `specimen_id`, `diagnostic_short_code` FROM $tbl2 WHERE `specimen_id` = '$specimen_id' )nail_report 
	   INNER JOIN 
	    (SELECT `sc`, `color` FROM ".NAIL_MACRO_CODES." WHERE `color` = 'red')short_code 
	    ON 
	    `nail_report`.`diagnostic_short_code` = `short_code`.`sc`";	

	  $histo_specimens_query   = $CI->db->query($histo_report_diagnoses_sql);     
      $histo_positive          = $histo_specimens_query->row();

      /**
	  * 
	  * @var PCR Positive Check
	  * 
	  */
	 
	  $pcr_positive_sql =  "SELECT `positive_negtaive`, `accessioning_num` FROM `$tbl4` WHERE `accessioning_num` = '$accessioning_num' AND `positive_negtaive` = 'positive' AND ( `target_name` != 'C. albicans_Fn04646233_s1' AND `target_name` != 'C. parapsilosis_Fn04646221_s1' AND `target_name` != 'Xeno_Ac00010014_a1' AND `target_name` != 'mecA_Pa04230908_s1') ";
       
      $pcr_positive_query   = $CI->db->query($pcr_positive_sql);
      $pcr_positive         = $pcr_positive_query->row();	
       
       if($histo_positive && $pcr_positive){
       
	   if(($histo_positive->color == "red") && ($pcr_positive->positive_negtaive == "positive")){	   	
	   return 1; 	
	   }
	  } 
	   else{
	   	return 0;
	   }

	}	

 
 	/**
	 * 
	 * Negative HISTO and PCR Process Check
	 * 
	 * @return
	 */ 
 	 
	function neg_histo_pcr_process_check($specimen_id, $accessioning_num, $tbl2, $tbl4 ){
	 $CI = & get_instance();	  
     /**
	 * 
	 * @var HISTO Negative Check
	 * 
	 */           
      
     $histo_report_diagnoses_sql = "SELECT `short_code`.`color`, `nail_report`.`specimen_id`
	    FROM     
	    (SELECT `specimen_id`, `diagnostic_short_code` FROM $tbl2 WHERE `specimen_id` = '$specimen_id' )nail_report 
	    INNER JOIN 
	    (SELECT `sc`, `color` FROM ".NAIL_MACRO_CODES." WHERE `color` != 'red')short_code 
	    ON 
	    `nail_report`.`diagnostic_short_code` = `short_code`.`sc`";	
	
	  $histo_specimens_query   = $CI->db->query($histo_report_diagnoses_sql);     
      $histo_negative          = $histo_specimens_query->row();

      /**
	  * 
	  * @var PCR Positive Check
	  * 
	  */
	  $pcr_neg = 0;
	  $pcr_positive_sql =  "SELECT `positive_negtaive`, `accessioning_num` FROM `$tbl4` WHERE `accessioning_num` = '$accessioning_num' AND `positive_negtaive` = 'positive' AND ( `target_name` != 'C. albicans_Fn04646233_s1' AND `target_name` != 'C. parapsilosis_Fn04646221_s1' AND `target_name` != 'Xeno_Ac00010014_a1' AND `target_name` != 'mecA_Pa04230908_s1') ";
       
      $pcr_positive_query   = $CI->db->query($pcr_positive_sql);
      $pcr_positive         = $pcr_positive_query->row();	
      
      if(!$pcr_positive){
	  	$pcr_neg = 1;
	  }
       
       if($histo_negative && $pcr_neg == 1){
       
	   if(($histo_negative->color != "red") && ($pcr_neg == 1)){	   	
	   return 1; 	
	    }
	   } 
	   else{
	   	return 0;
	   }
		
	 }	 	 


	/**
	 * 
	 * Histo Pos and PCR Neg Process check
	 * 
	 * @return
	 */ 
 	 
	function histo_pos_pcr_neg_process_check($specimen_id, $accessioning_num, $tbl2, $tbl4 ){
	 $CI = & get_instance();	  
     /**
	 * 
	 * @var HISTO Negative Check
	 * 
	 */           
      
      $histo_report_diagnoses_sql = "SELECT `short_code`.`color`, `nail_report`.`specimen_id`
	    FROM     
	    (SELECT `specimen_id`, `diagnostic_short_code` FROM $tbl2 WHERE `specimen_id` = '$specimen_id' )nail_report 
	   
	   
	    INNER JOIN 
	    (SELECT `sc`, `color` FROM ".NAIL_MACRO_CODES." WHERE `color` = 'red')short_code 
	    ON 
	    `nail_report`.`diagnostic_short_code` = `short_code`.`sc`";	
	
	  $histo_specimens_query   = $CI->db->query($histo_report_diagnoses_sql);     
      $histo_positive          = $histo_specimens_query->row();

      /**
	  * 
	  * @var PCR Positive Check
	  * 
	  */
	  $pcr_neg = 0;
	  $pcr_positive_sql =  "SELECT `positive_negtaive`, `accessioning_num` FROM `$tbl4` WHERE `accessioning_num` = '$accessioning_num' AND `positive_negtaive` = 'positive' AND ( `target_name` != 'C. albicans_Fn04646233_s1' AND `target_name` != 'C. parapsilosis_Fn04646221_s1' AND `target_name` != 'Xeno_Ac00010014_a1' AND `target_name` != 'mecA_Pa04230908_s1') ";
       
      $pcr_positive_query   = $CI->db->query($pcr_positive_sql);
      $pcr_positive         = $pcr_positive_query->row();	
      
      if(!$pcr_positive){
	  	$pcr_neg = 1;
	  }
       
       if($histo_positive && $pcr_neg == 1){
       
	   if(($histo_positive->color == "red" ) && ($pcr_neg == 1)){	   	
	   return 1; 	
	    }
	   } 
	   else{
	   	return 0;
	   }
		
	 }	 	 


    
    /**
	 * 
	 * HISTO Negative and PCR Positive Process Check
	 * 
	 * @return
	 */ 
 	 
	function histo_neg_pcr_pos_process_check($specimen_id, $accessioning_num, $tbl2, $tbl4 ){
	 $CI = & get_instance();	  
     /**
	 * 
	 * @var HISTO Negative Check
	 * 
	 */           
      
      $histo_report_diagnoses_sql = "SELECT `short_code`.`color`, `nail_report`.`specimen_id`
	    FROM     
	    (SELECT `specimen_id`, `diagnostic_short_code` FROM $tbl2 WHERE `specimen_id` = '$specimen_id' )nail_report 
	   
	   
	    INNER JOIN 
	    (SELECT `sc`, `color` FROM ".NAIL_MACRO_CODES." WHERE `color` != 'red')short_code 
	    ON 
	    `nail_report`.`diagnostic_short_code` = `short_code`.`sc`";	
	
	  $histo_specimens_query   = $CI->db->query($histo_report_diagnoses_sql);     
      $histo_negative          = $histo_specimens_query->row();

       /**
	  * 
	  * @var PCR Positive Check
	  * 
	  */
	 
	  $pcr_positive_sql =  "SELECT `positive_negtaive`, `accessioning_num` FROM `$tbl4` WHERE `accessioning_num` = '$accessioning_num' AND `positive_negtaive` = 'positive' AND ( `target_name` != 'C. albicans_Fn04646233_s1' AND `target_name` != 'C. parapsilosis_Fn04646221_s1' AND `target_name` != 'Xeno_Ac00010014_a1' AND `target_name` != 'mecA_Pa04230908_s1') ";
       
      $pcr_positive_query   = $CI->db->query($pcr_positive_sql);
      $pcr_positive         = $pcr_positive_query->row();	
  
       if($histo_negative && $pcr_positive){
       
	   if(($histo_negative->color != "red") &&  ($pcr_positive->positive_negtaive == "positive")){	   	
	      return 1; 	
	    }
	   } 
	   else{
	      return 0;
	   }
		
	 }



    function dateDiff($time1, $time2, $precision = 6) {
    // If not numeric then convert texts to unix timestamps
    if (!is_int($time1)) {
        $time1 = strtotime($time1);
    }
    if (!is_int($time2)) {
        $time2 = strtotime($time2);
    }

    // If time1 is bigger than time2
    // Then swap time1 and time2
    if ($time1 > $time2) {
        $ttime = $time1;
        $time1 = $time2;
        $time2 = $ttime;
    }

    // Set up intervals and diffs arrays
    $intervals = array('year','month','day','hour','minute');
    $diffs = array();

    // Loop thru all intervals
    foreach ($intervals as $interval) {
        // Create temp time from time1 and interval
        $ttime = strtotime('+1 ' . $interval, $time1);
        // Set initial values
        $add = 1;
        $looped = 0;
        // Loop until temp time is smaller than time2
        while ($time2 >= $ttime) {
        // Create new temp time from time1 and interval
        $add++;
        $ttime = strtotime("+" . $add . " " . $interval, $time1);
        $looped++;
        }
    
        $time1 = strtotime("+" . $looped . " " . $interval, $time1);
        $diffs[$interval] = $looped;
    }
    
    $count = 0;
    $times = array();
    // Loop thru all diffs
    foreach ($diffs as $interval => $value) {
        // Break if we have needed precission
        if ($count >= $precision) {
        break;
        }
        // Add value and interval 
        // if value is bigger than 0
        if ($value > 0) {
        // Add s if value is not 1
        if ($value != 1) {
            $interval .= "s";
        }
        // Add value and interval to times array
        $times[] = $value . " " . $interval;
        $count++;
        }
    }

    // Return string with times
    return implode(", ", $times);
    } 
    
    function get_stains_desc($info)
    {
        $CI = & get_instance();
        $result =  $CI->BlankModel->customquery("select `text` from `wp_abd_nail_macro_codes` where sc = '".$info."'" );  
        return $result[0]['text'];
    }

    function get_new_stains_desc($info)
    {
        $CI = & get_instance();
        $result =  $CI->db->query("select `text` from `wp_abd_nail_macro_codes` where sc = '".$info."'" )->row_array();  
        return $result;
    }

	function in_array_r($needle, $haystack, $strict = false) {
    foreach ($haystack as $item) {
        if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
            return true;
        }
    }

    return false;
    }



  function mailBody($bodypart){
    $data='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    </head>
    <body>
    <table width="600" border="0" cellspacing="1" cellpadding="3" align="center" style="border:1px solid #d6d6d6; font:normal 12px/16px Arial, Helvetica, sans-serif; color:#818181;">
       <tr>
      <td align="center" valign="top" style="height:120px; border-bottom:3px solid #eeefef;"><img src="https://www.abilitydiagnostics.com/abadmin/assets/dist/img/logo.png" alt="Ability Diagnostics" /></td>
      </tr>
      <tr>
      <td align="left" valign="top" style="padding:10px 20px 20px 20px; color:#4b4b4b;">
        <table width="100%" border="0" cellspacing="2" cellpadding="0">
          <tr>
          <td align="left" valign="top">'.$bodypart.'</td>
          </tr>
        </table>
      </td>
      </tr>
      <tr>
      <td align="center" valign="middle" style="height:50px; background-color:#eaf6e2; color:#4b4b4b">Copyright &copy; '.date("Y").' Ability Diagnostics, All Rights Reserved.</td>
      </tr>
    </table>
    </body>
    </html>';
    
    return $data;
  }

  function sendMail($to="", $subject="", $body="",$from="",$fromname="",$type="",$replyto="",$bcc="",$cc=""){
    if(empty($type))
    {
      $type="html";
    }
    if($type=="plain")
    {
      $body = strip_tags($body);
    }
    if($type=="html")
    {
      $body = "<font face='Verdana, Arial, Helvetica, sans-serif'>".$body."</font>";
    }
    /* To send HTML mail*/ 
    $headers = "MIME-Version: 1.0\r\n"; 
    $headers.= "Content-type: text/".$type."; charset=utf-8 \r\n";
    /* additional headers */ 
    //$headers .= "To: <".$to.">\r\n"; 
    if(!empty($from))
    {
      $headers .= "From: ".$fromname." <".$from.">\r\n";
    }
    if(!empty($replyto))
    {
      $headers .= "Reply-To: <".$replyto.">\r\n"; 
    }
    if(!empty($cc))
    {
      $headers .= "Cc: ".$cc."\r\n";
    }
    if(!empty($bcc))
    {
      $headers .= "Bcc: ".$bcc."\r\n";
    }
    if(@mail($to, $subject, $body, $headers))
    {
      return 1;
    }
    else
    {
      return $headers;
    }
  }



  /**
	* 
	* The function returns the no. of business days between two dates and it skips the holidays
	* 
	*/
    
    function getWorkingDays($startDate,$endDate,$holidays){
    // do strtotime calculations just once
    $endDate = strtotime($endDate);
    $startDate = strtotime($startDate);


    //The total number of days between the two dates. We compute the no. of seconds and divide it to 60*60*24
    //We add one to inlude both dates in the interval.
    $days = ($endDate - $startDate) / 86400 + 1;

    $no_full_weeks = floor($days / 7);
    $no_remaining_days = fmod($days, 7);

    //It will return 1 if it's Monday,.. ,7 for Sunday
    $the_first_day_of_week = date("N", $startDate);
    $the_last_day_of_week = date("N", $endDate);

    //---->The two can be equal in leap years when february has 29 days, the equal sign is added here
    //In the first case the whole interval is within a week, in the second case the interval falls in two weeks.
    if ($the_first_day_of_week <= $the_last_day_of_week) {
        if ($the_first_day_of_week <= 6 && 6 <= $the_last_day_of_week) $no_remaining_days--;
        if ($the_first_day_of_week <= 7 && 7 <= $the_last_day_of_week) $no_remaining_days--;
    }
    else {
        // (edit by Tokes to fix an edge case where the start day was a Sunday
        // and the end day was NOT a Saturday)

        // the day of the week for start is later than the day of the week for end
        if ($the_first_day_of_week == 7) {
            // if the start date is a Sunday, then we definitely subtract 1 day
            $no_remaining_days--;

            if ($the_last_day_of_week == 6) {
                // if the end date is a Saturday, then we subtract another day
                $no_remaining_days--;
            }
        }
        else {
            // the start date was a Saturday (or earlier), and the end date was (Mon..Fri)
            // so we skip an entire weekend and subtract 2 days
            $no_remaining_days -= 2;
        }
    }

    //The no. of business days is: (number of weeks between the two dates) * (5 working days) + the remainder
//---->february in none leap years gave a remainder of 0 but still calculated weekends between first and last day, this is one way to fix it
   $workingDays = $no_full_weeks * 5;
    if ($no_remaining_days > 0 )
    {
      $workingDays += $no_remaining_days;
    }

    //We subtract the holidays
    foreach($holidays as $holiday){
            $time_stamp=strtotime($holiday);
            //If the holiday doesn't fall in weekend
            if ($startDate <= $time_stamp && $time_stamp <= $endDate && date("N",$time_stamp) != 6 && date("N",$time_stamp) != 7)
                $workingDays--;
        }

        return $workingDays;
    }

   function getDatesFromRange($start, $end, $format = 'Y-m-d') {
        $array = array();
        $interval = new DateInterval('P1D');

        $realEnd = new DateTime($end);
        $realEnd->add($interval);

        $period = new DatePeriod(new DateTime($start), $interval, $realEnd);

        foreach($period as $date) { 
            $array[] = $date->format($format); 
        }

        return $array;
    }



function check_in_range($start_date, $end_date, $date_from_user)
{
  // Convert to timestamp
  $start_ts = strtotime($start_date);
  $end_ts = strtotime($end_date);
  $user_ts = strtotime($date_from_user);

  // Check that user date is between start & end
  return (($user_ts >= $start_ts) && ($user_ts <= $end_ts));
}






    function sum_arr($array1, $array2, $combine) {
        $array = array();
        foreach($combine as $index => $value) {
            $array[$index] = isset($array2[$index]) ? $array2[$index] + $value : $value;
        }
        return $array;
    }
        /*
    |--------------------------------------------------------------------------
    | curl Add User Table
    |--------------------------------------------------------------------   
    */


    function add_table_data_curl_users($table,$table_data,$data)
    {
          $CI = &get_instance();
          $api_url = $CI->config->item('api_url').'AbilityCurl/curl_add_table_users';
           $objarray=array("table"=>$table,
                  "data" =>$table_data,
                  "details" =>$data
                );
           // echo json_encode($objarray);die;
           $ch = curl_init();
           curl_setopt($ch, CURLOPT_URL, $api_url);
           curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
           curl_setopt($ch, CURLOPT_HEADER, FALSE);
           curl_setopt($ch, CURLOPT_POST,TRUE);
           curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($objarray));
           curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
           $response = curl_exec($ch);
           curl_close($ch);
           $result=json_decode($response,true);
           //print_r($result);die;
           return $result['status'];
  } 
    /*
    |--------------------------------------------------------------------------
    | curl Edit User Table
    |--------------------------------------------------------------------   
    */

      function edit_table_data_curl_users($table,$data)
      {

         $CI = &get_instance();
       $api_url = $CI->config->item('api_url').'AbilityCurl/curl_edit_table_users';
           $objarray=array("table"=>$table,
                  "data" =>$data,
                );
           //echo json_encode($objarray);die;
           $ch = curl_init();
           curl_setopt($ch, CURLOPT_URL, $api_url);
           curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
           curl_setopt($ch, CURLOPT_HEADER, FALSE);
           curl_setopt($ch, CURLOPT_POST,TRUE);
           curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($objarray));
           curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
           $response = curl_exec($ch);
           curl_close($ch);
           $result=json_decode($response,true);

           //print_r($result);die;
           return $result['status'];
    } 




?>