import { Injectable, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { UserserviceService } from './userservice.service';

const MINUTES_UNITL_AUTO_LOGOUT = 240 // in Minutes
const CHECK_INTERVALL = 1000 // in ms
const STORE_KEY = 'lastAction';
@Injectable({
  providedIn: 'root'
})
export class AutoLogoutService {
  deleteArray:any = [];
  message = false;
  loggedIn: boolean=true;
  constructor(
    private router: Router,
    private ngZone: NgZone,
    public itemsService:UserserviceService
  ) {
    this.check();
    this.initListener();
    this.initInterval();
  }

  get lastAction() {
    return parseInt(localStorage.getItem(STORE_KEY));
  }
  set lastAction(value) {
    localStorage.setItem(STORE_KEY,value.toString());
  }

  initListener() {
    this.ngZone.runOutsideAngular(() => {
      document.body.addEventListener('click', () => this.reset());
    });
  }

  initInterval() {
    this.ngZone.runOutsideAngular(() => {
      setInterval(() => {
        this.check();
      }, CHECK_INTERVALL);
    })
  }

  reset() {
    this.lastAction = Date.now();
  }

  check() {
    const now = Date.now();
    const timeleft = this.lastAction + MINUTES_UNITL_AUTO_LOGOUT * 60 * 1000;
    const diff = timeleft - now;
    const isTimeout = diff < 0;

    this.ngZone.run(() => {
      if (isTimeout && localStorage.loggedIn) {
        console.log(`Sie wurden automatisch nach ${MINUTES_UNITL_AUTO_LOGOUT} Minuten Inaktivität ausgeloggt.`);
        this.itemsService.logout(localStorage.userToken).subscribe(data => {
          this.deleteArray = data;
          localStorage.removeItem('userToken');
          localStorage.removeItem('loggedIn');
          localStorage.removeItem('userid');
          localStorage.removeItem('user_role');
          localStorage.removeItem('acc_cntrl');
          localStorage.removeItem('acc_cntrl_qc');
          if(this.deleteArray.status ==1)
          {
            this.message = true;
            this.loggedIn=true;
            this.router.navigate(["/home"]);
          }
          });
      }
    });
  }
}