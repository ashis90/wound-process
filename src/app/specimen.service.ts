import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { Headers, RequestOptions } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SpecimenService {

  headers: Headers = new Headers;
  options: any;
  constructor(private http: HttpClient) {
    this.headers.append('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    this.headers.append('Access-Control-Allow-Origin', '*');
    this.headers.append('Accept', 'application/json');
    this.options = new RequestOptions({ headers: this.headers });
  }


  public url = environment.baseUrl + 'Specimens/';
  public stage_url = environment.baseUrl + 'SpecimenStages/';



  makeSpecimenRequest() {
    return this.http.get(this.url + 'specimens_list', this.options);

  }
  makeArchiveSpecimenRequest() {
    return this.http.get(this.url + 'archive_specimens_list', this.options);

  }
  makeSuspendSpecimenRequest() {
    return this.http.get(this.url + 'suspend_specimens_list', this.options);

  }
  physicians_list() {
    return this.http.get(this.url + 'physicians_list', this.options);

  }
  search(data) {
    return this.http.post(this.url + 'search_sus', JSON.stringify(data), this.options);

  }
  search_specimen(data) {
    return this.http.post(this.url + 'search_specimen', JSON.stringify(data), this.options);

  }
  loadAllRequest() {
    return this.http.get(this.url + 'load_all_specimens', this.options);
  }
  makeQcSpecimenRequest() {
    return this.http.get(this.url + 'specimens_qclist', this.options);

  }
  search_qcspecimen(data) {
    return this.http.post(this.url + 'search_qcspecimen', JSON.stringify(data), this.options);

  }
  add_specimen(data) {
    return this.http.post(this.url + 'add_specimen', data, this.options);

  }

  specimen_check(data) {
    return this.http.post(this.url + 'specimen_exits_check', data, this.options);

  }
  makeCancelledSpecimenRequest() {
    return this.http.get(this.url + 'cancelled_specimens_list', this.options);

  }
  search_cancelled_specimen(data) {
    return this.http.post(this.url + 'search_cancelled_specimen', JSON.stringify(data), this.options);

  }
  get_bar_code(data) {
    return this.http.post(this.url + 'generate_barcode', JSON.stringify(data), this.options);
  }
  makeSpecimenStageRequest(data) {
    return this.http.post(this.url + 'specimen_stage_list', JSON.stringify(data), this.options);

  }

  makeSpecimenStageRequestByStageId(data) {
    return this.http.post(this.url + 'specimen_stage_list_by_id', JSON.stringify(data), this.options);

  }
  search_specimen_stages(data) {
    return this.http.post(this.url + 'search_stages', JSON.stringify(data), this.options);

  }
  search_specimen_stages_by_stage_id(data) {
    return this.http.post(this.url + 'search_stages_by_id', JSON.stringify(data), this.options);

  }
  makelastFiveSpecimenStage() {
    return this.http.get(this.url + 'last_five_stage_list', this.options);

  }
  makeSpecimenViewRequest(data) {
    return this.http.post(this.url + 'view_specimen', JSON.stringify(data), this.options);
  }
  makeStageViewRequest(data) {
    return this.http.post(this.stage_url + 'view_stages', JSON.stringify(data), this.options);
  }

  makeSpecimenNext(id, stageid) {
    return this.http.post(this.stage_url + 'specimen_next', JSON.stringify({ id, stageid }), this.options);
  }
  searchSpecimenStages(data) {
    return this.http.post(this.stage_url + 'specimen_next_stage_search', JSON.stringify(data), this.options);
  }
  updateStageDetails(data) {
    return this.http.post(this.stage_url + 'specimen_stage_update', JSON.stringify(data), this.options);
  }
  edit_specimen(data) {
    return this.http.post(this.url + 'edit_specimen', data, this.options);

  }

  getPhisicianDetails(id) {
    return this.http.post(this.url + 'get_physician_details', JSON.stringify(id), this.options);
  }

  makeSpecimenPdf() {
    return this.http.post(this.url + 'print_add_specimen_pdf', '');
  }
  sendToLab(data) {
    return this.http.post(this.url + 'qc_checklist_edit', data, this.options);
  }

  getAllTestTypes() {
    return this.http.get(this.url + 'get_all_test_types', this.options);
  }
  loadMoreRequest(data) {
    return this.http.post(this.url + 'specimen_stage_list', JSON.stringify(data), this.options);
  }
  loadMoreRequest_stage_details(data) {
    return this.http.post(this.url + 'specimen_stage_list_by_id', JSON.stringify(data), this.options);
  }
  getExeclExportData(data) {
    return this.http.post(this.url + 'get_execl_export_data', JSON.stringify(data), this.options);
  }
  getExeclExportData_MedUSA(data) {
    return this.http.post(this.url + 'get_execl_export_data_MedUSA', JSON.stringify(data), this.options);
  }
  importSpecimenData(data) {
    return this.http.post(this.url + 'import_specimen_data', JSON.stringify(data), this.options);
  }
  getPartnersCompanyDetails() {
    return this.http.get(this.url + 'get_partners_company_details', this.options);
  }

  getStageTATDetails() {
    return this.http.get(this.url + 'specimenStagesTatNotification', this.options);

  }
}
