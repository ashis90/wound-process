import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServicedetComponent } from './servicedet.component';

describe('ServicedetComponent', () => {
  let component: ServicedetComponent;
  let fixture: ComponentFixture<ServicedetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServicedetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServicedetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
