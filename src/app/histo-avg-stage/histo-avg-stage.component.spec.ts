import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoAvgStageComponent } from './histo-avg-stage.component';

describe('HistoAvgStageComponent', () => {
  let component: HistoAvgStageComponent;
  let fixture: ComponentFixture<HistoAvgStageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoAvgStageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoAvgStageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
