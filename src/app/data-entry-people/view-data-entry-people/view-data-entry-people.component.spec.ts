import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewDataEntryPeopleComponent } from './view-data-entry-people.component';

describe('ViewDataEntryPeopleComponent', () => {
  let component: ViewDataEntryPeopleComponent;
  let fixture: ComponentFixture<ViewDataEntryPeopleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewDataEntryPeopleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewDataEntryPeopleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
