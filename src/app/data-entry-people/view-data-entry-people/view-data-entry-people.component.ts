import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import { HttpClient } from '@angular/common/http';
import { DataEntryPeopleService } from '../../data-entry-people.service';
import { Router,ActivatedRoute, NavigationEnd } from '@angular/router';
import { FormGroup, Validators, FormArray,FormControl, FormBuilder } from '@angular/forms';
import {map, startWith} from 'rxjs/operators'; //add this
import { FieldErrorDisplayComponent } from '../../field-error-display/field-error-display.component';
import { AbstractControl } from '@angular/forms';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'cm-view-data-entry-people',
  templateUrl: './view-data-entry-people.component.html',
  styleUrls: ['./view-data-entry-people.component.css']
})
export class ViewDataEntryPeopleComponent implements OnInit {
  public data : any;
  addDataEntry: FormGroup;
  public loading = false;
  postsArray:any = [] ;
  load_status:boolean = true;
  user_count:any;
  no_data:any;
  no_data_status= false;
  constructor( private http:HttpClient, 
    public itemsService:DataEntryPeopleService,
    public route:Router, 
    private fb:FormBuilder,
    private _ActivatedRoute:ActivatedRoute,
    private spinner: NgxSpinnerService
    ){

     }

  ngOnInit() {
    var id=this._ActivatedRoute.snapshot.paramMap.get('id');
    this.getDataEntryPeopleById(id);
  }

  getDataEntryPeopleById(id:any){
    let params;
    params = {'user_id':id};
    this.spinner.show();
    this.itemsService.getDataEntryPeopleById(params).subscribe(data => { 
      this.postsArray = data;
      this.data = this.postsArray.data_entry_oparator[0];
      this.user_count = this.postsArray.list_count;
      if(this.postsArray['status'] === '1'){
        this.spinner.hide();
      }
      if(this.postsArray['status'] === '0'){
        this.no_data_status= true;
        this.no_data="No Data Found.";
        this.spinner.hide();
      }
    });
  }

}
