import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import { HttpClient } from '@angular/common/http';
import { DataEntryPeopleService } from '../../data-entry-people.service';
import { Router,ActivatedRoute, NavigationEnd } from '@angular/router';
import { FormGroup, Validators, FormArray,FormControl, FormBuilder } from '@angular/forms';
import {map, startWith} from 'rxjs/operators'; //add this
import { FieldErrorDisplayComponent } from '../../field-error-display/field-error-display.component';
import { AbstractControl } from '@angular/forms';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'cm-data-entry-people-list',
  templateUrl: './data-entry-people-list.component.html',
  styleUrls: ['./data-entry-people-list.component.css']
})
export class DataEntryPeopleListComponent implements OnInit {
  public data : any;
  addDataEntry: FormGroup;
  public loading = false;
  postsArray:any = [] ;
  load_status:boolean = true;
  user_count:any;
  no_data:any;
  no_data_status= false;
  constructor( private http:HttpClient, 
    public itemsService:DataEntryPeopleService,
    public route:Router, 
    private fb:FormBuilder,
    private _ActivatedRoute:ActivatedRoute,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService
    ){

     }

  ngOnInit() {
    this.loading = true;
    this.getAllDataEntryPeople();
  }

  getAllDataEntryPeople(){
    this.spinner.show();
    this.itemsService.getDataEntryPeople().subscribe(data => { 
      this.postsArray = data;
      this.data = this.postsArray.data_entry_oparator;
      this.user_count = this.postsArray.list_count;
      if(this.postsArray['status'] === '1'){
        this.spinner.hide();
      }
      if(this.postsArray['status'] === '0'){
        this.spinner.hide();
        this.no_data_status= true;
        this.no_data="No Data Found.";
      }
    });
  }

  deleteDataEntryPeople(id:any){
    let res = confirm("Want to delete?");
    if(res){
    let params;
    let response;
    params = {'id':id};
    this.itemsService.deleteDataEntryPeople(params).subscribe(data => { 
      response = data;
      if(response['status'] === '1'){
        this.loading = true;
        this.getAllDataEntryPeople();
        this.toastr.info('User deleted successfully.', 'Success', {
          timeOut: 3000
        });
      }
      if(response['status'] === '0'){
        this.toastr.info('Failed to delete user.', 'Error', {
          timeOut: 3000
        });
      }
    });
  }
  }

}
