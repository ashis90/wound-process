import { Component, OnInit } from '@angular/core';
import { DataEntryPeopleService } from '../../data-entry-people.service';
import {Observable} from "rxjs";
import { HttpClient } from '@angular/common/http';
import { Router,ActivatedRoute } from '@angular/router';
import { FormGroup, Validators, FormArray,FormControl, FormBuilder } from '@angular/forms';
import {map, startWith} from 'rxjs/operators'; //add this
import { FieldErrorDisplayComponent } from '../../field-error-display/field-error-display.component';
import { AbstractControl } from '@angular/forms';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { ConfirmPasswordValidator } from '../../confirm-password.validator';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'cm-add-data-entry-people',
  templateUrl: './add-data-entry-people.component.html',
  styleUrls: ['./add-data-entry-people.component.css']
})
export class AddDataEntryPeopleComponent implements OnInit {
  public data : any;
  list:any = [];
  physicians_list:any = [];
  physicians_list_data:any = [];
  add_specimen:any = [];
  addDataEntry: FormGroup;
  public loading = false;
  sendData;
  postsArray:any = [] ;
  load_status:boolean = true;
  controlAccessValidCheck:boolean=false;
  constructor( private http:HttpClient, 
    public itemsService:DataEntryPeopleService,
    public route:Router, 
    private fb:FormBuilder,
    private _ActivatedRoute:ActivatedRoute,
    private toastr: ToastrService
    ){
      this.addDataEntry = this.fb.group({
        user_id : [''],
        firstName : ['',Validators.required],
        userName : ['',Validators.required],password : ['',[Validators.required,Validators.minLength(8),Validators.pattern(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}/)]],
         
        confirmPassword : [''], 
        phone : ['',Validators.required], 
        address : [''],
        lastName : ['',Validators.required],
        email :['',Validators.required],
        website : [''],
        specimenControlAccess : [''],
        specimenControlAccessQc : [''],
        specimenControlAccessValidCheck:[this.controlAccessValidCheck,Validators.requiredTrue],
        wp_submit : ['Add Details']
      },{
        validator: ConfirmPasswordValidator.MatchPassword
     });
     }

  ngOnInit() {
  }


  specimenControlAccessChange(){
    let count=0;
    console.log(this.addDataEntry);
    let chk1 = this.addDataEntry.get('specimenControlAccess').value;
    let chk2 = this.addDataEntry.get('specimenControlAccessQc').value;
    
    this.controlAccessValidCheck=(chk1 === true || chk2 === true)?true:false;
    this.addDataEntry.controls['specimenControlAccessValidCheck'].setValue(this.controlAccessValidCheck);
    
   
  }

  onSubmit(){
    console.log(this.addDataEntry);
    if(this.addDataEntry.status == "VALID")
        {  
          let fmData = Object.assign({}, this.addDataEntry.value);
          fmData = Object.assign(fmData,{user_id:localStorage.userid});
          fmData = Object.assign(fmData,{specimenControlAccess:fmData.specimenControlAccess?'add':null});
          fmData = Object.assign(fmData,{specimenControlAccessQc:fmData.specimenControlAccessQc?'check':null});
          console.log(fmData);
          this.itemsService.addDataEntryPeople(fmData).subscribe(data => { 
            this.postsArray = data;

            if(this.postsArray['status'] === '2'){
              this.toastr.info('Email exits. Try with new email Id.', 'Error', {
                timeOut: 3000
              });
            }
            if(this.postsArray['status'] === '3'){
              this.toastr.info('Username exits. Try with new username.', 'Error', {
                timeOut: 3000
              });
            }
            if(this.postsArray['status'] === '1'){
              this.toastr.info('User successfully created.', 'Success', {
                timeOut: 3000
              });
              this.addDataEntry.reset();
            }
            if(this.postsArray['status'] === '0'){
              this.toastr.info('Failed to create new user.', 'Success', {
                timeOut: 3000
              });
            }
          });
        }else
        {
          this.validateAllFormFields(this.addDataEntry);
        }
  }

  validateAllFormFields(formGroup: FormGroup) {         //{1}
      Object.keys(formGroup.controls).forEach(field => {  //{2}
        const control = formGroup.get(field);             //{3}
        if (control instanceof FormControl) {             //{4}
          control.markAsTouched({ onlySelf: true });
        } else if (control instanceof FormGroup) {        //{5}
          this.validateAllFormFields(control);            //{6}
        }
      });
    }

  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }

  isFieldValid(field: string) {
    return !this.addDataEntry.get(field).valid && (this.addDataEntry.get(field).touched || this.addDataEntry.get(field).dirty);
  }
}
