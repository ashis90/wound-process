import { Component, OnInit } from '@angular/core';
import { DataEntryPeopleService } from '../../data-entry-people.service';
import {Observable} from "rxjs";
import { HttpClient } from '@angular/common/http';
import { Router,ActivatedRoute } from '@angular/router';
import { FormGroup, Validators, FormArray,FormControl, FormBuilder } from '@angular/forms';
import {map, startWith} from 'rxjs/operators'; //add this
import { FieldErrorDisplayComponent } from '../../field-error-display/field-error-display.component';
import { AbstractControl } from '@angular/forms';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { ConfirmPasswordValidator } from '../../confirm-password.validator';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'cm-edit-data-entry-people',
  templateUrl: './edit-data-entry-people.component.html',
  styleUrls: ['./edit-data-entry-people.component.css']
})
export class EditDataEntryPeopleComponent implements OnInit {
  public data : any;
  list:any = [];
  physicians_list:any = [];
  physicians_list_data:any = [];
  add_specimen:any = [];
  addDataEntry: FormGroup;
  public loading = false;
  sendData;
  postsArray:any = [] ;
  load_status:boolean = true;
  controlAccessValidCheck:boolean=false;
  constructor( private http:HttpClient, 
    public itemsService:DataEntryPeopleService,
    public route:Router, 
    private fb:FormBuilder,
    private _ActivatedRoute:ActivatedRoute,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService
    ){
      this.addDataEntry = this.fb.group({
        request_id:[''],
        user_id : [''],
        firstName : ['',Validators.required],
        userName : ['',Validators.required],
        password : ['',[Validators.minLength(8),Validators.pattern(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}/)]],
        //password : ['',Validators.minLength(5)], 
        confirmPassword : [''], 
        phone : ['',Validators.required], 
        address : [''],
        lastName : ['',Validators.required],
        email :['',Validators.required],
        website : [''],
        specimenControlAccess : [''],
        specimenControlAccessQc : [''],
        specimenControlAccessValidCheck:[this.controlAccessValidCheck,Validators.requiredTrue],
        wp_submit : ['Update Details']
      },{
        validator: ConfirmPasswordValidator.MatchPassword
     });
     }

  ngOnInit() {
    const id= +this._ActivatedRoute.snapshot.paramMap.get('id');
    this.setFormValue(id);
  }
  
  setFormValue(id:any){
    let params;
    let response;
    let userdata;
    params = {'user_id':id};
    this.addDataEntry.controls['user_id'].setValue(localStorage.userid);
    this.addDataEntry.controls['request_id'].setValue(id);
    this.spinner.show();
    this.itemsService.getDataEntryPeopleById(params).subscribe(data => {
    response = data;
    userdata = response.data_entry_oparator;
    this.addDataEntry.controls['firstName'].setValue(userdata[0]['fname']);
    this.addDataEntry.controls['lastName'].setValue(userdata[0]['lname']);
    this.addDataEntry.controls['userName'].setValue(userdata[0]['user_login']);
    this.addDataEntry.controls['phone'].setValue(userdata[0]['mob']);
    this.addDataEntry.controls['address'].setValue(userdata[0]['address']);
    this.addDataEntry.controls['email'].setValue(userdata[0]['user_email']);
    this.addDataEntry.controls['website'].setValue(userdata[0]['user_url']);
    this.addDataEntry.controls['specimenControlAccess'].setValue(userdata[0]['acc_cntrl']==='add'?true:false);
    this.addDataEntry.controls['specimenControlAccessQc'].setValue(userdata[0]['acc_cntrl_qc']==='check'?true:false);
    if(userdata[0]['acc_cntrl']==='add' || userdata[0]['acc_cntrl_qc']==='check'){
      this.controlAccessValidCheck = true;
      this.addDataEntry.controls['specimenControlAccessValidCheck'].setValue(this.controlAccessValidCheck);
    }
    this.spinner.hide();
   });
  }

  specimenControlAccessChange(){
    let count=0;
    let chk1 = this.addDataEntry.get('specimenControlAccess').value;
    let chk2 = this.addDataEntry.get('specimenControlAccessQc').value;
    
    this.controlAccessValidCheck=(chk1 === true || chk2 === true)?true:false;
    this.addDataEntry.controls['specimenControlAccessValidCheck'].setValue(this.controlAccessValidCheck);
    
   
  }

  onSubmit(){
    if(this.addDataEntry.controls.phone.status=='INVALID')
    {
      this.toastr.info('Please enter phone no.', 'Success', {
        timeOut: 3000
      });
    }
    if(this.addDataEntry.status == "VALID")
        {  
          const id= +this._ActivatedRoute.snapshot.paramMap.get('id');
          let fmData = Object.assign({}, this.addDataEntry.value);
          fmData = Object.assign(fmData,{user_id:localStorage.userid});
          fmData = Object.assign(fmData,{specimenControlAccess:fmData.specimenControlAccess?'add':null});
          fmData = Object.assign(fmData,{specimenControlAccessQc:fmData.specimenControlAccessQc?'check':null});
          console.log(fmData);
          this.itemsService.editDataEntryPeople(fmData).subscribe(data => { 
            this.postsArray = data;
            if(this.postsArray['status'] === '1'){
              this.setFormValue(id);
              this.toastr.info('User successfully updated.', 'Success', {
                timeOut: 3000
              });
            }
            if(this.postsArray['status'] === '0'){
              this.toastr.info('Failed to update new user.', 'Error', {
                timeOut: 3000
              });
            }
          });
        }else
        {
          this.validateAllFormFields(this.addDataEntry);
        }
  }

  validateAllFormFields(formGroup: FormGroup) {         //{1}
      Object.keys(formGroup.controls).forEach(field => {  //{2}
        const control = formGroup.get(field);             //{3}
        if (control instanceof FormControl) {             //{4}
          control.markAsTouched({ onlySelf: true });
        } else if (control instanceof FormGroup) {        //{5}
          this.validateAllFormFields(control);            //{6}
        }
      });
    }

  

  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }

  isFieldValid(field: string) {
    return !this.addDataEntry.get(field).valid && (this.addDataEntry.get(field).touched || this.addDataEntry.get(field).dirty);
  }
}
