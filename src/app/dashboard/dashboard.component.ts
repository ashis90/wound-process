import { Component, OnInit } from '@angular/core';
import { UserserviceService } from '../userservice.service';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { environment } from '../../environments/environment';
import { FrontendService } from '../frontend.service';
@Component({
  selector: 'cm-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  userArray: any = [];
  postsArray: any = [];
  deleteArray: any = [];
  chkData: any = [];
  message = false;
  serviceDetails: any;
  public url = environment.assetsUrl;
  public home_sknd_blk = environment.assetsUrl + 'assets/frontend/main_images/service_bg.jpg';
  constructor(
    private router: Router,
    public itemsService: UserserviceService,
    public activatedRoute: ActivatedRoute,
    public frontService: FrontendService
  ) { }

  ngOnInit() {

    this.itemsService.chkdashboardRequest(localStorage.userToken).subscribe(data => {
      this.chkData = data;
      if (this.chkData.status == 1) {

        this.itemsService.dashboardRequest(localStorage.userToken).subscribe(data => this.userArray = data);
        this.getuser();
      }
      else {
        this.router.navigate(["/home"]);
      }
    }
    );
    this.servicePageDetails();
  }
  getuser() {
    this.itemsService.makeHttpGetRequest().subscribe(data => this.postsArray = data);

  }

  servicePageDetails() {
    this.frontService.makeServiceRequest().subscribe(data => {
      this.serviceDetails = data
      //console.log(this.serviceDetails[0].service_desc);
    });

  }
}
