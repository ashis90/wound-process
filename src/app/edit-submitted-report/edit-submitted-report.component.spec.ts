import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditSubmittedReportComponent } from './edit-submitted-report.component';

describe('EditSubmittedReportComponent', () => {
  let component: EditSubmittedReportComponent;
  let fixture: ComponentFixture<EditSubmittedReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditSubmittedReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditSubmittedReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
