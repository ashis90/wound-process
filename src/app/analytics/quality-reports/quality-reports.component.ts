import { Component, OnInit } from "@angular/core";
import { AnlyticsService } from "../../anlytics.service";
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { Router, ActivatedRoute } from "@angular/router";
import { BsDatepickerConfig } from "ngx-bootstrap/datepicker";
import { NgxSpinnerService } from "ngx-spinner";
import { FormGroup, Validators, FormArray, FormControl, ValidatorFn, AbstractControl } from '@angular/forms';
import { map, startWith } from "rxjs/operators";
import { ToastrService } from "ngx-toastr";
import * as FusionCharts from "fusioncharts";

export interface StateGroup {
  ids: Number[];
  names: string[];
}

const sizeData = {
  chart: {
    caption: "Sample size Distribution",
    subcaption: "",
    xaxisname: "Max Specimen Dimension",
    yaxisname: "No. of Cases",
    numbersuffix: "",
    theme: "fusion"
  },
  data: [
    {
      label: "0-100",
      value: ""
    },
    {
      label: "101-200",
      value: ""
    },
    {
      label: "201-300",
      value: ""
    },
    {
      label: "301-400",
      value: ""
    },
    {
      label: "401-500",
      value: ""
    },
    {
      label: "501-600",
      value: ""
    },
    {
      label: "601-n",
      value: ""
    }
  ]
};


const infectionData = {
  chart: {
    caption: "Infection Graph",
    subcaption: "",
    xaxisname: "Volume Breakdown",
    yaxisname: "Positive Rate (%)",
    numbersuffix: "",
    theme: "fusion"
  },
  data: [
    {
      label: "0-100",
      value: ""
    },
    {
      label: "101-200",
      value: ""
    },
    {
      label: "201-300",
      value: ""
    },
    {
      label: "301-400",
      value: ""
    },
    {
      label: "401-500",
      value: ""
    },
    {
      label: "501-600",
      value: ""
    },
    {
      label: "601-n",
      value: ""
    }
  ]
};

const schemaUrl = [{
  "name": "Time",
  "type": "date",
  "format": "%Y-%m-%d"
}, {
  "name": "Type",
  "type": "string"
}, {
  "name": "Volumes",
  "type": "number"
}];

const schemaUrlTwo = [{
  "name": "Time",
  "type": "date",
  "format": "%Y-%m-%d"
}, {
  "name": "Type",
  "type": "string"
}, {
  "name": "Breakdowns",
  "type": "number"
}];

@Component({
  selector: "cm-quality-reports",
  templateUrl: "./quality-reports.component.html",
  styleUrls: ["./quality-reports.component.css"]
})
export class QualityReportsComponent implements OnInit {
  searchForm: FormGroup;
  pdfForm: FormGroup;
  datePickerConfig: Partial<BsDatepickerConfig>;
  load_status: boolean = true;
  loading;
  postsArray: any = [];
  histo_data: any = [];
  pcr_data: any = [];
  diagnostics: any=[];
  histo_pos_pcr_neg: any=[];
  histo_neg_pcr_pos: any=[];
  concordance:any;
  mrsa_pos: any=[];
  physicians_list;
  sendData;
  msg = false;
  filteredOptions: Observable<StateGroup[]>;
  allNames: any = [];
  allIds: any = [];
  options: any = [];
  ids: any = [];
  filterVal: any = [];
  breakdown:any;
  data;
  no_data;
  no_row;
  file;
  file_data;
  public stateGroups: StateGroup[] = [{
    ids: [],
    names: []
  }]
  public searchData: StateGroup[] = [{
    ids: [],
    names: []
  }]

  total_combination_specimen:number;
  total_pas_gms_fm_specimen:number;
  total_pas_gms_specimen:number;
  total_pcr_specimen:number;

  sum_accessioned_all_specimen:number;
  sum_accessioned_histo_specimen:number;
  sum_accessioned_pcr_specimen:number;
  sum_reported_all_specimen:number;
  sum_reported_histo_specimen:number;
  sum_reported_pcr_specimen:number;


  width = 500;
  height = 200;
  type = "column2d";
  dataFormat = "json";
  dataSource = sizeData;
  size_dist:any;

  infcWidth = 500;
  infcHeight = 200;
  infcType = "column2d";
  infcDataFormat = "json";
  infcDataSource = infectionData;
  infc_data:any;

  timeSeriesDataSource: any;
  timeSeriesDataSourceSec:any;
  timeSeriesDataSourceThird:any;

  timeSeriesType: string;
  timeSeriesWidth: string;
  timeSeriesHeight: string;
  accessioned_specimen_data:any;
  reporte_specimen_data:any;
  _archiveNote:string;


  constructor( private http: HttpClient, public itemsService: AnlyticsService, public route: Router, public activatedRoute: ActivatedRoute, private spinner: NgxSpinnerService,
    private toastr: ToastrService) {
      this._archiveNote = this.itemsService.archiveNote;
    this.searchForm = new FormGroup({

      "from_date": new FormControl('', Validators.required),
      "to_date": new FormControl('', Validators.required),
      "physician_name": new FormControl(''),
      "physician_id": new FormControl(''),
      "dataType": new FormControl('general', Validators.required)
    });

    this.datePickerConfig = Object.assign(
      {},
      {
        containerClass: "theme-blue",
        dateInputFormat: "YYYY-MM-DD"
      }
    );

    this.timeSeriesType = "timeseries";
    this.timeSeriesWidth = "100%";
    this.timeSeriesHeight = "400";

    // This is the dataSource of the chart
    this.timeSeriesDataSource = {
      chart: {},
      caption: {
        text: "Volumes of Accessioned Specimens"
      },
      subcaption: {
        text: "All / HISTO / PCR Accessionined"
      },
      series: "Type",
      yaxis: [
        {
          plot: "Volumes",
          title: "Volumes",
          format: {
            prefix: ""
          }
        }
      ]
    };
    this.timeSeriesDataSourceSec = {
      chart: {},
      caption: {
        text: "Volumes of Reported Specimens"
      },
      subcaption: {
        text: "All / HISTO / PCR Reported"
      },
      series: "Type",
      yaxis: [
        {
          plot: "Volumes",
          title: "Volumes",
          format: {
            prefix: ""
          }
        }
      ]
    };

    this.timeSeriesDataSourceThird = {
      chart: {},
      caption: {
        text: "Breakdown"
      },
      subcaption: {
        text: "Nail Units"
      },
      series: "Type",
      yaxis: [
        {
          plot: "Breakdown",
          title: "Breakdown",
          format: {
            prefix: ""
          }
        }
      ]
    };
  }

  ngOnInit() {
    if (this.load_status == true) {
      this.msg = false;
      this.spinner.show();
      this.itemsService.get_physician_clinical().subscribe(data => {
        this.spinner.hide();
        this.postsArray = data;
        this.physicians_list = this.postsArray.physicians_data;
        this.physicians_list.map((option) => this.allNames.push(option.physician_name));
        this.physicians_list.map((val) => this.allIds.push(val.id));
        this.physicians_list.map((data) => this.filterVal.push(data));
        this.filteredOptions = this.searchForm.get('physician_name').valueChanges.pipe(
          startWith(''),
          map(value => this._filterGroup(value ? value.toLowerCase() : ''))
        );

      });
      this.stateGroups = [{
        ids: this.allIds,
        names: this.allNames
      }];
    }
  }


  convertDate(str: Date) {
    if (!str) {
      var newDate = '';
    } else {

      var date = new Date(str),
        mnth = ("0" + (date.getMonth() + 1)).slice(-2),
        day = ("0" + date.getDate()).slice(-2),
        newDate = [date.getFullYear(), mnth, day].join("-");
    }
    return newDate;
  }

  onSubmit() {

    if (this.searchForm.status == "VALID") {
        this.spinner.show();
        this.load_status = false;
        this.sendData = {
        "from_date": this.convertDate(this.searchForm.value.from_date),
        "to_date": this.convertDate(this.searchForm.value.to_date),
        "physician_id": this.searchForm.value.physician_id,
        'dataType': this.searchForm.value.dataType
      };
      this.itemsService.quality_report(this.sendData).subscribe(data => {
        this.msg = true;
        this.spinner.hide(); 
        this.postsArray = data;    
        this.data = this.postsArray;
        let volumn = this.data.volume;
        let breakdown = this.data.breakdown;
        this.histo_data = this.data.histo_details;
        this.pcr_data   = this.data.pcr_details;
        this.diagnostics = this.data.diagnostics;
        this.histo_pos_pcr_neg = this.data.histo_pos_pcr_neg;
        this.histo_neg_pcr_pos = this.data.histo_neg_pcr_pos;
        this.concordance = this.data.concordance;
        this.mrsa_pos = this.data.mrsa_pos;

        this.size_dist = this.data.size_distribution;
        let size_distribution:any=[];
        for (var label of Object.keys(this.size_dist)) {
          let value = this.size_dist[label]
          size_distribution.push({label, value});
        }
        sizeData.data = size_distribution;
        this.infc_data = this.data.infection;
        let _infc_data:any=[];
        for (var label of Object.keys(this.infc_data)) {
          let value = this.infc_data[label]
          _infc_data.push({label, value});
        }
        infectionData.data = _infc_data;
        this.accessioned_specimen_data =volumn.accessioned_specimen_data;
        this.reporte_specimen_data = volumn.reporte_specimen_data;
        this.breakdown = breakdown.breakdown;
        console.log(this.postsArray);
        this.volumeChartOne();
        this.volumeChartTwo();
        this.breakDownChart();
        this.total_combination_specimen = this.data.total_combination_specimen;
        this.total_pas_gms_fm_specimen = this.data.total_pas_gms_fm_specimen;
        this.total_pas_gms_specimen = this.data.total_pas_gms_specimen;
        this.total_pcr_specimen = this.data.total_pcr_specimen;

        this.sum_accessioned_all_specimen = this.data.sum_accessioned_all_specimen;
        this.sum_accessioned_histo_specimen = this.data.sum_accessioned_histo_specimen;
        this.sum_accessioned_pcr_specimen = this.data.sum_accessioned_pcr_specimen;
        this.sum_reported_all_specimen = this.data.sum_reported_all_specimen;
        this.sum_reported_histo_specimen = this.data.sum_reported_histo_specimen;
        this.sum_reported_pcr_specimen = this.data.sum_reported_pcr_specimen;

        if (this.data.status === '0') {
          this.msg = false;
        }

      });
    } else {
      this.validateAllFormFields(this.searchForm);
    }
  }


  validateAllFormFields(formGroup: FormGroup) {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof FormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }

  resetForm() {
    this.searchForm.reset();
    this.ngOnInit();
  }


  private _filterGroup(value: string): StateGroup[] {
    if (value) {
      let itemCount: number = this.filterVal.length;
      this.options = [];
      this.ids = [];
      for (let i = 0; i < itemCount; i++) {
        if (
          this.filterVal[i].physician_name.toLowerCase().indexOf(value) !== -1
        ) {
          this.options.push(this.filterVal[i].physician_name);
          this.ids.push(this.filterVal[i].id);
        }
      }
      this.searchData = [
        {
          ids: this.ids,
          names: this.options
        }
      ];

      return this.searchData;
    }

    return this.stateGroups;
  }

      // In this method we will create our DataStore and using that we will create a custom DataTable which takes two
  // parameters, one is data another is schema.

  volumeChartOne() {
    var jsonify = res => res.json();
    var dataFetch = this.accessioned_specimen_data;
    var schemaFetch = schemaUrl;

    Promise.all([dataFetch, schemaFetch]).then(res => {
      const data = res[0];
      const schema = res[1];
      
      // First we are creating a DataStore
      const fusionDataStore = new FusionCharts.DataStore();
      // After that we are creating a DataTable by passing our data and schema as arguments
      const fusionTable = fusionDataStore.createDataTable(data, schema);
      // Afet that we simply mutated our timeseries datasource by attaching the above
      // DataTable into its data property.
      this.timeSeriesDataSource.data = fusionTable;
      
    });
  }

  volumeChartTwo() {
    var jsonify = res => res.json();
    var dataFetch = this.reporte_specimen_data;
    var schemaFetch = schemaUrl;

    Promise.all([dataFetch, schemaFetch]).then(res => {
      const data = res[0];
      const schema = res[1];
      
      // First we are creating a DataStore
      const fusionDataStore = new FusionCharts.DataStore();
      // After that we are creating a DataTable by passing our data and schema as arguments
      const fusionTable = fusionDataStore.createDataTable(data, schema);
      // Afet that we simply mutated our timeseries datasource by attaching the above
      // DataTable into its data property.
      this.timeSeriesDataSourceSec.data = fusionTable;
      
    });
  }

  breakDownChart() {
    var jsonify = res => res.json();
    var dataFetch = this.breakdown;
    var schemaFetch = schemaUrlTwo;

    Promise.all([dataFetch, schemaFetch]).then(res => {
      const data = res[0];
      const schema = res[1];
      
      // First we are creating a DataStore
      const fusionDataStore = new FusionCharts.DataStore();
      // After that we are creating a DataTable by passing our data and schema as arguments
      const fusionTable = fusionDataStore.createDataTable(data, schema);
      // Afet that we simply mutated our timeseries datasource by attaching the above
      // DataTable into its data property.
      this.timeSeriesDataSourceThird.data = fusionTable;
      
    });
  }


  callSomeFunction(phy_id: any) {
    this.searchForm.controls['physician_id'].setValue(phy_id);
  }

  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }

  isFieldValid(field: string) {
    return !this.searchForm.get(field).valid && (this.searchForm.get(field).touched || this.searchForm.get(field).dirty);
  }

  onEnter(evt: any) {
    this.searchForm.controls['physician_id'].setValue('');
  }
}
