import { NgModule } from '@angular/core';

import { LabdataRoutingModule } from './labdata-routing.module';
import { NavbarModule } from '../../core/navbar/navbar.module';
@NgModule({
  imports:      [ LabdataRoutingModule,NavbarModule ],
  declarations: [ LabdataRoutingModule.components ]
})
export class LabdataModule { }
