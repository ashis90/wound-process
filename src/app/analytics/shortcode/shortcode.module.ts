import { NgModule } from '@angular/core';

import { ShortcodeRoutingModule } from './shortcode-routing.module';
import { NavbarModule } from '../../core/navbar/navbar.module';
@NgModule({
  imports:      [ ShortcodeRoutingModule,NavbarModule ],
  declarations: [ ShortcodeRoutingModule.components ]
})
export class ShortcodeModule { }
