import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import html2canvas from "html2canvas";

import { AnlyticsService } from "../../anlytics.service";

import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
import { FormGroup, Validators, FormArray, FormControl } from "@angular/forms";
import { map, startWith } from "rxjs/operators"; //add this
import { BsDatepickerConfig } from "ngx-bootstrap/datepicker";
import { environment } from "../../../environments/environment";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import { BsModalRef } from "ngx-bootstrap/modal/bs-modal-ref.service";
import { TrackModalComponent } from "../../track-modal/track-modal.component";
import { BsModalService } from "ngx-bootstrap/modal";

@Component({
  selector: "cm-reference-lab-report",
  templateUrl: "./reference-lab-report.component.html",
  styleUrls: ["./reference-lab-report.component.css"]
})
export class ReferenceLabReportComponent implements OnInit {
  public data: any;
  datePickerConfig: Partial<BsDatepickerConfig>;
  postArray: any;
  parrners_data: any;
  searchForm: FormGroup;
  load_status: boolean = true;
  public loading = false;
  no_data: any;
  no_data_status = false;
  sendData;
  pdf_link: any;
  spe_count;
  specimens_list: any = [];
  specimen_data: any = [];
  toal_histo: any;
  toal_pcr: any;
  toal_histo_pcr: any;
  constructor(
    public itemsService: AnlyticsService,
    private spinner: NgxSpinnerService,
    private router:Router,
    private toastr: ToastrService
  ) {
    this.searchForm = new FormGroup({
      from_date: new FormControl(),
      to_date: new FormControl(),
      partners_company: new FormControl(),
      assessioning_num: new FormControl(),
      export_type: new FormControl()
    });

    this.datePickerConfig = Object.assign(
      {},
      {
        containerClass: "theme-blue",
        dateInputFormat: "MM/DD/YYYY"
      }
    );
  }
  resetForm() {
    this.searchForm.reset();
    this.ngOnInit();
    this.toal_histo = "";
    this.toal_pcr = "";
    this.toal_histo_pcr = "";
    this.data = "";
  }
  onSubmit() {
    if (this.searchForm.status == "VALID") {
      this.spinner.show();
      this.load_status = false;
      this.no_data_status = true;
      this.sendData = {
        from_date: this.convertDate(this.searchForm.value.from_date),
        to_date: this.convertDate(this.searchForm.value.to_date),
        export_type: this.searchForm.value.export_type,
        assessioning_num: this.searchForm.value.assessioning_num,
        partners_company: this.searchForm.value.partners_company
      };

      this.itemsService
        .getPartnersCompanyListSearch(this.sendData)
        .subscribe(data => {
          this.specimens_list = data;
          if (this.specimens_list.status == "1") {
            this.loading = false;
            this.no_data_status = false;
            this.data = this.specimens_list.specimen_data;
            this.spe_count = this.data.length;
            this.toal_histo = this.specimens_list.total_count_histo;
            this.toal_pcr = this.specimens_list.total_count_pcr;
            this.toal_histo_pcr = this.specimens_list.total_count;
            this.pdf_link = this.specimens_list.pdf_link;
            this.spinner.hide();
          }
          if (this.specimens_list.status == "0") {
            this.no_data_status = true;
            this.no_data = "No Data Found.";
            this.toal_histo = "";
            this.toal_pcr = "";
            this.toal_histo_pcr = "";
            this.data = "";
            this.spinner.hide();
            this.toastr.info("No Data Found","Error",{
              timeOut: 3000
            });
          }
        });
    }
  }

  ngOnInit() {
    this.itemsService.getPartnersCompanyList().subscribe(data => {
      let post_array;
      this.no_data_status = true;
      post_array = data;
      if (post_array.status === "1") {
        this.parrners_data = post_array.parners_detail;
      }
    });
  }
  convertDate(str: Date) {
    if (!str) {
      var newDate = "";
    } else {
      var date = new Date(str),
        mnth = ("0" + (date.getMonth() + 1)).slice(-2),
        day = ("0" + date.getDate()).slice(-2),
        newDate = [mnth, day, date.getFullYear()].join("/");
    }
    return newDate;
  }

  

}
