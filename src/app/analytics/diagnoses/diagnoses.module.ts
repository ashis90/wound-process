import { NgModule } from '@angular/core';

import { DiagnosesRoutingModule } from './diagnoses-routing.module';
import { NavbarModule } from '../../core/navbar/navbar.module';

@NgModule({
  imports:      [ DiagnosesRoutingModule,NavbarModule ],
  declarations: [ DiagnosesRoutingModule.components ]
})
export class DiagnosesModule { }
