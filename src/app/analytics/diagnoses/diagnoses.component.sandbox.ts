import { sandboxOf } from 'angular-playground';
import { DiagnosesComponent } from './diagnoses.component';

export default sandboxOf(DiagnosesComponent)
  .add('Diagnoses Component', {
    template: `<cm-diagnoses></cm-diagnoses>`
  });
