import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DiagnosesComponent } from './diagnoses.component';

const routes: Routes = [
  { path: '', component: DiagnosesComponent }
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class DiagnosesRoutingModule {
  static components = [ DiagnosesComponent ];
}
