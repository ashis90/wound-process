import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PcrComponent } from './pcr.component';

const routes: Routes = [
  { path: '', component: PcrComponent }
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class PcrRoutingModule {
  static components = [ PcrComponent ];
}
