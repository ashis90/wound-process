import { NgModule } from '@angular/core';

import { PcrRoutingModule } from './pcr-routing.module';
import { NavbarModule } from '../../core/navbar/navbar.module';
@NgModule({
  imports:      [ PcrRoutingModule,NavbarModule ],
  declarations: [ PcrRoutingModule.components ]
})
export class PcrModule { }
