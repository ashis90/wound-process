import { NgModule } from '@angular/core';

import { HistoRoutingModule } from './histo-routing.module';
import { NavbarModule } from '../../core/navbar/navbar.module';
@NgModule({
  imports:      [ HistoRoutingModule,NavbarModule ],
  declarations: [ HistoRoutingModule.components ]
})
export class HistoModule { }
