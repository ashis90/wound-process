import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HistoComponent } from './histo.component';

const routes: Routes = [
  { path: '', component: HistoComponent }
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class HistoRoutingModule {
  static components = [ HistoComponent ];
}
