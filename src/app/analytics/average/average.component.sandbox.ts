import { sandboxOf } from 'angular-playground';
import { AverageComponent } from './average.component';

export default sandboxOf(AverageComponent)
  .add('Average Component', {
    template: `<cm-average></cm-average>`
  });
