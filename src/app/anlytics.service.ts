import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { Headers, RequestOptions } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AnlyticsService {

  headers: Headers = new Headers;
  options: any;
  constructor(private http: HttpClient) {
    this.headers.append('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    this.headers.append('Access-Control-Allow-Origin', '*');
    this.headers.append('Accept', 'application/json');
    this.options = new RequestOptions({ headers: this.headers });
  }

  readonly archiveNote: string = '';
  public url = environment.baseUrl + 'Analytics/';
  public track_url = environment.baseUrl + 'Specimen_track/';
  public clinical_url = environment.baseUrl + 'ClinicalReport/';
  public clinical_pdf_url = environment.baseUrl + 'ClinicalReportPdf/';

  getdata() {
    return this.http.get(this.url + 'get_lab_anlytics', this.options);
  }
  search_data(data) {
    return this.http.post(this.url + 'search_get_lab_anlytics', JSON.stringify(data), this.options);
  }
  get_histo_pcr_data() {
    return this.http.get(this.url + 'get_histo_pcr_anlytics', this.options);
  }

  loadAllRequest() {
    return this.http.get(this.url + 'get_all_histo_pcr_anlytics', this.options);
  }
  loadMoreRequest(data) {
    return this.http.post(this.url + 'get_more_histo_pcr_anlytics', JSON.stringify(data), this.options);
  }
  search_histo_pcr(data) {
    return this.http.post(this.url + 'search_all_histo_pcr_anlytics', JSON.stringify(data), this.options);
  }

  get_lab_data() {
    return this.http.get(this.url + 'get_lab_data_anlytics', this.options);
  }

  search_lab_data(data) {
    return this.http.post(this.url + 'search_lab_data_anlytics', JSON.stringify(data), this.options);
  }
  get_short_code_data() {
    return this.http.get(this.url + 'get_physician_active_list', this.options);
  }

  search_short_code_data(data) {
    return this.http.post(this.url + 'search_short_code_anlytics', JSON.stringify(data), this.options);
  }
  get_diagnosis_analytics() {
    return this.http.get(this.url + 'get_diagnosis_list', this.options);
  }
  search_diagnosis_analytics(data) {
    return this.http.post(this.url + 'search_diagnosis', JSON.stringify(data), this.options);
  }

  get_avg_time() {
    return this.http.get(this.url + 'get_avg_time', this.options);
  }
  search_avg_time(data) {
    return this.http.post(this.url + 'search_avg_time', JSON.stringify(data), this.options);
  }

  get_slide_analytics() {
    return this.http.get(this.url + 'get_slide_analytics', this.options);
  }

  search_slide(data) {
    return this.http.post(this.url + 'search_slide_analytics', JSON.stringify(data), this.options);
  }
  get_histo_avg_time() {
    return this.http.get(this.url + 'get_histo_avg_time', this.options);
  }

  get_more_histo_avg_time(data) {
    return this.http.post(this.url + 'get_more_histo_avg_time', JSON.stringify(data), this.options);
  }

  search_histo_avg_time(data) {
    return this.http.post(this.url + 'search_histo_avg_time', JSON.stringify(data), this.options);
  }

  load_last_one_month() {
    return this.http.get(this.url + 'last_one_month', this.options);
  }

  get_pcr_avg_time() {
    return this.http.get(this.url + 'get_pcr_avg_time', this.options);
  }

  get_more_pcr_avg_time(data) {
    return this.http.post(this.url + 'get_more_pcr_avg_time', JSON.stringify(data), this.options);
  }

  search_pcr_avg_time(data) {
    return this.http.post(this.url + 'search_pcr_avg_time', JSON.stringify(data), this.options);
  }
  load_last_one_month_pcr() {
    return this.http.get(this.url + 'pcr_last_one_month', this.options);
  }

  specimen_track(data) {
    return this.http.post(this.track_url + 'track_specimen', JSON.stringify(data), this.options);
  }

  fullspecimen_track(data) {
    return this.http.post(this.track_url + 'track_specimen_add_to_report_generate', JSON.stringify(data), this.options);
  }

  get_physician_clinical() {
    return this.http.get(this.url + 'get_physician_active_list', this.options);
  }

  search_clinical_data(data) {
    return this.http.post(this.clinical_url + 'get_clinical_data', JSON.stringify(data), this.options);
  }

  submit_pdf_data(data) {
    return this.http.post(this.clinical_url + 'clinic_report_generate', data, this.options);
  }

  export_short_code_anlytics(data) {
    return this.http.post(this.url + 'export_short_code_anlytics', JSON.stringify(data), this.options);
  }
  quality_report(data) {
    return this.http.post(this.url + 'quality_report', JSON.stringify(data), this.options);
  }
  getPartnersCompanyList() {
    return this.http.get(this.url + 'reference_lab_report_partners_com_list', this.options);
  }
  getPartnersCompanyListSearch(data) {
    return this.http.post(this.url + 'reference_lab_report_search', JSON.stringify(data), this.options);

  }


}
