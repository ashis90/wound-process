import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignWellComponent } from './assign-well.component';

describe('AssignWellComponent', () => {
  let component: AssignWellComponent;
  let fixture: ComponentFixture<AssignWellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignWellComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignWellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
