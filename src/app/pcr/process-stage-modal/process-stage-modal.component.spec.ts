import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcessStageModalComponent } from './process-stage-modal.component';

describe('ProcessStageModalComponent', () => {
  let component: ProcessStageModalComponent;
  let fixture: ComponentFixture<ProcessStageModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcessStageModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcessStageModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
