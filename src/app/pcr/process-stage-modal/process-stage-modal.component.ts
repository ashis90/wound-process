import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { FormGroup, Validators, FormArray,FormControl, FormBuilder } from '@angular/forms';
import {PcrService } from '../../pcr.service';
import { Router,ActivatedRoute } from '@angular/router';
import { NotificationService } from '../../notification.service';
import { environment } from '../../../environments/environment';
@Component({
  selector: 'cm-process-stage-modal',
  templateUrl: './process-stage-modal.component.html',
  styleUrls: ['./process-stage-modal.component.css']
})
export class ProcessStageModalComponent implements OnInit {
  title;
  failRqForm: FormGroup;
  manualReviewProcessData:FormGroup;
  reviewData:FormGroup;
  extractionProcessData:FormGroup;
  pcrProcessData:FormGroup;
  modalreference:any;
  mouseEnterVal:boolean=false;
  mouseEnterNewVal:boolean=false;
  showDropDown:boolean=false;
  pcr_text = false;
  filter :boolean = false;
  sendData:any;
  showCancel:boolean=true;
  public url = environment.assetsUrl;
  constructor(
    public modalRef: BsModalRef,
    private fb:FormBuilder,
    public itemsService:PcrService,
    public route:Router, 
    public notificationService:NotificationService
  ) 
  { 
    this.failRqForm = this.fb.group({
      lists : ['',Validators.required],
      comment : ['',Validators.required],
      submit_fail_batch : ['Save'],
    });
    

    this.manualReviewProcessData = this.fb.group({
      lists : [''],
      report : ['',Validators.required],
      submit_process_stage : ['Save'],
    });

    this.reviewData = new FormGroup({
      "report" : new FormControl("",[Validators.required]),
      "comment" : new FormControl("",[Validators.required]),
      "internal_comment" : new FormControl(''),
    });

    // this.extractionProcessData = this.fb.group({
    //   report : ['',Validators.required],
    //   submit_process_stage : ['Save'],
    //   addtional_desc:['',Validators.required]
    // });
    this.pcrProcessData = this.fb.group({
      report : ['',Validators.required],
      submit_process_stage : ['Save'],
      addtional_desc:['',Validators.required]
    });

    this.extractionProcessData = new FormGroup({
      "pass" : new FormControl("",[Validators.required]),
      "xvalue" : new FormControl("",[Validators.required]),
      "yvalue" : new FormControl("",[Validators.required]),
      "zvalue" : new FormControl("",[Validators.required]),
      "color" : new FormControl("",[Validators.required]),
      "pieces" : new FormControl("",[Validators.required]),
      "tissue" : new FormControl(),
      //"cassettes" : new FormControl(null,[Validators.required]),
      "pcr" : new FormControl(),
      "pcr_val" : new FormControl()
    });
    this.modalreference = modalRef;
    
  }

  ngOnInit() {
  }
  failRqSubmit(){
    
    if(this.failRqForm.status == "VALID")
    {   
        let serializedForm = this.failRqForm.value;
        let user_id = localStorage.userid;
        let batch_no = this.modalreference.content['batch_data'];
        let formObj = {...serializedForm,user_id,batch_no}; // {name: '', description: ''}
        this.itemsService.submitFailBatch(formObj).subscribe(data => {
          //this.route.navigateByUrl('/manual-review');
          window.location.reload();
        });
    }
  }

  reviewProcessDataSubmit(){
    console.log(this.manualReviewProcessData);
    if(this.manualReviewProcessData.status == "VALID")
    {   
        let serializedForm = this.manualReviewProcessData.value;
        let user_id = localStorage.userid;
        let accessioning_no = this.modalreference.content['accessioning_no'];
        let specimen_id = this.modalreference.content['specimen_id'];
        let formObj = {...serializedForm,user_id,accessioning_no,specimen_id}; // {name: '', description: ''}
        this.itemsService.submitReviewProcessStage(formObj).subscribe(data => {
          
          //this.route.navigateByUrl('/manual-review');
          window.location.reload();
        });
    }
  }

  chooseReport(event){
    if(event.target.value === '0'){
      this.showDropDown=true;
      this.showCancel = true;
      this.manualReviewProcessData.controls['lists'].setValidators(Validators.required);
    }else{
      this.showDropDown=false;
      this.showCancel = false;
      this.manualReviewProcessData.controls['lists'].setErrors(null);
    }
  }

  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }

  isFieldValid(field: string) {
    return !this.failRqForm.get(field).valid && (this.failRqForm.get(field).touched || this.failRqForm.get(field).dirty);
  }

  newDisplayFieldCss(field: string) {
    return {
      'has-error': this.newIsFieldValid(field),
      'has-feedback': this.newIsFieldValid(field)
    };
  }

  newIsFieldValid(field: string) {
    return !this.manualReviewProcessData.get(field).valid && (this.manualReviewProcessData.get(field).touched || this.manualReviewProcessData.get(field).dirty);
  }

  extractionDisplayFieldCss(field: string) {
    return {
      'has-error': this.extractionIsFieldValid(field),
      'has-feedback': this.extractionIsFieldValid(field)
    };
  }

  extractionIsFieldValid(field: string) {
    return !this.extractionProcessData.get(field).valid && (this.extractionProcessData.get(field).touched || this.extractionProcessData.get(field).dirty);
  }
  extractionDataSubmit(){
      //console.log(this.extractionProcessData);
      if(this.extractionProcessData.status == "VALID")
      {   
          let serializedForm = this.extractionProcessData.value;
          let user_id = localStorage.userid;
          let specimen_id = this.modalreference.content['specimen_id'];
          let formObj = {...serializedForm,user_id,specimen_id}; // {name: '', description: ''}
          //console.log(formObj);
          this.itemsService.submitExtractionProcessStage(formObj).subscribe(data => {         
            this.route.navigateByUrl('/pcr-stages');
            window.location.reload();
          });
      }
      else

      {
        this.validateAllFormFields(this.extractionProcessData);
      }
  }

  pcrDisplayFieldCss(field: string) {
    return {
      'has-error': this.pcrIsFieldValid(field),
      'has-feedback': this.pcrIsFieldValid(field)
    };
  }

  pcrIsFieldValid(field: string) {
    return !this.pcrProcessData.get(field).valid && (this.pcrProcessData.get(field).touched || this.pcrProcessData.get(field).dirty);
  }
  pcrDataSubmit(){
      console.log(this.pcrProcessData);
      if(this.pcrProcessData.status == "VALID")
      {   
          let serializedForm = this.pcrProcessData.value;
          let user_id = localStorage.userid;
          let specimen_id = this.modalreference.content['specimen_id'];
          let formObj = {...serializedForm,user_id,specimen_id}; // {name: '', description: ''}
          this.itemsService.submitPcrProcessStage(formObj).subscribe(data => {
            
            //this.route.navigateByUrl('/manual-review');
            window.location.reload();
          });
      }
  }

  read_notification(note_id:any){
    let params;
    params = {'note_id':note_id};
    let post_arr;
    this.notificationService.readNotifications(params).subscribe(data => {
      post_arr = data;
      if(post_arr['status'] === '1'){
        window.location.reload();
      }
    });
  }

  dismiss_notification(note_id:any){
    let params;
    params = {'note_id':note_id,'reader_id':localStorage.userid};
    let post_arr;
    this.notificationService.dismissNotifications(params).subscribe(data => {
      post_arr = data;
      if(post_arr['status'] === '1'){
        window.location.reload();
      }
    });
  }

  validateAllFormFields(formGroup: FormGroup) {         

    Object.keys(formGroup.controls).forEach(field => {  

      const control = formGroup.get(field);             

      if (control instanceof FormControl) {             

        control.markAsTouched({ onlySelf: true });

      } else if (control instanceof FormGroup) {        

        this.validateAllFormFields(control);            

      }

    });

  }

  show_pcr(event)
   {
    this.filter = !this.filter;
   }

   reportReviewSubmit(acc_no)
   {
     if(this.reviewData.status == "VALID")
     {
       this.sendData = {
        "pass": this.reviewData.value.report,
        "acc": acc_no,
        "comment": this.reviewData.value.comment,
        "internal_comment": this.reviewData.value.internal_comment,
        "view_pdf":'no'
       };
       this.itemsService.requeuefail(this.sendData).subscribe( data=> {
        window.location.reload();
       
        }); 

     }
     else
     {
      this.validateAllFormFields(this.reviewData);
     }
   }

   viewCancelReport(acc_no)
   {
     let view_pdf_data;
     let pdf_url;
     if(this.reviewData.status == "VALID")
     {
       this.sendData = {
        "pass": this.reviewData.value.report,
        "acc": acc_no,
        "comment": this.reviewData.value.comment,
        "internal_comment": this.reviewData.value.internal_comment,
        "view_pdf":'yes'
       };
       this.itemsService.requeuefail(this.sendData).subscribe( data=> {
          view_pdf_data = data;
          pdf_url=this.url+'assets/uploads/pcr_report_pdf/'+view_pdf_data['cancel_pdf'];
            window.open(pdf_url, "_blank");
        }); 

     }
     else
     {
      this.validateAllFormFields(this.reviewData);
     }
   }

   reportCss(field: string) {
    return {
      'has-error': this.reportValid(field),
      'has-feedback': this.reportValid(field)
    };
  }

  reportValid(field: string) {
    return !this.reviewData.get(field).valid && (this.reviewData.get(field).touched || this.reviewData.get(field).dirty);
  }


}
