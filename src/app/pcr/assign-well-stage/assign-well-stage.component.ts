import { Component, OnInit, ViewChild } from '@angular/core';
import { PcrService } from '../../pcr.service';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { map, startWith } from 'rxjs/operators'; //add this
import { ToastrService } from 'ngx-toastr';
import { ExcelService } from '../../excel.service';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { TrackModalComponent } from '../../track-modal/track-modal.component';
import { BsModalService } from 'ngx-bootstrap/modal';
import { AnlyticsService } from '../../anlytics.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';

export interface StateGroup {
	ids: Number[];
	names: string[];
}

@Component({
	selector: 'cm-assign-well-stage',
	templateUrl: './assign-well-stage.component.html',
	styleUrls: [ './assign-well-stage.component.css' ]
})
export class AssignWellStageComponent implements OnInit {
	@ViewChild('chk_list') chk;
	datePickerConfig: Partial<BsDatepickerConfig>;
	public data: any;
	specimens_list: any = [];
	specimen_data: any = [];
	physicians_list: any = [];
	searchForm: FormGroup;
	public phy_data: any;
	load_status: boolean = true;
	public loading = false;
	sendData;
	postsArray: any = [];
	no_data: any;
	no_data_status = false;
	show = false;
	physicians_list_data: any = [];
	filteredOptions: Observable<StateGroup[]>;
	allNames: any = [];
	allIds: any = [];
	options: any = [];
	ids: any = [];
	filterVal: any = [];
	modalRef: BsModalRef;
	histo_modal = false;
	pcr_modal = false;
	acc_no = false;
	public stateGroups: StateGroup[] = [
		{
			ids: [],
			names: []
		}
	]; //test
	public searchData: StateGroup[] = [
		{
			ids: [],
			names: []
		}
	];
	toggle: boolean = false;
	constructor(
		private http: HttpClient,
		public itemsService: PcrService,
		public route: Router,
		private toastr: ToastrService,
		private excelService: ExcelService,
		private modalService: BsModalService,
		public analyticsService: AnlyticsService,
		private spinner: NgxSpinnerService
	) {
		this.searchForm = new FormGroup({
			barcode: new FormControl(),
			physician_name: new FormControl(),
			physician_id: new FormControl(),
			assessioning_num: new FormControl(),
			fromdate: new FormControl(),
			todate: new FormControl()
		});

		this.datePickerConfig = Object.assign(
			{},
			{
				containerClass: 'theme-blue',
				dateInputFormat: 'MM/DD/YYYY'
			}
		);
	}

	onSubmit() {
		if (this.searchForm.status == 'VALID') {
			this.spinner.show();
			this.show = true;
			this.loading = true;
			this.load_status = false;
			this.sendData = {
				barcode: this.searchForm.value.barcode,
				assessioning_num: this.searchForm.value.assessioning_num,
				physician: this.searchForm.value.physician_id,
				fromdate: this.convertDate(this.searchForm.value.fromdate),
				todate: this.convertDate(this.searchForm.value.todate)
			};
			this.itemsService.serachDeliveryToExtraction(this.sendData).subscribe((data) => {
				this.loading = false;
				this.specimens_list = data;
				if (this.specimens_list.status == '1') {
					this.data = this.specimens_list.specimen_results;
					this.spinner.hide();
				}
				if (this.specimens_list.status == '0') {
					this.no_data_status = true;
					this.no_data = 'No Data Found.';
					this.spinner.hide();
				}
			});
		} else {
		}
	}

	convertDate(str: Date) {
		if (!str) {
			var newDate = '';
		} else {
			var date = new Date(str),
				mnth = ('0' + (date.getMonth() + 1)).slice(-2),
				day = ('0' + date.getDate()).slice(-2),
				newDate = [ mnth, day, date.getFullYear() ].join('/');
		}
		return newDate;
	}

	resetForm() {
		this.searchForm.reset();
		//this.getdeliverytoextarction();
	}

	ngOnInit() {
		this.loading = true;
		//this.getdeliverytoextarction();
		this.filteredOptions = this.searchForm
			.get('physician_name')
			.valueChanges.pipe(startWith(''), map((value) => this._filterGroup(value.toLowerCase())));
	}

	load_all() {
		this.no_data_status = false;
		this.loading = true;
		this.load_status = false;
		this.itemsService.wellAssignLoadAllRequest().subscribe((data) => {
			this.loading = false;
			if (this.specimens_list.status == '1') {
				this.specimens_list = data;
				this.data = this.specimens_list.specimen_results;
			}
			if (this.specimens_list.status == '0') {
				this.no_data_status = true;
				this.no_data = 'No Data Found.';
			}
		});
	}

	getdeliverytoextarction() {
		this.loading = true;
		this.itemsService.assignWellStage().subscribe((data) => {
			this.specimens_list = data;
			this.loading = false;
			if (this.specimens_list.status == '1') {
				this.no_data_status = false;
				this.data = this.specimens_list.specimen_results;
				this.data.forEach((item) => (item.checked = this.toggle));
				this.physicians_list = this.specimens_list.physicians_data;

				this.physicians_list.map((option) => this.allNames.push(option.physician_name));
				this.physicians_list.map((val) => this.allIds.push(val.id));
				this.physicians_list.map((data) => this.filterVal.push(data));
				console.log(this.data);
			}
			if (this.specimens_list.status == '0') {
				this.no_data_status = true;
				this.no_data = 'No Data Found.';
			}
		});
		this.stateGroups = [
			{
				ids: this.allIds,
				names: this.allNames
			}
		];
	}

	private _filterGroup(value: string): StateGroup[] {
		if (value) {
			let itemCount: number = this.filterVal.length;
			this.options = [];
			this.ids = [];
			for (let i = 0; i < itemCount; i++) {
				if (this.filterVal[i].physician_name.toLowerCase().indexOf(value) === 0) {
					this.options.push(this.filterVal[i].physician_name);
					this.ids.push(this.filterVal[i].id);
				}
			}
			this.searchData = [
				{
					ids: this.ids,
					names: this.options
				}
			];

			return this.searchData;
		}

		return this.stateGroups;
	}

	callSomeFunction(phy_id: any) {
		this.searchForm.controls['physician_id'].setValue(phy_id);
	}

	toggleItem(item) {
		item.checked = !item.checked;
		this.toggle = this.data.every((item) => item.checked);
	}

	toggleAll() {
		this.toggle = !this.toggle;
		this.data.forEach((item) => (item.checked = this.toggle));
	}

	assignStages() {
		let assignArr: any = [];
		let stat: boolean = false;
		this.data.forEach((item, indx) => {
			if (item.assign_status === 'Unassign' && item.checked == true) {
				assignArr.push(item.id);
				stat = true;
			}
		});
		if (stat) {
			if (assignArr.length !== 0) {
				this.loading = true;
				this.itemsService.assignInStage(assignArr).subscribe((data) => {
					if (data['status'] === '1') {
						this.loading = false;
						this.toastr.info('Data Successfully updated', 'Success', {
							timeOut: 3000
						});
						this.route.navigateByUrl('/pcr-stages');
					}
				});
			} else {
				this.toastr.info('Please select data first', 'Error', {
					timeOut: 3000
				});
			}
		} else {
			this.toastr.info('Specimen allready assigned.', 'Error', {
				timeOut: 3000
			});
		}
	}

	unassignStages() {
		let assignArr: any = [];
		let stat: boolean = false;
		this.data.forEach((item, indx) => {
			if (item.assign_status === 'Assign' && item.checked == true) {
				assignArr.push(item.id);
				stat = true;
			}
		});
		if (stat) {
			if (assignArr.length !== 0) {
				this.loading = true;
				this.itemsService.unassignInStage(assignArr).subscribe((data) => {
					if (data['status'] === '1') {
						this.loading = false;
						this.toastr.info('Data Successfully updated', 'Success', {
							timeOut: 3000
						});
						this.route.navigateByUrl('/pcr-stages');
					}
				});
			} else {
				this.toastr.info('Please select data first', 'Error', {
					timeOut: 3000
				});
			}
		} else {
			this.toastr.info('Specimen allready unassigned', 'Error', {
				timeOut: 3000
			});
		}
	}

	assignStage(specimen_id: any) {
		let assignArr: any = [];
		assignArr.push(specimen_id);
		this.spinner.show();
		this.itemsService.assignInStage(assignArr).subscribe((data) => {
			if (data['status'] === '1') {
				this.spinner.hide();
				this.toastr.info('Data Successfully updated', 'Success', {
					timeOut: 3000
				});
				this.route.navigateByUrl('/pcr-stages');
			}
		});
	}

	unassignStage(specimen_id: any) {
		let assignArr: any = [];
		assignArr.push(specimen_id);
		this.spinner.show();
		this.itemsService.unassignInStage(assignArr).subscribe((data) => {
			if (data['status'] === '1') {
				this.spinner.hide();
				this.toastr.info('Data Successfully updated', 'Success', {
					timeOut: 3000
				});
				this.route.navigateByUrl('/pcr-stages');
			}
		});
	}

	exportAsXLSX(): void {
		let data: any = [];
		this.data.forEach((item, indx) => {
			if (item.checked == true) {
				data.push(item);
			}
		});

		if (data.length > 0) {
			this.excelService.exportAsExcelFile(data, 'sample');
		} else {
			this.toastr.info('Please check any specimen', 'Error', {
				timeOut: 3000
			});
		}
	}

	displaytrackmodal(specimen_id: any) {
		this.sendData = { acc_no: specimen_id };
		this.analyticsService.specimen_track(this.sendData).subscribe((data) => {
			this.postsArray = data;
			if (this.postsArray.histo_status == '1') {
				this.acc_no = true;
				this.histo_modal = true;
			} else {
				this.histo_modal = false;
			}
			if (this.postsArray.pcr_status == '1') {
				this.acc_no = true;
				this.pcr_modal = true;
			} else {
				this.pcr_modal = false;
			}
			if (this.postsArray.status == '0') {
				this.acc_no = false;
				this.pcr_modal = false;
				this.histo_modal = false;
			}
			this.modalRef = this.modalService.show(TrackModalComponent, {
				initialState: {
					title: 'track',
					acc_no: this.acc_no,
					postsArray: this.postsArray,
					histo_modal: this.histo_modal,
					pcr_modal: this.pcr_modal
				}
			});
		});
	}
}
