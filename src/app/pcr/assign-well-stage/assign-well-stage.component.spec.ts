import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignWellStageComponent } from './assign-well-stage.component';

describe('AssignWellStageComponent', () => {
  let component: AssignWellStageComponent;
  let fixture: ComponentFixture<AssignWellStageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignWellStageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignWellStageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
