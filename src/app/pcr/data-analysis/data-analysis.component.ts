import { Component, OnInit } from '@angular/core';
import {PcrService } from '../../pcr.service';
import * as XLSX from 'xlsx';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'cm-data-analysis',
  templateUrl: './data-analysis.component.html',
  styleUrls: ['./data-analysis.component.css']
})
export class DataAnalysisComponent implements OnInit {
  arrayBuffer:any;
  file:File;
  excelData: any[][];
  postArray:any;
  analyze_data:any;
  unique_data:any;
  data:any;
  barcode:any;
  analyze_data_count:any;
  wounds_in_house_select:any;
  loading:boolean;
  constructor(public itemsService:PcrService,private toastr: ToastrService,private spinner: NgxSpinnerService) { 
    this.excelData=[];
  }

  ngOnInit() {
      
  }

  incomingfile(event) 
  {
  this.file= event.target.files[0]; 
  }
  incomingWoundsSelect(event) 
  {
  this.wounds_in_house_select = event.target.value;
  }

  Upload() {
    if((this.wounds_in_house_select=='Yes')||(this.wounds_in_house_select=='No')){
    if(this.file){
      this.spinner.show();
      //console.log(this.wounds_in_house_select);
    let fileReader = new FileReader();
    let fileData : any;
    fileReader.readAsArrayBuffer(this.file);
      fileReader.onload = (e) => {
          this.arrayBuffer = fileReader.result;
          var data = new Uint8Array(this.arrayBuffer);
          var arr = new Array();
          for(var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
          var bstr = arr.join("");
          var workbook = XLSX.read(bstr, {type:"binary"});
          var first_sheet_name = workbook.SheetNames[0];
          var worksheet = workbook.Sheets[first_sheet_name];
          fileData = XLSX.utils.sheet_to_json(worksheet,{raw:true});
          if(this.wounds_in_house_select=='No'){
          this.itemsService.dataAnalysisImportData(fileData).subscribe(data => {
            this.spinner.hide();
            this.postArray = data;
            this.data = this.postArray.analyze_data;
            this.unique_data = this.postArray.unique_data;
            this.barcode = this.postArray.barcode['well_position'];
            this.analyze_data_count = this.postArray.analyze_data_count;
            if(data['status']==='1'){
              
              this.toastr.info('Data Successfully imported', 'Success', {
                timeOut: 3000
              });
            }
           }); 
          }
          else if(this.wounds_in_house_select=='Yes'){
            this.itemsService.dataAnalysisImportDataWounsProcess(fileData).subscribe(data => {
              this.spinner.hide();
              if(data['status']==='1'){

                this.postArray = data;
                this.data = this.postArray.analyze_data;
                this.unique_data = this.postArray.unique_data;
                this.barcode = this.postArray.barcode['well_position'];
                this.analyze_data_count = this.postArray.analyze_data_count;
                
                this.toastr.info('Data Successfully imported', 'Success', {
                  timeOut: 3000
                });
              }
              else if(data['status']==='2'){
                
                this.toastr.error('Batch data already exists', 'Error', {
                  timeOut: 3000
                });
              }
             }); 
            }
         
      }
    }else{
      this.toastr.info('Please choose a file.', 'Error', {
        timeOut: 3000
      });
    }
  }else{
    this.toastr.info('Please select wounds in house.', 'Error', {
      timeOut: 3000
    });
  }
      
}

}
