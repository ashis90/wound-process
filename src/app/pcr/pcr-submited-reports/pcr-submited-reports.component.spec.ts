import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PcrSubmitedReportsComponent } from './pcr-submited-reports.component';

describe('PcrSubmitedReportsComponent', () => {
  let component: PcrSubmitedReportsComponent;
  let fixture: ComponentFixture<PcrSubmitedReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PcrSubmitedReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PcrSubmitedReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
