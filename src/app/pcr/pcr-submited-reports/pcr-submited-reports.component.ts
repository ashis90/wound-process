import { Component, OnInit } from '@angular/core';
import {PcrService } from '../../pcr.service';
import {Observable} from "rxjs";
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormArray,FormControl } from '@angular/forms';
import { BsDatepickerConfig } from "ngx-bootstrap/datepicker";
import { BsModalRef } from "ngx-bootstrap/modal/bs-modal-ref.service";
import { BsModalService } from "ngx-bootstrap/modal";
import {map, startWith} from 'rxjs/operators'; //add this
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from '../../../environments/environment';

export interface StateGroup {
  ids: Number[];
  names: string[];
}

@Component({
  selector: 'cm-pcr-submited-reports',
  templateUrl: './pcr-submited-reports.component.html',
  styleUrls: ['./pcr-submited-reports.component.css']
})
export class PcrSubmitedReportsComponent implements OnInit {

  datePickerConfig: Partial<BsDatepickerConfig>;
  public data : any;
  specimens_list:any = [];
  post_array:any;
  batch_review:any;
  searchForm: FormGroup;
  specimen_count:any;
  test_type_search:any;
  loading:boolean;
  no_data:any;
  no_data_status:boolean=false;
  physicians_list_data: any = [];
  filteredOptions: Observable<StateGroup[]>;
  physicians_list: any = [];
  allNames: any = [];
  allIds: any = [];
  options: any = [];
  ids: any = [];
  filterVal: any = [];
  filterData:any;
  filterData_test_type:any;
  sendData;
  load_status: boolean = true;
  public url = environment.assetsUrl+'assets/uploads/';
  public stateGroups: StateGroup[] = [
    {
      ids: [],
      names: []
    }
  ]; //test
  public searchData: StateGroup[] = [
    {
      ids: [],
      names: []
    }
  ];
  constructor(
    private http:HttpClient, 
    public itemsService:PcrService,
    public route:Router,
    private modalService: BsModalService,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService
  ) { 
    this.searchForm = new FormGroup({
      test_type: new FormControl(),
      assessioning_num: new FormControl(),
      from_date: new FormControl(),
      to_date:new FormControl(),
      physician_name: new FormControl(),
      physician_id: new FormControl(),
    });
    this.datePickerConfig = Object.assign(
      {},
      {
        containerClass: "theme-blue",
        dateInputFormat: "MM/DD/YYYY"
      }
    );
    
  }
  onSubmit() {
    if (this.searchForm.status == "VALID") {
      this.spinner.show();
      this.load_status = false;
      //console.log(this.sendData);
      this.sendData = {
        test_type: this.searchForm.value.test_type,
        assessioning_num: this.searchForm.value.assessioning_num,
        from_date: this.convertDate(this.searchForm.value.from_date),
        to_date: this.convertDate(this.searchForm.value.to_date),
        physician: this.searchForm.value.physician_id,
        dataType: "general",
      };
      
      this.itemsService.search_pcr_submitted_reports(this.sendData).subscribe(data => {   
      
        this.post_array = data;
        this.data = this.post_array.submited_report_data;
        this.physicians_list = this.post_array.physicians_data;
        this.specimen_count = this.post_array.specimen_count;
        //console.log(this.post_array);
        this.no_data="";
          if(this.post_array.status === '1')
          {
            this.spinner.hide();
            this.test_type_search = "ON";
          }
          if(this.post_array.status === '0')
          {
            this.no_data_status=true;
            this.spinner.hide();
            this.no_data="No Data Found.";
          }
                   
        });
    } else {
    }
  }
  ngOnInit() {
    
    this.spinner.show();
    this.getSubmitedReportsData();
    this.filteredOptions = this.searchForm
    .get("physician_name")
    .valueChanges.pipe(
      startWith(""),
      map(value => this._filterGroup(value ? value.toLowerCase() : ""))
    ); 
    
  }
  getSubmitedReportsData() {
    let params = { 'dataType': 'general' };
    this.itemsService.getSubmitedReports(params).subscribe(data => {          
    this.post_array = data;
    this.data = this.post_array.submited_report_data;
    this.physicians_list = this.post_array.physicians_data;

    this.physicians_list.map(option =>this.allNames.push(option.physician_name));
    this.physicians_list.map(val => this.allIds.push(val.id));
    this.physicians_list.map(data => this.filterVal.push(data));

    this.specimen_count = this.post_array.specimen_count;
    //console.log(this.post_array);
      if(this.post_array.status === '1')
      {
        this.spinner.hide();
        this.test_type_search = "off";
      }
      if(this.post_array.status === '0')
      {
        this.no_data_status=true;
        this.spinner.hide();
        this.no_data="No Data Found.";
      }
               
    });
  }

  private _filterGroup(value: string): StateGroup[] {
    if (value) {
      let itemCount: number = this.filterVal.length;
      this.options = [];
      this.ids = [];
      for (let i = 0; i < itemCount; i++) {
        if (
          this.filterVal[i].physician_name.toLowerCase().indexOf(value) !== -1
        ) {
          this.options.push(this.filterVal[i].physician_name);
          this.ids.push(this.filterVal[i].id);
        }
      }
      this.searchData = [
        {
          ids: this.ids,
          names: this.options
        }
      ];

      return this.searchData;
    }

    return this.stateGroups;
  }
  convertDate(str: Date) {
    if (!str) {
      var newDate = "";
    } else {
      var date = new Date(str),
        mnth = ("0" + (date.getMonth() + 1)).slice(-2),
        day = ("0" + date.getDate()).slice(-2),
        newDate = [mnth, day, date.getFullYear()].join("/");
    }
    return newDate;
  }
  callSomeFunction(phy_id: any) {
    this.searchForm.controls["physician_id"].setValue(phy_id);
  }
  search(term: string) {
    let allData:any;
    allData = this.post_array.submited_report_data;
    if(!term) {
      this.filterData = this.data;
      this.data = allData;
    } else {
      this.filterData = allData.filter(x => 
         x.accessioning_num.trim().toLowerCase().includes(term.trim().toLowerCase())
      );
      this.data = this.filterData;
      this.no_data_status=false;
    }
    
  }


}
