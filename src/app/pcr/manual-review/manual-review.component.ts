import { Component, OnInit } from '@angular/core';
import {PcrService } from '../../pcr.service';
import {Observable} from "rxjs";
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormArray,FormControl } from '@angular/forms';
import {map, startWith} from 'rxjs/operators'; //add this
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ProcessStageModalComponent } from '../../pcr/process-stage-modal/process-stage-modal.component';
@Component({
  selector: 'cm-manual-review',
  templateUrl: './manual-review.component.html',
  styleUrls: ['./manual-review.component.css']
})
export class ManualReviewComponent implements OnInit {
  public data : any;
  specimens_list:any = [];
  post_array:any;
  batch_review:any;
  specimen_count:any;
  loading:boolean;
  no_data:any;
  no_data_status:boolean=false;
  modalRef: BsModalRef;
  filterData:any;
  constructor(
    private http:HttpClient, 
    public itemsService:PcrService,
    public route:Router,
    private modalService: BsModalService
    ) { }

  ngOnInit() {
      this.loading = true;
      this.getBatchReview();  
  }

  getBatchReview() {
    this.itemsService.batchDataReview().subscribe(data => {          
      this.post_array = data;
      this.loading = false;
      this.data = this.post_array.fail_specimens;
      this.batch_review = this.post_array.batch_review;
      this.specimen_count = this.post_array.specimen_count;
      if(this.post_array.status == '1')
      {
      
      }
      if(this.post_array.status == '0')
      {
        this.no_data_status=true;
        this.no_data="No Data Found.";
      }
               
    });
  }

  passBatchReview(data:any){
    let params:any;
    this.loading = true;
    params = {'batch_no':data,'user_id':localStorage.userid};
    this.itemsService.passBatchReview(params).subscribe(data => {          
      this.getBatchReview();
      });
  }


  failBatch(batch:any){
    this.modalRef = this.modalService.show(ProcessStageModalComponent,  {
      initialState: {
        title: 'first',     
        batch_data: batch
      }
    });
  }

  reviewProcessState(specimen_id:any,accessioning_no:any){
    this.modalRef = this.modalService.show(ProcessStageModalComponent,  {
      initialState: {
        title: 'sec',     
        specimen_id: specimen_id,
        accessioning_no:accessioning_no
      }
    });
  }

  batchHistory(){
    let historyData;
    let params = {'user_id':localStorage.userid};
    this.itemsService.getBatchHistory(params).subscribe(data => {
      this.post_array = data; 
      console.log(this.post_array['status']);
      if(this.post_array['status']==='1'){
        historyData = this.post_array.batch_history;
        console.log(historyData);
      }else{
        this.no_data_status = true;
        this.no_data = "Data not found";
      }

      this.modalRef = this.modalService.show(ProcessStageModalComponent,  {
        initialState: {
          title: 'third',
          data:historyData     
        }
      });         
     
      });
    
  }

  search(term: string) {
    let allData:any;
    allData = this.post_array.fail_specimens;
    if(!term) {
      this.filterData = this.data;
      this.data = allData;
    } else {
      this.filterData = allData.filter(x => 
         x.accessioning_num.trim().toLowerCase().includes(term.trim().toLowerCase())
      );
      this.data = this.filterData;
    }
    
  }

}
