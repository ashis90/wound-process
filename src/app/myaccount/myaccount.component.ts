import { Component, OnInit } from '@angular/core';
import { UserserviceService } from '../userservice.service';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, Validators, FormArray, FormControl, ValidatorFn, AbstractControl, FormBuilder } from '@angular/forms';
import { AuthService } from '../core/services/auth.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ConfirmPasswordValidator } from '../confirm-password.validator';

@Component({
  selector: 'cm-myaccount',
  templateUrl: './myaccount.component.html',
  styleUrls: ['./myaccount.component.css']
})
export class MyaccountComponent implements OnInit {

  chkData: any = [];
  sendData;
  first_role = false;
  second_role = false;
  myaccount: FormGroup;
  postsArray: any = [];
  loading;
  fname;
  lname;
  role;
  email;
  mob;
  url;
  add;
  user_name;
  message = false;
  load_status: boolean = true;
  acc_cntrl: any;
  acc_cntrl_qc: any;
  constructor(private router: Router, public itemsService: UserserviceService, public activatedRoute: ActivatedRoute, private http: HttpClient, private spinner: NgxSpinnerService,
    private fb: FormBuilder) {
    this.myaccount = this.fb.group({
      first_name: [''],
      last_name: [''],
      email: [''],
      password: [''],
      confirmPassword: [''],
      role: [''],
      mobile: [''],
      user_url: [''],
      address: ['']
    }, {
        validator: ConfirmPasswordValidator.MatchPassword
      });
  }

  ngOnInit() {
    if (this.load_status == true) {
      this.sendData = { "userid": localStorage.userid }
      this.spinner.show();
      this.itemsService.userDetailsRequest(this.sendData).subscribe(data => {
        this.chkData = data;
        this.fname = this.chkData.details.fname;
        this.lname = this.chkData.details.lname;
        this.user_name = this.chkData.details.user_name;
        this.role = this.chkData.details.role;
        this.email = this.chkData.details.email;
        this.url = this.chkData.details.url;
        this.mob = this.chkData.details.mob;
        this.add = this.chkData.details.add;
        this.acc_cntrl = this.chkData.details.acc_cntrl;
        this.acc_cntrl_qc = this.chkData.details.acc_cntrl_qc;
        //console.log(this.add);
        if (this.chkData.status == 1) {
          if (this.chkData.details.role == 'data_entry_oparator') {
            this.first_role = true;
          }
          if (this.chkData.details.role == 'physician') {
            this.second_role = true;
          }
          this.spinner.hide();
        }
        else {
          this.router.navigate(["/home"]);
        }
      }
      );
    }
  }

  equalto(repassword): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      let input = control.value;
      let isValid = control.root.value[repassword] == input;
      if (!isValid)
        return { 'equalTo': { isValid } };
      else
        return null;
    };
  }

  onSubmit() {
    if (this.myaccount.status == "VALID") {

      this.sendData = {
        "first_name": this.myaccount.value.first_name,
        "last_name": this.myaccount.value.last_name,
        "email": this.myaccount.value.email,
        "user_pass": this.myaccount.value.password,
        "con_pass": this.myaccount.value.confirmPassword,
        "mobile": this.myaccount.value.mobile ? this.phone_format(this.myaccount.value.mobile) : '',
        "user_url": this.myaccount.value.user_url,
        "address": this.myaccount.value.address,
        "userid": localStorage.userid
      };
      this.spinner.show();
      this.itemsService.updateUserDetails(this.sendData).subscribe(data => {
        this.message = true;
        this.spinner.hide();
        this.chkData = data;
      });

    } else {
      this.validateAllFormFields(this.myaccount);
    }
  }

  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }

  isFieldValid(field: string) {
    return !this.myaccount.get(field).valid && (this.myaccount.get(field).touched || this.myaccount.get(field).dirty);
  }
  validateAllFormFields(formGroup: FormGroup) {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof FormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }

  phone_format(phone) {
    var city, number;
    let ph_no = phone;
    var value = ph_no.toString().trim().replace(/^\+/, '');
    city = value.slice(0, 3);
    number = value.slice(3);
    number = number.slice(0, 3) + '-' + number.slice(3);
    number = city + "-" + number.trim();
    return number;
  }

}
