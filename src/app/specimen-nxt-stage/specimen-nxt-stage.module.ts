import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpecimenNxtStageRoutingModule } from './specimen-nxt-stage-routing.module';
import { NavbarModule } from '../core/navbar/navbar.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FieldErrorDisplayModule } from '../field-error-display/field-error-display.module';

@NgModule({
  imports:      [ CommonModule,SpecimenNxtStageRoutingModule,NavbarModule,FormsModule,
    ReactiveFormsModule,FieldErrorDisplayModule ],
  declarations: [ SpecimenNxtStageRoutingModule.components ]
})
export class SpecimenNxtStageModule { }
