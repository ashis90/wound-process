import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SpecimenNxtStageComponent } from '../specimen-nxt-stage/specimen-nxt-stage.component';

const routes: Routes = [
  { path: '', component: SpecimenNxtStageComponent },
  { path: ':id/:stageid', component: SpecimenNxtStageComponent }
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class SpecimenNxtStageRoutingModule {
  static components = [ SpecimenNxtStageComponent ];
}
