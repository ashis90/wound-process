import { Component, OnInit } from '@angular/core';
import { SpecimenService } from '../specimen.service';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { Router,ActivatedRoute } from '@angular/router';
import { FormGroup, Validators, FormArray,FormControl,ValidatorFn,AbstractControl } from '@angular/forms';
import { AuthService } from '../core/services/auth.service';
import { NgxSpinnerService } from 'ngx-spinner'

@Component({
  selector: 'cm-specimen-nxt-stage',
  templateUrl: './specimen-nxt-stage.component.html',
  styleUrls: ['./specimen-nxt-stage.component.css']
})
export class SpecimenNxtStageComponent implements OnInit {
  load_status:boolean = true;
  postsArray: any =[];
  spe_data:any;
  stage_data:any;
  stage_det:any;
  spe_next: FormGroup;
  clinical_info = false;
  test_section = false;
  pass:any;
  add_desc:any;
  agg= false;
  comment:any;
  pass_fail_status= false;
  sendData;
  loading;
  chkData:any;
  clinic:any =[];
  pass_fail_msg = false;
  histo_text = false;
  pcr_text = false;
  xvalue;
  yvalue;
  zvalue;
  color;
  pieces;
  cassettes;
  nail;
  skin;
  sub;
  histo_chk;
  pcr_chk;
  histo_val;
  pcr_val;
  constructor(private http:HttpClient, public itemsService:SpecimenService,public route:Router,public activatedRoute : ActivatedRoute,private spinner: NgxSpinnerService) 
  {
    var stageid=this.activatedRoute.snapshot.paramMap.get('stageid');
    if(stageid == '2')
    {
      this.spe_next = new FormGroup({
        "pass" : new FormControl(),
        "xvalue" : new FormControl("",[Validators.required]),
        "yvalue" : new FormControl("",[Validators.required]),
        "zvalue" : new FormControl("",[Validators.required]),
        "color" : new FormControl("",[Validators.required]),
        "pieces" : new FormControl("",[Validators.required]),
        "tissue" : new FormControl(),
        "cassettes" : new FormControl(null,[Validators.required]),
        "opt_sub" : new FormControl(),
        "histo_val" : new FormControl(),
        "pcr" : new FormControl(),
        "pcr_val" : new FormControl(),
        "histo" : new FormControl(),
      });
    }
    else if(stageid == '1' || stageid == '3' || stageid == '4')
    {
      this.spe_next = new FormGroup({
        "pass" : new FormControl(),
        "xvalue" : new FormControl(),
        "yvalue" : new FormControl(),
        "zvalue" : new FormControl(),
        "color" : new FormControl(),
        "pieces" : new FormControl(),
        "tissue" : new FormControl(),
        "cassettes" : new FormControl(),
        "opt_sub" : new FormControl(),
        "histo_val" : new FormControl(),
        "pcr" : new FormControl(),
        "pcr_val" : new FormControl(),
        "histo" : new FormControl(),
      });
    }
 
  }

  ngOnInit() 
  {
    if(this.load_status==true)
    {
        var id=this.activatedRoute.snapshot.paramMap.get('id');
        var stageid=this.activatedRoute.snapshot.paramMap.get('stageid');
        this.spinner.show();
        this.itemsService.makeSpecimenNext(id,stageid).subscribe(data => {
          this.spinner.hide();
          this.postsArray = data;
          if(this.postsArray.stage_det.pass_fail)
          {
            this.pass=this.postsArray.stage_det.pass_fail;
          }
          this.xvalue= this.postsArray.first;
          this.yvalue= this.postsArray.second;
          this.zvalue= this.postsArray.third;
          this.color= this.postsArray.color;
          this.cassettes= this.postsArray.cassettes;
          if(this.postsArray.stage_det === 'Nil')
          {
            this.pieces= '1';
          }
          else
          {
            this.pieces= this.postsArray.stage_det.pieces;
          }
         
          this.nail= this.postsArray.nail;
          this.sub= this.postsArray.sub;
          this.histo_chk= this.postsArray.histo_chk;
          this.pcr_chk= this.postsArray.pcr_chk;
          this.spe_next.controls['opt_sub'].setValue(this.sub);
          this.spe_next.controls['histo'].setValue(this.histo_chk);
          this.spe_next.controls['pcr'].setValue(this.pcr_chk);
          if(this.postsArray.stage_det.histo)
          {
            this.histo_val = this.postsArray.stage_det.histo;
          }
          if(this.postsArray.stage_det.pcr)
          {
            this.pcr_val = this.postsArray.stage_det.pcr;
          }
          // if(this.postsArray.stage_det.addtional_desc)
          // {
          //  this.add_desc=this.postsArray.stage_det.addtional_desc;
          // }
          // if(this.postsArray.stage_det.comments)
          // {
          //  this.comment=this.postsArray.stage_det.comments;
          // }
           if(this.pass == '1')
           {
             this.pass_fail_status = true;
           }
           else
           {
            this.pass_fail_status = false;
           }
          if(this.postsArray.stage_data.specime_stage_id == '2')
          {
            this.clinical_info= true;
          }
          else{
            this.clinical_info =false;
          }
        });
      
    }
   
  }
  validateAllFormFields(formGroup: FormGroup) {         

     Object.keys(formGroup.controls).forEach(field => {  

       const control = formGroup.get(field);             

       if (control instanceof FormControl) {             

         control.markAsTouched({ onlySelf: true });

       } else if (control instanceof FormGroup) {        

         this.validateAllFormFields(control);            

       }

     });

   }
   displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }

  isFieldValid(field: string) {
    return !this.spe_next.get(field).valid && (this.spe_next.get(field).touched || this.spe_next.get(field).dirty);
  }

  AddToTextArea(e,iName)
  {
    if (e.target.checked ) {
    this.add_desc=this.add_desc+" "+iName;
    }
  }
   showselected(){
    this.add_desc;
   }
   AddToTextAreasub(e,iName)
  {
    if (e.target.checked ) {
    this.add_desc=this.add_desc+" "+iName;
    }
  }
  showselectedsub(){
    this.add_desc;
   }
   show_histo()
   {
     this.histo_text = true;
   }
   show_pcr()
   {
    this.pcr_text = true;
   }
  onSubmit()
  {
    if(this.spe_next.status == "VALID")
    {
        var id=this.activatedRoute.snapshot.paramMap.get('id');
        var stageid=this.activatedRoute.snapshot.paramMap.get('stageid');

      this.sendData = { "pass":this.spe_next.value.pass,
      "xvalue": this.spe_next.value.xvalue,
      "yvalue": this.spe_next.value.yvalue,
      "zvalue": this.spe_next.value.zvalue,
      "color": this.spe_next.value.color,
      "pieces": this.spe_next.value.pieces,
      "tissue": this.spe_next.value.tissue,
      "cassettes": this.spe_next.value.cassettes,
      "opt_sub": this.spe_next.value.opt_sub?'submitted in toto':'',
      "histo": this.spe_next.value.histo?'histo':'',
      "histo_val": this.spe_next.value.histo_val,
      "pcr": this.spe_next.value.pcr?'pcr':'',
      "pcr_val": this.spe_next.value.pcr_val,
      "stage_id": stageid,
      "specimen_id":id,
      "lab_tech": localStorage.userid
     };

     if(this.sendData.pass!== undefined)
     {
     this.pass_fail_msg = false;  
     this.spinner.show();
     this.itemsService.updateStageDetails(this.sendData).subscribe( data=> {
      this.spinner.hide();
     this.chkData = data;
     if(this.chkData.status == '1')
     {
      this.route.navigate(["/listed-specimen-stages"]);
     }
   });   

  }
  else{
    this.pass_fail_msg = true;
  }

    }
    else
    {
      this.validateAllFormFields(this.spe_next);
    }
  } 

 
}
