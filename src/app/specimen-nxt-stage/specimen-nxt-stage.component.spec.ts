import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecimenNxtStageComponent } from './specimen-nxt-stage.component';

describe('SpecimenNxtStageComponent', () => {
  let component: SpecimenNxtStageComponent;
  let fixture: ComponentFixture<SpecimenNxtStageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpecimenNxtStageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecimenNxtStageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
