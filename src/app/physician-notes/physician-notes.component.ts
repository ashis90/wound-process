import { Component, OnInit } from '@angular/core';
import { NotesService } from '../notes.service';
import { SpecimenService } from '../specimen.service';
import {Observable} from "rxjs";
import { HttpClient } from '@angular/common/http';
import { Router,ActivatedRoute, NavigationEnd } from '@angular/router';
import { FormGroup, Validators, FormArray,FormControl, FormBuilder } from '@angular/forms';
import {map, startWith} from 'rxjs/operators'; //add this
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'cm-physician-notes',
  templateUrl: './physician-notes.component.html',
  styleUrls: ['./physician-notes.component.css']
})
export class PhysicianNotesComponent implements OnInit {

  public data : any;
  public loading = false;
  sendData;
  postsArray:any = [] ;
  load_status:boolean = true;
  filterData: any;
  constructor(
    public itemsService:SpecimenService,
    public notesService:NotesService,
    public route:Router, 
    private fb:FormBuilder,
    private _ActivatedRoute:ActivatedRoute,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.getAllNotesLists();
  }

  getAllNotesLists(){
    this.spinner.show();
    this.notesService.getAllNotesData().subscribe(data => { 
        this.postsArray = data;
        this.data = this.postsArray.all_notes_data;
        if(this.postsArray['status'] === '1'){
          this.spinner.hide();
        }
        if(this.postsArray['status'] === '0'){
          this.spinner.hide();
        }
    });
  }

  search(term: string) {
    let allData:any;
    allData = this.postsArray.all_notes_data;
    if(!term) {
      this.filterData = this.data;
      this.data = allData;
    } else {
      this.filterData = allData.filter(x => 
         x.physician_name.trim().toLowerCase().includes(term.trim().toLowerCase())
      );
      this.data = this.filterData;
    }
    
  }

}
