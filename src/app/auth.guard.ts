import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router) { }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot):  boolean {
      let roles = next.routeConfig.data.roles;
      if (localStorage.getItem('userToken') != null){
        if(roles.indexOf(localStorage.getItem('user_role')) !== -1){
          return true;
        }else{
          this.router.navigate(['/restricted']);
          return false;
        }
        
      }else{
        this.router.navigate(['/login']);
        return false;
      }
      
      
  }
}
