import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArchiveSubmittedReportsComponent } from './archive-submitted-reports.component';

describe('ArchiveSubmittedReportsComponent', () => {
  let component: ArchiveSubmittedReportsComponent;
  let fixture: ComponentFixture<ArchiveSubmittedReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArchiveSubmittedReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArchiveSubmittedReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
