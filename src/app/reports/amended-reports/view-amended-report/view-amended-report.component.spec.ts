import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewAmendedReportComponent } from './view-amended-report.component';

describe('ViewAmendedReportComponent', () => {
  let component: ViewAmendedReportComponent;
  let fixture: ComponentFixture<ViewAmendedReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewAmendedReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewAmendedReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
