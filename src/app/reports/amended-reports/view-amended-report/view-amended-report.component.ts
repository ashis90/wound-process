import { Component, OnInit } from '@angular/core';
import {PendingSpecimensService } from '../../../report.service';
import {Observable} from "rxjs";
import { HttpClient } from '@angular/common/http';
import { Router,ActivatedRoute } from '@angular/router';
import { FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'cm-view-amended-report',
  templateUrl: './view-amended-report.component.html',
  styleUrls: ['./view-amended-report.component.css']
})
export class ViewAmendedReportComponent implements OnInit {
  postsArray: any = [] ;
  load_status:boolean = true;
  loading;
  id;
  public url = environment.assetsUrl+'assets/uploads/';
  public link = environment.assetsUrl;
  constructor(
    private http: HttpClient, 
    public itemsService: PendingSpecimensService, 
    public route: Router,
    public activatedRoute : ActivatedRoute,
    private spinner: NgxSpinnerService
    ) 
  {}

  ngOnInit() 
  {
    var sid=this.activatedRoute.snapshot.paramMap.get('id');
    if(this.load_status==true)
    {
        let params = {"id": sid};
        this.spinner.show();
        this.itemsService.get_amended_report(params).subscribe(data => {
        this.loading = false;
        this.postsArray = data;
        this.spinner.hide();
    });

    }
  }

}
