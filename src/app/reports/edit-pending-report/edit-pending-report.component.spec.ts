import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditPendingReportComponent } from './edit-pending-report.component';

describe('EditPendingReportComponent', () => {
  let component: EditPendingReportComponent;
  let fixture: ComponentFixture<EditPendingReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditPendingReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditPendingReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
