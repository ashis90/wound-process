import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSpecimenReportComponent } from './add-specimen-report.component';

describe('AddSpecimenReportComponent', () => {
  let component: AddSpecimenReportComponent;
  let fixture: ComponentFixture<AddSpecimenReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddSpecimenReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSpecimenReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
