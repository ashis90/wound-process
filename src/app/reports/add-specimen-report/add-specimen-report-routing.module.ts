import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AddSpecimenReportComponent } from '../add-specimen-report/add-specimen-report.component';

const routes: Routes = [
  { path: '', component: AddSpecimenReportComponent },
  { path: ':id', component: AddSpecimenReportComponent }
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class AddSpecimenReportRoutingModule {
  static components = [ AddSpecimenReportComponent ];
}
