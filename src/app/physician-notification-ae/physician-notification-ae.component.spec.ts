import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhysicianNotificationAeComponent } from './physician-notification-ae.component';

describe('PhysicianNotificationAeComponent', () => {
  let component: PhysicianNotificationAeComponent;
  let fixture: ComponentFixture<PhysicianNotificationAeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhysicianNotificationAeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhysicianNotificationAeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
