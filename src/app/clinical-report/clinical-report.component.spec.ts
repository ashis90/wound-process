import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClinicalReportComponent } from './clinical-report.component';

describe('ClinicalReportComponent', () => {
  let component: ClinicalReportComponent;
  let fixture: ComponentFixture<ClinicalReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClinicalReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClinicalReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
