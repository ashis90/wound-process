import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { Headers, RequestOptions } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DataEntryPeopleService {

  headers: Headers = new Headers;
  options: any;
  constructor(private http:HttpClient) { 
    this.headers.append('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    this.headers.append( 'Access-Control-Allow-Origin', '*');
    this.headers.append('Accept','application/json');
    this.options = new RequestOptions({ headers: this.headers });
  }

  public url = environment.baseUrl+'DataEntryPeople/'

  addDataEntryPeople(data)
  {
    return this.http.post(this.url+'add_data_entry_people',data, this.options);
   
  }
  getDataEntryPeople()
  {
    return this.http.get(this.url+'get_data_entry_people', this.options);
   
  }

  getDataEntryPeopleById(data)
  {
    return this.http.post(this.url+'get_data_entry_people_by_id',JSON.stringify(data), this.options);
   
  }
  editDataEntryPeople(data)
  {
    return this.http.post(this.url+'edit_data_entry_people',data, this.options);
   
  }

  deleteDataEntryPeople(data)
  {
    return this.http.post(this.url+'delete_data_entry_people',JSON.stringify(data), this.options);
   
  }
}
