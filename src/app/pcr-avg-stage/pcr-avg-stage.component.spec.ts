import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PcrAvgStageComponent } from './pcr-avg-stage.component';

describe('PcrAvgStageComponent', () => {
  let component: PcrAvgStageComponent;
  let fixture: ComponentFixture<PcrAvgStageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PcrAvgStageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PcrAvgStageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
