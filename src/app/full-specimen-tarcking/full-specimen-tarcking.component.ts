import { Component, OnInit } from '@angular/core';
import { AnlyticsService } from '../anlytics.service';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, Validators, FormArray, FormControl, ValidatorFn, AbstractControl } from '@angular/forms';
import { AuthService } from '../core/services/auth.service';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { map, startWith } from 'rxjs/operators';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'cm-full-specimen-tarcking',
  templateUrl: './full-specimen-tarcking.component.html',
  styleUrls: ['./full-specimen-tarcking.component.css']
})
export class FullSpecimenTarckingComponent implements OnInit {
  searchForm: FormGroup;
  load_status: boolean = true;
  loading;
  postsArray: any = [];
  sendData;
  histo_modal = false;
  pcr_modal = false;
  acc_no = false;
  _archiveNote:any;
  constructor(private http: HttpClient, public itemsService: AnlyticsService, public route: Router, public activatedRoute: ActivatedRoute, private spinner: NgxSpinnerService) {
    this._archiveNote = this.itemsService.archiveNote;
    this.searchForm = new FormGroup({
      "acc_no": new FormControl('', Validators.required),
      "dataType": new FormControl('general', Validators.required)
    });

  }
  resetForm() {
    this.searchForm.reset();
  }
  ngOnInit() { }

  onSubmit() {

    if (this.searchForm.status == "VALID") {
      if (this.load_status == true) {
        this.spinner.show();
        this.sendData = { "acc_no": this.searchForm.value.acc_no, 'dataType': this.searchForm.value.dataType };
        //  this.sendData = "190820-0001-NF";
        this.itemsService.fullspecimen_track(this.sendData).subscribe(data => {
          this.spinner.hide();
          this.postsArray = data;
          console.log('data', data);

          if (this.postsArray.histo_status == '1') {
            this.acc_no = true;
            this.histo_modal = true;
          }
          else {
            this.histo_modal = false;
          }
          if (this.postsArray.pcr_status == '1') {
            this.acc_no = true;
            this.pcr_modal = true;
          }
          else {
            this.pcr_modal = false;
          }
          if (this.postsArray.status == '0') {
            this.acc_no = false;
            this.pcr_modal = false;
            this.histo_modal = false;
          }
        });
      }
    } else {
      this.validateAllFormFields(this.searchForm);
    }
  }

  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }

  isFieldValid(field: string) {
    return !this.searchForm.get(field).valid && (this.searchForm.get(field).touched || this.searchForm.get(field).dirty);
  }

  validateAllFormFields(formGroup: FormGroup) {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof FormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }



}
