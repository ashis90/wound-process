import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FullSpecimenTarckingComponent } from './full-specimen-tarcking.component';

describe('FullSpecimenTarckingComponent', () => {
  let component: FullSpecimenTarckingComponent;
  let fixture: ComponentFixture<FullSpecimenTarckingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FullSpecimenTarckingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FullSpecimenTarckingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
