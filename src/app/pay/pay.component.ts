import { Component, OnInit } from '@angular/core';
import { environment } from '../../environments/environment';
@Component({
  selector: 'cm-pay',
  templateUrl: './pay.component.html',
  styleUrls: ['./pay.component.css']
})
export class PayComponent implements OnInit {
  public url = environment.assetsUrl;
  public APIurl = environment.baseUrl;
  public home_ban = environment.assetsUrl + 'assets/frontend/main_images/inner_banner.jpg';
  constructor() { }

  ngOnInit() {

  }

}
