import { Component, OnInit } from '@angular/core';
import { AnlyticsService } from '../anlytics.service';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, Validators, FormArray, FormControl, ValidatorFn, AbstractControl } from '@angular/forms';
import { AuthService } from '../core/services/auth.service';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { ExcelService } from '../excel.service';
import { map, startWith } from 'rxjs/operators';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { formatDate } from '@angular/common';

export interface StateGroup {
  ids: Number[];
  names: string[];
}

@Component({
  selector: 'cm-short-code-analytics',
  templateUrl: './short-code-analytics.component.html',
  styleUrls: ['./short-code-analytics.component.css']
})
export class ShortCodeAnalyticsComponent implements OnInit {

  datePickerConfig: Partial<BsDatepickerConfig>;
  searchForm: FormGroup;
  load_status: boolean = true;
  loading;
  postsArray: any = [];
  physicians_list;
  sendData;
  msg = false;
  filteredOptions: Observable<StateGroup[]>;
  allNames: any = [];
  allIds: any = [];
  options: any = [];
  ids: any = [];
  filterVal: any = [];
  data;
  no_data;
  no_row = false;
  public stateGroups: StateGroup[] = [{
    ids: [],
    names: []
  }]//test
  public searchData: StateGroup[] = [{
    ids: [],
    names: []
  }]
  export_data: any;
  _archiveNote:any;
  constructor(private excelService: ExcelService, private http: HttpClient, public itemsService: AnlyticsService, public route: Router, public activatedRoute: ActivatedRoute, private spinner: NgxSpinnerService, private toastr: ToastrService, ) {
    this._archiveNote = this.itemsService.archiveNote;
    this.searchForm = new FormGroup({

      "from_date": new FormControl(),
      "to_date": new FormControl(),
      "sex": new FormControl(''),
      "report_code": new FormControl(),
      "patient_name": new FormControl(),
      "physician_name": new FormControl(),
      "physician_id": new FormControl(),
      "state": new FormControl(),
      "city": new FormControl(),
      "stain_code": new FormControl(),
      "acc_no": new FormControl(),
      "patient_age_from": new FormControl(),
      "patient_age_to": new FormControl(),
      "gross_dimension": new FormControl(),
      "addendum": new FormControl(),
      "report_genarate_date": new FormControl(),
      "diagnostic_short_code": new FormControl(),
      "physicianId": new FormControl(),
      "gender": new FormControl(),
      "patientName": new FormControl(),
      "patient_state": new FormControl(),
      "patient_city": new FormControl(),
      "stains": new FormControl(),
      "accessioning_number": new FormControl(),
      "patient_age": new FormControl(),
      "pcr_test": new FormControl(),
      "addn_comment":new FormControl(),
      "dataType": new FormControl('general', Validators.required)
    });

    this.datePickerConfig = Object.assign({}, {
      containerClass: 'theme-blue',
      dateInputFormat: 'YYYY-MM-DD'

    });
  }

  ngOnInit() {
    if (this.load_status == true) {
      this.msg = false;
      this.spinner.show();
      this.itemsService.get_short_code_data().subscribe(data => {
        this.spinner.hide();
        this.postsArray = data;
        this.physicians_list = this.postsArray.physicians_data;
        this.physicians_list.map((option) => this.allNames.push(option.physician_name));
        this.physicians_list.map((val) => this.allIds.push(val.id));
        this.physicians_list.map((data) => this.filterVal.push(data));
        this.filteredOptions = this.searchForm.get('physician_name').valueChanges.pipe(
          startWith(''),
          map(value => this._filterGroup(value.toLowerCase()))
        );

      });
      this.stateGroups = [{
        ids: this.allIds,
        names: this.allNames
      }];
    }
  }

  resetForm() {
    this.searchForm.reset();
    this.ngOnInit();
  }
  private _filterGroup(value: string): StateGroup[] {
    if (value) {
      let itemCount: number = this.filterVal.length;
      this.options = [];
      this.ids = [];
      for (let i = 0; i < itemCount; i++) {
        if ((this.filterVal[i].physician_name).toLowerCase().indexOf(value) !== -1) {
          this.options.push(this.filterVal[i].physician_name);
          this.ids.push(this.filterVal[i].id);
        }
      }
      this.searchData = [{
        ids: this.ids,
        names: this.options
      }];

      return this.searchData;
    }

    return this.stateGroups;
  }

  callSomeFunction(phy_id: any) {
    this.searchForm.controls['physician_id'].setValue(phy_id);
  }

  convertDate(str: Date) {
    if (!str) {
      var newDate = '';
    } else {

      var date = new Date(str),
        mnth = ("0" + (date.getMonth() + 1)).slice(-2),
        day = ("0" + date.getDate()).slice(-2),
        newDate = [date.getFullYear(), mnth, day].join("-");
    }
    return newDate;
  }

  onSubmit() {
    if (this.searchForm.status == "VALID") {
      this.spinner.show();
      //this.load_status=false;
      this.sendData = {
        "from_date": this.convertDate(this.searchForm.value.from_date),
        "to_date": this.convertDate(this.searchForm.value.to_date),
        "report_code": this.searchForm.value.report_code,
        "physician_id": this.searchForm.value.physician_id,
        "sex": this.searchForm.value.sex,
        "patient_name": this.searchForm.value.patient_name,
        "state": this.searchForm.value.state,
        "city": this.searchForm.value.city,
        "stain_code": this.searchForm.value.stain_code,
        "acc_no": this.searchForm.value.acc_no,
        "patient_age_from": this.searchForm.value.patient_age_from,
        "gross_dimension": this.searchForm.value.gross_dimension,
        "addendum": this.searchForm.value.addendum,
        "patient_age_to": this.searchForm.value.patient_age_to,
        'dataType': this.searchForm.value.dataType,
        'addn_comment': this.searchForm.value.addn_comment,
      };

      this.itemsService.search_short_code_data(this.sendData).subscribe(data => {
        this.msg = true;
        this.spinner.hide();
        this.postsArray = data;
        this.data = this.postsArray.details;
        this.no_data = this.postsArray.msg;
        if (this.no_data == 'No data found') {
          this.no_row = true;
        }
        else {
          this.no_row = false;
        }
      });
    }

  }

  exportAsXLSX(): void {
    let fmData = Object.assign({}, this.searchForm.value);
    let apiData;
    let cur_date = formatDate(new Date(), 'yyyy-MM-dd', 'en');
    this.spinner.show();
    fmData = Object.assign(fmData, { from_date: this.convertDate(fmData.from_date) });
    fmData = Object.assign(fmData, { to_date: this.convertDate(fmData.to_date) });
    fmData = Object.assign(fmData, { dataType: fmData.dataType });
    if (this.searchForm.value.physicianId || this.searchForm.value.report_genarate_date || this.searchForm.value.diagnostic_short_code || this.searchForm.value.gender || this.searchForm.value.patientName || this.searchForm.value.patient_state || this.searchForm.value.patient_city || this.searchForm.value.stains || this.searchForm.value.accessioning_number || this.searchForm.value.gross_description || this.searchForm.value.pcr_test || this.searchForm.value.addendum || this.searchForm.value.patient_age) {

      this.itemsService.export_short_code_anlytics(fmData).subscribe(data => {
        apiData = data;
        this.spinner.hide();
        if (apiData.status === '1') {
          this.export_data = apiData.details;
          this.excelService.exportAsExcelFile(this.export_data, 'sort_code_analytics');
        } else {
          this.toastr.info('No data found.', 'Error', {
            timeOut: 3000
          });
        }

      });
    } else {
      this.spinner.hide();
      this.toastr.info('Please check atleast one checkbox.', 'Error', {
        timeOut: 3000
      });
    }

  }
  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }

  isFieldValid(field: string) {
    return !this.searchForm.get(field).valid && (this.searchForm.get(field).touched || this.searchForm.get(field).dirty);
  }

  validateAllFormFields(formGroup: FormGroup) {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof FormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }

}
