import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AboutRoutingModule } from './about-routing.module';
import { NavbarModule } from '../core/navbar/navbar.module';

@NgModule({
  imports:      [ CommonModule,AboutRoutingModule,NavbarModule ],
  declarations: [ AboutRoutingModule.components ]
})
export class AboutModule { }
