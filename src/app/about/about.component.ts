import { Component, OnInit } from '@angular/core';
import { FrontendService } from '../frontend.service';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';

@Component({
  selector: 'cm-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {
  public url = environment.assetsUrl;
  public home_ban = environment.assetsUrl + 'assets/frontend/main_images/lab_inner_banner.jpg';
  postsArray: any = [];
  constructor(private http: HttpClient, public itemsService: FrontendService, public route: Router) { }

  ngOnInit() {
    this.getuser();
  }
  getuser() {
    this.itemsService.makeAboutRequest().subscribe(data => this.postsArray = data);

  }


}
