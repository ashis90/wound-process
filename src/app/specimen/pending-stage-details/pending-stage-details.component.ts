import { Component, OnInit, Renderer2 } from '@angular/core';
import { SpecimenService } from '../../specimen.service';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { Router,ActivatedRoute } from '@angular/router';
import { FormGroup, Validators, FormArray,FormControl } from '@angular/forms';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { NgxSpinnerService } from 'ngx-spinner';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { TrackModalComponent } from '../../track-modal/track-modal.component';
import { BsModalService } from 'ngx-bootstrap/modal';
import { AnlyticsService } from '../../anlytics.service';

@Component({
  selector: 'cm-pending-stage-details',
  templateUrl: './pending-stage-details.component.html',
  styleUrls: ['./pending-stage-details.component.css']
})
export class PendingStageDetailsComponent implements OnInit {

  datePickerConfig:Partial<BsDatepickerConfig>;
  postsArray:any = [] ;
  sendData;
  physicianArray:any = [] ;
  searchForm: FormGroup;
  public data : any;
  public phy_data : any;
  load_status:boolean = true;
  public loading = false;
  first_msg= false;
  second_msg= false;
  third_msg= false;
  fourth_msg= false;
  histo_modal=false;
  pcr_modal=false;
  acc_no=false;
  modalRef: BsModalRef;
  load_more_data:any=0;
  spe_count:any;
  specimen_stages:any = [];
  st1;
  st2;
  st3;
  st4;
  load_btn = true;
  stage_id:any;
  stage_name:any='';

  spe_data:any;
  stage_data:any;
  stage_det:any;
  spe_next: FormGroup;
  clinical_info = false;
  test_section = false;
  pass:any;
  add_desc:any;
  agg= false;
  comment:any;
  pass_fail_status= false;
  chkData:any;
  clinic:any =[];
  pass_fail_msg = false;
  histo_text = false;
  pcr_text = false;
  xvalue;
  yvalue;
  zvalue;
  color;
  pieces;
  cassettes;
  nail;
  skin;
  sub;
  histo_chk;
  pcr_chk;
  histo_val;
  pcr_val;
  filterData:any;
  search_res:boolean=false;
  serch_arr:any;
  constructor(
    private http:HttpClient, 
    public itemsService:SpecimenService,
    public route:Router,
    private spinner: NgxSpinnerService,
    private modalService: BsModalService,
    public analyticsService:AnlyticsService,
    private _ActivatedRoute:ActivatedRoute,
    private renderer: Renderer2) 
  { 
    this.stage_id= +this._ActivatedRoute.snapshot.paramMap.get('stageid');
      this.searchForm = new FormGroup({
    
          "acc_no" : new FormControl()
        });

        if(this.stage_id == '2')
    {
      this.spe_next = new FormGroup({
        "specimen_id" : new FormControl(),
        "pass" : new FormControl(),
        "xvalue" : new FormControl("",[Validators.required]),
        "yvalue" : new FormControl("",[Validators.required]),
        "zvalue" : new FormControl("",[Validators.required]),
        "color" : new FormControl("",[Validators.required]),
        "pieces" : new FormControl("",[Validators.required]),
        "tissue" : new FormControl(),
        "cassettes" : new FormControl(null,[Validators.required]),
        "opt_sub" : new FormControl(),
        "histo_val" : new FormControl(),
        "pcr" : new FormControl(),
        "pcr_val" : new FormControl(),
        "histo" : new FormControl(),
      });
    }
    else if(this.stage_id == '1' || this.stage_id == '3' || this.stage_id == '4')
    {
      this.spe_next = new FormGroup({
        "specimen_id" : new FormControl(),
        "pass" : new FormControl(),
        "xvalue" : new FormControl(),
        "yvalue" : new FormControl(),
        "zvalue" : new FormControl(),
        "color" : new FormControl(),
        "pieces" : new FormControl(),
        "tissue" : new FormControl(),
        "cassettes" : new FormControl(),
        "opt_sub" : new FormControl(),
        "histo_val" : new FormControl(),
        "pcr" : new FormControl(),
        "pcr_val" : new FormControl(),
        "histo" : new FormControl(),
      });
    }
      this.datePickerConfig = Object.assign({},{
      containerClass:'theme-blue',
      dateInputFormat:'MM/DD/YYYY'
      
      });

      this.specimen_stages = localStorage.specimen_stages;

      if(this.specimen_stages){
        this.st1 = this.specimen_stages.indexOf('1') !== -1;
        this.st2 = this.specimen_stages.indexOf('2') !== -1;
        this.st3 = this.specimen_stages.indexOf('3') !== -1;
        this.st4 = this.specimen_stages.indexOf('4') !== -1;
      }

      
      if(this.stage_id == '1'){
        this.stage_name = 'Stage 1: Delivery to lab';
      }else if(this.stage_id == '2'){
        this.stage_name = 'Stage 2: Gross';
      }else if(this.stage_id == '3'){
        this.stage_name = 'Stage 3: Microtomy';
      }else if(this.stage_id == '4'){
        this.stage_name = 'Stage 4: QC Check';
      }
  }

  ngOnInit() {
      if(this.load_status==true)
      {
          this.load_btn = true; 
          this.getstagedetails();
      }

  }

  searchStages() 
  {
    if(this.load_status==true)
    {
      this.spinner.show();
      this.load_status=false;
      let sendData = { "accessioning_number":this.searchForm.value.acc_no, "stageid":this.stage_id};
        this.itemsService.searchSpecimenStages(sendData).subscribe(data => {
          this.spinner.hide();
          this.postsArray = data;
          if(this.postsArray.status == '1'){
            this.search_res = true;
          }
          if(this.postsArray.stage_det.pass_fail)
          {
            this.pass=this.postsArray.stage_det.pass_fail;
          }
          this.xvalue= this.postsArray.first;
          this.yvalue= this.postsArray.second;
          this.zvalue= this.postsArray.third;
          this.color= this.postsArray.color;
          this.cassettes= this.postsArray.cassettes;
          if(this.postsArray.stage_det === 'Nil')
          {
            this.pieces= '1';
          }
          else
          {
            this.pieces= this.postsArray.stage_det.pieces;
          }
         
          this.nail= this.postsArray.nail;
          this.sub= this.postsArray.sub;
          this.histo_chk= this.postsArray.histo_chk;
          this.pcr_chk= this.postsArray.pcr_chk;
          this.spe_next.controls['specimen_id'].setValue(this.postsArray.spe_details.id);
          this.spe_next.controls['opt_sub'].setValue(this.sub);
          this.spe_next.controls['histo'].setValue(this.histo_chk);
          this.spe_next.controls['pcr'].setValue(this.pcr_chk);
          if(this.postsArray.stage_det.histo)
          {
            this.histo_val = this.postsArray.stage_det.histo;
          }
          if(this.postsArray.stage_det.pcr)
          {
            this.pcr_val = this.postsArray.stage_det.pcr;
          }
          // if(this.postsArray.stage_det.addtional_desc)
          // {
          //  this.add_desc=this.postsArray.stage_det.addtional_desc;
          // }
          // if(this.postsArray.stage_det.comments)
          // {
          //  this.comment=this.postsArray.stage_det.comments;
          // }
           if(this.pass == '1')
           {
             this.pass_fail_status = true;
           }
           else
           {
            this.pass_fail_status = false;
           }
          if(this.postsArray.stage_data.specime_stage_id == '2')
          {
            this.clinical_info= true;
          }
          else{
            this.clinical_info =false;
          }
        });
      
    }
   
  }
  resetForm(){
      this.load_btn = true; 
      this.searchForm.reset();
      this.getstagedetails();
    }
  getstagedetails()
  {
    let params = {'load':0,'stage_id':this.stage_id};
    this.spinner.show();
    this.itemsService.makeSpecimenStageRequestByStageId(params).subscribe( data=> {
    this.spinner.hide();
    this.postsArray = data;
    this.data  =  this.postsArray.specimens_detail;
    this.serch_arr = this.postsArray.specimens_detail;
    this.spe_count = this.data?this.data.length:'';
    } );
    
  }

  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }

  isFieldValid(field: string) {
    return !this.spe_next.get(field).valid && (this.spe_next.get(field).touched || this.spe_next.get(field).dirty);
  }

  onSubmit()
  {
    if(this.spe_next.status == "VALID")
    {

      let sendData = { "pass":this.spe_next.value.pass,
      "xvalue": this.spe_next.value.xvalue,
      "yvalue": this.spe_next.value.yvalue,
      "zvalue": this.spe_next.value.zvalue,
      "color": this.spe_next.value.color,
      "pieces": this.spe_next.value.pieces,
      "tissue": this.spe_next.value.tissue,
      "cassettes": this.spe_next.value.cassettes,
      "opt_sub": this.spe_next.value.opt_sub?'submitted in toto':'',
      "histo": this.spe_next.value.histo?'histo':'',
      "histo_val": this.spe_next.value.histo_val,
      "pcr": this.spe_next.value.pcr?'pcr':'',
      "pcr_val": this.spe_next.value.pcr_val,
      "stage_id": this.stage_id,
      "specimen_id":this.spe_next.value.specimen_id,
      "lab_tech": localStorage.userid
     };

     if(sendData.pass!== undefined)
     {
     this.pass_fail_msg = false;  
     this.spinner.show();
     this.itemsService.updateStageDetails(sendData).subscribe( data=> {
      this.spinner.hide();
     this.chkData = data;
     if(this.chkData.status == '1')
     {
      this.route.navigate(["/listed-specimen-stages"]);
     }
   });   

  }
  else{
    this.pass_fail_msg = true;
  }

    }
    else
    {
      this.validateAllFormFields(this.spe_next);
    }
  }

  validateAllFormFields(formGroup: FormGroup) {         

    Object.keys(formGroup.controls).forEach(field => {  

      const control = formGroup.get(field);             

      if (control instanceof FormControl) {             

        control.markAsTouched({ onlySelf: true });

      } else if (control instanceof FormGroup) {        

        this.validateAllFormFields(control);            

      }

    });

  }
  load_last_five()
  {
    this.load_btn = false; 
    this.spinner.show();
    this.itemsService.makelastFiveSpecimenStage().subscribe( data=> {
    this.spinner.hide();
    this.postsArray = data;
    this.data  =  this.postsArray.specimens_detail;
    this.spe_count = this.data?this.data.length:'';
  });
    
  }

  convertDate(str: Date) {
      if(!str){
        var newDate = '';
      }else{
        
        var date = new Date(str),
          mnth = ("0" + (date.getMonth()+1)).slice(-2),
          day  = ("0" + date.getDate()).slice(-2),
          newDate = [ date.getFullYear(), mnth, day  ].join("-");
      }
      return newDate;
    }

    displaytrackmodal(specimen_id:any){
      this.spinner.show();
      this.sendData = { "acc_no":specimen_id};
      this.analyticsService.specimen_track(this.sendData).subscribe(data => {
        this.postsArray = data;
        this.spinner.hide();
        if(this.postsArray.histo_status=='1')
        {
          this.acc_no=true;
          this.histo_modal =true;
        }
        else
        {
          this.histo_modal =false;
        }
        if(this.postsArray.pcr_status=='1')
        {
          this.acc_no=true;
          this.pcr_modal =true;
        }
        else
        {
          this.pcr_modal =false;
        }
        if(this.postsArray.status=='0')
        {
           this.acc_no=false;
           this.pcr_modal =false;
           this.histo_modal =false;
        }
        this.modalRef = this.modalService.show(TrackModalComponent,  {
          initialState: {
            title: 'track',
            acc_no:  this.acc_no,
            postsArray: this.postsArray,
            histo_modal: this.histo_modal,
            pcr_modal: this.pcr_modal
          }
    
        });
    
      });
    
    
    }

    load_more(){
      this.load_more_data = this.load_more_data + 10;
      this.spinner.show();
      let params = {'load':this.load_more_data, "stage_id":this.stage_id};
        this.itemsService.loadMoreRequest_stage_details(params).subscribe(data => {
         
        this.spinner.hide();  
        this.postsArray = data;
        if(this.postsArray){
          this.postsArray.specimens_detail.forEach(element => {
            this.data.push(element);
          });

          this.spe_count = this.data?this.data.length:'';
        }
      });    
    }

    AddToTextArea(e,iName)
  {
    if (e.target.checked ) {
    this.add_desc=this.add_desc+" "+iName;
    }
  }
   showselected(){
    this.add_desc;
   }
   AddToTextAreasub(e,iName)
  {
    if (e.target.checked ) {
    this.add_desc=this.add_desc+" "+iName;
    }
  }
  showselectedsub(){
    this.add_desc;
   }
   show_histo()
   {
     this.histo_text = true;
   }
   show_pcr()
   {
    this.pcr_text = true;
   }

   search(term: string) {
    let allData:any;
    allData = this.serch_arr;
    if(!term) {
      this.filterData = this.data;
      this.data = allData;
    } else {
      this.filterData = allData.filter(x => 
         x.assessioning_num.trim().toLowerCase().includes(term.trim().toLowerCase())
      );
      this.data = this.filterData;
    }
    
  }

}
