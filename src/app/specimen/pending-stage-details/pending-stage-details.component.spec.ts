import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingStageDetailsComponent } from './pending-stage-details.component';

describe('PendingStageDetailsComponent', () => {
  let component: PendingStageDetailsComponent;
  let fixture: ComponentFixture<PendingStageDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingStageDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingStageDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
