import { Component, OnInit } from '@angular/core';
import { SpecimenService } from '../../specimen.service';
import {Observable} from "rxjs";
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormArray,FormControl } from '@angular/forms';
import {map, startWith} from 'rxjs/operators'; //add this
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { NgxSpinnerService } from 'ngx-spinner';

export interface StateGroup {
    ids: Number[];
    names: string[];
  }
@Component({
    selector: 'cm-cancelled',
    templateUrl: './cancelled.component.html',
    styleUrls: ['./cancelled.component.css']
})
export class CancelledComponent implements OnInit {
    datePickerConfig:Partial<BsDatepickerConfig>;
    public data : any;
    specimens_list:any = [];
    physicians_list:any = [];
    searchForm: FormGroup;
    public phy_data : any;
    load_status:boolean = true;
    public loading = false;
    sendData;
    postsArray:any = [] ;
    no_data:any;
    no_data_status= false;
    physicians_list_data:any = [];
    filteredOptions: Observable<StateGroup[]>;
    allNames: any = [];
    allIds: any = [];
    options: any = [];
    ids: any = [];
    filterVal: any = [];
    public stateGroups: StateGroup[] = [{
        ids: [],
        names: []
      }]//test
    public searchData: StateGroup[] = [{
        ids: [],
        names: []
      }]

    constructor(
      private http:HttpClient, 
      public itemsService:SpecimenService,
      public route:Router,
      private spinner: NgxSpinnerService
      )
    {
     this.searchForm = new FormGroup({
       
       "date" : new FormControl(),
       "barcode" : new FormControl(),
       "fname" : new FormControl(), 
       "lname" : new FormControl(), 
       "physician_name" : new FormControl(), 
        "physician_id" : new FormControl(),  
       "assessioning_num" : new FormControl(), 
     });

     this.datePickerConfig = Object.assign({},{
        containerClass:'theme-blue',
        dateInputFormat:'MM/DD/YYYY'
      
      });
 
    }
 
    onSubmit()
    {
        if(this.searchForm.status == "VALID")
        {   
            this.spinner.show();
            this.load_status=false;
            this.no_data_status= false;
            this.sendData = { "date":this.convertDate(this.searchForm.value.date),"barcode":this.searchForm.value.barcode,"fname":this.searchForm.value.fname,"lname":this.searchForm.value.lname, "assessioning_num":this.searchForm.value.assessioning_num, "physician":this.searchForm.value.physician_id  };
            this.itemsService.search_cancelled_specimen(this.sendData).subscribe( data=> {
                this.loading = false;
                this.specimens_list = data;
                this.data  =  this.specimens_list.specimens_detail;
                this.physicians_list = this.specimens_list.physicians_data;
                if(this.specimens_list['status'] === '1'){
                  this.spinner.hide();
                }
                if(this.specimens_list['status'] === '0'){
                  this.no_data_status= true;
                  this.no_data="No Data Found.";
                  this.spinner.hide();
                }
             });
        }
        else
        {
 
        }
    }

    resetForm(){
      this.searchForm.reset();
      this.getspecimen();
    }
    ngOnInit() {
        if(this.load_status==true)
        {
          this.spinner.show();
          this.getspecimen();

          this.filteredOptions = this.searchForm.get('physician_name').valueChanges.pipe(
            startWith(''),
            map(value => this._filterGroup(value?value.toLowerCase():''))
          );
        }
      }
      getspecimen()
        {
          this.spinner.show();
          this.itemsService.makeCancelledSpecimenRequest().subscribe(data => {
            this.no_data_status= false;
              this.specimens_list = data;
              this.data  =  this.specimens_list.specimens_detail;
              this.physicians_list = this.specimens_list.physicians_data;     
              this.no_data_status= false;

              if(this.specimens_list['status'] === '1'){
                this.physicians_list.map((option) => this.allNames.push ( option.physician_name));
                this.physicians_list.map((val) => this.allIds.push ( val.id));
                this.physicians_list.map((data) => this.filterVal.push ( data));
                this.spinner.hide();
              }
              if(this.specimens_list['status'] === '0'){
                this.no_data_status= true;
                this.no_data="No Data Found.";
                this.spinner.hide();
              }
          });

          this.stateGroups = [{
            ids:this.allIds,
            names:this.allNames
          }];
          
        }

        private _filterGroup(value: string): StateGroup[] {
            if (value) {
                let itemCount : number = this.filterVal.length;
                this.options = [];
                this.ids=[];
                for(let i=0;i<itemCount;i++){
                    if((this.filterVal[i].physician_name).toLowerCase().indexOf(value) !== -1 ){
                      this.options.push(this.filterVal[i].physician_name);
                      this.ids.push(this.filterVal[i].id);
                    }
                }
                this.searchData = [{
                  ids:this.ids,
                  names:this.options
                }];
                
                return this.searchData;
            }
            
            return this.stateGroups;
        }
      
        callSomeFunction(phy_id :any){
          this.searchForm.controls['physician_id'].setValue(phy_id);
        }
    
        convertDate(str: Date) {
            if(!str){
              var newDate = '';
            }else{
              
              var date = new Date(str),
                mnth = ("0" + (date.getMonth()+1)).slice(-2),
                day  = ("0" + date.getDate()).slice(-2),
                newDate = [ mnth, day, date.getFullYear() ].join("/");
            }
            return newDate;
          }
      
          getBarcode(acc_no)
          {
            this.itemsService.get_bar_code(acc_no).subscribe(data => {
              this.postsArray = data;
              this.print(this.postsArray.acc_no,this.postsArray.qrCode,this.postsArray.base_url);
            });
          }
          print(assessioning_num:any,qrname:any,qr_path:any): void {
            let printContents, popupWin;
            //printContents = document.getElementById('print-section').innerHTML;
            popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
            popupWin.document.open();
            popupWin.document.write(`
              <html>
                <head>
                  <title>Print tab</title>
                  <style>
                  //........Customized style.......
                  </style>
                </head>
            <body onload="window.print();window.close()">
            <div id="${assessioning_num}_1;?>" class="print_qr_code">
            <img src='${qr_path}assets/uploads/qr_images/${qrname}'>
            <p style='font-size:15px; margin-top: -12px;'><strong>${assessioning_num}</strong></p>
            </div>
            </body>
              </html>`
            );
            popupWin.document.close();
          } 


}
