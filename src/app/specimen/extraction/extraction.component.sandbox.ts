import { sandboxOf } from 'angular-playground';
import { ExtractionComponent } from './extraction.component';

export default sandboxOf(ExtractionComponent)
  .add('Extraction Component', {
    template: `<cm-extraction></cm-extraction>`
  });
