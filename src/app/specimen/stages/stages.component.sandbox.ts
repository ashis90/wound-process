import { sandboxOf } from 'angular-playground';
import { StagesComponent } from './stages.component';

export default sandboxOf(StagesComponent)
  .add('Stages Component', {
    template: `<cm-stages></cm-stages>`
  });
