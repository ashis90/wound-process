import { sandboxOf } from 'angular-playground';
import { SuspendedComponent } from './suspended.component';

export default sandboxOf(SuspendedComponent)
  .add('Suspended Component', {
    template: `<cm-suspended></cm-suspended>`
  });
