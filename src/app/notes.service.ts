import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { Headers, RequestOptions } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { environment } from '../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class NotesService {

  headers: Headers = new Headers;
  options: any;
  constructor(private http:HttpClient) { 
    this.headers.append('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    this.headers.append( 'Access-Control-Allow-Origin', '*');
    this.headers.append('Accept','application/json');
    this.options = new RequestOptions({ headers: this.headers });
  }

  public url = environment.baseUrl+'PhysicianNotes/'

  getNotesDetails(data)
  {
    return this.http.post(this.url+'notes_details',data, this.options);
   
  }

  addNotes(data)
  {
    return this.http.post(this.url+'add_notes',data, this.options);
  }

  updatesNotes(data)
  {
    return this.http.post(this.url+'update_notes',data, this.options);
  }

  getAllNotesDetails()
  {
    return this.http.get(this.url+'get_all_notes_details', this.options);
  }

  getAllNotesData()
  {
    return this.http.get(this.url+'get_all_notes_data', this.options);
  }

  notesDelete(data)
  {
    return this.http.post(this.url+'delete_notes_data',JSON.stringify(data),this.options);
  }
}
