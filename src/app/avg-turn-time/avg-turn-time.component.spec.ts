import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AvgTurnTimeComponent } from './avg-turn-time.component';

describe('AvgTurnTimeComponent', () => {
  let component: AvgTurnTimeComponent;
  let fixture: ComponentFixture<AvgTurnTimeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AvgTurnTimeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AvgTurnTimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
