import { Component, OnInit } from '@angular/core';
import { AnlyticsService } from '../anlytics.service';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, Validators, FormArray, FormControl, ValidatorFn, AbstractControl } from '@angular/forms';
import { AuthService } from '../core/services/auth.service';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { ExcelService } from '../excel.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'cm-lab-analytics',
  templateUrl: './lab-analytics.component.html',
  styleUrls: ['./lab-analytics.component.css']
})
export class LabAnalyticsComponent implements OnInit {
  datePickerConfig: Partial<BsDatepickerConfig>;
  load_status: boolean = true;
  postsArray: any = [];
  searchForm: FormGroup;
  public loading = false;
  sendData;
  specimens_list;
  data: any;
  _archiveNote:any;
  constructor(private excelService: ExcelService, private http: HttpClient, public itemsService: AnlyticsService, public route: Router, public activatedRoute: ActivatedRoute, private spinner: NgxSpinnerService) {
    this._archiveNote = this.itemsService.archiveNote;
    this.searchForm = new FormGroup({
      "from_date": new FormControl('', Validators.required),
      "to_date": new FormControl('', Validators.required),
      "histo_pcr": new FormControl(),
      "dataType": new FormControl('general', Validators.required)
    });

    this.datePickerConfig = Object.assign({}, {
      containerClass: 'theme-blue',
      dateInputFormat: 'YYYY-MM-DD'

    });
  }


  ngOnInit() {
    if (this.load_status == true) {
      this.spinner.show();
      this.itemsService.getdata().subscribe(data => {
        this.spinner.hide();
        this.postsArray = data;

        this.data = [{
          Duration: this.postsArray.current_Month + "-" + this.postsArray.Current_Month_Day,
          Specimens_Added: this.postsArray.current_specimen_count,
          Physicians_Added: this.postsArray.current_phy_count,
          Federal: this.postsArray.current_federal_count,
          Commercial: this.postsArray.current_comercial_count,
          Specimens_Per_Physician: this.postsArray.specimen_per_phy_current,
          Specimens_Per_Active_Physician: this.postsArray.specimen_per_active_current,
          New_Physicians: this.postsArray.physicians_sending_specimens_count,
          Report_Issued: this.postsArray.report_issued_curr
        },
        {
          Duration: this.postsArray.last_Month + "-" + this.postsArray.last_Month_Name + "-" + this.postsArray.last_Month_Day,
          Specimens_Added: this.postsArray.previous_specimen_count,
          Physicians_Added: this.postsArray.pre_phy_count,
          Federal: this.postsArray.previous_federal_count,
          Commercial: this.postsArray.previous_comercial_count,
          Specimens_Per_Physician: this.postsArray.specimen_per_phy_prev,
          Specimens_Per_Active_Physician: this.postsArray.specimen_per_active_prev,
          New_Physicians: this.postsArray.pre_physicians_sending_specimens_count,
          Report_Issued: this.postsArray.report_issued_prev
        }];

      });
    }
  }

  onSubmit() {
    if (this.searchForm.status == "VALID") {
      this.spinner.show();
      //this.load_status=false;
      this.sendData = { "from_date": this.convertDate(this.searchForm.value.from_date), "to_date": this.convertDate(this.searchForm.value.to_date), "histo_pcr": this.searchForm.value.histo_pcr, "dataType": this.searchForm.value.dataType };
      this.itemsService.search_data(this.sendData).subscribe(data => {
        this.spinner.hide();
        this.postsArray = data;

        this.data = [{
          Duration: this.postsArray.current_Month + "-" + this.postsArray.Current_Month_Day,
          Specimens_Added: this.postsArray.current_specimen_count,
          Physicians_Added: this.postsArray.current_phy_count,
          Federal: this.postsArray.current_federal_count,
          Commercial: this.postsArray.current_comercial_count,
          Specimens_Per_Physician: this.postsArray.specimen_per_phy_current,
          Specimens_Per_Active_Physician: this.postsArray.specimen_per_active_current,
          New_Physicians: this.postsArray.physicians_sending_specimens_count,
          Report_Issued: this.postsArray.report_issued_curr
        }];

      });
    } else {
      this.validateAllFormFields(this.searchForm);
    }
  }

  validateAllFormFields(formGroup: FormGroup) {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof FormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }

  resetForm() {
    this.searchForm.reset();
    this.ngOnInit();
  }

  convertDate(str: Date) {
    if (!str) {
      var newDate = '';
    } else {

      var date = new Date(str),
        mnth = ("0" + (date.getMonth() + 1)).slice(-2),
        day = ("0" + date.getDate()).slice(-2),
        newDate = [date.getFullYear(), mnth, day].join("-");
    }
    return newDate;
  }

  exportAsXLSX(): void {
    this.excelService.exportAsExcelFile(this.data, 'sample');
  }

  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }

  isFieldValid(field: string) {
    return !this.searchForm.get(field).valid && (this.searchForm.get(field).touched || this.searchForm.get(field).dirty);
  }

}
