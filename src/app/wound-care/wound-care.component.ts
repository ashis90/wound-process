import { Component, OnInit } from '@angular/core';
import { FrontendService } from '../frontend.service';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'cm-wound-care',
  templateUrl: './wound-care.component.html',
  styleUrls: ['./wound-care.component.css']
})
export class WoundCareComponent implements OnInit {

  public url = environment.assetsUrl;
  public home_ban = environment.assetsUrl + 'assets/frontend/main_images/inner_banner.jpg';
  postsArray: any = [];
  constructor(
    private http: HttpClient,
    public itemsService: FrontendService,
    public route: Router,
    private spinner: NgxSpinnerService,
  ) { }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.spinner.show();
    this.itemsService.makeWoundRequest().subscribe(data => {
      this.postsArray = data
      console.log(this.postsArray)
      this.spinner.hide();
    });
  }

}
