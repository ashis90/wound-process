import { Component, OnInit } from '@angular/core';
import { SpecimenService } from '../specimen.service';
import {Observable} from "rxjs";
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormArray,FormControl } from '@angular/forms';
import {map, startWith} from 'rxjs/operators'; //add this
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { NgxSpinnerService } from 'ngx-spinner';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { TrackModalComponent } from '../track-modal/track-modal.component';
import { BsModalService } from 'ngx-bootstrap/modal';
import { AnlyticsService } from '../anlytics.service';

export interface StateGroup {
  ids: Number[];
  names: string[];
}
@Component({
  selector: 'cm-qc-confirmation',
  templateUrl: './qc-confirmation.component.html',
  styleUrls: ['./qc-confirmation.component.css']
})
export class QcConfirmationComponent implements OnInit {
  datePickerConfig:Partial<BsDatepickerConfig>;
  public data : any;
  specimens_list:any = [];
  physicians_list:any = [];
  searchForm: FormGroup;
  public phy_data : any;
  load_status:boolean = true;
  public loading = false;
  sendData;
  postsArray:any = [] ;
  no_data:any;
  no_data_status= false;
  physicians_list_data:any = [];
  filteredOptions: Observable<StateGroup[]>;
  allNames: any = [];
  allIds: any = [];
  options: any = [];
  ids: any = [];
  filterVal: any = [];
  modalRef: BsModalRef;
  histo_modal=false;
  pcr_modal=false;
  acc_no=false;
  user_id;
  public stateGroups: StateGroup[] = [{
      ids: [],
      names: []
    }]//test
  public searchData: StateGroup[] = [{
      ids: [],
      names: []
    }]

  constructor(
    private http:HttpClient, 
    public itemsService:SpecimenService,
    public route:Router,
    private spinner: NgxSpinnerService,
    private modalService: BsModalService,
    public analyticsService:AnlyticsService
    ) 
  {
    this.searchForm = new FormGroup({
      
      "date" : new FormControl(),
      "fname" : new FormControl(), 
      "lname" : new FormControl(), 
      "physician_name" : new FormControl(), 
      "physician_id" : new FormControl(),  
      "acc" : new FormControl(),     
    });

    this.datePickerConfig = Object.assign({},{
      containerClass:'theme-blue',
      dateInputFormat:'MM/DD/YYYY'
    
    });
    this.user_id = localStorage.userid;
  }

  onSubmit()
  {
      if(this.searchForm.status == "VALID")
      {   
          this.spinner.show();
          this.load_status=false;
          this.sendData = { "date":this.convertDate(this.searchForm.value.date),"fname":this.searchForm.value.fname,"lname":this.searchForm.value.lname, "physician":this.searchForm.value.physician_id,
          "acc_no": this.searchForm.value.acc  };
          this.itemsService.search_qcspecimen(this.sendData).subscribe( data=> {
              this.loading = false;
              this.specimens_list = data;
              this.data  =  this.specimens_list.specimens_detail;
              this.physicians_list = this.specimens_list.physicians_data;
              this.no_data_status= false;
              if(this.specimens_list['status'] === '1'){
                this.spinner.hide();
              }
              if(this.specimens_list['status'] === '0'){
                this.no_data_status= true;
                this.no_data="No Data Found.";
                this.spinner.hide();
              }
           });
      }
      else
      {

      }
  }

 ngOnInit() {
   if(this.load_status==true)
   {
    this.spinner.show();
     this.getspecimen();
     this.filteredOptions = this.searchForm.get('physician_name').valueChanges.pipe(
      startWith(''),
      map(value => this._filterGroup(value?value.toLowerCase():''))
    );
   }
 }
 resetForm(){
  this.searchForm.reset();
  this.getspecimen();
}
 getspecimen()
   {
    this.spinner.show();
     this.itemsService.makeQcSpecimenRequest().subscribe(data => {
         
         this.specimens_list = data;
         this.data  =  this.specimens_list.specimens_detail;
         //console.log(this.data);
         this.physicians_list = this.specimens_list.physicians_data;   

        if(this.specimens_list['status'] === '1'){
          this.physicians_list.map((option) => this.allNames.push ( option.physician_name));
          this.physicians_list.map((val) => this.allIds.push ( val.id));
          this.physicians_list.map((data) => this.filterVal.push ( data));
          this.spinner.hide();
        }
        if(this.specimens_list['status'] === '0'){
          this.no_data_status= true;
          this.no_data="No Data Found.";
          this.spinner.hide();
        }
     });

     this.stateGroups = [{
      ids:this.allIds,
      names:this.allNames
    }];
     
   }

   private _filterGroup(value: string): StateGroup[] {
    if (value) {
        let itemCount : number = this.filterVal.length;
        this.options = [];
        this.ids=[];
        for(let i=0;i<itemCount;i++){
            if((this.filterVal[i].physician_name).toLowerCase().indexOf(value) !== -1 ){
              this.options.push(this.filterVal[i].physician_name);
              this.ids.push(this.filterVal[i].id);
            }
        }
        this.searchData = [{
          ids:this.ids,
          names:this.options
        }];
        
        return this.searchData;
    }
    
    return this.stateGroups;
}

callSomeFunction(phy_id :any){
  this.searchForm.controls['physician_id'].setValue(phy_id);
}

convertDate(str: Date) {
  if(!str){
    var newDate = '';
  }else{
    
    var date = new Date(str),
      mnth = ("0" + (date.getMonth()+1)).slice(-2),
      day  = ("0" + date.getDate()).slice(-2),
      newDate = [ mnth, day, date.getFullYear() ].join("/");
  }
  return newDate;
}
displaytrackmodal(specimen_id:any){
  this.spinner.show();
  this.sendData = { "acc_no":specimen_id};
  this.analyticsService.specimen_track(this.sendData).subscribe(data => {
    this.postsArray = data;
    this.spinner.hide();
    if(this.postsArray.histo_status=='1')
    {
      this.acc_no=true;
      this.histo_modal =true;
    }
    else
    {
      this.histo_modal =false;
    }
    if(this.postsArray.pcr_status=='1')
    {
      this.acc_no=true;
      this.pcr_modal =true;
    }
    else
    {
      this.pcr_modal =false;
    }
    if(this.postsArray.status=='0')
    {
       this.acc_no=false;
       this.pcr_modal =false;
       this.histo_modal =false;
    }
    this.modalRef = this.modalService.show(TrackModalComponent,  {
      initialState: {
        title: 'track',
        acc_no:  this.acc_no,
        postsArray: this.postsArray,
        histo_modal: this.histo_modal,
        pcr_modal: this.pcr_modal
      }

    });

  });


}

}
