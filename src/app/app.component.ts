import { Component } from "@angular/core";
import { AutoLogoutService } from "./auto-logout.service";
import { Router, NavigationEnd } from "@angular/router";
import { filter } from "rxjs/operators";

declare var gtag;
@Component({
  selector: "cm-app-component",
  templateUrl: "./app.component.html"
})
export class AppComponent {
  constructor(
    // Inject service
    private autoLogout: AutoLogoutService,
    router: Router
  ) {
    const navEndEvents = router.events.pipe(
      filter(event => event instanceof NavigationEnd)
    );
    navEndEvents.subscribe((event: NavigationEnd) => {
      gtag("config", "UA-98563082-1", {
        page_path: event.urlAfterRedirects
      });
    });
  }
}
