import { Component, OnInit } from '@angular/core';
import { UserserviceService } from '../userservice.service';
import {Observable} from "rxjs";
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { NotificationService } from '../notification.service';
import { environment } from '../../environments/environment';

@Component({
  selector: 'cm-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  public url = environment.assetsUrl;
  public cur_date = new Date();
  constructor(public itemsService:NotificationService) { }

  ngOnInit() {
    this.add_notification();
  }

  add_notification(){
    let params = {'user_id':localStorage.userid};
    this.itemsService.addDataEntryNotifications(params).subscribe(data => {

    });
    this.itemsService.physicianReportAfterThirtyDaysNotification(params).subscribe(data => {

    });
  }
}
