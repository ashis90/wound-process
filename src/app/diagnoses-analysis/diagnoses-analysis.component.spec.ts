import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiagnosesAnalysisComponent } from './diagnoses-analysis.component';

describe('DiagnosesAnalysisComponent', () => {
  let component: DiagnosesAnalysisComponent;
  let fixture: ComponentFixture<DiagnosesAnalysisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiagnosesAnalysisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiagnosesAnalysisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
