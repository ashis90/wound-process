import { Component, OnInit } from '@angular/core';
import { AnlyticsService } from '../anlytics.service';
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, Validators, FormArray, FormControl, ValidatorFn, AbstractControl } from '@angular/forms';
import { AuthService } from '../core/services/auth.service';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { ExcelService } from '../excel.service';
import { map, startWith } from 'rxjs/operators';
import { NgxSpinnerService } from 'ngx-spinner';

export interface StateGroup {
  ids: Number[];
  names: string[];
}

@Component({
  selector: 'cm-diagnoses-analysis',
  templateUrl: './diagnoses-analysis.component.html',
  styleUrls: ['./diagnoses-analysis.component.css']
})
export class DiagnosesAnalysisComponent implements OnInit {
  datePickerConfig: Partial<BsDatepickerConfig>;
  searchForm: FormGroup;
  load_status: boolean = true;
  loading;
  postsArray: any = [];
  physicians_list;
  sendData;
  filteredOptions: Observable<StateGroup[]>;
  allNames: any = [];
  allIds: any = [];
  options: any = [];
  ids: any = [];
  filterVal: any = [];
  data;
  no_data;
  public stateGroups: StateGroup[] = [{
    ids: [],
    names: []
  }]//test
  public searchData: StateGroup[] = [{
    ids: [],
    names: []
  }]
  _archiveNote:any;
  constructor(private excelService: ExcelService, private http: HttpClient, public itemsService: AnlyticsService, public route: Router, public activatedRoute: ActivatedRoute, private spinner: NgxSpinnerService) {
    this._archiveNote = this.itemsService.archiveNote;
    this.searchForm = new FormGroup({

      "from_date": new FormControl('', Validators.required),
      "to_date": new FormControl('', Validators.required),
      "physician_name": new FormControl(''),
      "physician_id": new FormControl(''),
      "dataType": new FormControl('general', Validators.required)
    });

    this.datePickerConfig = Object.assign({}, {
      containerClass: 'theme-blue',
      dateInputFormat: 'YYYY-MM-DD'

    });
  }

  ngOnInit() {
    if (this.load_status == true) {
      this.spinner.show();
      this.itemsService.get_diagnosis_analytics().subscribe(data => {
        this.spinner.hide();
        this.postsArray = data;
        this.physicians_list = this.postsArray.physicians_data;
        this.physicians_list.map((option) => this.allNames.push(option.physician_name));
        this.physicians_list.map((val) => this.allIds.push(val.id));
        this.physicians_list.map((data) => this.filterVal.push(data));
        this.filteredOptions = this.searchForm.get('physician_name').valueChanges.pipe(
          startWith(''),
          map(value => this._filterGroup(value.toLowerCase()))
        );

      });
      this.stateGroups = [{
        ids: this.allIds,
        names: this.allNames
      }];
    }
  }
  resetForm() {
    this.searchForm.reset();
    this.ngOnInit();
  }

  private _filterGroup(value: string): StateGroup[] {
    if (value) {
      let itemCount: number = this.filterVal.length;
      this.options = [];
      this.ids = [];
      for (let i = 0; i < itemCount; i++) {
        if ((this.filterVal[i].physician_name).toLowerCase().indexOf(value) !== -1) {
          this.options.push(this.filterVal[i].physician_name);
          this.ids.push(this.filterVal[i].id);
        }
      }
      this.searchData = [{
        ids: this.ids,
        names: this.options
      }];

      return this.searchData;
    }

    return this.stateGroups;
  }

  callSomeFunction(phy_id: any) {
    this.searchForm.controls['physician_id'].setValue(phy_id);
  }

  convertDate(str: Date) {
    if (!str) {
      var newDate = '';
    } else {

      var date = new Date(str),
        mnth = ("0" + (date.getMonth() + 1)).slice(-2),
        day = ("0" + date.getDate()).slice(-2),
        newDate = [date.getFullYear(), mnth, day].join("-");
    }
    return newDate;
  }

  onSubmit() {
    if (this.searchForm.status == "VALID") {
      this.spinner.show();
      //this.load_status=false;
      this.sendData = { "from_date": this.convertDate(this.searchForm.value.from_date), "physician": this.searchForm.value.physician_id, 'to_date': this.convertDate(this.searchForm.value.to_date), "dataType": this.searchForm.value.dataType };
      this.itemsService.search_diagnosis_analytics(this.sendData).subscribe(data => {
        this.spinner.hide();
        this.postsArray = data;
        this.physicians_list = this.postsArray.physicians_data;


      });
    }
    else {
      this.validateAllFormFields(this.searchForm);
    }
  }

  isObjectEmpty(ob: any) {
    return Object.keys(ob).length === 0;
  }

  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }

  isFieldValid(field: string) {
    return !this.searchForm.get(field).valid && (this.searchForm.get(field).touched || this.searchForm.get(field).dirty);
  }

  validateAllFormFields(formGroup: FormGroup) {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof FormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }

}
