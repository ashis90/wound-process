export const environment = {
  production: true,

  //-----------------Live site----------------------//
  // baseUrl: "https://www.abilitydiagnostics.com/abadmin/api/",
  // assetsUrl: "https://www.abilitydiagnostics.com/abadmin/"

  //-----------------beta-2.0 site----------------------//
  baseUrl: "https://www.abilitydiagnostics.com/beta-2.0/abadmin/api/",
  assetsUrl: "https://www.abilitydiagnostics.com/beta-2.0/abadmin/"
};
